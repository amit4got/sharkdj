<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

	include_once 'header.php';
	
	if(!defined("RUTA_ABSOLUTA")){
	  define("RUTA_ABSOLUTA",dirname(__FILE__));
	}
	
	require_once RUTA_ABSOLUTA.'/administradorDatos/administradorDatos.class.php';
	require_once RUTA_ABSOLUTA.'/administradorDatos/artist.class.php';
	require_once RUTA_ABSOLUTA.'/administradorDatos/imagenes.class.php';
	#INCLUDES
	require_once '../includes/connect.ini.php';
	require_once '../includes/functions.ini.php';
	
	$listaArtistas="";
	$pagina = isset($_GET['p']) ? $_GET['p'] : 0;
	$enlacePag = "./indexArtista.php?";
	$adminDatos = new administradorDatos();
	
	$numTotalLocales = count($adminDatos->obtenerLocalesInfo());
	$arrayLocales = $adminDatos->obtenerLocalesInfo(2, $pagina*2);
	$numLocales = count($arrayLocales);
?>

	
<div data-role="page" id="locales">

<?php echo $header;?>

  <div data-role="content"> 
  <div class="image">
	<div class="ui-grid-a">
	
	<?php 
	if ($numLocales > 0){
	  $i = 1;
	  foreach($arrayLocales as $local) {
	    $imagen = '../images/perfil_masculino_sinfoto.png';
	    if (file_exists('../images/profiles/'.$local['idUser'].'_crop.jpeg')){
	      $imagen = 'http://sharkdj.com/images/profiles/'.$local['idUser'].'_crop.jpeg';
	    }
	    
?>
			<div class="ui-block-<?php echo (($i % 2)==0)?'b':'a' ?>">
				<a href="local.php?id=<?php echo $local['idUser']; ?>"><img src="<?php echo $imagen; ?>"></a>
				<div class="left artistName">
          <span class="left marginTop7 marginLeft5 font1 fontDarkGrey"><?php echo $local['nombre']?></span>
        </div>
			</div>

<?php $i++;
	  }
	}
?>			
	</div><!-- /grid-c --> 
	</div>
	<div align="center"> 
	 <div data-role="controlgroup" data-type="horizontal">
	 		<?php if ($pagina > 0){?>
        <a href="http://sharkdj.com/m/indexLocal.php?p=<?php echo $pagina-1;?>" data-role="button" data-icon="arrow-l"
          data-iconpos="notext">Back</a>
      <?php }?>
        <a href="http://sharkdj.com/m/indexLocal.php" data-role="button" id="buttonUp" data-icon="arrow-u"
          data-iconpos="notext">Up</a>
      <?php if (($pagina+1)*2 < $numTotalLocales){?>	
        <a href="http://sharkdj.com/m/indexLocal.php?p=<?php echo $pagina+1;?>" data-role="button" data-icon="arrow-r"
          data-iconpos="notext">Forward</a>
      <?php }?>
      </div>  
      </div>
    <p><a href="http://sharkdj.com/m/index.php" data-role="button" data-icon="home">Home</a></p>
  </div><!-- /content -->

<?php echo $footer;?>
</div><!-- /page -->