<?php
/****************************
 *  Created 26/06/11
 *  Last update 04/12/11   
 ****************************/

#INCLUDES
if(!defined("RUTA_ABSOLUTA")){
	define("RUTA_ABSOLUTA",dirname(__FILE__));
}

require_once RUTA_ABSOLUTA.'/administradorDatos/administradorDatos.class.php';
require_once RUTA_ABSOLUTA.'/administradorDatos/artist.class.php';
require_once RUTA_ABSOLUTA.'/administradorDatos/imagenes.class.php';

$pagina = isset($_GET['p']) ? $_GET['p'] : 0;
$listaArtistas="";
$adminDatos = new administradorDatos();
$numTotalArtistas = count($adminDatos->obtenerArtistas(1));
$arrayArtistas = $adminDatos->obtenerArtistasInfo(15, $pagina*15, 1);
$numArtistas = count($arrayArtistas);
if ($numArtistas>0){
	$i=0;
	foreach ($arrayArtistas as $artista) {
		$i = ($i>=3) ? 0 : $i;
		if ($i==0) {
			$listaArtistas.=<<<EOF
    		<div class="row">
      		<div class="insideRow">  
EOF;
		}
		// bla bla bla
		$idUser = $artista['idUser'];
		$imagenUrl = imagenes::obtenerImagenUsuario($idUser);
		$imagenShark = ($artista['team']) ? "<img class=\"artistSharkImg\" src=\"images/emblema_sharkdj_artistas.png\" width=\"42px\" height=\"41px\" />" : "";
		$listaArtistas.=<<<EOF
        	  <div class="box6">
          	    <a href="perfilArtista.php?id={$idUser}"><img src="{$imagenUrl}" /></a>
                <div class="left artistName">
                  <span class=\"left marginTop7 marginLeft5 font1 fontDarkGrey">{$artista['nombre']}</span>
                  {$imagenShark}
                </div>
              </div>
EOF;
		if ($i==2) {
			$listaArtistas.=<<<EOF
          </div>
          <div class="advertisementRow2"></div>
        </div>
EOF;
		}
		$i++;
	}
	$i = ($i>=3) ? 0 : $i;
	if ($i>0) {
		for($i; $i<3; $i++) {
			$listaArtistas.=<<<EOF
                  <div class="box6">
                    <a href="#"></a>                    
                  </div>
EOF;
		}
		$listaArtistas.=<<<EOF
              </div>
              <div class="advertisementRow2"></div>
            </div>
EOF;

	}
}

include_once 'includes/header.inc.php';
?>
  <div id="main">
    <div id="searchBar">
      <form>
        <label class="left fontWhite marginTop5 marginLeft5">Nombre:</label>
        <input type="text" name="searchText" class="left searchText2 noBorder fontGrey fontBold" value="" />
        <label class="left fontWhite marginTop5 marginLeft5">Lugar:</label>
        <input type="text" name="searchText" class="left searchText2 noBorder fontGrey fontBold" value="" />
        <label class="left fontWhite marginTop5 marginLeft5">Estilo:</label>
        <input type="image" name="submit" class="right marginRight7 marginTop2" id="searchButton" value="" />
        <input type="text" name="searchText" class="left searchText2 noBorder fontGrey fontBold" value="" />
      </form>
    </div>
    <div class="column2">
    <div class="row">
      <div class="insideRow">
        <div class="box6">
          <a href="perfilArtista.php?id=8"><img src="images/profiles/8_crop.jpeg" /></a>
          <div class="left artistName">
            <span class="left marginTop7 marginLeft5 font1 fontDarkGrey">Ventura Mendoza</span>
            <img class="artistSharkImg" src="images/emblema_sharkdj_artistas.png" width="42px" height="41px" />
          </div>
        </div>
        <div class="box6">
          <img src="images/perfil_masculino_sinfoto.png" />
          <div class="left artistName">
            <span class="left marginTop7 marginLeft5 font1 fontDarkGrey">Artista 2</span>
            <img class="artistSharkImg" src="images/emblema_sharkdj_artistas.png" width="42px" height="41px" />
          </div>
        </div>
        <div class="box6">
          <img src="images/perfil_femenino_sinfoto.png" />
          <div class="left artistName">
            <span class="left marginTop7 marginLeft5 font1 fontDarkGrey">Artista 3</span>
            <img class="artistSharkImg" src="images/emblema_sharkdj_artistas.png" width="42px" height="41px" />
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="insideRow">
        <div class="box6">
          <img src="images/perfil_masculino_sinfoto.png" />
          <div class="left artistName"><span class="left marginTop7 marginLeft5 font1 fontDarkGrey">Artista 4</span></div>
        </div>
        <div class="box6">
          <img src="images/perfil_femenino_sinfoto.png" />
          <div class="left artistName"><span class="left marginTop7 marginLeft5 font1 fontDarkGrey">Artista 5</span></div>
        </div>
        <div class="box6">
          <img src="images/perfil_masculino_sinfoto.png" />
          <div class="left artistName"><span class="left marginTop7 marginLeft5 font1 fontDarkGrey">Artista 6</span></div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="insideRow">
        <div class="box6">
          <img src="images/perfil_femenino_sinfoto.png" />
          <div class="left artistName"><span class="left marginTop7 marginLeft5 font1 fontDarkGrey">Artista 7</span></div>
        </div>
        <div class="box6">
          <img src="images/perfil_masculino_sinfoto.png" />
          <div class="left artistName"><span class="left marginTop7 marginLeft5 font1 fontDarkGrey">Artista 8</span></div>
        </div>
        <div class="box6">
          <img src="images/perfil_femenino_sinfoto.png" />
          <div class="left artistName"><span class="left marginTop7 marginLeft5 font1 fontDarkGrey">Artista 9</span></div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="insideRow">
        <div class="box6">
          <img src="images/perfil_masculino_sinfoto.png" />
          <div class="left artistName"><span class="left marginTop7 marginLeft5 font1 fontDarkGrey">Artista 10</span></div>
        </div>
        <div class="box6">
          <img src="images/perfil_femenino_sinfoto.png" />
          <div class="left artistName"><span class="left marginTop7 marginLeft5 font1 fontDarkGrey">Artista 11</span></div>
        </div>
        <div class="box6">
          <img src="images/perfil_masculino_sinfoto.png" />
          <div class="left artistName"><span class="left marginTop7 marginLeft5 font1 fontDarkGrey">Artista 12</span></div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="insideRow">
        <div class="box6">
          <img src="images/perfil_femenino_sinfoto.png" />
          <div class="left artistName"><span class="left marginTop7 marginLeft5 font1 fontDarkGrey">Artista 13</span></div>
        </div>
        <div class="box6">
          <img src="images/perfil_masculino_sinfoto.png" />
          <div class="left artistName"><span class="left marginTop7 marginLeft5 font1 fontDarkGrey">Artista 14</span></div>
        </div>
        <div class="box6">
          <img src="images/perfil_femenino_sinfoto.png" />
          <div class="left artistName"><span class="left marginTop7 marginLeft5 font1 fontDarkGrey">Artista 15</span></div>
        </div>
      </div>
    </div>
    <?php echo $listaArtistas;?>
    </div>
    <div class="advertisementRow3"></div>
    <div id="nextPreviousMenu" class="clear left insideRow">
      <div class="left width33"><a href="#" class="noDecoration fontGrey2 fontBold hoverLargeGrey">&laquo; Anterior</a></div>
      <div class="left width33 textCentered marginLeft5"><a href="#" class="noDecoration fontGrey2 fontBold hoverLargeGrey">Inicio</a></div>
      <div class="right"><a href="#" class="fontBold noDecoration fontGrey2 hoverLargeGrey">Siguiente &raquo;</a></div>
    </div>
  </div>
<?php include_once 'includes/footer.inc.php'; ?>