<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

	include_once 'header.php';
	
	if(!defined("RUTA_ABSOLUTA")){
	  define("RUTA_ABSOLUTA",dirname(__FILE__));
	}
	
	require_once RUTA_ABSOLUTA.'/administradorDatos/administradorDatos.class.php';
	require_once RUTA_ABSOLUTA.'/administradorDatos/artist.class.php';
	require_once RUTA_ABSOLUTA.'/administradorDatos/imagenes.class.php';
	#INCLUDES
	require_once '../includes/connect.ini.php';
	require_once '../includes/functions.ini.php';
	
	$listaArtistas="";
	$pagina = isset($_GET['p']) ? $_GET['p'] : 0;
	$enlacePag = "./indexEvento.php?";
	$adminDatos = new administradorDatos();
	
	$numTotalEventos = count($adminDatos->obtenerEventos());
	$arrayEventos = $adminDatos->obtenerEventos(8, $pagina*8);
	$numEventos = count($arrayEventos);
?>

<div data-role="page" id="eventos">

<?php echo $header;?>

  <div data-role="content"> 
    <ul data-role="listview" data-filter="true" data-divider-theme="a"> 
    <?php 
	if ($numEventos > 0){
	  $i = 1;
	  foreach($arrayEventos as $evento) {
?>
    	<li data-role="list-divider"><?php echo $evento['nombre'];?><!-- <span class="ui-li-count">25€</span>--></li>
				<li><a href="evento.php?id=<?php echo $evento['idEvento'];?>">
					<h3><?php echo $evento['fechaInicio'];?></h3>
					<p><?php echo $evento['direccion'];?></p>
					<!--  <p class="ui-li-aside"><strong>20:00</strong></p> -->
			</a></li>
<?php }
	  }?>
    </ul>
    
     
	<div align="center"> 
		<br>
	 <div data-role="controlgroup" data-type="horizontal">
	 		<?php if ($pagina > 0){?>
        <a href="http://sharkdj.com/m/indexEvento.php?p=<?php echo $pagina-1;?>" data-role="button" data-icon="arrow-l"
          data-iconpos="notext">Back</a>
      <?php }?>
        <a href="http://sharkdj.com/m/indexEvento.php" data-role="button" id="buttonUp" data-icon="arrow-u"
          data-iconpos="notext">Up</a>
      <?php if (($pagina+1)*8 < $numTotalEventos){?>	
        <a href="http://sharkdj.com/m/indexEvento.php?p=<?php echo $pagina+1;?>" data-role="button" data-icon="arrow-r"
          data-iconpos="notext">Forward</a>
      <?php }?>
      </div>  
      </div>
    <p><a href="#home" data-role="button" data-icon="home">Home</a></p>
  </div><!-- /content -->
  
  <?php echo $footer;?>
</div><!-- /page --> 