<?php

if(!defined("RUTA_ABSOLUTA")){
  define("RUTA_ABSOLUTA",dirname(__FILE__));
}

#INCLUDES
require_once RUTA_ABSOLUTA.'/administradorDatos/administradorDatos.class.php';
include	RUTA_ABSOLUTA.'/editor/spaw2/spaw.inc.php';

session_start();

if ($_SESSION['statusAdmin'] != "authorized") {
  header ("Location: index.php");
  exit();
}
include_once 'includes/headerAdmin.inc.php';

$texto="";
$spaw = new SpawEditor("texto", $texto);
$spaw->setConfigValue('default_toolbarset','standard');
$idUser = $_SESSION['idUser'];

?>

<div class="body">

<?php 
$datos = new administradorDatos();
if ($_POST){
	if (isset($_POST['modificar'])) {
		$evento['idEvento'] = isset($_POST['idEvento']) ? $_POST['idEvento'] : 0;
		$evento['idUser'] = isset($_POST['idUser']) ? $_POST['idUser'] : 0;
		$evento['nombre'] = $_POST['nombre'];		
		$evento['direccion'] = $_POST['direccion'];
		$evento['transporte'] = $_POST['transporte'];
		$evento['fechaInicio'] = $_POST['fecha']." ".$_POST['hora'];
		$evento['visible'] = isset($_POST['visible']);
		$texto = $_POST['texto'];		
		$texto = str_replace("\\", '', $texto);
		$texto = preg_replace('/<img border=\"0\" style=\"display: none; \" src=\"http:\/\/visit\.webhosting\.yahoo\.com\/visit\.gif.*<\/noscript>/im', '', $texto);
		$texto = preg_replace('/<noscript>.*?<\/noscript>/i','',$texto);
		$_POST['Texto'] = $texto;
		$evento['descripcion'] = $texto;
		$evento['precio'] = isset($_POST['precio']) ? $_POST['precio'] : 0;
		
		$datos->actualizaEvento($evento);
	} else {
		$texto = $_POST['texto'];
		$texto = str_replace("\\", '', $texto);
		$texto = preg_replace('/<img border=\"0\" style=\"display: none; \" src=\"http:\/\/visit\.webhosting\.yahoo\.com\/visit\.gif.*<\/noscript>/im', '', $texto);
		$texto = preg_replace('/<noscript>.*?<\/noscript>/i','',$texto);
		$_POST['Texto'] = $texto;

		$evento['descripcion'] = $texto;
		$evento['idUser'] = $idUser;				
		$evento['nombre'] = $_POST['nombre'];		
		$evento['direccion'] = $_POST['direccion'];
		$evento['transporte'] = $_POST['transporte'];
		$evento['fechaInicio'] = $_POST['fecha']." ".$_POST['hora'];
		$evento['visible'] = isset($_POST['visible']);
		$evento['descripcion'] = $texto;
		$evento['precio'] = isset($_POST['precio']) ? $_POST['precio'] : 0;
	}
}

$option = (isset($_GET['o'])) ? $_GET['o'] : 0;
if ($option == '1'){
	$imgEvento = $datos->proximoEvento();
	$idEvento = $imgEvento;
	var_dump($idEvento);
	?>
	
	  <div id='content'>
	    <div class='container'>
	    <form action="adminBackendEventos.php?o=2" method="post" id="form">
	  		<div id="label1">Nombre</div>
	  		<input id="field1" name="nombre" type="text" value=""><br><br>
	  		<div id="label1">Fecha Inicio</div>
	  		<input id="datepicker" name="fecha" type="text" value="<?php echo  date('Y-m-d', time()+86400);?>"><br><br>
	  		<div id="label1">Hora Inicio</div>
	  		<input name="hora" type="text" value="20:00"><br><br>
	  		<div id="label1">Lugar</div>
	  		<input id="field1" name="direccion" type="text" value=""><br><br>
	  		<div id="label1">Transporte</div>
	  		<input id="field1" name="transporte" type="text" value=""><br><br>
	  		<div id="label1">Precio</div>
	  		<input id="field1" name="precio" type="text" value="0"><br><br>
	  		<div id="label1">Visible<input name="visible" type="checkbox" <?php echo $visibleCheck;?> /></div><br><br>
	  		<?php $imagen = file_exists('images/eventos/'.$imgEvento.'_c.jpeg') ? 'images/eventos/'.$imgEvento.'_c.jpeg' : 'images/eventos/0.jpeg';?>  		     
	  		<div>
	  			<img src="<?php echo $imagen; echo '?nocache='.date("isu");?>" width="200" height="280">
	  		  	<a href="editarFotoEvento.php?im=<?php echo $idEvento;?>&i=<?php echo $idEvento;?>&p='<?php echo $_SERVER['argv'][0]?>'">Editar Foto</a>
	  		</div>
	        <?php $spaw->show();?><br><br>
	      	<p><input class="buttonLogin" type="submit" value="Guardar" /></p>
	  	</form>
	    </div>
	  </div>
	
<?php
}elseif ($option == '2' || $option == '3'){
  if ($option=='2'){
    $datos->introduceEvento($evento);
  }
?>
  <div id='content'>
    <div class='container'>
	   	<b><a href='adminBackendEventos.php?o=1'>Crear evento nuevo</a></b><br/><br/>
	<?php   
	  $tabla="";
	  $eventos = $datos->obtenerEventos();  
	  if (count($eventos)>0){
	    $tabla="<table>";    
	    foreach($eventos as $evento){
	      $tabla.="<tr>";
	      $tabla.="<td>".$evento['nombre']." -</td>";
	      $tabla.="<td><a href='adminBackendEventos.php?o=4&e=".$evento['idEvento']."'>Editar</a></td>";
		  $tabla.="<td>&nbsp;&nbsp;&nbsp;<a href='adminBackendEventos.php?o=4&e=".$evento['idEvento']."&erase=1'>Eliminar</a></td>";
	      $tabla.="</tr>";
	    }
	    $tabla.="</table>";
	  }
	  echo $tabla;
}elseif ($option == '4'){

  #ELIMINAR EVENTO
  if (isset ($_GET['erase'])) {
    $datos->eliminaFila($table='eventos',$columnName='idEvento',$id=$_GET['e']);
	echo 'Se ha eliminado el evento. ';
    echo '<a href="adminBackendEventos.php?o=2">Volver a eventos</a>';
	return;
  }

	$idEvento = (isset($_GET['e'])) ? $_GET['e'] : 0;
	if ($idEvento>0) {
		$evento = $datos->obtenerEvento($idEvento);
		$idEvento = $evento['idEvento'];		
		$idUser = $evento['idUser'];		
		$visible = $evento['visible'];
		$visibleCheck = ($visible) ? "checked='checked'" : "";
		$nombre = $evento['nombre'];
		$direccion = $evento['direccion'];
		$transporte = $evento['transporte'];
		$precio = $evento['precio'];
		$fechaInicio = strtotime($evento['fechaInicio']);
		$fecha = date("Y-m-d", $fechaInicio);
		$hora = date("H:i:s", $fechaInicio);
		$texto = $evento['descripcion'];
		$texto = str_replace("\\", '', $texto);
		$texto = preg_replace('/<img border=\"0\" style=\"display: none; \" src=\"http:\/\/visit\.webhosting\.yahoo\.com\/visit\.gif.*<\/noscript>/im', '', $texto);
		$texto = preg_replace('/<noscript>.*?<\/noscript>/i','',$texto);
		$spaw = new SpawEditor("texto", $texto);
		?>
	    
	  <div id='content'>
	    <div class='container'>
	    <form action="adminBackendEventos.php?o=3" method="post" id="form">
	  		<div id="label1">Nombre</div>
	  		<input id="field1" name="nombre" type="text" value="<?php echo $nombre;?>"><br><br>
	  		<div id="label1">Fecha Inicio</div>  		
	  		<input id="datepicker" name="fecha" type="text" value="<?php echo $fecha;?>"><br><br>
	  		<div id="label1">Hora Inicio</div>
	  		<input name="hora" type="text" value="<?php echo $hora;?>"><br><br>
	  		<div id="label1">Lugar</div>
	  		<input id="field1" name="direccion" type="text" value="<?php echo $direccion;?>"><br><br>
	  		<div id="label1">Transporte</div>
	  		<input id="field1" name="transporte" type="text" value="<?php echo $transporte;?>"><br><br>
	  		<div id="label1">Precio</div>
	  		<input id="field1" name="precio" type="text" value="<?php echo $precio;?>"><br><br>
	  		<div id="label1">Visible<input name="visible" type="checkbox" <?php echo $visibleCheck;?> /></div><br><br>
	  		<input type="hidden" name="idEvento" value="<?php echo $idEvento;?>" />
	  		<input type="hidden" name="idUser" value="<?php echo $idUser;?>" />	  		
	  		<?php $imagen = file_exists('images/eventos/'.$idEvento.'_c.jpeg') ? 'images/eventos/'.$idEvento.'_c.jpeg' : 'images/eventos/0.jpeg';?>  		      
	  		<div>
	  			<img src="<?php echo $imagen; echo '?nocache='.date("isu"); ?>" width="200" height="280">
	  		  	<a href="editarFotoEvento.php?im=<?php echo $idEvento;?>&i=<?php echo $idEvento;?>&p='<?php echo $_SERVER['argv'][0]?>'">Editar Foto</a>
	  		</div>
	        <?php $spaw->show();?><br><br>
	      	<p><input type="hidden" name="modificar" value=""/><input class="buttonLogin" type="submit" value="Guardar" /></p>
	  	</form>
	    </div>
	  </div>
	      
<?php 
	}
}
?>

  <div id="menu">
    <img src="images/logoHeader.png" width="150">
    <br><br>
    <ul>
      <li><a href="adminBackend.php?o=3">Noticias</a></li><br><br>
      <li><a href="adminBackendEventos.php?o=3">Eventos</a></li><br><br>
      <li><a href="adminBackendUser.php?o=1">DJ's</a></li><br><br>
      <li><a href="adminBackendLocal.php?o=1">Locales</a></li><br><br>
      <li><a href="adminPublicidad.php">Publicidad</a></li><br><br>
      <li><a href="adminBanner.php">Banner</a></li><br><br>
      <li><a href="admin.php?logout" style="color:red;">Logout</a></li>
    </ul> 
  </div>
</div>