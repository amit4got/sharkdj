<?php
class Index extends Controller {
	function __construct() 
	{
		parent::Controller();
	}
	function index()
	{
		$this->form_validation->set_rules('email', 'Dirección mail', 'required');
	    $this->form_validation->set_rules('password', 'Password', 'required');
	    $this->form_validation->set_error_delimiters('<p class="error">', '</p>');
	    if ($this->form_validation->run() == false)
	    {
			$this->redux_auth->logout();
			$data['head'] = "";
			$data['navigation'] = "";
			$data['content_menu'] = "";
	        $data['content'] = $this->load->view('admin/login_view', null, true);
			$data['foot'] = "";
	    	$this->load->view('templates/admin_view', $data);
	    }
	    else
	    {
	        $email    = $this->input->post('email');
	        $password = $this->input->post('password');
	        $login = $this->redux_auth->login($email, $password, 'admin');
	        if ($login)
			{
				redirect('admin/portada');
			}
			else
			{
				$this->session->set_flashdata('message', '<p class="error">Datos erroneos.</p>');
	            redirect('admin');
			}
	    }
	}
	function home ()
	{
		$this->redux_auth->logout();
		redirect('');
	}
	function logout ()
	{
		$this->redux_auth->logout();
		redirect('admin');		
	}
}


