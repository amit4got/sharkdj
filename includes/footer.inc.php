<?php
/****************************
*  Created 21/08/11
*  Last update 04/12/11
****************************/
?>

	<div class="push"></div>
  </div> <!-- Container -->
  <div id="footerFull">
  <div id="footer">
    <div class="fontWhite font2">
      <div class="left marginTop35 marginLeft5"><?=$arrayIdiomas['copyright'];?></div>
      <a href="http://www.sharkiberia.com" target="_blank" alt="Shark Iberia"><img class="footerSharkImage" src="images/logo_pie.png" alt="logo footer shark" title="logo footer shark" /></a>
      <div class="left marginTop35 marginLeft5">
        <a href="" class="fontWhite noDecoration"><span class="hoverLargeRed"><?=$arrayIdiomas['privacy'];?></span></a>
        &bull;&nbsp;<a href="" class="fontWhite noDecoration"><span class="hoverLargeRed"><?=$arrayIdiomas['ads'];?></span></a>
        &bull;&nbsp;<a href="" class="fontWhite noDecoration"><span class="hoverLargeRed"><?=$arrayIdiomas['language'];?></span></a>
        &bull;&nbsp;<a href="" class="fontWhite noDecoration"><span class="hoverLargeRed"><?=$arrayIdiomas['contact'];?></span></a>
      </div>
    </div>
  </div>
  </div>
  </div> <!-- headerFULL -->
</body>
</html>