<?php
class Noticias_banner extends Controller {
	function __construct() 
	{
		parent::Controller();
		if ($this->redux_auth->logged_in()==1 ){
			$profile_var = $this->redux_auth->profile();
			if ($profile_var->group!="admin" && $profile_var->group!="super" ){
				$this->redux_auth->logout();
				redirect('admin');
			}
		}else{
			$this->redux_auth->logout();
			redirect('admin');
		}
		
		if ( !function_exists('version_compare') || version_compare( phpversion(), '5', '<' ) ){
			$this->load->library('php4/ckeditor');
			$this->load->library('php4/ckfinder');
		}else{
			$this->load->library('php5/ckeditor');
			$this->load->library('php5/ckfinder');
		}
		$this->load->helper('ckeditor');
	}
	function index()
	{
			//echo "aki0";
		/*$this->data['profile']= $this->redux_auth->profile();
		$this->data['head'] = $this->load->view('admin/head_view', $this->data, true);
		$this->data['navigation'] = $this->load->view('admin/main_menu_view', null, true);
		$this->data['content_menu'] = $this->load->view('admin/textos_menu_view', null, true);
		
		$this->data['content'] =$this->load->view('admin/usuarios_listado_view', $this->data, true); 
		$this->data['foot'] = "";
		$this->load->view('templates/admin_view', $this->data);*/
		
	}
	function listado()
	{
		$this->data['profile']= $this->redux_auth->profile();
		$this->data['head'] = $this->load->view('admin/head_view', $this->data, true);
		$this->data['navigation'] = $this->load->view('admin/main_menu_view', null, true);
		$this->data['content_menu'] = $this->load->view('admin/noticias_banner_menu_view', null, true);
		$this->db->where('es_clave', 'noticias_banner');
		
		$this->data['query']=$this->db->get('Textos');
		$this->data['content'] =$this->load->view('admin/noticias_banner_listado_view', $this->data, true); 
		$this->data['foot'] = "";
		$this->load->view('templates/admin_view', $this->data);
		
	}
	function nuevo()
	{
		/*$ckeditor = new CKEditor();
		$ckeditor->basePath = 'js/ckeditor/';
		$ckfinder = new CKFinder();
		$ckfinder->BasePath = 'js/ckfinder/'; // Note: BasePath property in CKFinder class starts with capital letter
		$ckfinder->SetupCKEditorObject($ckeditor);
		$ckeditor->editor('CKEditor1');*/
		
		
		
		
		
		
		$this->form_validation->set_rules('es_titulo', '"Título en español"', 'required');
	  $this->form_validation->set_rules('en_titulo', '"Título en ingles"', 'required');
	  $this->form_validation->set_rules('du_titulo', '"Título en holandes"', 'required');
		$this->form_validation->set_rules('es_link', '"Link para español"', 'required');
		$this->form_validation->set_rules('en_link', '"Link para ingles"', 'required');
		$this->form_validation->set_rules('du_link', '"Link para holandes"', 'required');
		$this->form_validation->set_rules('imagen', '"Imagen"', '');
	    $this->form_validation->set_error_delimiters('<p class="error">', '</p>');
	    if ($this->form_validation->run() == false)
	    { 
			$this->data['profile']= $this->redux_auth->profile();
			$this->data['head'] = $this->load->view('admin/head_view', $this->data, true);
			$this->data['navigation'] = $this->load->view('admin/main_menu_view', null, true);
			$this->data['content_menu'] = $this->load->view('admin/noticias_banner_menu_view', null, true);
			$this->data['accion_form']='admin/noticias_banner/nuevo';
			
			$this->data['registro'] = array(
							"es_titulo" => '' , 
							"en_titulo" => '' ,
							"du_titulo" => '' ,
							"es_link" => '' , 
							"en_link" => '' ,
							"du_link" => '' );
			
			$ckeditor =  CK_Editor();
			$this->data['ckeditor']= $ckeditor; 
		
	        $this->data['content'] = $this->load->view('admin/noticias_banner_form_view', $this->data, true);
	       	$this->data['foot'] = "";
			$this->load->view('templates/admin_view', $this->data);
	    }
	    else
	    {
			$config_upload['upload_path']    = './uploads/noticias_banner/'; 
			$config_upload['allowed_types']  = 'gif|jpg|png'; 
			$config_upload['max_size']       = '1000'; 
			$config_upload['max_width']      = '2048'; 
			$config_upload['max_height']     = '1536'; 
			$this->load->library('upload', $config_upload);
			if ( $this->upload->do_upload('imagen')) 
			{ 
				$imagen=$this->upload->data();
				$this->data['registro']['imagen'] = $imagen['file_name'];
				$config_resize['image_library']   = 'GD2'; 
				$config_resize['source_image']    = './uploads/noticias_banner/'.$imagen['file_name']; 
				$config_resize['thumb_marker']       = 'p';
				//$config_resize['new_image']       = './uploads/fiestas/'.$imagen['raw_name'].'p'.$imagen['file_ext']; 
				$config_resize['create_thumb']    = TRUE; 
				$config_resize['maintain_ratio']  = TRUE; 
				$config_resize['width']           = 175; 
				$config_resize['height']          = 175; 
				$this->load->library('image_lib', $config_resize); 
				$this->image_lib->resize(); 

			} 
			
	    $this->data['registro']['es_titulo'] 		= $this->input->post('es_titulo');
			$this->data['registro']['en_titulo'] 		= $this->input->post('en_titulo');
			$this->data['registro']['du_titulo'] 		= $this->input->post('du_titulo');
			$this->data['registro']['es_link'] 			= $this->input->post('es_link');
			$this->data['registro']['en_link'] 			= $this->input->post('en_link');
			$this->data['registro']['du_link'] 			= $this->input->post('du_link');
			$this->data['registro']['es_clave'] 		= 'noticias_banner';
			
			
			$this->db->set($this->data['registro']); 
			
			if ($this->db->insert('Textos')){
				$this->session->set_flashdata('message', '<p class="notice">La información ha sido insertada.</p>');
	            redirect('admin/noticias_banner/nuevo/');
			} else
	        {
	            $this->session->set_flashdata('message', '<p class="error">No se ha podido guardar.</p>');
	            redirect('admin/noticias_banner/nuevo/');
	        }
	    }	
		
		
	}
	function modificar()
	{
		$this->form_validation->set_rules('es_titulo', '"Título en español"', 'required');
	  $this->form_validation->set_rules('en_titulo', '"Título en ingles"', 'required');
	  $this->form_validation->set_rules('du_titulo', '"Título en holandes"', 'required');
		$this->form_validation->set_rules('es_link', '"Link para español"', 'required');
		$this->form_validation->set_rules('en_link', '"Link para ingles"', 'required');
		$this->form_validation->set_rules('du_link', '"Link para holandes"', 'required');
	    $this->form_validation->set_error_delimiters('<p class="error">', '</p>');
	    if ($this->form_validation->run() == false)
	    { 
			
			$this->data['profile']= $this->redux_auth->profile();
			$this->data['head'] = $this->load->view('admin/head_view', $this->data, true);
			$this->data['navigation'] = $this->load->view('admin/main_menu_view', null, true);
			$this->data['content_menu'] = $this->load->view('admin/noticias_banner_menu_view', null, true);
	        
			$this->data['accion_form']='admin/noticias_banner/modificar/'.$this->uri->segment(4);
			$this->db->where('es_clave', 'noticias_banner');
			$this->db->where('id', $this->uri->segment(4) );
			$query=$this->db->get('Textos');
			$this->data['errores']='';
			$ckeditor =  CK_Editor();
			$this->data['ckeditor']= $ckeditor; 
			foreach ($query->result() as $row)
			{
				if ($row->imagen!=""){
					$ar_imagen=explode(".",$row->imagen);
					$imagen=base_url().'uploads/noticias_banner/'.$ar_imagen[0].'p.'.$ar_imagen[1];
				}else{
					$imagen=base_url().'uploads/noticias_banner/espacio.gif';
				}
				$this->data['registro'] = array(
											"es_titulo" 		=> $row->es_titulo , 
											"en_titulo" 		=> $row->en_titulo ,
											"du_titulo" 		=> $row->du_titulo ,
											"imagen"			=> $imagen ,
											"es_link" 			=> $row->es_link , 
											"en_link" 			=> $row->en_link ,
											"du_link" 			=> $row->du_link );
			}
			if (!isset($this->data['registro'])){
				 redirect('admin/noticias_banner/listado');
			}
			$this->data['content'] = $this->load->view('admin/noticias_banner_form_view', $this->data, true);
	       	$this->data['foot'] = "";
			$this->load->view('templates/admin_view', $this->data);
	    }
	    else
	    {
	        $this->data['registro']['es_titulo'] = $this->input->post('es_titulo');
			$this->data['registro']['en_titulo'] = $this->input->post('en_titulo');
			$this->data['registro']['du_titulo'] = $this->input->post('du_titulo');
			$this->db->set($this->data['registro']); 
			
			$update = array(
							"es_titulo" 		=> $this->input->post('es_titulo') , 
							"en_titulo" 		=> $this->input->post('en_titulo') ,
							"du_titulo" 		=> $this->input->post('du_titulo') ,
							"es_link" 			=> $this->input->post('es_link'),
							"en_link" 			=> $this->input->post('en_link'),
							"du_link" 			=> $this->input->post('du_link'));
			
			$config_upload['upload_path']    = './uploads/noticias_banner/'; 
			$config_upload['allowed_types']  = 'gif|jpg|png'; 
			$config_upload['max_size']       = '1000'; 
			$config_upload['max_width']      = '2048'; 
			$config_upload['max_height']     = '1536'; 
			
			$this->load->library('upload', $config_upload);
			if ( $this->upload->do_upload('imagen')) 
			{ 
				$imagen=$this->upload->data();
				$update['imagen'] = $imagen['file_name'];
				$config_resize['image_library']   = 'GD2'; 
				$config_resize['source_image']    = './uploads/noticias_banner/'.$imagen['file_name']; 
				$config_resize['thumb_marker']       = 'p';
				//$config_resize['new_image']       = './uploads/fiestas/'.$imagen['raw_name'].'p'.$imagen['file_ext']; 
				$config_resize['create_thumb']    = TRUE; 
				$config_resize['maintain_ratio']  = TRUE; 
				$config_resize['width']           = 175; 
				$config_resize['height']          = 175; 
				$this->load->library('image_lib', $config_resize); 
				$this->image_lib->resize(); 
				
				$this->db->where('id', $this->uri->segment(4) );
				$this->db->where('es_clave', 'noticias_banner');
				$query=$this->db->get('Textos');
				foreach ($query->result() as $row)
				{
					$ar_imagen=explode(".",$row->imagen);
					$imagen=PUBPATH.'uploads/noticias_banner/'.$ar_imagen[0].'p.'.$ar_imagen[1];
					unlink($imagen);
					$imagen=PUBPATH.'uploads/noticias_banner/'.$ar_imagen[0].'.'.$ar_imagen[1];
					unlink($imagen);
				}
			} 
			
			
			
			$this->db->where('es_clave', 'noticias_banner' );
			$this->db->where('id', $this->uri->segment(4) );
			
			if ($this->db->update("Textos",$update)){
				$this->session->set_flashdata('message', '<p class="notice">La información ha sido modifica.</p>');
	            redirect('admin/noticias_banner/modificar/'.$this->uri->segment(4));
			} else
	        {
	            $this->session->set_flashdata('message', '<p class="error">No se ha podido modificar.</p>');
	            redirect('admin/noticias_banner/modificar/'.$this->uri->segment(4));
	        }
	    }	
	}
	function borrar()
	{
		$this->data['profile']= $this->redux_auth->profile();
		$this->data['head'] = $this->load->view('admin/head_view', $this->data, true);
		$this->data['navigation'] = $this->load->view('admin/main_menu_view', null, true);
		$this->data['content_menu'] = $this->load->view('admin/noticias_banner_menu_view', null, true);
		
		
		$this->db->where('id', $this->uri->segment(4) );
		$this->db->where('es_clave', 'noticias_banner');
		$query=$this->db->get('Textos');
		foreach ($query->result() as $row)
		{
			$this->data['es_titulo'] =$row->es_titulo;
		}
		
		$this->data['content'] =$this->load->view('admin/noticias_banner_borrar_view', $this->data, true); ;
		$this->data['foot'] = "";
		$this->load->view('templates/admin_view', $this->data);
	}
	function borrar_confirm()
	{
		$this->data['profile']= $this->redux_auth->profile();
		$this->db->where('id', $this->uri->segment(4) );
		$this->db->where('es_clave', 'noticias_banner');
		$query=$this->db->get('Textos');
		foreach ($query->result() as $row)
		{
			$this->data['es_titulo'] =$row->es_titulo;
			if ($row->imagen!=""){
				$ar_imagen=explode(".",$row->imagen);
				
				$imagen=PUBPATH.'uploads/noticias_banner/'.$ar_imagen[0].'p.'.$ar_imagen[1];
				if(file_exists($imagen)) { 	unlink($imagen);	}
				
				$imagen= PUBPATH.'uploads/noticias_banner/'.$ar_imagen[0].'.'.$ar_imagen[1];
				if(file_exists($imagen)) {	unlink($imagen);    }		
			}
			
		}
		if (isset($this->data['es_titulo'])){
				$this->db->where('id', $this->uri->segment(4) );
				$this->db->where('es_clave', 'noticias_banner');
				$this->db->delete('Textos'); 
		}
		redirect('admin/noticias_banner/listado');
	}
}


