<script type="text/javascript">
//<![CDATA[	
	function ir_noticia(id_noticia){
		location.href= '<?=base_url(); ?>index.php/web/escuela/videos_detalle/'+id_noticia;	
	}
//]]>
</script>
<div id="cuerpo_1">
    <div id="cuerpo_noticias">
        <ul id="noticias">
            <?php if ($query->num_rows()>0): ?>
			<?php foreach($query->result() as $row): ?>
            <li onclick="ir_noticia(<?=$row->id; ?>)" >
                <div class="noticias_imagen">
                <?php
                      $imagen ='<img src="'.base_url().'uploads/videos_detalle/'; 
						if ($row->imagen!=""){
							$ar_imagen=explode(".",$row->imagen);
							$imagen.=$ar_imagen[0].'p.'.$ar_imagen[1].'" ';
						}else{
							$imagen.='espacio.gif" ';
						}
				  	  $imagen.='alt="shark" >';
					 
                ?>
                     <?=$imagen; ?>
                </div>
                <div class="noticias_info">
                    <h2><?=$row->es_titulo;?></h2>
                    <p><?=$row->es_subtitulo;?></p>
                </div>
                <div class="noticias_enlace"><?=anchor('web/escuela/videos_detalle/'.$row->id.'/'.$row->es_titulo	, 'Seguir leyendo' ); ?></div>
            </li> 
            <?php endforeach;?>
			<?php endif; ?>  
        </ul>
        <div id="paginas"><?= $this->pagination->create_links(); ?></div>
    </div>
    <div id="banner">
   	  <?= $this->banner_model->cargar_banner_es (); ?>
    </div>
</div>