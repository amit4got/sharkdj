<?php
/****************************
 *  Created 11/11/11
 *  Last update 11/11/11   
 ****************************/

#Headers

//error_reporting(E_ALL);
//ini_set("display_errors", 1);

if(!defined("RUTA_ABSOLUTA")){
  define("RUTA_ABSOLUTA",dirname(__FILE__));
}

#INCLUDES
require_once RUTA_ABSOLUTA.'/administradorDatos/artist.class.php';
require_once RUTA_ABSOLUTA.'/administradorDatos/administradorDatos.class.php';
include	RUTA_ABSOLUTA.'/editor/spaw2/spaw.inc.php';

$user = new administradorDatos();
#Load languages
include RUTA_ABSOLUTA.'/includes/lang.inc.php';
$arrayIdiomas = $user->cargaIdioma($idioma);

session_start();

if ($_SESSION['status'] != "authorized") {
  header ("Location: index.php");
  exit();
}
	
function paises($nombre_del_select,$pais){
  
    $array_paises = array("Elige tu pais","Spain","Chipre","Holanda","--------------", "Republica Dominicana","Afganistan","Africa del Sur",
    "Albania","Alemania","Andorra","Angola","Antigua y Barbuda","Antillas Holandesas","Arabia Saudita","Argelia","Argentina","Armenia","Aruba",
    "Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarusia","Belgica","Belice","Benin","Bermudas","Bolivia",
    "Bosnia","Botswana","Brasil","Brunei Darussulam","Bulgaria","Burkina Faso","Burundi","Butan","Camboya","Camerun","Canada","Cape Verde","Chad",
    "Chile","China","Chipre","Colombia","Comoros","Congo","Corea del Norte","Corea del Sur","Costa de Marfíl","Costa Rica","Croasia","Cuba",
    "Dinamarca","Djibouti","Dominica","Ecuador","Egipto","El Salvador","Emiratos Arabes Unidos","Eritrea","Eslovenia","Spain","Estados Unidos",
    "Estonia","Etiopia","Fiji","Filipinas","Finlandia","Francia","Gabon","Gambia","Georgia","Ghana","Granada","Grecia","Groenlandia","Guadalupe",
    "Guam","Guatemala","Guayana Francesa","Guerney","Guinea","Guinea-Bissau","Guinea Equatorial","Guyana","Haiti","Holanda","Honduras","Hong Kong",
    "Hungria","India","Indonesia","Irak","Iran","Irlanda","Islandia","Islas Caiman","Islas Faroe","Islas Malvinas","Islas Marshall","Islas Solomon",
    "Islas Virgenes Britanicas","Islas Virgenes (U.S.)","Israel","Italia","Jamaica","Japon","Jersey","Jordania","Kazakhstan","Kenia","Kiribati",
    "Kuwait","Kyrgyzstan","Laos","Latvia","Lesotho","Libano","Liberia","Libia","Liechtenstein","Lituania","Luxemburgo","Macao","Macedonia",
    "Madagascar","Malasia","Malawi","Maldivas","Mali","Malta","Marruecos","Martinica","Mauricio","Mauritania","Mexico","Micronesia","Moldova",
    "Monaco","Mongolia","Mozambique","Myanmar (Burma)","Namibia","Nepal","Nicaragua","Niger","Nigeria","Noruega","Nueva Caledonia","Nueva Zealandia",
    "Oman","Pakistan","Palestina","Panama","Papua Nueva Guinea","Paraguay","Peru","Polinesia Francesa","Polonia","Portugal","Puerto Rico","Qatar",
    "Reino Unido","Republica Centroafricana","Republica Checa","Republica Democratica del Congo","Republica Eslovaca","Reunion","Ruanda","Rumania",
    "Rusia","Sahara","Samoa","San Cristobal-Nevis (St. Kitts)","San Marino","San Vincente y las Granadinas","Santa Helena","Santa Lucia",
    "Vaticano","Sao Tome & Principe","Senegal","Seychelles","Sierra Leona","Singapur","Siria","Somalia","Sri Lanka (Ceilan)","Sudan","Suecia",
    "Suiza","Sur Africa","Surinam","Swaziland","Tailandia","Taiwan","Tajikistan","Tanzania","Timor Oriental","Togo","Tokelau","Tonga",
    "Trinidad & Tobago","Tunisia","Turkmenistan","Turquia","Ucrania","Uganda","Uruguay","Uzbekistan","Vanuatu","Venezuela","Vietnam","Yemen",
    "Yugoslavia","Zambia","Zimbabwe");
    $cantidad_paises = count($array_paises);
    echo '<select name="'.$nombre_del_select.'" id="'.$nombre_del_select.'">';
    for($i = 0; $i<$cantidad_paises; $i++){
        $array_paises_i = $array_paises[$i];
        echo '<option value="'.$array_paises_i.'"'; 
            if($pais=="$array_paises_i"){
                    echo "selected";
            }
        echo '>'.$array_paises_i.'</option>';
    }
    echo '</select>';
}
//como llamar la function?
//paises("select_paises");





$artista = new artist();
$idUser = $_SESSION['idUser'];

if (isset($_POST['enviado'])){	 
  $artista->insertaDatosArtista($_POST);
  if (isset($_POST['facebook'])){
    $facebook = str_replace('http://', '', $_POST['facebook']);
    $facebook = str_replace('https://', '', $_POST['facebook']);
    $artista->insertaRedSocial($idUser,1, $facebook);
  }
  if (isset($_POST['twitter'])){
    $twitter = explode('/', $_POST['twitter']);
    $twitter = $twitter[count($twitter) - 1];
    $artista->insertaRedSocial($idUser,2, $twitter);
  }
  if (isset($_POST['soundcloud'])){
    $soundcloud = explode('/', $_POST['soundcloud']);
    $soundcloud = $soundcloud[count($soundcloud) - 1];
    $artista->insertaRedSocial($idUser,3, $soundcloud);
  }
  if (isset($_POST['youtube'])){
    $youtube= explode('/', $_POST['youtube']);
    $youtube = $youtube[count($youtube) - 1];
    $artista->insertaRedSocial($idUser,4, $youtube);
  }
  if (isset($_POST['myspace'])){
    $myspace = explode('/', $_POST['myspace']);
    $myspace = $myspace[count($myspace) - 1];
    $artista->insertaRedSocial($idUser,5, $myspace);
  }
  header('Location: artistaBackend.php');
  exit;
}
else{

  $datos = $artista->obtenerDatosArtista($idUser);
  
  if (empty($datos)){
    $datos = $user->obtenerUsuario($idUser);
    if (empty($datos)){
      header('Location: index.php');
      exit;
    }
  }
  
  $idUser = isset($datos['idUser']) ? $datos['idUser'] : 0;
  $userName = isset($datos['userName']) ? $datos['userName'] : "";
  $email = isset($datos['email']) ? $datos['email'] : "";
  $idPerfil = isset($datos['idPerfil']) ? $datos['idPerfil'] : 0;
  $nombre = isset($datos['nombre']) ? $datos['nombre'] : "";
  $apellido1 = isset($datos['apellido1']) ? $datos['apellido1'] : "";
  $apellido2 = isset($datos['apellido2']) ? $datos['apellido2'] : "";
  $direccion = isset($datos['direccion']) ? $datos['direccion'] : "";
  $cp = isset($datos['cp']) ? $datos['cp'] : "";
  $telefono = isset($datos['telefono']) ? $datos['telefono'] : "";
  $movil = isset($datos['movil']) ? $datos['movil'] : "";
  $bio = isset($datos['bio']) ? $datos['bio'] : "";
  $web = isset($datos['web']) ? $datos['web'] : "";
  $estilo = isset($datos['estilo']) ? $datos['estilo'] : "";
  $clubResidencia = isset($datos['clubResidencia']) ? $datos['clubResidencia'] : "";
  $lat = isset($datos['lat']) ? $datos['lat'] : "";
  $lng = isset($datos['lng']) ? $datos['lng'] : "";
  $pais = isset($datos['pais']) ? $datos['pais'] : "";
  $spaw = new SpawEditor("bio", $bio);
  $visible = isset($datos['visible']) ? $datos['visible'] : 0;
  $team =  isset($datos['team']) ? $datos['team'] : 0;
  
  $redes = $artista->obtenerRedesSociales($idUser);

  $facebook = '';
  $twitter = '';
  $soundcloud = '';
  $youtube = '';
  $myspace = '';

  if (!empty($redes)){
    foreach($redes as $red){
      switch ($red) {
        case $red['idRed'] == '1':
          $facebook = isset($red['url']) ? $red['url'] : "";
        break;
        case $red['idRed'] == '2':
          $twitter = isset($red['url']) ? $red['url'] : "";
          break;
        case $red['idRed'] == '3':
          $soundcloud = isset($red['url']) ? $red['url'] : "";
          break;
        case $red['idRed'] == '4':
          $youtube = isset($red['url']) ? $red['url'] : "";
          break;
        case $red['idRed'] == '5':
          $myspace = isset($red['url']) ? $red['url'] : "";
          break;
        default:
          ;
        break;
      }
    }
    
  }

  include_once 'includes/headerBackend.inc.php';
  ?>
  	<div id="mainTab">
  		<div id="tabs">
  			<ul>
  				<li><a href="#tabs-1"><?=$arrayIdiomas['personalDetails'];?></a></li>
  				<li><a href="#tabs-2"><?=$arrayIdiomas['biography'];?></a></li>
  				<li><a href="#tabs-3"><?=$arrayIdiomas['map'];?></a></li>
  				<li><a href="#tabs-4"><?=$arrayIdiomas['socialNetworks'];?></a></li>
  				<li><a href="#tabs-5"><?=$arrayIdiomas['images'];?></a></li>
  			</ul>
     		<form action="artistaBackend.php" method="post" id="form">
  			<div id="tabs-1">
  					<div id="label1" class="labelFormLogin"><?=$arrayIdiomas['name'];?></div>
  					<div class="rounded_input">
  						<input id="field1" name="nombre" type="text" maxlength="30" value="<?php echo $nombre;?>">
  					</div>
  					<div class="right movil">
  					<div id="label1" class="labelFormLogin"><?=$arrayIdiomas['welcome'];?> <?php echo $userName;?></div>
  					</div>
  					<div id="label2" class="labelFormLogin"><?=$arrayIdiomas['surnames'];?></div>
  					<div class="rounded_input">
  						<input id="field2" name="apellido1" type="text" maxlength="30" value="<?php echo $apellido1;?>">
  					</div>
  					<div class="right apellido2">
  						<div class="rounded_input">
  							<input id="field3" name="apellido2" type="text" maxlength="30" value="<?php echo $apellido2;?>">
  						</div>
  					</div>
  					<div id="label4" class="labelFormLogin"><?=$arrayIdiomas['telephone'];?></div>
  					<div class="rounded_input">
  						<input id="field4" name="telefono" type="tel" maxlength="30" value="<?php echo $telefono;?>">
  					</div>
  					<div class="right movil">
  					<div id="label5" class="labelFormLogin"><?=$arrayIdiomas['mobile'];?></div>
  					<div class="rounded_input">
  						<input id="field5" name="movil" type="tel" maxlength="30" value="<?php echo $movil;?>">
  					</div>
  					</div>
  					<div id="label6" class="labelFormLogin"><?=$arrayIdiomas['email'];?></div>
  					<div class="rounded_input">
  						<input id="field6" name="email" type="email" maxlength="30" value="<?php echo $email;?>">
  					</div>
  					<div id="label7" class="labelFormLogin"><?=$arrayIdiomas['password'];?></div>
  					<div class="rounded_input">
  						<input id="field7" name="password" type="password" maxlength="30" value="">
  					</div>
  					<div class="right movil">
  						<div id="label7" class="labelFormLogin"><?=$arrayIdiomas['repeatPassword'];?></div>
  						<div class="rounded_input">
  							<input id="field7" name="password2" type="password" maxlength="30" value="">
  						</div>
  						<div class="right boton">
  							<button><?=$arrayIdiomas['save'];?></button>
  							<a href="inputLogin.php?logout"><?=$arrayIdiomas['logout'];?></a>
  						</div>
  					</div>
  					<?php $imagen = file_exists('images/profiles/'.$idUser.'_crop.jpeg') ? $idUser.'_crop.jpeg' : '0.jpeg';
  					      $idImagen = $imagen == '0.jpeg' ? 0 : $idUser;?>
  					<div class="imagenPerfil">
  						<img src="images/profiles/<?php echo $imagen; echo '?nocache='.date("isu");?>" width="200" height="200" style="float:right;">
  						<a href="editarFotoPerfil.php?im=<?php echo $idImagen;?>" style="float: right;padding: 5px 0 0 0;"><button><?=$arrayIdiomas['editPhoto'];?></button></a>
  					</div>
  			</div>
  			<div id="tabs-2">
          <?php
          $spaw->hideModeStrip();
//           $spaw->addPage(new SpawEditorPage("ingles","English",$article));
          $spaw->show();?>
          <br>
          <div id="label8" class="labelFormLogin"><?=$arrayIdiomas['style'];?></div>
  				<div class="rounded_input">
  					<input id="field8" name="estilo" type="text" maxlength="30" value="<?php echo $estilo;?>">
  				</div>
  				<div class="right movil">
  					<div id="label9" class="labelFormLogin"><?=$arrayIdiomas['residenceClub'];?></div>
  					<div class="rounded_input">
  						<input id="field8" name="clubResidencia" type="text" maxlength="30" value="<?php echo $clubResidencia;?>">
  					</div>
  				</div>
  				<div id="label10" class="labelFormLogin"><?=$arrayIdiomas['web'];?></div>
  				<div class="rounded_input">
  					<input id="field10" name="web" type="text" maxlength="30" value="<?php echo $web;?>">
  				</div>
          <div class="right botonBio">
  					<button><?=$arrayIdiomas['save'];?></button>
  				</div>
  			</div>
  			<div id="tabs-3">
  					<div id="label13" class="labelFormLogin"><?=$arrayIdiomas['address'];?></div>
  					<div class="rounded_input_mapa">
  						<input id="address" name="direccion" type="text" value="<?php echo $direccion;?>"/>
  					</div>
  					<div class="right botonBuscar">
      				<input class="buttonLogin" type="button" value="Buscar" onclick="codeAddress()" />
      				<input type="hidden" name="lat" id="lat" value="<?php echo $lat;?>" />
      				<input type="hidden" name="lng" id="lng" value="<?php echo $lng;?>" />
      			</div>
      			<div style="position:relative;height:300px;clear:both;top:-23px">
       				<div id="map_canvas" style="height:100%;top:2px"></div>
       			</div>
  					<div id="label11" class="labelFormLogin"><?=$arrayIdiomas['postalCode'];?></div>
  					<div class="rounded_input">
  						<input id="field11" name="cp" type="text" maxlength="30" value="<?php echo $cp;?>">
  					</div>
  					<div class="right movil">
  						<div id="label12" class="labelFormLogin"><?=$arrayIdiomas['country'];?></div>
                <?php paises('pais',$pais) ?>
               <!-- <input type="hidden" name="pais" value="1"> -->
  					</div>
  					<div class="right botonMapa">
  						<button><?=$arrayIdiomas['save'];?></button>
  					</div>
  				</div>
			<div id="tabs-4">
				<div id="label1" class="labelFormLogin">Facebook (<?=$arrayIdiomas['fullURL'];?>)</div>
				<div class="rounded_input">
					<input id="field1" name="facebook" type="text" maxlength="200"
						value="<?php echo $facebook;?>">
				</div>
				<div id="label2" class="labelFormLogin">Twitter (<?=$arrayIdiomas['fullURL'];?>)</div>
				<div class="rounded_input">
					<input id="field2" name="twitter" type="text" maxlength="200"
						value="<?php echo $twitter;?>">
				</div>
				<div class="right movil">
					<div id="label5" class="labelFormLogin">mySpace (<?=$arrayIdiomas['fullURL'];?>)</div>
					<div class="rounded_input">
						<input id="field5" name="myspace" type="text" maxlength="200" value="<?php echo $myspace;?>">
					</div>
				</div>
				<div id="label4" class="labelFormLogin">SoundCloud (<?=$arrayIdiomas['fullURL'];?>)</div>
				<div class="rounded_input">
					<input id="field4" name="soundcloud" type="text" maxlength="200"
						value="<?php echo $soundcloud;?>">
				</div>
				<div class="right movil">
					<div id="label5" class="labelFormLogin">You Tube (<?=$arrayIdiomas['fullURL'];?>)</div>
					<div class="rounded_input">
						<input id="field5" name="youtube" type="text" maxlength="200" value="<?php echo $youtube;?>">
					</div>
				<div class="right botonMapa">
					<button><?=$arrayIdiomas['save'];?></button>
					  <a href="inputLogin.php?logout"><?=$arrayIdiomas['logout'];?></a>
				</div>
				</div>
				</div>
			<input type="hidden" name="idUser" value="<?php echo $idUser;?>">
			<input type ="hidden" name="visible" value="0">
			<input type="hidden" name="team" value="0">
  			<input type="hidden" name="enviado" value="submitted">
  			</form>
  			<div id="tabs-5">
  			    <div id="fileupload">
          <form action="upload/php/index.php" method="POST" enctype="multipart/form-data">
              <div class="fileupload-buttonbar">
                  <label class="fileinput-button">
                      <span><?=$arrayIdiomas['addFiles'];?>...</span>
                      <input type="file" name="files[]" multiple>
                  </label>
                  <button type="button" class="delete"><?=$arrayIdiomas['deleteSelected'];?></button>
                  <input type="hidden" name="idUser" value="<?php echo $idUser;?>" />
                  <input type="checkbox" class="toggle">
              </div>
          </form>
          <div class="fileupload-content">
              <table class="files"></table>
              <div class="fileupload-progressbar"></div>
          </div>
      </div>
      <div id="notes">
          <h3><?=$arrayIdiomas['notes'];?></h3>
          <ul>
              <li><?=$arrayIdiomas['maxFileSize'];?> 700 Kb.</li>
          </ul>
      </div>
      
  <script id="template-upload" type="text/x-jquery-tmpl">
    <tr class="template-upload{{if error}} ui-state-error{{/if}}">
        <td class="preview"></td>
        <td class="name">{{if name}}${name}{{else}}Untitled{{/if}}</td>
        <td class="size">${sizef}</td>
        {{if error}}
            <td class="error" colspan="2">Error:
                {{if error === 'maxFileSize'}}File is too big
                {{else error === 'minFileSize'}}File is too small
                {{else error === 'acceptFileTypes'}}Filetype not allowed
                {{else error === 'maxNumberOfFiles'}}Max number of files exceeded
                {{else}}${error}
                {{/if}}
            </td>
        {{else}}
            <td class="progress"><div></div></td>
            <td class="start"><button>Start</button></td>
        {{/if}}
        <td class="cancel"><button>Cancel</button></td>
    </tr>
  </script>
  <script id="template-download" type="text/x-jquery-tmpl">
    <tr class="template-download{{if error}} ui-state-error{{/if}}">
        {{if error}}
            <td></td>
            <td class="name">${name}</td>
            <td class="size">${sizef}</td>
            <td class="error" colspan="2">Error:
                {{if error === 1}}File exceeds upload_max_filesize (php.ini directive)
                {{else error === 2}}File exceeds MAX_FILE_SIZE (HTML form directive)
                {{else error === 3}}File was only partially uploaded
                {{else error === 4}}No File was uploaded
                {{else error === 5}}Missing a temporary folder
                {{else error === 6}}Failed to write file to disk
                {{else error === 7}}File upload stopped by extension
                {{else error === 'maxFileSize'}}File is too big
                {{else error === 'minFileSize'}}File is too small
                {{else error === 'acceptFileTypes'}}Filetype not allowed
                {{else error === 'maxNumberOfFiles'}}Max number of files exceeded
                {{else error === 'uploadedBytes'}}Uploaded bytes exceed file size
                {{else error === 'emptyResult'}}Empty file upload result
                {{else}}${error}
                {{/if}}
            </td>
        {{else}}
            <td class="preview">
                {{if thumbnail_url}}
                    <a href="${url}" title="${name}" rel="gallery"><img src="${thumbnail_url}"></a>
                {{/if}}
            </td>
            <td class="name">
                <a href="${url}" title="${name}"{{if thumbnail_url}} rel="gallery"{{/if}}>${name}</a>
            </td>
            <td class="size">${sizef}</td>
            <td colspan="2"></td>
        {{/if}}
        <td class="delete">
           <button data-type="${delete_type}" data-url="${url}&idUser=<?php echo $idUser;?>" onclick="javascript:deletePhoto('${url}');javascript:window.location.reload();">Delete</button>
           <input type="checkbox" name="delete" value="1" />
        </td>
    </tr>
  </script>
  
  <!-- sdfjklaslkfasdflkjasdf -->

  <script type="text/javascript">
	function deletePhoto(url) {
	  AbreVentana ('eliminar-imagen.php?photoName='+url,'200','100','1');
      if (true) {
  	    window.location.reload();
	  }
    }
	function AbreVentana(popup,w,h,s)
      {this.name="Principal";LeftPosition=(screen.width)?(screen.width-w)/2:0;TopPosition=(screen.height)?(screen.height-h)/2:0;
      WindowHandle=window.open(popup,"","toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars="+s+",resizable=no,width="+w+",height="+h+",top="+TopPosition+",left="+LeftPosition);
      if(WindowHandle){if(!WindowHandle.closed){if(false){WindowHandle.focus();}else{WindowHandle.close();}}}WindowHandle=window.open(popup,"","toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars="+s+",resizable=no,width="+w+",height="+h+",top="+TopPosition+",left="+LeftPosition);}
  </script>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>
  <script src="http://ajax.aspnetcdn.com/ajax/jquery.templates/beta1/jquery.tmpl.min.js"></script>
  <script src="js/jquery.imagegallery.js"></script>
  <script src="upload/jquery.iframe-transport.js"></script>
  <script src="upload/jquery.xdr-transport.js"></script>
  <script src="upload/jquery.fileupload.js"></script>
  <script src="upload/jquery.fileupload-ui.js"></script>
  <script src="upload/application.js"></script>
  			</div>
  		</div>
  	</div>

 <?php 
  } 
  ?>
  <?php
include_once 'includes/footer.inc.php';