<?php
class Locales extends Controller {
	function __construct() 
	{
		parent::Controller();
		if ($this->redux_auth->logged_in()==1 ){
			$profile_var = $this->redux_auth->profile();
			if ($profile_var->group!="admin" && $profile_var->group!="super" ){
				$this->redux_auth->logout();
				redirect('admin');
			}
		}else{
			$this->redux_auth->logout();
			redirect('admin');
		}
		
		if ( !function_exists('version_compare') || version_compare( phpversion(), '5', '<' ) ){
			$this->load->library('php4/ckeditor');
			$this->load->library('php4/ckfinder');
		}else{
			$this->load->library('php5/ckeditor');
			$this->load->library('php5/ckfinder');
		}
		$this->load->helper('ckeditor');
		$this->load->model('web_fiestas_model');
		function hacer_thumb($nombre_del_archivo,$clave_thumb, $nuevo_ancho , $nuevo_alto){
				
				$config_resize['image_library']   = 'GD2'; 
				$config_resize['source_image']    = './uploads/locales/'.$nombre_del_archivo; 
				$config_resize['thumb_marker']       = $clave_thumb;
				$config_resize['create_thumb']    = TRUE; 
				$config_resize['maintain_ratio']  = TRUE; 
				$config_resize['width']           = $nuevo_ancho; 
				$config_resize['height']          = $nuevo_alto; 
				return $config_resize;
		}
		function nombrenuevo(){
			$fecha = time (); 
			$lafecha=date("y_m_j_H_i_s",$fecha);
			return $lafecha;
		}
		
	}

	
	function index()
	{
		redirect('admin/locales/listado/');
	}
	function listado()
	{
		$this->data['profile']= $this->redux_auth->profile();
		$this->data['head'] = $this->load->view('admin/head_view', $this->data, true);
		$this->data['navigation'] = $this->load->view('admin/main_menu_view', null, true);
		$this->data['content_menu'] = $this->load->view('admin/locales_menu_view', null, true);
	
		//$this->db->order_by("fecha","DESC");
		$this->data['query']=$this->db->get('Locales');
		$this->data['content'] =$this->load->view('admin/locales_listado_view', $this->data, true); 
		$this->data['foot'] = "";
		$this->load->view('templates/admin_view', $this->data);
		
	}
	function nuevo()
	{
		$this->data['body_carga']=' onload="load();" onUnload="GUnload()" ';
		$this->form_validation->set_rules('pc_nombre', '"Nombre del contacto"', 'required');
	    $this->form_validation->set_rules('pc_telefono', '"Teléfono del contacto"', 'required');
		$this->form_validation->set_rules('pc_mail', '"Mail del contacto"', 'required');
		$this->form_validation->set_rules('local', '"Nombre del local"', 'required');
	  	$this->form_validation->set_rules('posx', '"PosX"', '');
		$this->form_validation->set_rules('posy', '"PosY"', '');
		$this->form_validation->set_rules('imagen', '"Imagen"', '');
		$this->form_validation->set_rules('es_informacion', '"Texto en español"', '');
		$this->form_validation->set_rules('en_informacion', '"Texto en ingles"', '');
		$this->form_validation->set_rules('du_informacion', '"Texto en holandes"', '');
		$this->form_validation->set_rules('es_contacto', '"Texto de contacto en español"', '');
		$this->form_validation->set_rules('en_contacto', '"Texto de contacto  en ingles"', '');
		$this->form_validation->set_rules('du_contacto', '"Texto de contacto  en holandes"', '');
	    $this->form_validation->set_error_delimiters('<p class="error">', '</p>');
	    if ($this->form_validation->run() == false)
	    { 
			
			$this->data['profile']= $this->redux_auth->profile();
			$this->data['head'] = $this->load->view('admin/head_view', $this->data, true);
			$this->data['navigation'] = $this->load->view('admin/main_menu_view', null, true);
			$this->data['content_menu'] = $this->load->view('admin/locales_menu_view', null, true);
			$this->data['accion_form']='admin/locales/nuevo';
			
			$this->data['registro'] = array(
							"pc_nombre" => '' , 
							"pc_telefono" => '' ,
							"pc_mail" => '' , 
							"local" => '' , 
							"id_ciudad" => '' , 
							"id_pais" => '' , 
							"posx"   => '-16.6178' ,
							"posy"   => '28.2633' ,
							
							"es_informacion" => '' , 
							"en_informacion" => '' ,
			"du_informacion" => '' ,
							"es_contacto" => '' , 
							"en_contacto" => '',
			"du_contacto" => ''
							);
			$this->data['slct_estilo'] 	= $estilo 	= array();
			$this->data['qry_estilo']  = $this->web_fiestas_model->cargar_estilos();
			$this->data['query']=$this->db->get('Pais');
			
			$this->db->where('id_pais', '1' );
			$this->data['query_ciudad']=$this->db->get('Ciudad');
			
			$ckeditor =  CK_Editor();
			$this->data['ckeditor']= $ckeditor; 
		
	        $this->data['content'] = $this->load->view('admin/locales_form_view', $this->data, true);
	       	$this->data['foot'] = "";
			$this->load->view('templates/admin_view', $this->data);
	    }
	    else
	    {
	    	$config_upload['upload_path']    = './uploads/locales/'; 
			$config_upload['allowed_types']  = 'gif|jpg|png'; 
			$config_upload['max_size']       = '1000'; 
			$config_upload['max_width']      = '2048'; 
			$config_upload['max_height']     = '1536'; 
			$this->load->library('upload', $config_upload);
			if ( $this->upload->do_upload('imagen')) 
			{ 
				$imagen=$this->upload->data();
				
				$nuevo_nombre=nombrenuevo();
				$ar_imagen_ant=explode(".",$imagen['file_name']);
				$this->data['registro']['imagen'] =$update['imagen'] = $nuevo_nombre.'.'.$ar_imagen_ant[1];
				rename('./uploads/locales/'.$imagen['file_name'],'./uploads/locales/'.$update['imagen']); 
		
				$this->load->library('image_lib', hacer_thumb($update['imagen'], 'p', 180 , 1000 ) ); 
				$this->image_lib->resize(); 	
				$this->image_lib->clear(); 	
				$this->image_lib->initialize(hacer_thumb($update['imagen'], 'm', 220 , 1600 ) ); 
				$this->image_lib->resize(); 	
				

			} 
		   	$this->data['registro']['id_pais'] 		= $this->input->post('select_pais');
		   	if ($this->input->post('es_new_ciudad')!=''){
				$this->data['registro_ciudad']['es_titulo'] 		= $this->input->post('es_new_ciudad');
				$this->data['registro_ciudad']['en_titulo'] 		= $this->input->post('en_new_ciudad');
				$this->data['registro_ciudad']['du_titulo'] 		= $this->input->post('du_new_ciudad');
				$this->data['registro_ciudad']['id_pais'] 			= $this->input->post('select_pais');
				$this->db->set($this->data['registro_ciudad']); 
				$this->db->insert('Ciudad');
				$this->data['registro']['id_ciudad'] =$this->db->insert_id();
			}else{
				$this->data['registro']['id_ciudad'] = $this->input->post('select_ciudad');
			}
		   
			$this->data['registro']['pc_nombre'] 		= $this->input->post('pc_nombre');
			$this->data['registro']['pc_telefono'] 		= $this->input->post('pc_telefono');
			$this->data['registro']['pc_mail'] 		    = $this->input->post('pc_mail');
			$this->data['registro']['local'] 			= $this->input->post('local');
			$this->data['registro']['posx'] 			= $this->input->post('posx');
			$this->data['registro']['posy'] 			= $this->input->post('posy');
			$this->data['registro']['estilos'] 			= $this->input->post('estilos_id');
			
			$this->data['registro']['es_informacion'] 	= $this->input->post('es_informacion');
			$this->data['registro']['en_informacion'] 	= $this->input->post('en_informacion');
			$this->data['registro']['du_informacion'] 	= $this->input->post('du_informacion');
			$this->data['registro']['es_contacto'] 		= $this->input->post('es_contacto');
			$this->data['registro']['en_contacto'] 		= $this->input->post('en_contacto');
			$this->data['registro']['du_contacto'] 		= $this->input->post('du_contacto');
			
			
			$this->db->set($this->data['registro']); 
			
			if ($this->db->insert('Locales')){
				$this->session->set_flashdata('message', '<p class="notice">La información ha sido insertada.</p>');
	            redirect('admin/locales/nuevo/'.$this->uri->segment(4));
			} else
	        {
	            $this->session->set_flashdata('message', '<p class="error">No se ha podido guardar.</p>');
	            redirect('admin/locales/nuevo/'.$this->uri->segment(4));
	        }
	    }	
		
		
	}
	function modificar()
	{
		$this->data['body_carga']=' onload="load();" onUnload="GUnload()" ';
		$this->form_validation->set_rules('pc_nombre', '"Nombre del contacto"', 'required');
	    $this->form_validation->set_rules('pc_telefono', '"Teléfono del contacto"', 'required');
		$this->form_validation->set_rules('pc_mail', '"Mail del contacto"', 'required');
		$this->form_validation->set_rules('local', '"Nombre del local"', 'required');
		$this->form_validation->set_rules('posx', '"PosX"', '');
		$this->form_validation->set_rules('posy', '"PosY"', '');
		$this->form_validation->set_rules('imagen', '"Imagen"', '');
		
		$this->form_validation->set_rules('es_informacion', '"Texto en español"', '');
		$this->form_validation->set_rules('en_informacion', '"Texto en ingles"', '');
		$this->form_validation->set_rules('du_informacion', '"Texto en holandes"', '');
		$this->form_validation->set_rules('es_contacto', '"Texto de contacto en español"', '');
		$this->form_validation->set_rules('en_contacto', '"Texto de contacto  en ingles"', '');
		$this->form_validation->set_rules('du_contacto', '"Texto de contacto  en holandes"', '');
	    $this->form_validation->set_error_delimiters('<p class="error">', '</p>');
	    if ($this->form_validation->run() == false)
	    { 
			
			
			$this->data['profile']= $this->redux_auth->profile();
			
	        
			$this->data['accion_form']='admin/locales/modificar/'.$this->uri->segment(4);
			
			$this->db->where('id', $this->uri->segment(4) );
			$query=$this->db->get('Locales');
			$this->data['errores']='';
			$ckeditor =  CK_Editor();
			$this->data['ckeditor']= $ckeditor; 
			foreach ($query->result() as $row)
			{
				if ($row->imagen!=""){
					$ar_imagen=explode(".",$row->imagen);
					$imagen=base_url().'uploads/locales/'.$ar_imagen[0].'p.'.$ar_imagen[1];
				}else{
					$imagen=base_url().'uploads/locales/espacio.gif';
				}
				$this->data['registro'] = array(
											"pc_nombre" 		=> $row->pc_nombre , 
											"pc_telefono" 		=> $row->pc_telefono ,
											"pc_mail" 			=> $row->pc_mail , 
											"local" 			=> $row->local , 
											"posx" 				=> $row->posx ,
											"posy" 				=> $row->posy , 
											
											"id_ciudad" 		=> $row->id_ciudad ,
											"id_pais" 			=> $row->id_pais , 
											"imagen"			=> $imagen ,
											"es_informacion" 	=> htmlspecialchars_decode($row->es_informacion) , 
											"en_informacion" 	=> htmlspecialchars_decode($row->en_informacion) ,
				"du_informacion" 	=> htmlspecialchars_decode($row->du_informacion) ,
											"es_contacto" 		=> htmlspecialchars_decode($row->es_contacto) , 
											"en_contacto" 		=> htmlspecialchars_decode($row->en_contacto),
				"du_contacto" 		=> htmlspecialchars_decode($row->du_contacto)
											);
				$this->data['local'] 	= $row->local;
				$this->data['slct_estilo'] 	= explode(",",$row->estilos);
				$this->data['qry_estilo']  = $this->web_fiestas_model->cargar_estilos();
				$this->data['query']=$this->db->get('Pais');
			
				$this->db->where('id_pais', $row->id_pais );
				
				$this->data['query_ciudad']=$this->db->get('Ciudad');
			}
			if (!isset($this->data['registro'])){
				 redirect('admin/locales/listado');
			}
			$this->data['head'] = $this->load->view('admin/head_view', $this->data, true);
			$this->data['navigation'] = $this->load->view('admin/main_menu_view', null, true);
			$this->data['content_menu'] = $this->load->view('admin/locales_detalle_menu_view', $this->data, true);
			$this->data['content'] = $this->load->view('admin/locales_form_view', $this->data, true);
	       	$this->data['foot'] = "";
			$this->load->view('templates/admin_view', $this->data);
	    }
	    else
	    {
	        
			
			$update = array(
							"pc_nombre" 		=> $this->input->post('pc_nombre') , 
							"pc_telefono" 		=> $this->input->post('pc_telefono') , 
							"pc_mail" 			=> $this->input->post('pc_mail') ,
							"local" 			=> $this->input->post('local') , 
							"posx" 				=> $this->input->post('posx') , 
							"posy" 				=> $this->input->post('posy') , 
							
							"estilos" 			=> $this->input->post('estilos_id'),
							"es_informacion" 	=> $this->input->post('es_informacion'),
							"en_informacion" 	=> $this->input->post('en_informacion'),
			"du_informacion" 	=> $this->input->post('du_informacion'),
							"es_contacto" 		=> $this->input->post('es_contacto'),
							"en_contacto" 		=> $this->input->post('en_contacto'),
			"du_contacto" 		=> $this->input->post('du_contacto')
							);
			
			
			$config_upload['upload_path']    = './uploads/locales/'; 
			$config_upload['allowed_types']  = 'gif|jpg|png'; 
			$config_upload['max_size']       = '1000'; 
			$config_upload['max_width']      = '2048'; 
			$config_upload['max_height']     = '1536'; 
			
			$this->load->library('upload', $config_upload);
			if ( $this->upload->do_upload('imagen')) 
			{ 
				$imagen=$this->upload->data();
				$nuevo_nombre=nombrenuevo();
				$ar_imagen_ant=explode(".",$imagen['file_name']);
				$this->data['registro']['imagen'] =$update['imagen'] = $nuevo_nombre.'.'.$ar_imagen_ant[1];
				rename('./uploads/locales/'.$imagen['file_name'],'./uploads/locales/'.$update['imagen']); 
		
				$this->load->library('image_lib', hacer_thumb($update['imagen'], 'p', 180 , 1000 ) ); 
				$this->image_lib->resize(); 	
				$this->image_lib->clear(); 	
				$this->image_lib->initialize(hacer_thumb($update['imagen'], 'm', 220 , 1600 ) ); 
				$this->image_lib->resize(); 	
				
				$this->db->where('id', $this->uri->segment(4) );
				$query=$this->db->get('Locales');
				foreach ($query->result() as $row)
				{
					$ar_imagen=explode(".",$row->imagen);
					$imagen=PUBPATH.'uploads/locales/'.$ar_imagen[0].'p.'.$ar_imagen[1];
					@unlink($imagen);
					$imagen=PUBPATH.'uploads/locales/'.$ar_imagen[0].'m.'.$ar_imagen[1];
					@unlink($imagen);
					$imagen=PUBPATH.'uploads/locales/'.$ar_imagen[0].'.'.$ar_imagen[1];
					@unlink($imagen);
				}
			} 
			$update['id_pais'] 		= $this->input->post('select_pais');
		   	if ($this->input->post('es_new_ciudad')!=''){
				$this->data['registro_ciudad']['es_titulo'] 		= $this->input->post('es_new_ciudad');
				$this->data['registro_ciudad']['en_titulo'] 		= $this->input->post('en_new_ciudad');
				$this->data['registro_ciudad']['du_titulo'] 		= $this->input->post('du_new_ciudad');
				$this->data['registro_ciudad']['id_pais'] 			= $this->input->post('select_pais');
				$this->db->set($this->data['registro_ciudad']); 
				$this->db->insert('Ciudad');
				$update['id_ciudad'] =$this->db->insert_id();
			}else{
				$update['id_ciudad']= $this->input->post('select_ciudad');
			}
			
			$this->db->where('id', $this->uri->segment(4) );
			
			if ($this->db->update("Locales",$update)){
				$this->session->set_flashdata('message', '<p class="notice">La información ha sido modifica.</p>');
	            redirect('admin/locales/modificar/'.$this->uri->segment(4));
			} else
	        {
	            $this->session->set_flashdata('message', '<p class="error">No se ha podido modificar.</p>');
	            redirect('admin/locales/modificar/'.$this->uri->segment(4));
	        }
	    }	
	}
	function borrar()
	{
		$this->data['profile']= $this->redux_auth->profile();
		$this->data['head'] = $this->load->view('admin/head_view', $this->data, true);
		$this->data['navigation'] = $this->load->view('admin/main_menu_view', null, true);
		$this->data['content_menu'] = $this->load->view('admin/locales_menu_view', null, true);
		
		
		$this->db->where('id', $this->uri->segment(4 ) );
		
		$query=$this->db->get('Locales');
		foreach ($query->result() as $row)
		{
			$this->data['es_titulo'] =$row->es_titulo;
		}
		
		$this->data['content'] =$this->load->view('admin/locales_borrar_view', $this->data, true); ;
		$this->data['foot'] = "";
		$this->load->view('templates/admin_view', $this->data);
	}
	function borrar_confirm()
	{
		$this->data['profile']= $this->redux_auth->profile();
		$this->db->where('id', $this->uri->segment(4) );
		$query=$this->db->get('Locales');
		if ($query->num_rows() > 0){
				if ($row->imagen!=""){
						$ar_imagen=explode(".",$row->imagen);
						
						$imagen=PUBPATH.'uploads/locales/'.$ar_imagen[0].'p.'.$ar_imagen[1];
						if(file_exists($imagen)) { 	@unlink($imagen);	}
						
						$imagen= PUBPATH.'uploads/locales/'.$ar_imagen[0].'.'.$ar_imagen[1];
						if(file_exists($imagen)) {	@unlink($imagen);    }		
				}
			
				$this->db->where('id', $this->uri->segment(4) );
				$this->db->delete('Locales'); 
		}
		redirect('admin/locales/listado/');
	}
}


