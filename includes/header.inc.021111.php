<?php
/****************************
*  Created 21/08/11
*  Last update 21/08/11
****************************/
ini_set("display_errors", "E_STRICT");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="expires" content="Sat, 27 May 1978 12:00:00 GMT" />
<meta name="country" content="es" />
<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>SHARK DJ</title>
<link href="carousel/skin.css" rel="stylesheet" type="text/css" media="screen" />
<link href="shark.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="js/jquery-1.6.4.min.js"></script> 
<script type="text/javascript" src="js/jquery.jcarousel.min.js"></script>
<style type="text/css"> 
.jcarousel-skin-tango .jcarousel-container-horizontal {
    width: 620px;
}
 
.jcarousel-skin-tango .jcarousel-clip-horizontal {
    width: 605px;
}
</style>
<script type="text/javascript"> 
jQuery(document).ready(function() {
    jQuery('#mycarousel').jcarousel({
        visible: 1
    });
});
</script> 
</head>
<body>
<div id="container">
  <div id="header">
    <div id="logo"></div>
    <div id="loggout" class="right"><a href="login.php?status=loggedout">LOGOUT</a></div>
    <div id="radio" class="right marginRight5">RADIO</div>
  </div>
  <div id="menu">
    <div class="left width68 marginTop10 marginLeft10">
      <a href="index.php">HOME</a> | <a href="artist.php">ARTIST</a> | <a href="">MENU3</a> | <a href="newArtist.php">ALTA USUARIO</a> | <a href="adminArtists.php">ADMIN ARTISTS FORM</a> 
    </div>
  </div>
  <?php include 'carousel/carousel.php'; ?>
  <div class="sideBar2">Artista SHARK del mes.</div>