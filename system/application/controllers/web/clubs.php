<?php
class Clubs extends Controller {
	function __construct() 
	{
		parent::Controller();
		$this->load->model('web_fiestas_model');
		$this->load->model('banner_model');
		
	}
	
	
	function index()
	{
		
		$this->data['registro']['local']   ='';
		$this->data['registro']['ciudad']  =0;
		$this->data['registro']['pais']    =0;
		
		$comp_form=$this->input->post('select_pais');
		$sql = 'SELECT * FROM Locales ';// WHERE word = ? AND ip_address = ? AND captcha_time > ?";
		$ct_where=0;
		if ($comp_form!='')
		{	
			
			if ( $this->input->post('select_pais')!='0' )
			{
				$this->data['registro']['pais'] 	=  $this->input->post('select_pais') ;
				$this->data['registro']['ciudad'] 	= $this->input->post('select_ciudad');
				$ct_where=1;
				$sql.= ' WHERE ';
				$sql.= ' id_pais = '.$this->data['registro']['pais'];
				
				
				if ( $this->data['registro']['ciudad']!='0' )
				{
					$sql.= ' AND id_ciudad = '.$this->data['registro']['ciudad'];
					
				}
			}
			if ( $this->input->post('local')!='' )
			{
				if ($ct_where==0){
					$ct_where=1;
					$sql.= ' WHERE ';
				}else {
					$sql.= ' AND ';
				}
				$this->data['registro']['local']   = $this->input->post('local');
				$sql.= ' local LIKE "%'.$this->data['registro']['local'].'%" ';
				
				
			}
			$this->data['select_estilo']=$this->input->post('check_list'); 
			if (count($this->data['select_estilo'])>0){
				if ($ct_where==0){
					$sql.= ' WHERE ';
				}else{
					$sql.= ' AND (';
				}
				for ($i=0 ; $i<count($this->data['select_estilo']) ; $i++)    
				{   
					
					if ($i==0){
						$sql.= ' estilos LIKE "%,'.$this->data['select_estilo'][$i].',%" ';
						
					}else{
						$sql.= ' OR estilos LIKE "%,'.$this->data['select_estilo'][$i].',%" ';
					}
				}
				if ($ct_where==1){
					$sql.= ' )';
				}
			}
		}
		
		$this->data['query']=$this->db->query($sql);
		$this->data['query_pais']=$this->db->get('Pais');
		$this->db->where('id_pais', $this->data['registro']['pais'] );
		$this->data['query_ciudad']=$this->db->get('Ciudad');
		
		$this->data['query_estilo']=$this->db->get('Estilo');
		
		$this->data['pulsado']='clubs';
		$this->data['sub_head'] = $this->load->view('web/sub_head_clubs_view', $this->data, true);
		
		$this->data['cuerpo'] = $this->load->view('web/clubs_listado_view', $this->data, true);
		$this->load->view('templates/es_web_view',  $this->data);
		
	}
	
	function informacion()
	{
		$this->data['body_carga']=' onload="load();" onUnload="GUnload()" ';
		$this->data['pulsado']='clubs';
		$this->data['sub_pulsado']='info';
		$this->db->where('id', $this->uri->segment(4) );
		$query=$this->db->get('Locales');
		foreach ($query->result() as $row)
		{
			if ($row->imagen!=""){
				$ar_imagen=explode(".",$row->imagen);
				$imagen=base_url().'uploads/locales/'.$ar_imagen[0].'m.'.$ar_imagen[1];
			}else{
				$imagen=base_url().'uploads/locales/espacio.gif';
			}
			$this->data['registro'] = array(
										
										
										"local" 			=> $row->local , 
										"posx" 				=> $row->posx ,
										"posy" 				=> $row->posy , 
										
										"imagen"			=> $imagen ,
										"es_informacion" 	=> htmlspecialchars_decode($row->es_informacion),
										"es_contacto" 		=> htmlspecialchars_decode($row->es_contacto) 
				
										);
			$this->db->where('id', $row->id_ciudad );
			$query_ciudad=$this->db->get('Ciudad');
			if ($query_ciudad->num_rows() > 0){
				foreach ($query_ciudad->result() as $row_ciudad)
				{
					$this->data['registro']['ciudad']=$row_ciudad->es_titulo;
				}
			}
			
			$this->db->where('id', $row->id_pais );
			$query_pais=$this->db->get('Pais');
			if ($query_pais->num_rows() > 0){
				foreach ($query_pais->result() as $row_pais)
				{
					$this->data['registro']['pais']=$row_pais->es_titulo;
				}
			}
			
		}
		if (!isset($this->data['registro'])){
			 redirect('web/clubs');
		}
		$this->data['sub_head'] = $this->load->view('web/sub_head_clubs_detalle_view', $this->data, true);
		$this->data['cuerpo'] = $this->load->view('web/clubs_info_view', $this->data, true);
		$this->load->view('templates/es_web_view',  $this->data);
		
	}
	function calendario()
	{
		$this->data['pulsado']='clubs';
		$this->data['sub_pulsado']='calendario';
		$this->db->where('id', $this->uri->segment(4) );
		$query=$this->db->get('Locales');
		foreach ($query->result() as $row)
		{
			if ($row->imagen!=""){
				$ar_imagen=explode(".",$row->imagen);
				$imagen=base_url().'uploads/locales/'.$ar_imagen[0].'m.'.$ar_imagen[1];
			}else{
				$imagen=base_url().'uploads/locales/espacio.gif';
			}
			
			
			
			
			
			$this->data['registro'] = array(
										
										
										"local" 			=> $row->local , 
										"posx" 				=> $row->posx ,
										"posy" 				=> $row->posy , 
										
										"imagen"			=> $imagen ,
										"es_informacion" 	=> htmlspecialchars_decode($row->es_informacion),
										"es_contacto" 		=> htmlspecialchars_decode($row->es_contacto) 
				
										);
			$this->db->where('id', $row->id_ciudad );
			$query_ciudad=$this->db->get('Ciudad');
			if ($query_ciudad->num_rows() > 0){
				foreach ($query_ciudad->result() as $row_ciudad)
				{
					$this->data['registro']['ciudad']=$row_ciudad->es_titulo;
				}
			}
			
			$this->db->where('id', $row->id_pais );
			$query_pais=$this->db->get('Pais');
			if ($query_pais->num_rows() > 0){
				foreach ($query_pais->result() as $row_pais)
				{
					$this->data['registro']['pais']=$row_pais->es_titulo;
				}
			}
			
		}
		if (!isset($this->data['registro'])){
			 redirect('web/clubs');
		}
		$this->db->where( 'id_local' , $this->uri->segment(4) );
		$this->db->order_by('fecha','DESC');
		$this->data['query']=$this->db->get('Locales_fiestas');
		$this->data['sub_head'] = $this->load->view('web/sub_head_clubs_detalle_view', $this->data, true);
		$this->data['cuerpo'] = $this->load->view('web/clubs_fiestas_view', $this->data, true);
		$this->load->view('templates/es_web_view',  $this->data);
		
	}
	function localizacion()
	{
		$this->data['body_carga']=' onload="load();" onUnload="GUnload()" ';
		$this->data['pulsado']='clubs';
		$this->data['sub_pulsado']='localizacion';
		$this->db->where('id', $this->uri->segment(4) );
		$query=$this->db->get('Locales');
		foreach ($query->result() as $row)
		{
			if ($row->imagen!=""){
				$ar_imagen=explode(".",$row->imagen);
				$imagen=base_url().'uploads/locales/'.$ar_imagen[0].'m.'.$ar_imagen[1];
			}else{
				$imagen=base_url().'uploads/locales/espacio.gif';
			}
			
			
			
			
			
			$this->data['registro'] = array(
										
										
										"local" 			=> $row->local , 
										"posx" 				=> $row->posx ,
										"posy" 				=> $row->posy , 
										
										"imagen"			=> $imagen ,
										"es_informacion" 	=> htmlspecialchars_decode($row->es_informacion),
										"es_contacto" 		=> htmlspecialchars_decode($row->es_contacto) 
				
										);
			$this->db->where('id', $row->id_ciudad );
			$query_ciudad=$this->db->get('Ciudad');
			if ($query_ciudad->num_rows() > 0){
				foreach ($query_ciudad->result() as $row_ciudad)
				{
					$this->data['registro']['ciudad']=$row_ciudad->es_titulo;
				}
			}
			
			$this->db->where('id', $row->id_pais );
			$query_pais=$this->db->get('Pais');
			if ($query_pais->num_rows() > 0){
				foreach ($query_pais->result() as $row_pais)
				{
					$this->data['registro']['pais']=$row_pais->es_titulo;
				}
			}
			
		}
		if (!isset($this->data['registro'])){
			 redirect('web/clubs');
		}
		$this->data['sub_head'] = $this->load->view('web/sub_head_clubs_detalle_view', $this->data, true);
		$this->data['cuerpo'] = $this->load->view('web/clubs_localizacion_view', $this->data, true);
		$this->load->view('templates/es_web_view',  $this->data);
		
	}
	
	function fiesta()
	{
		$this->data['pulsado']='clubs';
		$this->data['sub_pulsado']='calendario';
		$this->db->where('id', $this->uri->segment(4) );
		$query=$this->db->get('Locales');
		foreach ($query->result() as $row)
		{
			if ($row->imagen!=""){
				$imagen=base_url().'uploads/locales/'.$row->imagen;
			}else{
				$imagen=base_url().'uploads/locales/espacio.gif';
			}
			$this->data['registro'] = array(
										
										
										"local" 			=> $row->local , 
										"posx" 				=> $row->posx ,
										"posy" 				=> $row->posy , 
										"imagen"			=> $imagen 
				
										);
			$this->db->where('id', $row->id_ciudad );
			$query_ciudad=$this->db->get('Ciudad');
			if ($query_ciudad->num_rows() > 0){
				foreach ($query_ciudad->result() as $row_ciudad)
				{
					$this->data['registro']['ciudad']=$row_ciudad->es_titulo;
				}
			}
			
			$this->db->where('id', $row->id_pais );
			$query_pais=$this->db->get('Pais');
			if ($query_pais->num_rows() > 0){
				foreach ($query_pais->result() as $row_pais)
				{
					$this->data['registro']['pais']=$row_pais->es_titulo;
				}
			}
			
		}
		if (!isset($this->data['registro'])){
			 redirect('web/clubs');
		}
		
		$this->db->where('id', $this->uri->segment(5) );
		$query=$this->db->get('Locales_fiestas');
		foreach ($query->result() as $row)
		{
			list($f_date,$f_hora)=explode(" ",$row->fecha);
			list($hora,$minutos,$segundos)=explode(":",$f_hora);
			if ($row->imagen!=""){
				$ar_imagen=explode(".",$row->imagen);
				$imagen=base_url().'uploads/fiestas/'.$ar_imagen[0].'p.'.$ar_imagen[1];
			}else{
				$imagen=base_url().'uploads/fiestas/espacio.gif';
			}
			$this->data['registro_fiesta'] = array(
						
										"es_titulo" 		=> $row->es_titulo , 
										"f_date" 			=> $f_date , 
										"hora" 				=> $hora ,
										"minutos" 			=> $minutos/5 ,
										"imagen"		=> $imagen ,
										"es_informacion" 	=> htmlspecialchars_decode($row->es_informacion)  
										
										
										);
			$this->data['slct_estilo'] 	= explode(",",$row->estilos);
			$this->data['qry_estilo']  = $this->web_fiestas_model->cargar_estilos();
		}
		
		
		$this->data['sub_head'] = $this->load->view('web/sub_head_clubs_detalle_view', $this->data, true);
		$this->data['cuerpo'] = $this->load->view('web/clubs_fiestas_detalle_view', $this->data, true);
		$this->load->view('templates/es_web_view',  $this->data);
		
	}
	
}