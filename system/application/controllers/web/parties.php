<?php
class Parties extends Controller {
	function __construct() 
	{
		parent::Controller();
		$this->load->model('web_fiestas_model');
		$this->load->model('banner_model');
	}
	
	function index()
	{
		$this->load->plugin('js_calendar');
		$this->data['calendario']='1';
		$this->data['pulsado']='fiestas';
		$this->form_validation->set_rules('fecha', '', '');
	    $this->form_validation->set_error_delimiters('<p class="error">', '</p>');
	    
		if ($this->form_validation->run() == false)
	    { 	
			$this->data['fecha'] 		= $fecha 	= date("Y-m-d");
			$this->data['fecha_corta']  = date("Ymd");
			$this->data['pais'] 		= $pais 	= '';
			$this->data['ciudad'] 		= $ciudad	= '';
			$this->data['qry_ciudad']	= array();
			$this->data['local']       	= $local 	= '';
			$this->data['slct_estilo'] 	= $estilo 	= array();
		}else{
			$this->data['fecha']  		= $fecha 	= $this->input->post('fecha'); 
			$this->data['fecha_corta']  = str_replace('-', '', $this->input->post('fecha'));
			$this->data['pais']   		= $pais  	= $this->input->post('select_pais');
			$this->data['ciudad'] 		= $ciudad 	= $this->input->post('select_ciudad');
			if ($pais!='0'){
				$this->data['qry_ciudad']	= $this->web_fiestas_model->cargar_ciudades($pais);
			}else{
				$this->data['qry_ciudad']	= array();
			}
			$this->data['local']        = $local 	= $this->input->post('local');								   
			$this->data['slct_estilo'] 	= $estilo 	= $this->input->post('check_list'); 
		}
		
		$this->data['arr_fiestas'] = $this->web_fiestas_model->cargar_fiestas_en ( $fecha , $pais , $ciudad , $local , $estilo );
		$this->data['qry_estilo']  = $this->web_fiestas_model->cargar_estilos();
		$this->data['qry_pais']    = $this->web_fiestas_model->cargar_pais();
		
		$this->data['sub_head'] = $this->load->view('web_en/sub_head_fiestas_view', $this->data, true);
		$this->data['cuerpo']   = $this->load->view('web_en/fiestas_listado_view', $this->data, true);
		$this->load->view('templates/en_web_view',  $this->data);
		
	}
	
	function ajax_cargar_ciudades (){
		$this->data['qry_ciudad']	= $this->web_fiestas_model->cargar_ciudades( $this->uri->segment(4) );
		$this->load->view('modulos/aj_select_ciudades_view',  $this->data);
	}
	

	function information()
	{
		$this->data['pulsado']='fiestas';
		$this->data['sub_pulsado']='info';
		$this->db->where('id', $this->uri->segment(4) );
		$query=$this->db->get('Fiestas');
		foreach ($query->result() as $row)
		{
			if ($row->imagen!=""){
				$ar_imagen=explode(".",$row->imagen);
				$imagen=base_url().'uploads/fiestas/'.$ar_imagen[0].'p.'.$ar_imagen[1];
			}else{
				$imagen=base_url().'uploads/fiestas/espacio.gif';
			}
			
			
			
			
			
			$this->data['registro'] = array(
										
										
										"es_titulo" 		=> $row->en_titulo , 
										"posx" 				=> $row->posx ,
										"posy" 				=> $row->posy , 
										"imagen" 			=> $imagen , 
										
										"es_informacion" 	=> htmlspecialchars_decode($row->en_informacion),
										"es_contacto" 		=> htmlspecialchars_decode($row->en_contacto) 
				
										);
		
			
			$this->db->where('id', $row->id_ciudad );
			$query_ciudad=$this->db->get('Ciudad');
			if ($query_ciudad->num_rows() > 0){
				foreach ($query_ciudad->result() as $row_ciudad)
				{
					$this->data['registro']['ciudad']=$row_ciudad->en_titulo;
				}
			}
			
			$this->db->where('id', $row->id_pais );
			$query_pais=$this->db->get('Pais');
			if ($query_pais->num_rows() > 0){
				foreach ($query_pais->result() as $row_pais)
				{
					$this->data['registro']['pais']=$row_pais->en_titulo;
				}
			}
			
		}
		if (!isset($this->data['registro'])){
			 redirect('web/parties');
		}
		$this->data['sub_head'] = $this->load->view('web_en/sub_head_fiestas_detalle_view', $this->data, true);
		$this->data['cuerpo'] = $this->load->view('web_en/fiestas_info_view', $this->data, true);
		$this->load->view('templates/en_web_view',  $this->data);
		
	}
	function location()
	{
		$this->data['body_carga']=' onload="load();" onUnload="GUnload()" ';
		$this->data['pulsado']='fiestas';
		$this->data['sub_pulsado']='localizacion';
		$this->db->where('id', $this->uri->segment(4) );
		$query=$this->db->get('Fiestas');
		foreach ($query->result() as $row)
		{
			if ($row->imagen!=""){
				$imagen=base_url().'uploads/fiestas/'.$row->imagen;
			}else{
				$imagen=base_url().'uploads/fiestas/espacio.gif';
			}
			
			
			
			
			
			$this->data['registro'] = array(
										
										
										"es_titulo" 		=> $row->en_titulo , 
										"posx" 				=> $row->posx ,
										"posy" 				=> $row->posy , 
										
										"imagen"			=> $imagen ,
										"es_informacion" 	=> htmlspecialchars_decode($row->en_informacion),
										"es_contacto" 		=> htmlspecialchars_decode($row->en_contacto) 
				
										);
			$this->db->where('id', $row->id_ciudad );
			$query_ciudad=$this->db->get('Ciudad');
			if ($query_ciudad->num_rows() > 0){
				foreach ($query_ciudad->result() as $row_ciudad)
				{
					$this->data['registro']['ciudad']=$row_ciudad->en_titulo;
				}
			}
			
			$this->db->where('id', $row->id_pais );
			$query_pais=$this->db->get('Pais');
			if ($query_pais->num_rows() > 0){
				foreach ($query_pais->result() as $row_pais)
				{
					$this->data['registro']['pais']=$row_pais->en_titulo;
				}
			}
			
		}
		if (!isset($this->data['registro'])){
			 redirect('web/parties');
		}
		$this->data['sub_head'] = $this->load->view('web_en/sub_head_fiestas_detalle_view', $this->data, true);
		$this->data['cuerpo'] = $this->load->view('web_en/fiestas_localizacion_view', $this->data, true);
		$this->load->view('templates/en_web_view',  $this->data);
		
	}
}


