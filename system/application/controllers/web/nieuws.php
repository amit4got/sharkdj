<?php
class Nieuws extends Controller {
	function __construct() 
	{
		parent::Controller();
		$this->load->model('banner_model');
		$this->load->library('pagination');
	}
	function index()
	{
		redirect('web/nieuws/lijst/0');
	}
	function lijst()
	{
		$config['base_url'] = 'http://www.sharkdj.com/index.php/web/nieuws/lijst';
		
		$query=$this->db->where('es_clave','Noticias')->get('Textos');
			
		$config["total_rows"] = $query->num_rows();
		$config['per_page'] = '5';
		$config['cur_page']=$this->uri->segment(4);
	
		
		$this->pagination->initialize($config);
		
		$this->data['pulsado']='noticias';
		$this->data['sub_head'] = $this->load->view('web_du/sub_head_1_view', $this->data, true);
		$this->db->where('es_clave', 'noticias');
		$this->db->order_by('id','DESC');
		$this->data['query']=$this->db->get('Textos', $config['per_page'] , $this->uri->segment(4) );
		$this->data['cuerpo'] = $this->load->view('web_du/noticias_listado_view', $this->data, true);
		$this->load->view('templates/du_web_view',  $this->data);
	
	}
	
	function detail()
	{
		$this->data['pulsado']='noticias';
		$this->data['sub_head'] = $this->load->view('web_du/sub_head_1_view', $this->data, true);
		$this->db->where('es_clave',  'noticias');
		$this->db->where('id', $this->uri->segment(4) );
		$query=$this->db->get('Textos');
		foreach ($query->result() as $row)
		{
			if ($row->imagen!=""){
				$imagen=base_url().'uploads/Noticias/'.$row->imagen;
			}else{
				$imagen=base_url().'uploads/Noticias/espacio.gif';
			}
			$this->data['registro'] = array(
										"es_titulo" 		=> $row->du_titulo , 
										"imagen"			=> $imagen ,
										"es_informacion" 	=> htmlspecialchars_decode($row->du_informacion) 
										);
		}
		if (!isset($this->data['registro'])){
			 redirect('web/nieuws');
		}
		
		$this->data['cuerpo'] = $this->load->view('web_du/noticias_detalle_view', $this->data, true);
		$this->load->view('templates/du_web_view',  $this->data);
		
	}
}


