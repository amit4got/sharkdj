<?php
/****************************
 *  Created 24/11/11
 *  Last update 24/11/11   
 ****************************/
error_reporting(E_ALL);
ini_set("display_errors", 1);


if(!defined("RUTA_ABSOLUTA")){
  define("RUTA_ABSOLUTA",dirname(__FILE__));
}


#INCLUDES
require_once RUTA_ABSOLUTA.'/administradorDatos/administradorDatos.class.php';
require_once RUTA_ABSOLUTA.'/includes/functions.ini.php';

session_start();

#Load languages
include RUTA_ABSOLUTA.'/includes/lang.inc.php';
$user = new administradorDatos();
$arrayIdiomas = $user->cargaIdioma($idioma);
$langURL  = '';
$langURL2 = '';
if ($idioma != 'es') {
  $langURL  = '?lang=' . $idioma;
  $langURL2 = '&lang=' . $idioma;
}


#Out of session

if (isset($_GET['logout'])) {
  $_SESSION['status'] = $_GET['logout'];
  header ('location: index.php'.$langURL);
  exit;
}
if ($_POST){
   if($_POST['email'] != ''){
    $usuario = new administradorDatos();
    #Load languages
    #include RUTA_ABSOLUTA.'/includes/lang.inc.php';
    #$arrayIdiomas = $usuario->cargaIdioma($idioma);

    $user = $usuario->existeEmail($_POST['email']);
    $_SESSION['idUser'] = $user['idUser'];
    $_SESSION['idPerfil'] = $user['idPerfil'];
    if ($_SESSION['idUser'] == 0){
      $_SESSION['error'] = 1;
    }
    else {
      $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
      $cad = "";
      for($i=0;$i<12;$i++) {
        $cad .= substr($str,rand(0,62),1);
      }
      $usuario->cambiaPassword($user['idUser'], $cad);

      mail($_POST['email'], 'Cambio contraseña Shark DJ', 'Su nueva contraseña para Shark DJ es: '.$cad);

      echo '<div id="dialog" title="Password enviado">
          <p>' . $arrayIdiomas['sendPass'] .' '. $_POST['email'] .'</p>
        </div>';

          session_destroy();
    }
  }
}

include_once 'includes/header.inc.php';
if ($_SESSION['error'] == 1){
  echo '<div id="dialog" title="Error Login">
  	      <p>' . $arrayIdiomas['err1']. '</p>
  	    </div>';



  session_destroy();
}


?>
  
  	<div class="rowLogin">
    		<div id="boxLogin">
    			<form action="<?php echo $_SERVER['PHP_SELF'] . $langURL; ?>" method="post" id="form">
      			<div id="label1" class="labelFormLogin"><?=$arrayIdiomas['email'];?></div>
      			<div class="rounded_input">
      				<input id="field1" name="email" type="email" value>
      			</div>
						<input class="buttonLogin" type="submit" value="<?=$arrayIdiomas['enviar'];?>" />
					</form>
    		</div>
  	</div>
<?php include_once 'includes/footer.inc.php'; ?>