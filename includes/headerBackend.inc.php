<?php
/****************************
*  Created 02/11/11
*  Last update 09/11/11
****************************/
ini_set("display_errors", "E_STRICT");

if(!defined("RUTA_ABSOLUTA")){
  define("RUTA_ABSOLUTA",dirname(__FILE__));
}

session_start();

if ($_SESSION['status'] == "authorized") {
  $sesion = true;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<meta name="country" content="es" />
<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>SHARK DJ</title>
        <link href="shark.css" rel="stylesheet" type="text/css" media="screen" />
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/base/jquery-ui.css" id="theme">
        <link type="text/css" href="css/custom-theme/jquery-ui-1.8.16.custom.css" rel="stylesheet" />	
        <!--<link rel="stylesheet" href="http://blueimp.github.com/jQuery-Image-Gallery/jquery.image-gallery.css">-->
        <link rel="stylesheet" href="upload/jquery.fileupload-ui.css">
        <link rel="stylesheet" href="upload/style.css">
<link rel="icon" type="image/png" href="images/logoSharkDJ.png" />
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
		<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript">
  var geocoder;
  var map;
  var marker;
    
  function load() {
    geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(28.69950221325465,-17.241618192968758);
    var myOptions = {
      zoom: 9,
      center: latlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);    
    infoWindow = new google.maps.InfoWindow();
    google.maps.event.addListener(map, 'click', function(event) {
      placeMarker(event.latLng);
      codeLocation(event.latLng);
    });
  }

  function codeAddress() {
    var address = document.getElementById("address").value;
    geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
    	  placeMarker(results[0].geometry.location);    	
      } else {
        alert("Geocode was not successful for the following reason: " + status);
      }
    });
  }
  
  function codeLocation(location) {
	  geocoder.geocode( { 'location': location}, function(results, status) {
		  if (status == google.maps.GeocoderStatus.OK) {	    	
		  	if (results[0].address_components[1].long_name) {
		    	document.getElementById('address').value = results[0].address_components[1].long_name;
		    	if  (results[0].address_components[0].long_name)
		    		document.getElementById('address').value = document.getElementById('address').value + ", " + results[0].address_components[0].long_name;
		    	if  (results[0].address_components[2].long_name)
		    		document.getElementById('address').value = document.getElementById('address').value + ", " + results[0].address_components[2].long_name;
		    }	    	  
		  } else {
		  	document.getElementById('address').value = "";
		  }
	  });
  }

  function placeMarker(location) {
	  var clickedLocation = new google.maps.LatLng(location);
	  if (marker) {
	  	marker.setMap(null);
	  }
	  marker = new google.maps.Marker({
	      position: location, 
	      map: map,
	      draggable: true
	  });
	  
	  google.maps.event.addListener(marker, 'dragend', function(event){
		  placeMarker(marker.getPosition());
		  codeLocation(marker.getPosition());
	  });
	  
	  changeArtistLocation(location);
	  
	}

  function changeArtistLocation(location) {	  
	  document.getElementById("lat").value = location.lat();
	  document.getElementById("lng").value = location.lng();
	  map.setCenter(location);
  }

  function placeMarkerFromLatLng(lat, lng) {
    var address = lat + ", " + lng;    
    geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
    	  placeMarker(results[0].geometry.location);    	    	
      } else {
        alert("Geocode was not successful for the following reason: " + status);
      }
    });
  }
</script>
<script type="text/javascript">
			$(function(){

				// Accordion
				$("#accordion").accordion({ header: "h3" });
	
				// Tabs
				$('#tabs').tabs();

				$("#tabs").tabs({                    
				    show: function(event, ui) {
				        google.maps.event.trigger(map, 'resize');
				        <?php if ($lat!="" && $lng!="") {?>
				        placeMarkerFromLatLng('<?php echo $lat;?>','<?php echo $lng;?>');
				        <?php } ?>
				    }                    
				});

				// Dialog			
				$('#dialog').dialog({
					autoOpen: false,
					width: 600,
					buttons: {
						"Ok": function() { 
							$(this).dialog("close"); 
						}, 
						"Cancel": function() { 
							$(this).dialog("close"); 
						} 
					}
				});
				
				// Dialog Link
				$('#dialog_link').click(function(){
					$('#dialog').dialog('open');
					return false;
				});

				// Datepicker
				$('#datepicker').datepicker({
					inline: true,
					dateFormat: 'dd/mm/yy'
				});
				
				// Slider
				$('#slider').slider({
					range: true,
					values: [17, 67]
				});
				
				// Progressbar
				$("#progressbar").progressbar({
					value: 20 
				});
				
				//hover states on the static widgets
				$('#dialog_link, ul#icons li').hover(
					function() { $(this).addClass('ui-state-hover'); }, 
					function() { $(this).removeClass('ui-state-hover'); }
				);

				$(function() {
					$( ".imagenPerfil button:first" ).button({
			            icons: {
			                primary: "ui-icon-image"
			            }
			        })
				});
				$(function() {
					$( ".boton button:first" ).button({
			            icons: {
			                primary: "ui-icon-folder-collapsed"
			            }
			        })
			    $( ".botonBio button:first" ).button({
			            icons: {
			                primary: "ui-icon-folder-collapsed"
			            }
			        })
			    $( ".botonImagen button:first" ).button({
			            icons: {
			                primary: "ui-icon-image"
			            }
			        })
			   $( ".botonMapa button:first" ).button({
			            icons: {
			                primary: "ui-icon-folder-collapsed"
			            }
			        })
				});
			});
		</script>
	
	</head>
<body onload="load()" onunload="GUnload()">
<div id="container">
  <div id="header">
    <div id="loginLinks">
      <a href="./inputLogin.php<?=$langURL;?>" class="login"><?=$arrayIdiomas['login'];?></a>
      <span class="left fontGrey2 marginTop3">-</span>
      <a href="./inputSignup.php<?=$langURL;?>" class="login2"><?=$arrayIdiomas['signup'];?></a>
      <div id="flag">
	    <?php
		#SHOW FLAGS
        foreach ($arrayFlagLanguages as $key => $value) {
          echo '<a href="' . $_SERVER['PHP_SELF'] . '?lang=' . $key . '">';
		  echo   '<img class="marginLeft3" src="images/flags/' . $key . '.png" alt="' . $value . '" title="' . $value . '" />';
          echo '</a>';
	    }
	    ?>
	  </div>
      <?php include 'includes/socialLinks.inc.php'; ?>
    </div>
    <div id="menu">
      <div class="clear insideMenu font1">
      <?php 
        $file = explode("/",$_SERVER['SCRIPT_FILENAME']);
        $filename = $file[count($file)-1];
            
      ?>
        <a href="index.php<?=$langURL;?>"><span <?php echo ($filename=="index.php") ? "class=\"fontRed hoverLargeRed\"" : "class=\"hoverLargeGrey\"";?>><?=$arrayIdiomas['home'];?></span></a>
        &bull;&nbsp;<a href="artistas.php<?=$langURL;?>"><span <?php echo ($filename=="artistas.php") ? "class=\"fontRed hoverLargeRed\"" : "class=\"hoverLargeGrey\"";?>><?=$arrayIdiomas['artists'];?></span></a>
        &bull;&nbsp;<a href="locales.php<?=$langURL;?>"><span <?php echo ($filename=="locales.php") ? "class=\"fontRed hoverLargeRed\"" : "class=\"hoverLargeGrey\"";?>><?=$arrayIdiomas['clubs'];?></span></a>
        &bull;&nbsp;<a href="eventos.php<?=$langURL;?>"><span <?php echo ($filename=="eventos.php") ? "class=\"fontRed hoverLargeRed\"" : "class=\"hoverLargeGrey\"";?>><?=$arrayIdiomas['events'];?></span></a>
        &bull;&nbsp;<a href="escuela.php<?=$langURL;?>"><span <?php echo ($filename=="escuela.php") ? "class=\"fontRed hoverLargeRed\"" : "class=\"hoverLargeGrey\"";?>><?=$arrayIdiomas['school'];?></span></a>
        <?php if ($sesion && $_SESSION['idPerfil'] == 3){?>
          &bull;&nbsp;<a href="localBackend.php<?=$langURL;?>"><span <?php echo ($filename=="localBackend.php") ? "class=\"fontRed hoverLargeRed\"" : "class=\"hoverLargeGrey\"";?>><?=$arrayIdiomas['profile'];?></span></a>
        <?php }elseif ($sesion && $_SESSION['idPerfil'] == 2){?>
        &bull;&nbsp;<a href="artistaBackend.php<?=$langURL;?>"><span <?php echo ($filename=="artistaBackend.php") ? "class=\"fontRed hoverLargeRed\"" : "class=\"hoverLargeGrey\"";?>><?=$arrayIdiomas['profile'];?></span></a>
        <?php }?>
      </div>
    </div>
    <div id="logo"><a href="index.php"><img src="images/logoHeader.png"/></a></div>
  </div>
