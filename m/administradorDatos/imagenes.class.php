<?php

if(!defined("RUTA_ABSOLUTA")){
	define("RUTA_ABSOLUTA",dirname(__FILE__));
}


class imagenes {
	public static function obtenerImagenPost($idPost) {
		$imagenUrl = "../images/posts/0.jpeg";
		if (file_exists("../images/posts/".$idPost."_c.jpg")) {
			$imagenUrl = "../images/posts/".$idPost."_c.jpg";
		}
		if (file_exists("../images/posts/".$idPost."_c.jpeg")) {
			$imagenUrl = "../images/posts/".$idPost."_c.jpeg";
		}
		return $imagenUrl;
	}

	public static function obtenerImagenUsuario($idUser,$esLocal=0) {
	  $imagenUrl = ($esLocal) ? "./images/perfil_locales_sinfoto.png" : "./images/profiles/0_crop.jpeg";	  
	  if (file_exists(RUTA_ABSOLUTA."/images/profiles/".$idUser."_crop.jpg")) {
	    $imagenUrl = "../images/profiles/".$idUser."_crop.jpg";
	  }
	  if (file_exists(RUTA_ABSOLUTA."/images/profiles/".$idUser."_crop.jpeg")) {
	    $imagenUrl = "../images/profiles/".$idUser."_crop.jpeg";
	  }
	  return $imagenUrl;
	}
}