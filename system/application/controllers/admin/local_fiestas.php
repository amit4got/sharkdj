<?php
class Local_fiestas extends Controller {
	function __construct() 
	{
		parent::Controller();
		if ($this->redux_auth->logged_in()==1 ){
			$profile_var = $this->redux_auth->profile();
			if ($profile_var->group!="admin" && $profile_var->group!="super" ){
				$this->redux_auth->logout();
				redirect('admin');
			}
		}else{
			$this->redux_auth->logout();
			redirect('admin');
		}
		
		if ( !function_exists('version_compare') || version_compare( phpversion(), '5', '<' ) ){
			$this->load->library('php4/ckeditor');
			$this->load->library('php4/ckfinder');
		}else{
			$this->load->library('php5/ckeditor');
			$this->load->library('php5/ckfinder');
		}
		
		$this->load->helper('ckeditor');
		$this->load->helper('file'); 
		$this->load->plugin('js_calendar');
		$this->load->model('web_fiestas_model');
	}
	
	function index()
	{
		redirect('admin/fiestas/');
	}
	
	function listado()
	{
		$this->data['profile']= $this->redux_auth->profile();
		
		$this->db->where('id', $this->uri->segment(4) );
		$query=$this->db->get('Locales');
		foreach ($query->result() as $row)
		{
			$this->data['local'] 	= $row->local;
		}
		$this->db->where( 'id_local' , $this->uri->segment(4) );
		$this->db->order_by('fecha','DESC');
		$this->data['query']=$this->db->get('Locales_fiestas');
		$this->data['head'] = $this->load->view('admin/head_view', $this->data, true);
		$this->data['navigation'] = $this->load->view('admin/main_menu_view', null, true);
		$this->data['content_menu'] = $this->load->view('admin/locales_detalle_menu_view', $this->data, true);
		$this->data['content'] =$this->load->view('admin/locales_fiestas_listado_view', $this->data, true); 
		$this->data['foot'] = '';
		$this->load->view('templates/admin_view', $this->data);
		
	}	
	
	function nuevo()
	{
		$this->data['body_carga']='';
	
		$this->form_validation->set_rules('es_titulo', '"Título en español"', 'required');
	    $this->form_validation->set_rules('en_titulo', '"Título en ingles"', 'required');
	    $this->form_validation->set_rules('du_titulo', '"Título en holandes"', 'required');
		$this->form_validation->set_rules('f_date', '"Fecha de la fiesta"', 'required');
		$this->form_validation->set_rules('hora', '"Hora"', '');
		$this->form_validation->set_rules('minutos', '"Minutos"', '');
		$this->form_validation->set_rules('imagen', '"Imagen"', '');
		$this->form_validation->set_rules('es_informacion', '"Texto en español"', '');
		$this->form_validation->set_rules('en_informacion', '"Texto en ingles"', '');
		$this->form_validation->set_rules('du_informacion', '"Texto en holades"', '');
		$this->form_validation->set_rules('estilos_id', '"estilos_id"', '');
		
	    $this->form_validation->set_error_delimiters('<p class="error">', '</p>');
	    if ($this->form_validation->run() == false)
	    { 
			
			$this->db->where('id', $this->uri->segment(4) );
			$query=$this->db->get('Locales');
			foreach ($query->result() as $row)
			{
				$this->data['local'] 	= $row->local;
			}
			$this->data['profile']= $this->redux_auth->profile();
			$this->data['head'] = $this->load->view('admin/head_view', $this->data, true);
			$this->data['navigation'] = $this->load->view('admin/main_menu_view', null, true);
			$this->data['content_menu'] = $this->load->view('admin/locales_detalle_menu_view', null, true);
			$this->data['accion_form']='admin/local_fiestas/nuevo/'.$this->uri->segment(4).'/'.$this->uri->segment(5);
			
			$this->data['registro'] = array(
							"es_titulo" => '' , 
							"en_titulo" => '' ,
			"du_titulo" => '' ,
							"f_date" => '' , 
							"hora" => '' , 
							"minutos" => '' ,
							"posx"   => '-16.6178' ,
							"posy"   => '28.2633' ,
							"es_informacion" => '' , 
							"en_informacion" => '',
			"du_informacion" => ''
							);
			
			$ckeditor =  CK_Editor();
			$this->data['ckeditor']= $ckeditor; 
			$this->data['query']=$this->db->get('Pais');
			
			$this->db->where('id_pais', '1' );
			$this->data['query_ciudad']=$this->db->get('Ciudad');
			
			$this->data['slct_estilo'] 	= $estilo 	= array();
			$this->data['qry_estilo']  = $this->web_fiestas_model->cargar_estilos();
			
	        $this->data['content'] = $this->load->view('admin/locales_fiestas_form_view', $this->data, true);
	       	$this->data['foot'] = "";
			
			$this->load->view('templates/admin_view', $this->data);
	    }
	    else
	    {
	    	
			$config_upload['upload_path']    = './uploads/locales/'; 
			$config_upload['allowed_types']  = 'gif|jpg|png'; 
			$config_upload['max_size']       = '1000'; 
			$config_upload['max_width']      = '2048'; 
			$config_upload['max_height']     = '1536'; 
			$this->load->library('upload', $config_upload);
			if ( $this->upload->do_upload('imagen')) 
			{ 
				$imagen=$this->upload->data();
				$this->data['registro']['imagen'] = $imagen['file_name'];
				$config_resize['image_library']   = 'GD2'; 
				$config_resize['source_image']    = './uploads/locales/'.$imagen['file_name']; 
				$config_resize['thumb_marker']       = 'p';
				//$config_resize['new_image']       = './uploads/fiestas/'.$imagen['raw_name'].'p'.$imagen['file_ext']; 
				$config_resize['create_thumb']    = TRUE; 
				$config_resize['maintain_ratio']  = TRUE; 
				$config_resize['width']           = 175; 
				$config_resize['height']          = 175; 
				$this->load->library('image_lib', $config_resize); 
				$this->image_lib->resize(); 

			} 
			
			$this->data['registro']['es_titulo'] 		= $this->input->post('es_titulo');
			$this->data['registro']['en_titulo'] 		= $this->input->post('en_titulo');
			$this->data['registro']['du_titulo'] 		= $this->input->post('du_titulo');
			$this->data['registro']['estilos'] 			= $this->input->post('estilos_id');
			$this->data['registro']['es_informacion'] 	= $this->input->post('es_informacion');
			$this->data['registro']['en_informacion'] 	= $this->input->post('en_informacion');
			$this->data['registro']['du_informacion'] 	= $this->input->post('du_informacion');
			$this->data['registro']['id_local'] 		= $this->uri->segment(4);		
			$this->data['registro']['fecha'] 			= $this->input->post('f_date').' '.$this->input->post('hora').':'.($this->input->post('minutos')*5).':00';
			
			$this->db->set($this->data['registro']); 
			
			if ($this->db->insert('Locales_fiestas')){
				$this->session->set_flashdata('message', '<p class="notice">La información ha sido insertada.</p>');
	            redirect('admin/local_fiestas/nuevo/'.$this->uri->segment(4).'/'.$this->uri->segment(5));
			} else
	        {
	            $this->session->set_flashdata('message', '<p class="error">No se ha podido guardar.</p>');
	            redirect('admin/local_fiestas/nuevo/'.$this->uri->segment(4).'/'.$this->uri->segment(5));
	        }
	    }	
		
		
	}
	function modificar()
	{
		
		
		
		$this->data['body_carga']='';
		$this->form_validation->set_rules('es_titulo', '"Título en español"', 'required');
	    $this->form_validation->set_rules('en_titulo', '"Título en ingles"', 'required');
	    $this->form_validation->set_rules('du_titulo', '"Título en holandes"', 'required');
		$this->form_validation->set_rules('f_date', '"Fecha de la fiesta"', 'required');
		$this->form_validation->set_rules('hora', '"Hora"', '');
		$this->form_validation->set_rules('minutos', '"Minutos"', '');
		$this->form_validation->set_rules('es_informacion', '"Texto en español"', '');
		$this->form_validation->set_rules('en_informacion', '"Texto en ingles"', '');
		$this->form_validation->set_rules('du_informacion', '"Texto en holandes"', '');
		$this->form_validation->set_rules('estilos_id', '"estilos_id"', '');
	    $this->form_validation->set_error_delimiters('<p class="error">', '</p>');
		
	    if ($this->form_validation->run() == false)
	    { 
			
			$this->db->where('id', $this->uri->segment(4) );
			$query=$this->db->get('Locales');
			foreach ($query->result() as $row)
			{
				$this->data['local'] 	= $row->local;
			}
			
			$this->data['profile']= $this->redux_auth->profile();
			$this->data['head'] = $this->load->view('admin/head_view', $this->data, true);
			$this->data['navigation'] = $this->load->view('admin/main_menu_view', null, true);
			$this->data['content_menu'] = $this->load->view('admin/locales_detalle_menu_view', $this->data, true);
	        
			$this->data['accion_form']='admin/local_fiestas/modificar/'.$this->uri->segment(4).'/'.$this->uri->segment(5) ;
			
			$this->db->where('id', $this->uri->segment(5) );
			$query=$this->db->get('Locales_fiestas');
			$this->data['errores']='';
			$ckeditor =  CK_Editor();
			$this->data['ckeditor']= $ckeditor; 
			foreach ($query->result() as $row)
			{
				list($f_date,$f_hora)=explode(" ",$row->fecha);
				list($hora,$minutos,$segundos)=explode(":",$f_hora);
				if ($row->imagen!=""){
					$ar_imagen=explode(".",$row->imagen);
					$imagen=base_url().'uploads/fiestas/'.$ar_imagen[0].'p.'.$ar_imagen[1];
				}else{
					$imagen=base_url().'uploads/fiestas/espacio.gif';
				}
				$this->data['registro'] = array(
							
											"es_titulo" 		=> $row->es_titulo , 
											"en_titulo" 		=> $row->en_titulo ,
				"du_titulo" 		=> $row->en_titulo ,
											"f_date" 			=> $f_date , 
											"hora" 				=> $hora ,
											"minutos" 			=> $minutos/5 ,
											"imagen"			=> $imagen ,
											"es_informacion" 	=> htmlspecialchars_decode($row->es_informacion) , 
											"en_informacion" 	=> htmlspecialchars_decode($row->en_informacion) ,
				"du_informacion" 	=> htmlspecialchars_decode($row->du_informacion)
											
											);
				$this->data['slct_estilo'] 	= explode(",",$row->estilos);
				$this->data['qry_estilo']  = $this->web_fiestas_model->cargar_estilos();
			}
			if (!isset($this->data['registro'])){
				 redirect('admin/fiestas/listado');
			}
			$this->data['content'] = $this->load->view('admin/locales_fiestas_form_view', $this->data, true);
	       	$this->data['foot'] = "";
			$this->load->view('templates/admin_view', $this->data);
	    }
	    else
	    {
	        
			
			
			$update = array(
							"es_titulo" 		=> $this->input->post('es_titulo') , 
							"en_titulo" 		=> $this->input->post('en_titulo') ,
			"du_titulo" 		=> $this->input->post('du_titulo') ,
							"fecha" 			=> $this->input->post('f_date').' '.$this->input->post('hora').':'.($this->input->post('minutos')*5).':00' , 
							"estilos" 			=> $this->input->post('estilos_id'),
							"es_informacion" 	=> $this->input->post('es_informacion'),
							"en_informacion" 	=> $this->input->post('en_informacion'),
			"du_informacion" 	=> $this->input->post('du_informacion')
							
							);
			
			$config_upload['upload_path']    = './uploads/locales/'; 
			$config_upload['allowed_types']  = 'gif|jpg|png'; 
			$config_upload['max_size']       = '1000'; 
			$config_upload['max_width']      = '2048'; 
			$config_upload['max_height']     = '1536'; 
			
			$this->load->library('upload', $config_upload);
			if ( $this->upload->do_upload('imagen')) 
			{ 
				$imagen=$this->upload->data();
				$update['imagen'] = $imagen['file_name'];
				$config_resize['image_library']   = 'GD2'; 
				$config_resize['source_image']    = './uploads/locales/'.$imagen['file_name']; 
				$config_resize['thumb_marker']       = 'p';
				//$config_resize['new_image']       = './uploads/fiestas/'.$imagen['raw_name'].'p'.$imagen['file_ext']; 
				$config_resize['create_thumb']    = TRUE; 
				$config_resize['maintain_ratio']  = TRUE; 
				$config_resize['width']           = 175; 
				$config_resize['height']          = 175; 
				$this->load->library('image_lib', $config_resize); 
				$this->image_lib->resize(); 
				
				$this->db->where('id', $this->uri->segment(5) );
				$query=$this->db->get('Locales_fiestas');
				foreach ($query->result() as $row)
				{
					$ar_imagen=explode(".",$row->imagen);
					$imagen=base_url().'uploads/locales/'.$ar_imagen[0].'p.'.$ar_imagen[1];
					unlink($imagen);
					$imagen=base_url().'uploads/locales/'.$ar_imagen[0].'.'.$ar_imagen[1];
					unlink($imagen);
				}
			} 
			
			
			$this->db->where('id', $this->uri->segment(5) );
			
			if ($this->db->update("Locales_fiestas",$update)){
				$this->session->set_flashdata('message', '<p class="notice">La información ha sido modifica.</p>'); 
			} else
	        {
	            $this->session->set_flashdata('message', '<p class="error">No se ha podido modificar.</p>');
	        }
			redirect('admin/local_fiestas/modificar/'.$this->uri->segment(4).'/'.$this->uri->segment(5));
	    }	
	}
	function borrar()
	{
		$this->db->where('id', $this->uri->segment(4) );
		$query=$this->db->get('Locales');
		foreach ($query->result() as $row)
		{
			$this->data['local'] 	= $row->local;
		}
		$this->data['profile']= $this->redux_auth->profile();
		$this->data['head'] = $this->load->view('admin/head_view', $this->data, true);
		$this->data['navigation'] = $this->load->view('admin/main_menu_view', null, true);
		$this->data['content_menu'] = $this->load->view('admin/locales_detalle_menu_view', $this->data, true);
		
		
		$this->db->where('id', $this->uri->segment(5) );
		
		$query=$this->db->get('Locales_fiestas');
		foreach ($query->result() as $row)
		{
			$this->data['es_titulo'] =$row->es_titulo;
		}
		
		$this->data['content'] =$this->load->view('admin/locales_fiestas_borrar_view', $this->data, true); ;
		$this->data['foot'] = "";
		$this->load->view('templates/admin_view', $this->data);
	}
	function borrar_confirm()
	{
		$this->data['profile']= $this->redux_auth->profile();
		$this->db->where('id', $this->uri->segment(5) );
		$query=$this->db->get('Locales_fiestas');
		if ($query->num_rows() > 0){
				foreach ($query->result() as $row)
				{
					if ($row->imagen!=""){
						$ar_imagen=explode(".",$row->imagen);
						
						$imagen=PUBPATH.'uploads/fiestas/'.$ar_imagen[0].'p.'.$ar_imagen[1];
						if(file_exists($imagen)) { 	unlink($imagen);	}
						
						$imagen= PUBPATH.'uploads/fiestas/'.$ar_imagen[0].'.'.$ar_imagen[1];
						if(file_exists($imagen)) {	unlink($imagen);    }		
					}
				}
			
				$this->db->where('id', $this->uri->segment(5) );
				$this->db->delete('Locales_fiestas'); 
		}
		redirect( 'admin/local_fiestas/listado/'.$this->uri->segment(4) );
	}
}


