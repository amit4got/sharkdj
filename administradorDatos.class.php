<?php

if(!defined("RUTA_ABSOLUTA")){
  define("RUTA_ABSOLUTA",dirname(__FILE__));
}

require_once RUTA_ABSOLUTA.'/config/constants.php';
require_once RUTA_ABSOLUTA.'/database/insert.class.php';
require_once RUTA_ABSOLUTA.'/database/database.class.php';


class administradorDatos {

  private $insert;
  private $bd;
  
  public function __construct() {
    $this->insert = new insert();
    $this->bd = new database();
  }
  
  public function insertaUsuario($username, $password, $email, $idPerfil=2, $activo=1) {
    $idUser="";
    // idPerfil mayor que 1 porque no vamos a permitir añadir usuarios admin. Se hace manualmente ;)
    if ($username!="" && $password!="" && $email!="" && $idPerfil>1) {
      $idUser = $this->insert->inserta("INSERT INTO users (userName, password, email, idPerfil, activo) VALUES
      														('%s', MD5('%s'), '%s', %i, %i)", array($username, $password, $email, $idPerfil, $activo));
    }
    return $idUser;
  }
  
  public function  obtenerUsuario($idUser){
    if ($idUser>0) {
      $resultado = $this->bd->query("SELECT idUser, userName, password, email, idPerfil, activo FROM users WHERE idUser='%s'", array($idUser));
    }
    return isset($resultado[0]) ? $resultado[0] : 0;
  }
    
  public function existeUsuario($idUser) {
    if ($idUser!="" && $idUser>0) {
      $resultado = $this->bd->query("SELECT idUser FROM users WHERE idUser=%i", array($idUser));
    }
    return isset($resultado[0]['idUser']) ? $resultado[0]['idUser'] : 0;
  }
  
  
  public function existeUserName($username) {  	
  	if ($username!="") {
  		$resultado = $this->bd->query("SELECT idUser FROM users WHERE userName LIKE '%s'", array($username)); 
  	}
  	return isset($resultado[0]) ? $resultado[0]['idUser'] : 0;
  }

  public function existeEmail($email) {
    if ($email!="") {
    		$resultado = $this->bd->query("SELECT idUser FROM users WHERE email LIKE '%s'", array($email));
    }
    return isset($resultado[0]) ? $resultado[0]['idUser'] : 0;
  }
  
  public function cambiaPassword($idUser,$password){
    if ($idUser>0) {
      $this->insert->inserta("UPDATE users SET password=MD5('%s') WHERE IDUser=%i", array($password,$idUser));
    }
  }

  public function cambiaEstadoActivo($idUser,$activo=0){
  	if ($idUser>0) {
  		$this->insert->inserta("UPDATE users SET activo=%i WHERE IDUser=%i", array($activo,$idUser));
  	}
  }
  
  public function cambiaVisibleTeam($idUser,$visible=0,$team=0){
    if ($idUser>0) {
    		$this->insert->inserta("UPDATE artistas SET visible=%i,team=%i WHERE IDUser=%i", array($visible,$team,$idUser));
    }
  }
  
  
  
  public function existeCategoria($idCategoria) {
    if ($idCategoria>0) {
      $resultado = $this->bd->query("SELECT idCategoria FROM categorias WHERE idCategoria=%i", array($idCategoria));
    }
    return (isset($resultado[0]['idCategoria'])) ? $resultado[0]['idCategoria'] : 0;
  }
  
  public function existeRedSocial($nombre) {
  	if ($nombre!="") {
  		$resultado = $this->bd->query("SELECT idRed FROM redes_sociales WHERE nombre LIKE '%s'", array($nombre));
  	}
  	return (isset($resultado['idRed'])) ? $resultado['idRed'] : 0;
  }
  
  public function insertaRedSocial($nombre) {
  	$idRed="";
  	if ($nombre!="" && !$this->existeRedSocial($nombre)) {
  		$idRed = $this->insert->inserta("INSERT INTO redes_sociales (nombre) VALUES ('%s')", array($nombre));
  	}
  	return $idRed;
  }
  
  public function obtenerArtistas($visible="",$team="") {
  	return $this->bd->query("SELECT idUser FROM artistas WHERE 1=1 [AND visible=%I] [AND team=%I]", array($visible,$team));
  }
  
  public function obtenerArtistasInfo($numArtistas=999999, $offset=0, $visible="",$team="") {
  	return $this->bd->query("SELECT u.idUser, u.userName, u.password, u.email, u.idPerfil, u.activo, a.nombre, a.apellido1,
  		                           a.apellido2, a.direccion, a.cp, a.ciudad, a.pais, a.telefono, a.movil, a.lat, a.lng, a.bio, 
  		                           a.web, a.clubResidencia, a.visible, a.team, a.estilo 
  		                           FROM users u, artistas a WHERE u.idUser=a.idUser [AND a.visible=%I] [AND a.team=%I] ORDER BY a.team DESC", array($visible,$team), $offset, $numArtistas);
  }
  
  public function obtenerIdUsuario($email, $password) {  	
  	if ($email!="" && $password!="") {
  		$resultado = $this->bd->query("SELECT idUser, idPerfil FROM users WHERE email='%s' AND password=MD5('%s')", array($email, $password));  		
  	}
  	return (isset($resultado[0])) ? $resultado[0] : 0;
  }
  
  public function introduceNoticia($datos) {
  	$idUser = isset($datos['idUser']) ? $datos['idUser'] : 0;
  	$titulo = isset($datos['titulo']) ? $datos['titulo'] : "";
  	$texto = isset($datos['texto']) ? $datos['texto'] : "";
  	$lang_en = isset($datos['lang_en']) ? $datos['lang_en'] : "";
  	$fecha = isset($datos['fecha']) ? $datos['fecha'] : "";
  	$idCategoria = isset($datos['idCategoria']) ? $datos['idCategoria'] : 0;
  	$visible = isset($datos['visible']) ? $datos['visible'] : 0;
  	
  	if ($this->existeUsuario($idUser) && $titulo!="" && $texto!="" && $fecha!="" && $this->existeCategoria($idCategoria)) {  	  
  	  $idRed = $this->insert->inserta("INSERT INTO post (idUser,titulo,texto,lang_en,fecha,idCategoria,visible) VALUES (%i,'%s','%s','%s','%s',%i,%i)", array($idUser,$titulo,$texto,$lang_en,$fecha,$idCategoria,$visible));
  	  
  	}
  }
  
  public function actualizaNoticia($datos) {
    $idPost = isset($datos['idPost']) ? $datos['idPost'] : 0;
    $idUser = isset($datos['idUser']) ? $datos['idUser'] : 0;
    $titulo = isset($datos['titulo']) ? $datos['titulo'] : "";
    $texto = isset($datos['texto']) ? $datos['texto'] : "";
    $lang_en = isset($datos['lang_en']) ? $datos['lang_en'] : "";
    $fecha = isset($datos['fecha']) ? $datos['fecha'] : "";
    $idCategoria = isset($datos['idCategoria']) ? $datos['idCategoria'] : 0;
    $visible = isset($datos['visible']) ? $datos['visible'] : 0;
     
    if ($this->existeUsuario($idUser) && $titulo!="" && $texto!="" && $fecha!="" && $this->existeCategoria($idCategoria)) {      
      $this->insert->inserta("UPDATE post SET idUser=%i, titulo='%s', texto='%s', lang_en='%s', fecha='%s', idCategoria=%i, visible=%i WHERE idPost=%i", array($idUser,$titulo,$texto,$lang_en,$fecha,$idCategoria,$visible,$idPost));      	
    }
  }
  
  public function obtenerNoticias($numNoticias=999999, $offset=0, $idUser="", $idCategoria="",$fechaMaxima="",$visible="") {
  	return $this->bd->query("SELECT p.idPost, p.idUser, u.userName as nombreUsuario, p.titulo, p.texto, p.lang_en, DATE_FORMAT(p.fecha,'%s') as fecha, c.nombre as nombreCategoria, p.visible 
  	                        FROM post p, categorias c, users u 
  	                        WHERE p.idCategoria=c.idCategoria AND p.idUser=u.idUser [AND p.idUser=%I] [AND c.idCategoria=%I] [(p.fecha <= NOW())=%I] [AND p.visible=%I]
  	                        GROUP BY p.idPost ORDER BY p.fecha DESC "
  							, array('%d/%c/%Y',$idUser,$idCategoria, $fechaMaxima, $visible), $offset, $numNoticias);
  }
  
  public function obtenerNoticia($idPost){
    $resultado = $this->bd->query("SELECT p.idPost, p.idUser, u.userName as nombreUsuario, p.titulo, p.texto, p.lang_en, DATE_FORMAT(p.fecha,'%s') as fecha, c.nombre as nombreCategoria, p.idCategoria, p.visible
      	                        FROM post p, categorias c, users u 
      	                        WHERE p.idPost=%i AND p.idUser=u.idUser"
                                , array('%d/%c/%Y',$idPost)); 
    
    return (isset($resultado[0])) ? $resultado[0] : array();
  }
  
  public function obtenerNoticiaAnteriorSiguiente($idPost){
      $anterior = $this->obtenerIdAnterior($idPost);
      $siguiente = $this->obtenerIdSiguiente($idPost);
      var_dump ($anterior);
      var_dump($siguiente);
      die;
      $resultado = $this->db->query("SELECT ");
  }
  
  public function obtenerIdAnterior($id){
      $resultado = $this->db->query("SELECT idPost
                                    FROM post
                                    WHERE idPost < %i
                                    ORDER BY idPost DESC
                                    LIMIT 1"
                                    , array($id));
      return (isset($resultado[0])) ? $resultado[0] : array();
  }
  
  public function obtenerIdSiguiente($id){
      $resultado = $this->db->query("SELECT idPost
                                    FROM post
                                    WHERE idPost > %i
                                    ORDER BY idPost ASC
                                    LIMIT 1"
                                    , array($id));
      return (isset($resultado[0])) ? $resultado[0] : array();
  }
  
  public function totalNoticias(){
    return $this->bd->query("SELECT count(*) as total FROM post");
  }
  
  public function proximaNoticia(){
  	$result = $this->bd->query("SHOW TABLE STATUS LIKE 'post'",array(),0,0);  	
  	$nextId = $result[0]['Auto_increment'];  	
  	return $nextId;
  }
  
  public function proximoEvento(){
  	$result = $this->bd->query("SHOW TABLE STATUS LIKE 'eventos'",array(),0,0);
  	$nextId = $result[0]['Auto_increment'];
  	return $nextId;
  }  
  
  public function buscarNoticias($texto, $numNoticias=999999, $offset=0, $visible="") {
  	return $this->bd->query("SELECT p.idPost, p.idUser, u.userName as nombreUsuario, p.titulo, p.texto, p.lang_en, DATE_FORMAT(p.fecha,'%s') as fecha, c.nombre as nombreCategoria
  	  	                        FROM post p, categorias c, users u 
  	  	                        WHERE p.idCategoria=c.idCategoria AND p.idUser=u.idUser AND (p.titulo LIKE '%%s%' OR p.texto LIKE '%%s%') [AND p.visible=%I]  
  	  	                        GROUP BY p.idPost ORDER BY p.fecha DESC "
  	, array('%d/%c/%Y',$texto, $texto, $visible), $offset, $numNoticias);
  }
  
  public function obtenerLocales($visible="") {
    return $this->bd->query("SELECT idUser FROM locales WHERE 1=1 [AND visible=%I]", array($visible));
  }
  
  public function obtenerLocalesInfo($numLocales=999999, $offset=0,$visible="") {
    return $this->bd->query("SELECT u.idUser, u.userName, u.password, u.email, u.idPerfil, u.activo, l.nombre,
    		                           l.direccion, l.ciudad, l.pais, l.lat, l.lng, l.visible, l.descripcion    		                           
    		                           FROM users u, locales l WHERE u.idUser=l.idUser [AND l.visible=%I]", array($visible), $offset, $numLocales);
  }
    
  public function buscarArtistas($nombre="",$lugar="",$estilo="",$numArtistas=999999, $offset=0) {
    if (stristr ($lugar, 'españa')) $lugar = 'spain';
    return $this->bd->query("SELECT DISTINCT u.idUser, u.userName, u.password, u.email, u.idPerfil, u.activo, a.nombre, a.apellido1,
      		                           a.apellido2, a.direccion, a.cp, a.ciudad, a.pais, a.telefono, a.movil, a.lat, a.lng, a.bio, 
      		                           a.web, a.clubResidencia, a.visible, a.team 
      		                           FROM users u, artistas a WHERE u.idUser=a.idUser AND a.visible=1 AND u.activo=1
    					  [AND (a.nombre LIKE '%%S%' OR a.apellido1 LIKE '%%S%')] [AND (a.direccion LIKE '%%S%' OR a.ciudad LIKE '%%S%' OR a.pais LIKE '%%S%')] [AND a.estilo LIKE '%%S%']", array($nombre,$nombre,$lugar,$lugar,$lugar,$estilo), $offset, $numArtistas);
 }
  
  public function buscarLocales($nombre="",$lugar="",$estilo="",$numLocales=999999, $offset=0) {
    if (strcmp (strtolower($lugar), 'españa') == 1) $lugar = 'Spain';
    return $this->bd->query("SELECT DISTINCT u.idUser, u.userName, u.password, u.email, u.idPerfil, u.activo, l.nombre,
    		                           l.direccion, l.ciudad, l.pais, l.lat, l.lng, l.visible, l.descripcion
    		                           FROM users u, locales l WHERE u.idUser=l.idUser AND l.visible=1 AND u.activo=1
      					[AND l.nombre LIKE '%%S%'] [AND (l.direccion LIKE '%%S%' OR l.pais LIKE '%%S%' OR l.ciudad LIKE '%%S%')] [AND l.estilo LIKE '%%s%']", array($nombre,$lugar,$lugar,$lugar,$estilo), $offset, $numLocales);
}
  
  public function buscarEventos($nombre="",$lugar="",$estilo="",$numLocales=999999, $offset=0) {
  	return $this->bd->query("SELECT e.idEvento, e.nombre, e.direccion, e.fechaInicio, e.visible, e.descripcion, e.precio 
  			FROM eventos e
  			WHERE e.visible=1
  			[AND e.nombre LIKE '%%S%'] [AND e.direccion LIKE '%%S%'] [AND e.estilo LIKE '%%S%']
  			ORDER BY e.fechaInicio DESC", array($nombre,$lugar,$estilo), $offset, $numLocales);
  }
  
  public function obtenerEventos($numEventos=999999,$offset=0,$now="",$visible="") {
  	return $this->bd->query("SELECT e.idEvento, e.nombre, e.direccion, e.fechaInicio, e.visible, e.descripcion, e.precio, e.transporte    		                           
  	    		             FROM eventos e WHERE 1=1 [AND e.fechaInicio>=NOW()=%I] [AND e.visible=%I]
  							 ORDER BY e.fechaInicio DESC", array($now,$visible), $offset, $numEventos);
  }
  
  public function obtenerEvento($idEvento) {
  	$resultado = $this->bd->query("SELECT e.idEvento, e.idUser, e.nombre, e.direccion,e.transporte, e.fechaInicio, e.visible, e.descripcion, e.precio, e.transporte  
    	    		             FROM eventos e WHERE idEvento=%i", array($idEvento)); 
  	return (isset($resultado[0])) ? $resultado[0] : array();
  }
  
  public function introduceEvento($datos) {
  	$idUser = isset($datos['idUser']) ? $datos['idUser'] : 0;
  	$nombre = isset($datos['nombre']) ? $datos['nombre'] : "";
  	$descripcion = isset($datos['descripcion']) ? $datos['descripcion'] : "";
  	$direccion = isset($datos['direccion']) ? $datos['direccion'] : "";
    $transporte = isset($datos['transporte']) ? $datos['transporte'] : "";
  	$fechaInicio = isset($datos['fechaInicio']) ? $datos['fechaInicio'] : "";
	$precio = isset($datos['precio']) ? $datos['precio'] : 0;
  	$visible = isset($datos['visible']) ? $datos['visible'] : 0;
  	 
  	if ($this->existeUsuario($idUser) && $nombre!="" && $fechaInicio!="") {
  		$idRed = $this->insert->inserta("INSERT INTO eventos (idUser,nombre,descripcion,direccion,transporte,fechaInicio,precio,visible) VALUES (%i,'%s','%s','%s','%s','%s','%s',%i)", array($idUser,$nombre,$descripcion,$direccion,$transporte,$fechaInicio,$precio,$visible));
  			
  	}
  }

 public function introducePublicidad($datos) {
    
    $imagen = isset($datos['imagen']) ? $datos['imagen'] : "";
    $url = isset($datos['url']) ? $datos['url'] : "";
     
    if ($imagen != "") {
      $idRed = $this->insert->inserta("INSERT INTO publicidad (imagen,url) VALUES ('%s','%s')", array($imagen,$url));
        
    }
  }

  public function obtenerPublicidad() {
    return $this->bd->query("SELECT e.id, e.imagen, e.url                                   
                         FROM publicidad e WHERE 1=1");
  }

  public function obtenerPublicidadAleatoria() {
    return $this->bd->query("SELECT *                                   
                         FROM publicidad ORDER BY RAND() LIMIT 1");
  }

  public function actualizaPublicidad($datos) {
    $id = isset($datos['id']) ? $datos['id'] : 0;
    $url = isset($datos['url']) ? $datos['url'] : "";
     
    if ($id>0) {
      $this->insert->inserta("UPDATE publicidad SET id=%i, url='%s' WHERE id=%i", array($id,$url));
    }
  }

  
  public function actualizaEvento($datos) {
  	$idEvento = isset($datos['idEvento']) ? $datos['idEvento'] : 0;
  	$idUser = isset($datos['idUser']) ? $datos['idUser'] : 0;
  	$nombre = isset($datos['nombre']) ? $datos['nombre'] : "";
  	$descripcion = isset($datos['descripcion']) ? $datos['descripcion'] : "";
  	$direccion = isset($datos['direccion']) ? $datos['direccion'] : "";
    $transporte = isset($datos['transporte']) ? $datos['transporte'] : "";
  	$fechaInicio = isset($datos['fechaInicio']) ? $datos['fechaInicio'] : "";  	
  	$precio = isset($datos['precio']) ? $datos['precio'] : 0;
	$visible = isset($datos['visible']) ? $datos['visible'] : 0;
  	 
  	if ($idEvento>0 && $this->existeUsuario($idUser) && $nombre!="" && $descripcion!="" && $fechaInicio!="") {
  		$this->insert->inserta("UPDATE eventos SET idUser=%i, nombre='%s', descripcion='%s', direccion='%s',transporte='%s', fechaInicio='%s', precio='%s', visible=%i WHERE idEvento=%i", array($idUser,$nombre,$descripcion,$direccion,$transporte,$fechaInicio,$precio,$visible,$idEvento));
  	}
  }
  
  #Idioma sale de un GET y se usa para formar el nombre del fichero lang_"IDIOMA".xml
  public function cargaIdioma($lang) {
    $xmlFile = 'languages/lang_' . $lang . '.xml';
    $xml = simplexml_load_file($xmlFile); 
    foreach($xml->children() as $child) {
      $arrayLang[(string)$child->getName()] = (string)$child;
    }
	return $arrayLang;
	//echo $arrayLang['login'];
    //echo $arrayLang['artist'];
    //echo $arrayLang['clubs'];
    //echo $arrayLang['events'];
  }

  public function eliminaFila($table,$columnName,$id) {
	$this->bd->runQuery("DELETE FROM " . $table . " WHERE " . $columnName . " = '" . $id . "' LIMIT 1");
	return;
  }
}