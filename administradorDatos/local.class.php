<?php

if(!defined("RUTA_ABSOLUTA")){
  define("RUTA_ABSOLUTA",dirname(__FILE__));
}

require_once RUTA_ABSOLUTA.'/config/constants.php';
require_once RUTA_ABSOLUTA.'/database/insert.class.php';
require_once RUTA_ABSOLUTA.'/database/database.class.php';
require_once RUTA_ABSOLUTA.'/administradorDatos/user.class.php';

class local extends user {
  
  public function __construct() {
	parent::__construct();
  }
  
  public function existeLocal($idUser) {  	
  	if ($idUser!="" && $idUser>0) {
  		$resultado = $this->bd->query("SELECT idUser FROM locales WHERE idUser=%i", array($idUser));  		
  	}
  	return isset($resultado[0]['idUser']) ? $resultado[0]['idUser'] : 0;
  }  
  
  public function insertaDatosLocal($datos) {
  	$idUser = isset($datos['idUser']) ? $datos['idUser'] : 0;
  	$nombre = isset($datos['nombre']) ? $datos['nombre'] : "";  	 
  	$direccion = isset($datos['direccion']) ? $datos['direccion'] : "";  	
  	$ciudad = isset($datos['ciudad']) ? $datos['ciudad'] : 0;
  	$web = isset($datos['web']) ? $datos['web'] : "";
  	$pais = isset($datos['pais']) ? $datos['pais'] : "";
  	$lat = isset($datos['lat']) ? $datos['lat'] : "";
  	$lng = isset($datos['lng']) ? $datos['lng'] : "";
  	$visible = isset($datos['visible']) ? 1 : 0;
  	$descripcion = isset($datos['descripcion']) ? $datos['descripcion'] : "";
  	  	
  	if ($idUser>0 && $nombre!="" && $pais != "" && $descripcion!="") {  		
	  	if ($this->existeLocal($idUser)) {	  			  		
	  		$this->insert->inserta("UPDATE locales SET nombre='%s',direccion='%s',ciudad=%i,web='%s', pais='%s',lat=%f,lng=%f,
	  					   visible=%i, descripcion='%s' WHERE idUser=%i", 
	  		               array($nombre,$direccion,$ciudad,$web,$pais,$lat,$lng,$visible,$descripcion,$idUser));
	  	}else{	
		  	$this->insert->inserta("INSERT INTO locales (idUser,nombre,direccion,web,pais,lat,lng,visible,descripcion) VALUES
		  	   						             (%i, '%s', '%s','%s', '%s', %f, %f, %i, '%s')", 
		  	                                     array($idUser,$nombre,$direccion,$web,$pais,$lat,$lng,$visible,$descripcion));
	  	}
  	}
  	return $idUser;
  }

  public function obtenerDatosLocal($idUser) {  	
  	if ($this->existeLocal($idUser)) {
  		$resultado = $this->bd->query("SELECT u.idUser, u.userName, u.password, u.email, u.idPerfil, u.activo, l.nombre, 
  								   l.direccion, l.ciudad,l.web, l.pais, l.lat, l.lng, l.visible, l.descripcion  		                           
  		                           FROM users u, locales l WHERE u.idUser=%i AND u.idUser=l.idUser",
  		                           array($idUser));
  	}  	
  	return isset($resultado[0]) ? $resultado[0] : array();
  } 
  
  public function cambiaVisible($idUser, $visible) {
    if ($this->existeLocal($idUser)) {
    		$this->insert->inserta("UPDATE locales SET visible=%i WHERE idUser=%i",
    		array($visible,$idUser));
    }
    return $idUser;
  }
}