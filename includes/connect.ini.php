<?php
/****************************
 *  Created 26/06/11
 *  Last update 24/07/11  
 ****************************/

require_once 'config/constants.php';

#Define the userName, password, host or server name, database name
/*define("user","sharky");
define("pass","veturis");
define("host","localhost");
define("db","shark");*/


#Opens a connection to the Server and DB with the user and pass defined previously.
function connection()
{
  if (@mysql_connect (HOST_DB, USER_DB, PASS_DB)) {
    $link = mysql_connect (HOST_DB, USER_DB, PASS_DB) or die (mysql_error());
    mysql_set_charset ('utf8',$link); 
    mysql_select_db (NAME_DB, $link) or die (mysql_error());
    return $link;
  }
} #END function connection()


function close ($link)
{
  #Closes connection to database
  mysql_close ($link);
} #END function close ($link)


#Returns the results of the query and link (host and database details) in an array
function mkArray ($query, $link)
{
  $result = mysql_query ($query, $link) or die (mysql_error());
  while ($row = mysql_fetch_array ($result, MYSQL_ASSOC))  {
    $res[] = $row;
  }
  return $res;
} #END function mkArray ($query, $link)


#Return the variable after applying mysql_real_escape_string
function escapeSQL ($escapeValue, $link)
{
  if (is_array ($escapeValue)) {
    foreach ($escapeValue as $key => $value) {
      $escapeValue[$key] = mysql_real_escape_string ($value, $link);
    }
  }
  else {
    $escapeValue = mysql_real_escape_string ($escapeValue, $link);
  }
  return $escapeValue;
} #END function escapeSQL ($escapeValue, $Link)


#inserts into the table the values of the columns indicated
function insertRow ($table, $arrayColumns, $link)
{
  $numColumns = count ($arrayColumns);
  $query = "INSERT INTO " . $table . "(";
  #Column names and column values "String escape"
  $keys  = mysql_real_escape_string (array_keys ($arrayColumns), $link);
  $values = escapeSQL (array_values ($arrayColumns), $link);
  for ($i = 0; $i < $numColumns; $i++) {
    $query .= $keys[$i];
    if ($i < $numColumns - 1) {
      $query .= ", ";
    }
  }
  $query .= ") ";
  #Column values
  $query .= " VALUES (";
  for ($i = 0; $i < $numColumns; $i++) {
    $query .= "\"" . $values[$i] . "\"";
    if ($i < $numColumns - 1) {
      $query .= ", ";
    }
  }
  $query .= ")";
  mysql_query ($query, $link) or die (mysql_error ($link));
} #END function insertRow ($table, $arrayColumns, $arrayValues);
?>