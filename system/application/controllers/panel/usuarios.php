<?php
class Usuarios extends Controller {
	function __construct() 
	{
		parent::Controller();
		if ($this->redux_auth->logged_in()==1 ){
			$profile_var = $this->redux_auth->profile();
			if ($profile_var->group!="artistas" && $profile_var->group!="admin"){
				$this->redux_auth->logout();
				redirect('web/login');
			}
		}else{
			$this->redux_auth->logout();
			redirect('web/login');
		}
	}
	function index()
	{
		redirect('panel/portada');
	}
	
	function modificar()
	{
		$this->form_validation->set_rules('username', '"Nombre de usuario"', 'required');
	    $this->form_validation->set_rules('email', '"Email"', 'required|valid_email');
		$this->form_validation->set_rules('first_name', '"Nombre"', '');
		$this->form_validation->set_rules('last_name', '"Apellidos"', '');
		$this->form_validation->set_rules('telefono', '"telefono"', '');
	    $this->form_validation->set_error_delimiters('<p class="error">', '</p>');
		
		$data['profile']= $this->redux_auth->profile();
		$data['head'] = $this->load->view('panel/head_view', $data, true);
		$data['navigation'] = $this->load->view('panel/main_menu_view', null, true);
		$data['content_menu'] = $this->load->view('panel/usuarios_menu_view', null, true);
		
		$this->db->where('id', $data['profile']->id );
		$query=$this->db->get('users');
		$data['errores']='';
		foreach ($query->result() as $row)
		{
			$data['username'] =$row->email;
		}
		$data['profile_edit'] = $this->redux_auth->profile_select($data['username']);	
		if ($data['profile']->group=="panel" ){
			 redirect('panel/usuarios');
		}
	 
	    if ($this->form_validation->run() == false)
	    {
			$data['content'] = $this->load->view('panel/usuarios_detalle_view', $data, true);
	    }
	    else
	    {
	       if ($this->input->post('username')!=$data['profile_edit']->username){
			  if ($this->redux_auth_model->username_check($this->input->post('username'))  ){
			   	 	$data['errores'].='<p class="error">El nombre de usuario ya existe</p>';
			  }
		   }
		  if ($this->input->post('email')!=$data['profile_edit']->email){
			  if ($this->redux_auth_model->email_check($this->input->post('email'))  ){
			   	 	$data['errores'].='<p class="error">El email de usuario ya existe</p>';
			  }
		   }
		   if ($data['errores']==''){
		   		$update = array("username" => $this->input->post('username') , "email" => $this->input->post('email'));
				$this->db->where("id",$this->uri->segment(4) );
				$this->db->update("users",$update);
				
				$update = array(
								"first_name" => $this->input->post('first_name') , 
								"last_name" => $this->input->post('last_name') , 
								"telefono" => $this->input->post('telefono'));
				$this->db->where("user_id",$this->uri->segment(4) );
				$this->db->update("meta",$update);
				
		   		$data['errores'].='<p class="notice">La información del usuario ha sido modificada</p>';
		   }
		   $data['content'] = $this->load->view('panel/usuarios_detalle_view', $data, true);
	    }
		$data['foot'] = "";
		$this->load->view('templates/panel_view', $data);
	}
	function modificar_pw()
	{
		$data['profile']= $this->redux_auth->profile();
		$data['head'] = $this->load->view('panel/head_view', $data, true);
		$data['navigation'] = $this->load->view('panel/main_menu_view', null, true);
		$data['content_menu'] = $this->load->view('panel/usuarios_menu_view', null, true);
		
		$this->form_validation->set_rules('old', 'Password anterior', 'required');
	    $this->form_validation->set_rules('new', 'Password nuevo', 'required|matches[new_repeat]');
	    $this->form_validation->set_rules('new_repeat', 'Repita el nuevo Password', 'required');
	    $this->form_validation->set_error_delimiters('<p class="error">', '</p>');
	    $this->db->where('id', $data['profile']->id );
		$query=$this->db->get('users');
		$data['errores']='';
		foreach ($query->result() as $row)
		{
			$data['username'] =$row->username;
		}
	    if ($this->form_validation->run() == false)
	    {
			$data['content'] = $this->load->view('panel/usuarios_modificar_pw_view', $data, true);
	    }
	    else
	    {
	        $old = $this->input->post('old');
	        $new = $this->input->post('new');
	        
	        $this->db->where('id', $this->uri->segment(4) );
			$query=$this->db->get('users');
			foreach ($query->result() as $row)
			{
				$identity =$row->email;
			}
			
	        $change = $this->redux_auth->change_password($identity, $old, $new);
		
    		if ($change)
    		{
    			$data['errores'] = '<div id="cuerpo"><p class="notice">El cambio del password se ha realizado</p></div>';
    		}
    		else
    		{
    			$data['errores'] = '<div id="cuerpo"><p class="error">El cambio del password no se ha podido realizar</p></div>';
				
    		}
			$data['content'] = $this->load->view('panel/usuarios_modificar_pw_view', $data, true);
	    }
		
		$data['foot'] = "";
		$this->load->view('templates/panel_view', $data);
		
		
		
	}
	
	
	
	///////
	///////
	public function username_check($username)
	{
	    $check = $this->redux_auth_model->username_check($username);
	    
	    if ($check)
	    {
	        $this->form_validation->set_message('username_check', 'El nombre de usuario "'.$username.'" ya existe.');
	        return false;
	    }
	    else
	    {
	        return true;
	    }
	}
	
	/**
	 * Email check
	 *
	 * @return void
	 * @author Mathew
	 **/
	public function email_check($email)
	{
	    $check = $this->redux_auth_model->email_check($email);
	    
	    if ($check)
	    {
	        $this->form_validation->set_message('email_check', 'El email "'.$email.'" ya existe.');
	        return false;
	    }
	    else
	    {
	        return true;
	    }
	}
}


