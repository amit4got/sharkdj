<?php
if(!defined("RUTA_ABSOLUTA")){
	define("RUTA_ABSOLUTA",dirname(__FILE__));
}

require_once RUTA_ABSOLUTA.'/config/constants.php';   
require_once RUTA_ABSOLUTA.'/database/SafeSQLSharkDJ.class.php';
  
  class insert
  {
    /**
     * Constructor de la clase
     * Creamos la conexi�n a mysql. Asumimos conexi�n persistente
     * @param string $path trayecto de donde leeremos la configuracion      
     */
  	private $connection;
  	private $result;
  	
    function __construct()
    {
    	$this->connection = mysql_connect(HOST_DB, USER_DB, PASS_DB) or die	('Error connecting to mysql');
    	mysql_select_db(NAME_DB);
    }

    /**
     * Preparamos y ejecutamos la sentencia select
     * Y la pasamos por SafeSQL para que la sentencia sea segura
     * @param string $sentencia sentencia a usar para la select
     * @param array $parametros parámetros en forma de array tal y como se usan en la clase SafeSQL
     * @return void
     */           
    public function inserta($sentencia,$parametros="")
    {       
      $parametrosOK=array();
      if(!$this->connection){
        trigger_error("Se ha invocado un método insert y no hay conexión a base de datos activa.",E_USER_NOTICE);
        return false;
      }
      // Hago un trim a todos los parámetros
      foreach ($parametros as $parametro){
        if(is_string($parametro)){
          $parametrosOK[]=trim($parametro);
        } else {
          $parametrosOK[]=$parametro;
        }
      }
      // PARSEAMOS LA SENTENCIA PARA QUE SEA SEGURA Y LA EJECUTAMOS 
      $safeSQL =new SafeSQLSharkDJ();
      $safeQuery = $safeSQL->query($sentencia,$parametrosOK,0 ,0);      
      $this->result=mysql_query($safeQuery,$this->connection) or die (mysql_error($this->connection));
      if (!$this->result) {
        trigger_error(mysql_error(),E_USER_ERROR);
        return false;
      }
      $id=mysql_insert_id($this->connection); //devuelve el id del ultimo auto_increment, OR 0 si no se ha generado un id 
                            //con autoincrement OR FALSE si se ha generado un error mysql
      if ($id==0) {$id=-1;}
      return $id;
    }
  }
