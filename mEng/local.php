<?php 
	include_once 'header.php';
	
	function leerDirectorio($idUser)
	{
	  $dir='../upload/php/files/'.$idUser.'/thumbnails/';
	  $directorio=opendir($dir);
	  while ($archivo = readdir($directorio)){
	    $imagenes[]=$archivo;
	  }
	  closedir($directorio);
	  return $imagenes;
	}
	
	if(!defined("RUTA_ABSOLUTA")){
	  define("RUTA_ABSOLUTA",dirname(__FILE__));
	}
	
	require_once RUTA_ABSOLUTA.'/administradorDatos/administradorDatos.class.php';
	require_once RUTA_ABSOLUTA.'/administradorDatos/local.class.php';
	require_once RUTA_ABSOLUTA.'/administradorDatos/imagenes.class.php';
	#INCLUDES
	require_once '../includes/connect.ini.php';
	require_once '../includes/functions.ini.php';
	
	$idUser = isset($_GET['id']) ? $_GET['id'] : 0;
	$adminLocal = new local();
	$local = $adminLocal->obtenerDatosLocal($idUser);
	$facebook = $adminLocal->obtenerRedesSociales($idUser,'1');
	$twitter = $adminLocal->obtenerRedesSociales($idUser,'2');
	$myspace = $adminLocal->obtenerRedesSociales($idUser,'5');
	

  $imagen = '../images/perfil_locales_sinfoto.png';
  if (file_exists('../images/profiles/'.$local['idUser'].'_crop.jpeg')){
    $imagen = 'http://sharkdj.com/images/profiles/'.$local['idUser'].'_crop.jpeg';
  }
?>
	<div data-role="page" data-theme="c" data-content-theme="c" id="local">
		
		
  <?php echo $header;?>
  <div data-role="content">
    <div class="artist">
      <div class="ui-grid-a">
        <div class="ui-block-a">
    			<div class="image">
    				<img src="<?php echo $imagen; ?>">
    			</div>
        </div>
        <div class="ui-block-b">
          <div class="artistname"><?php echo $local['nombre']; ?></div>
          <p><a href="http://maps.google.es/maps?q=<?php echo $local['direccion'];?>"><img src="./imagen/icons/103-map.png"></a></p>
          <div class="iconSocial">
            <a href="http://www.twitter.com/<?php echo $twitter[0]['url']; ?>"><img src="./imagen/icons/twitter.png" width="35"></a>
            <a href="<?php echo 'http://'.$facebook[0]['url'];?>"><img src="./imagen/icons/facebook.png" width="35"></a>
            <a href="<?php echo 'http://www.myspace.com/'.$myspace[0]['url']?>"><img src="./imagen/icons/myspace.png" width="35"></a>
          </div>
        </div> 
      </div>
		</div>
    <div data-role="collapsible">
      <h3>Bio</h3>
      <p><?php echo $local['descripcion']; ?></p>
    </div>
    <div data-role="collapsible">
      <h3>Fotos</h3>
      <div align="center">
      <?php $imagenes = leerDirectorio($local['idUser']);
          foreach ($imagenes as $imagen){
            if (preg_match('/(gif|jpe?g|png)$/i', $imagen)){?>
              <img src="../upload/php/files/<?php echo $local['idUser'];?>/<?php echo $imagen;?>">
<?php       }
          }?>
    </div>
  </div>
  </div>
		
		
		
		
    <?php echo $footer?>
    </div>