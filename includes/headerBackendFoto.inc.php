
<?php
/****************************
*  Created 02/11/11
*  Last update 09/11/11
****************************/
ini_set("display_errors", "E_STRICT");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>


<meta name="country" content="es" />
<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>SHARK DJ</title>
        <link rel="stylesheet" href="css/enhanced.css" type="text/css" />
        <link type="text/css" href="css/custom-theme/jquery-ui-1.8.16.custom.css" rel="stylesheet" />	
        <link href="shark.css" rel="stylesheet" type="text/css" media="screen" />
		<link rel="stylesheet" href="css/jquery.Jcrop.css" type="text/css" />
        <link href="css/basic.css" type="text/css" rel="stylesheet" />
        <link rel="icon" type="image/png" href="images/logoSharkDJ.png" />
		<script type="text/javascript" src="js/jquery-1.7.min.js"></script>
        <script type="text/javascript" src="js/enhance.js"></script>
        <script type="text/javascript" src="js/jQuery.fileinput.js"></script>
        <script type="text/javascript" src="js/example.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
		<script src="js/jquery.Jcrop.js"></script>

<script type="text/javascript">
			$(function(){

				// Accordion
				$("#accordion").accordion({ header: "h3" });
	
				// Tabs
				$('#tabs').tabs();

				$("#tabs").tabs({                    
				    show: function(event, ui) {
				        google.maps.event.trigger(map, 'resize');
				    }                    
				});

				// Dialog			
				$('#dialog').dialog({
					autoOpen: true,
					height:100,
					hide:"fold",
					show:"fold",
					dialogClass: 'alert',
					closeOnEscape: true
				});
				
				// Dialog Link
				$('#dialog_link').click(function(){
					$('#dialog').dialog('open');
					return false;
				});

				// Datepicker
				$('#datepicker').datepicker({
					inline: true,
					dateFormat: 'dd/mm/yy'
				});
				
				// Slider
				$('#slider').slider({
					range: true,
					values: [17, 67]
				});
				
				// Progressbar
				$("#progressbar").progressbar({
					value: 20 
				});
				
				//hover states on the static widgets
				$('#dialog_link, ul#icons li').hover(
					function() { $(this).addClass('ui-state-hover'); }, 
					function() { $(this).removeClass('ui-state-hover'); }
				);

				$(function() {
					$( ".imagenPerfil button:first" ).button({
			            icons: {
			                primary: "ui-icon-image"
			            }
			        })
				});
				$(function() {
					$( ".boton button:first" ).button({
			            icons: {
			                primary: "ui-icon-folder-collapsed"
			            }
			        })
			    $( ".botonBio button:first" ).button({
			            icons: {
			                primary: "ui-icon-folder-collapsed"
			            }
			        })
			    $( ".botonImagen button:first" ).button({
			            icons: {
			                primary: "ui-icon-image"
			            }
			        })
			   $( ".botonMapa button:first" ).button({
			            icons: {
			                primary: "ui-icon-folder-collapsed"
			            }
			        })
				});
			});
		</script>
			<script language="Javascript">

			$(function(){

				$('#cropbox').Jcrop({
					aspectRatio: 1,
					setSelect: [ 240, 240, 20, 20 ],
					minSize: [240,240],
					onSelect: updateCoords
				});

			});

			function updateCoords(c)
			{
				$('#x').val(c.x);
				$('#y').val(c.y);
				$('#w').val(c.w);
				$('#h').val(c.h);
			};

			function checkCoords()
			{
				if (parseInt($('#w').val())) return true;
				alert('Please select a crop region then press submit.');
				return false;
			};

		</script>
	</head>
<body>
<div id="container">
  <div id="header">
    <div id="loginLinks">
      <a href="./inputLogin.php<?=$langURL;?>" class="login"><?=$arrayIdiomas['login'];?></a>
      <span class="left fontGrey2 marginTop3">-</span>
      <a href="./inputSignup.php<?=$langURL;?>" class="login2"><?=$arrayIdiomas['signup'];?></a>
      <div id="flag">
	    <?php
		#SHOW FLAGS
        foreach ($arrayFlagLanguages as $key => $value) {
          echo '<a href="' . $_SERVER['PHP_SELF'] . '?lang=' . $key . '">';
		  echo   '<img class="marginLeft3" src="images/flags/' . $key . '.png" alt="' . $value . '" title="' . $value . '" />';
          echo '</a>';
	    }
	    ?>
	  </div>
      <?php include 'includes/socialLinks.inc.php'; ?>
    </div>
    <div id="menu">
      <div class="clear insideMenu font1">
      <?php 
        $file = explode("/",$_SERVER['SCRIPT_FILENAME']);
        $filename = $file[count($file)-1];
            
      ?>
        <a href="index.php<?=$langURL;?>"><span <?php echo ($filename=="index.php") ? "class=\"fontRed hoverLargeRed\"" : "class=\"hoverLargeGrey\"";?>><?=$arrayIdiomas['home'];?></span></a>
        &bull;&nbsp;<a href="artistas.php<?=$langURL;?>"><span <?php echo ($filename=="artistas.php") ? "class=\"fontRed hoverLargeRed\"" : "class=\"hoverLargeGrey\"";?>><?=$arrayIdiomas['artists'];?></span></a>
        &bull;&nbsp;<a href="locales.php<?=$langURL;?>"><span <?php echo ($filename=="locales.php") ? "class=\"fontRed hoverLargeRed\"" : "class=\"hoverLargeGrey\"";?>><?=$arrayIdiomas['clubs'];?></span></a>
        &bull;&nbsp;<a href="eventos.php<?=$langURL;?>"><span <?php echo ($filename=="eventos.php") ? "class=\"fontRed hoverLargeRed\"" : "class=\"hoverLargeGrey\"";?>><?=$arrayIdiomas['events'];?></span></a>
        &bull;&nbsp;<a href="escuela.php<?=$langURL;?>"><span <?php echo ($filename=="escuela.php") ? "class=\"fontRed hoverLargeRed\"" : "class=\"hoverLargeGrey\"";?>><?=$arrayIdiomas['school'];?></span></a>
        <?php if ($sesion && $_SESSION['idPerfil'] == 3){?>
          &bull;&nbsp;<a href="localBackend.php<?=$langURL;?>"><span <?php echo ($filename=="localBackend.php") ? "class=\"fontRed hoverLargeRed\"" : "class=\"hoverLargeGrey\"";?>><?=$arrayIdiomas['profile'];?></span></a>
        <?php }elseif ($sesion && $_SESSION['idPerfil'] == 2){?>
        &bull;&nbsp;<a href="artistaBackend.php<?=$langURL;?>"><span <?php echo ($filename=="artistaBackend.php") ? "class=\"fontRed hoverLargeRed\"" : "class=\"hoverLargeGrey\"";?>><?=$arrayIdiomas['profile'];?></span></a>
        <?php }?>
      </div>
    </div>
    <div id="logo"><a href="index.php"><img src="images/logoHeader.png"/></a></div>
  </div>
