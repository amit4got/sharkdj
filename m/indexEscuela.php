<?php
  include_once 'header.php';
  
  define('USERSHARK','adminshark');
?>

	
<div data-role="page" id="artistas">

<?php echo $header;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'http://gdata.youtube.com/feeds/api/videos?author='.USERSHARK);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
    $xml = curl_exec($ch);
    $error = curl_error($ch);
    curl_close($ch);
    
    $object = @simplexml_load_string($xml);
//     echo '<pre>';
//     var_dump($object);
//     echo '</pre>';
?>

  <div data-role="content"> 
		<div data-role="collapsible-set" data-theme="c" data-content-theme="d" data-mini="true">
		
<?php     $i = 1;
    foreach ($object->entry as $video){
      $aux = explode("/", (string) $video->id);
      $id = $aux[count($aux) - 1];
      $title = str_replace('.mp4', '', $video->title);
      $title = str_replace('SHARK DJ:', '', $title);
      $title = strtolower($title);
      $title = trim($title);
      $title = ucfirst($title);
      $fecha = explode('T', $video->updated);
      $fecha = explode('-',$fecha[0]);
      $fecha = $fecha[1].'/'.$fecha[0];?>
      <div data-role="collapsible" <?php echo $collapse = ($i == 1) ? 'data-collapsed="false"':'' ?>>
        <h3><?php echo $title?></h3>
        <p><?php echo $video->content?></p>
        <div align="center"><iframe class="youtube-player" type="text/html" src="http://www.youtube.com/embed/<?php echo $id?>" frameborder="0" width="250"></iframe></div>
      </div>
<?php $i++;}?>
</div>
	 		<div data-role="controlgroup" data-type="horizontal">
        <a href="#" data-role="button" data-icon="arrow-l"
          data-iconpos="notext">Back</a>
        <a href="#" data-role="button" id="buttonUp" data-icon="arrow-u"
          data-iconpos="notext">Up</a>	
        <a href="#" data-role="button" data-icon="arrow-r"
          data-iconpos="notext">Forward</a>
      </div>  
    <p><a href="#home" data-role="button" data-icon="home">Home</a></p>
  </div><!-- /content -->

<?php echo $footer;?>
</div><!-- /page -->