<?php
/****************************
 *  Created 11/11/11
 *  Last update 11/11/11   
 ****************************/

#INCLUDES
//require_once 'includes/connect.ini.php';
//require_once 'includes/functions.ini.php';

//error_reporting(E_ALL);
//ini_set("display_errors", 1);

if(!defined("RUTA_ABSOLUTA")){
	define("RUTA_ABSOLUTA",dirname(__FILE__));
}

require_once RUTA_ABSOLUTA.'/administradorDatos/administradorDatos.class.php';
require_once RUTA_ABSOLUTA.'/administradorDatos/imagenes.class.php';

$listaEventos="";
$pagina = isset($_GET['p']) ? $_GET['p'] : 0;
$searchText = isset($_GET['s']) ? $_GET['s'] : "";
$enlacePag = "./index.php?";
$adminDatos = new administradorDatos();

#Load languages
include RUTA_ABSOLUTA.'/includes/lang.inc.php';
$arrayIdiomas = $adminDatos->cargaIdioma($idioma);
$langURL = '';
if ($idioma != 'es') {
  $langURL = '&lang=' . $idioma;
}

if ($searchText!="") {
	$enlacePag .= "s=".$searchText."&";
	$numTotalEventos = count($adminDatos->buscarEventos($searchText));
	$arrayEventos = $adminDatos->buscarEventos($searchText, 15, $pagina*15);
}else{
	$numTotalEventos = count($adminDatos->obtenerEventos(MAX,0,1));
	$arrayEventos = $adminDatos->obtenerEventos(15,$pagina*15,1);
}

$numEventos = count($arrayEventos);
if ($numEventos>0){
	foreach ($arrayEventos as $evento) {
		$idEvento = $evento['idEvento'];		
		$nombre = $evento['nombre'];		
		$fechaInicio = strtotime($evento['fechaInicio']);
		$mes = date("M", $fechaInicio);
		$dia = date("d", $fechaInicio);
		$hora = date("H:i", $fechaInicio);
		$direccion = $evento['direccion'];
		$precio = $evento['precio'];
		$descripcion = $evento['descripcion'];
		$imagenUrl = imagenes::obtenerImagenEvento($idEvento);
		$listaEventos.=<<<EOF
			<div id="eventBoxClose{$idEvento}" class="boxEvents" onclick="javascript:showElement('eventBoxOpen{$idEvento}');javascript:hideElement('eventBoxClose{$idEvento}');">
              <div class="calendarEvent marginTop10 marginLeft10">
                <div class="monthWhiteEvent textCentered fontBold">{$mes}</div>
                <div class="dayEvent textCentered fontBold">{$dia}</div>
              </div>
              <div class="infoEvent">
                <div class="width100">
                  <span class="font3 marginTop5 marginBottom5 left">{$nombre}</span>
      			</div>
                <span class="clear left marginTop10">{$arrayIdiomas['time']}: {$hora}<br />{$arrayIdiomas['place']}: {$direccion}</span>
                <span class="right marginTop25 marginRight5">{$arrayIdiomas['price']}: {$precio} &euro;</span>
              </div>
            </div>
      		<div id="eventBoxOpen{$idEvento}" class="boxEventsOpen marginBottom10 displayNone">
              <div class="eventPhoto marginTop10 marginLeft10"><img src='{$imagenUrl}' width="200" heigth="280"/></div>
              <div class="box11">
                <div class="calendarEventOpen marginLeft5">
                  <div class="monthEventOpen textCentered fontBold">{$mes}</div>
                  <div class="dayEventOpen textCentered fontBold">{$dia}</div>
                </div>
                <div class="infoEventOpen fontWhite">
                  <div class="width100">
                    <span class="font3 left">{$nombre}</span>              
      			  </div>
                  <span class="clear left">{$arrayIdiomas['time']}: {$hora}<br />{$arrayIdiomas['place']}: {$direccion}</span>
                  <span class="right marginTop15 marginRight5">{$arrayIdiomas['price']}: {$precio} &euro;</span>
                </div>
                <div class="box12 textJustified fontWhite">{$descripcion}</div>
              </div>
            </div>
EOF;
	}
}

include_once 'includes/header.inc.php';
?>
  <div id="main">
	  <div id="searchBar">
	    <form>
	      <label class="left fontWhite marginTop5 marginLeft5"><?=$arrayIdiomas['name'];?>:</label>
	      <input type="text" name="searchText" class="left searchText2 noBorder fontGrey fontBold" value="" />
	      <label class="left fontWhite marginTop5 marginLeft5"><?=$arrayIdiomas['place'];?>:</label>
	      <input type="text" name="searchText" class="left searchText2 noBorder fontGrey fontBold" value="" />
	      <label class="left fontWhite marginTop5 marginLeft5"><?=$arrayIdiomas['style'];?>:</label>
	      <input type="image" name="submit" class="right marginRight7 marginTop2" id="searchButton" value="" />
	      <input type="text" name="searchText" class="right searchText2 noBorder fontGrey fontBold" value="" />
	      <input type="hidden" name="lang" value="<?=$idioma?>" />
	    </form>
	  </div>
  <div class="insideRow fontBold marginTop5 fontDarkGrey">
    <?php 
    	echo $listaEventos;
    ?>
  </div>
  <div class="advertisementRow3 marginTop5"></div>
  <div id="nextPreviousMenu" class="clear left insideRow">
      <div class="left width33"><a href="<?=$langURL;?>" class="noDecoration fontGrey2 fontBold hoverLargeGrey">&laquo; <?=$arrayIdiomas['previousMenu'];?></a></div>
      <div class="left width33 textCentered marginLeft5"><a href="<?=$langURL;?>" class="noDecoration fontGrey2 fontBold hoverLargeGrey"><?=$arrayIdiomas['homeMenu'];?></a></div>
      <div class="right"><a href="<?=$langURL;?>" class="fontBold noDecoration fontGrey2 hoverLargeGrey"><?=$arrayIdiomas['nextMenu'];?> &raquo;</a></div>
    </div>
</div>
<?php include_once 'includes/footer.inc.php'; ?>