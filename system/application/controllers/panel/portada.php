<?php
class Portada extends Controller {
	function __construct() 
	{
		parent::Controller();
		if ($this->redux_auth->logged_in()==1 ){
			$profile_var = $this->redux_auth->profile();
			if ($profile_var->group!="artistas" && $profile_var->group!="admin"){
				$this->redux_auth->logout();
				redirect('web/login');
			}
		}else{
			$this->redux_auth->logout();
			redirect('web/login');
		}
	}
	function index()
	{
		$data['profile']= $this->redux_auth->profile();
		$data['head'] = $this->load->view('panel/head_view', $data, true);
		$data['navigation'] = $this->load->view('panel/main_menu_view', null, true);
		$data['content_menu'] = "";
		$data['content'] =$this->load->view('panel/portada_view', null, true);; 
		$data['foot'] = "";
		$this->load->view('templates/panel_view', $data);
	}
}


