<?php
class Recuperar_password extends Controller {
	function __construct() 
	{
		parent::Controller();
	}
	function index()
	{
		$this->form_validation->set_rules('email', 'Email Address', 'required');
	    $this->form_validation->set_error_delimiters('<p class="error">', '</p>');
	    if ($this->form_validation->run() == false)
	    {
			$data['head'] = "";
			$data['navigation'] = "";
			$data['content_menu'] = "";
			$data['content'] = $this->load->view('panel/recuperar_password_view', null, true);
			$data['foot'] = "";
	    	$this->load->view('templates/panel_view', $data);
	    }
	    else
	    {
	        $email = $this->input->post('email');
			$forgotten = $this->redux_auth->forgotten_password($email);
		    
			if ($forgotten)
			{
				$this->session->set_flashdata('message', '<p class="success">Se le ha enviado a su email la clave de recuperación.</p>');
	            redirect('panel/recuperar_password'); 
			}
			else
			{
				$this->session->set_flashdata('message', '<p class="error">No se a podido realizar la operación.</p>');
	            redirect('panel/recuperar_password');
			}
	    }
		
	}
	function activar()
	{
		$this->form_validation->set_rules('code', 'Verification Code', 'required');
	    $this->form_validation->set_error_delimiters('<p class="error">', '</p>');
	    if ($this->form_validation->run() == false)
	    {
			redirect('panel/recuperar_password');
	    }
	    else
	    {
	        $code = $this->input->post('code');
			$forgotten = $this->redux_auth->forgotten_password_complete($code);
		    
			if ($forgotten)
			{
	            $this->session->set_flashdata('message', '<p class="success">Se le ha enviado a su email un pasword nuevo.</p>');
	            redirect('panel/recuperar_password');
			}
			else
			{
				$this->session->set_flashdata('message', '<p class="error">No se a podido realizar la operación.</p>');
	            redirect('panel/recuperar_password');
			}
	    }
	}
}
