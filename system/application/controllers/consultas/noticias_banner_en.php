<?php
class noticias_banner_en extends Controller {
	function __construct() 
	{
		parent::Controller();
	
		
	}
	function index()
	{
		$this->db->where('es_clave', 'noticias_banner');
		$query=$this->db->get('Textos');
		$xmlenvio='<?xml version="1.0" encoding="utf-8"?><lista>';
		if ($query->num_rows()>0){
			foreach($query->result() as $row){ 
				if ($row->imagen!=""){
					$ar_imagen=explode(".",$row->imagen);
					$imagen=base_url().'uploads/noticias_banner/'.$ar_imagen[0].'.'.$ar_imagen[1];
				}else{
					$imagen=base_url().'uploads/noticias_banner/espacio.gif';
				}
				$xmlenvio.='<noticia titulo="'.$row->en_titulo.'" imagen="'.$imagen.'" link="'.$row->en_link.'" ></noticia>'; 	
			}
		}
		$xmlenvio.='</lista>';
		
		echo $xmlenvio;
	}
}


