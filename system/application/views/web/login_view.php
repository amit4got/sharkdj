<div id="cuerpo_1">
    <div id="cuerpo_noticias">
    
        <div id="Identificarse" >
        <?=form_open('web/login', array('id' => 'form1' )); ?>
        <?=form_hidden('acc', 'identificarse'); ?>
            	<div class="campos_txt">
            	  <p>Esta sección es exclusiva para Djs, Productores, Promotoras, Bandas o Colectivos, si eres un club, agencia, etc y quieres que tu nombre aparezca en la sección de clubes o quieres contactar con SHARK para fiestas o promociones deberás dirigirte con un email a <a href="mailto:marketing@sharkiberia.sl">marketing@sharkiberia.sl</a></p>      	
       	  </div>
            <div class="formularios_titulo">Identificarse</div>
            <div class="campos">
                <div class="campos_titulo">Email</div>
                <div class="campos_introducir"><?=form_input('email_iden', set_value('email_iden')); ?></div>
            </div>
           
            <div class="campos">
                <div class="campos_titulo">Password</div>
                <div class="campos_introducir"><?=form_password('password_iden'); ?></div>
            </div>
            <div class="formularios_sumbit"><?=form_submit('submit', 'Validar'); ?></div>
            <?php 
				if (isset($error_login)){
					echo ' <div class="campos">'.$error_login.'</div>';
				}
			?>
            
            <div class="formularios_sumbit"><?=anchor('panel/recuperar_password/' , 'Recuperar password' ); ?></div>
        <?=form_close(''); ?>

        </div>
        <div id="Registrarse" >
        <?=form_open('web/login' , array('id' => 'form2' )); ?>
        <?=form_hidden('acc', 'registrarse'); ?>
        	<div class="formularios_titulo">Registrarse</div>
            <div class="campos">
                <div class="campos_titulo">Alias del artista</div>
                <div class="campos_introducir"><?=form_input('username', set_value('username')); ?></div>
            </div>
            <div class="campos">
                <div class="campos_titulo">Email</div>
                <div class="campos_introducir"><?=form_input('email', set_value('email')); ?></div>
            </div>
            <div class="campos">
                <div class="campos_titulo">Password</div>
                <div class="campos_introducir"><?=form_password('password'); ?></div>
            </div>
            <div class="campos">
                <div class="campos_titulo">Nombre</div>
                <div class="campos_introducir"><?=form_input('first_name', set_value('first_name')); ?></div>
            </div>
            <div class="campos">
                <div class="campos_titulo">Apellidos</div>
                <div class="campos_introducir"><?=form_input('last_name', set_value('last_name')); ?></div>
            </div>
            <div class="campos">
                <div class="campos_titulo">Teléfono</div>
                <div class="campos_introducir"><?=form_input('telefono', set_value('telefono')); ?></div>
            </div>
            <div class="campos">
            	<?php
					$atts = array(
					  'width'      => '800',
					  'height'     => '600',
					  'scrollbars' => 'yes',
					  'status'     => 'yes',
					  'resizable'  => 'no',
					  'screenx'    => '0',
					  'screeny'    => '0'
					);

					echo anchor_popup('web/legal', 'Información Legal', $atts);
				?>
            </div>
            <div class="formularios_sumbit"><?=form_submit('submit', 'Guardar'); ?></div>
        <?=form_close(''); ?>
        	<?php 
				if (isset($error_registro)){
					echo ' <div class="campos">'.$error_registro.'</div>';
				}
			?>
        	
            <div class="campos">
                *Recibirá un mail para darse de alta por favor compruebe su bandeja de correo electrónico no deseado 
            </div>
        </div>            
  
    </div>
    <div id="banner">
   	  <?= $this->banner_model->cargar_banner_es (); ?>
    </div>
	
</div>
