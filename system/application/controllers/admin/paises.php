<?php
class Paises extends Controller {
	function __construct() 
	{
		parent::Controller();
		if ($this->redux_auth->logged_in()==1 ){
			$profile_var = $this->redux_auth->profile();
			if ($profile_var->group!="admin" && $profile_var->group!="super" ){
				$this->redux_auth->logout();
				redirect('admin');
			}
		}else{
			$this->redux_auth->logout();
			redirect('admin');
		}
	}
	
	function index()
	{
		redirect('admin/paises/listado/');
	}
	function listado()
	{
		$this->data['profile']= $this->redux_auth->profile();
		$this->data['head'] = $this->load->view('admin/head_view', $this->data, true);
		$this->data['navigation'] = $this->load->view('admin/main_menu_view', null, true);
		$this->data['content_menu'] = $this->load->view('admin/paises_menu_view', null, true);
	
		$this->db->order_by('es_titulo','ASC');
		$this->data['query']=$this->db->get('Pais');
		$this->data['content'] =$this->load->view('admin/paises_listado_view', $this->data, true); 
		$this->data['foot'] = '';
		$this->load->view('templates/admin_view', $this->data);
		
	}	
	
	function nuevo()
	{
		$this->data['body_carga']=' onload="load();" onUnload="GUnload()" ';
		$this->form_validation->set_rules('es_titulo', '"Título en español"', 'required');
	    $this->form_validation->set_rules('en_titulo', '"Título en ingles"', 'required');
	    $this->form_validation->set_rules('du_titulo', '"Título en holades"', 'required');

	    $this->form_validation->set_error_delimiters('<p class="error">', '</p>');
	    if ($this->form_validation->run() == false)
	    { 
			
			
			$this->data['profile']= $this->redux_auth->profile();
			$this->data['head'] = $this->load->view('admin/head_view', $this->data, true);
			$this->data['navigation'] = $this->load->view('admin/main_menu_view', null, true);
			$this->data['content_menu'] = $this->load->view('admin/paises_menu_view', null, true);
			$this->data['accion_form']='admin/paises/nuevo';
			
			$this->data['registro'] = array(
							
							"es_titulo" => '' , 
							"en_titulo" => '' ,
			"du_titulo" => ''
					
							);
			
	        $this->data['content'] = $this->load->view('admin/paises_form_view', $this->data, true);
	       	$this->data['foot'] = "";
			
			$this->load->view('templates/admin_view', $this->data);
	    }
	    else
	    {
	    	
			$this->data['registro']['es_titulo'] 		= $this->input->post('es_titulo');
			$this->data['registro']['en_titulo'] 		= $this->input->post('en_titulo');
			$this->data['registro']['du_titulo'] 		= $this->input->post('du_titulo');
			$this->db->set($this->data['registro']); 
			
			if ($this->db->insert('Pais')){
				$this->session->set_flashdata('message', '<p class="notice">La información ha sido insertada.</p>');
	            redirect('admin/paises/nuevo/'.$this->uri->segment(4));
			} else
	        {
	            $this->session->set_flashdata('message', '<p class="error">No se ha podido guardar.</p>');
	            redirect('admin/paises/nuevo/'.$this->uri->segment(4));
	        }
	    }	
		
		
	}
	function modificar()
	{
		
		
		
		$this->data['body_carga']='';
		
		$this->form_validation->set_rules('es_titulo', '"Título en español"', 'required');
	    $this->form_validation->set_rules('en_titulo', '"Título en ingles"', 'required');
	    $this->form_validation->set_rules('du_titulo', '"Título en holandes"', 'required');
		
	    $this->form_validation->set_error_delimiters('<p class="error">', '</p>');
	    if ($this->form_validation->run() == false)
	    { 
			
			$this->data['profile']= $this->redux_auth->profile();
			$this->data['head'] = $this->load->view('admin/head_view', $this->data, true);
			$this->data['navigation'] = $this->load->view('admin/main_menu_view', null, true);
			$this->data['content_menu'] = $this->load->view('admin/paises_menu_view', null, true);
	        
			$this->data['accion_form']='admin/paises/modificar/'.$this->uri->segment(4);
			
			$this->db->where('id', $this->uri->segment(4) );
			$query=$this->db->get('Pais');
			$this->data['errores']='';
		
			foreach ($query->result() as $row)
			{
				
				$this->data['registro'] = array(
											
											"es_titulo" 		=> $row->es_titulo , 
											"en_titulo" 		=> $row->en_titulo ,
				"du_titulo" 		=> $row->du_titulo
											
											);
	
			}
			if (!isset($this->data['registro'])){
				 redirect('admin/paises/listado');
			}
			$this->data['content'] = $this->load->view('admin/paises_form_view', $this->data, true);
	       	$this->data['foot'] = "";
			$this->load->view('templates/admin_view', $this->data);
	    }
	    else
	    {
			$update = array(
							
							"es_titulo" 		=> $this->input->post('es_titulo') , 
							"en_titulo" 		=> $this->input->post('en_titulo') ,
			"du_titulo" 		=> $this->input->post('du_titulo')
							
							);
			$this->db->where('id', $this->uri->segment(4) );
			
			if ($this->db->update("Pais",$update)){
				$this->session->set_flashdata('message', '<p class="notice">La información ha sido modifica.</p>');
	            redirect('admin/paises/modificar/'.$this->uri->segment(4));
			} else
	        {
	            $this->session->set_flashdata('message', '<p class="error">No se ha podido modificar.</p>');
	            redirect('admin/paises/modificar/'.$this->uri->segment(4));
	        }
	    }	
	}
}


