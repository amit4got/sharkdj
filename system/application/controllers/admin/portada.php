<?php
class Portada extends Controller {
	function __construct() 
	{
		parent::Controller();
		if ($this->redux_auth->logged_in()==1 ){
			$profile_var = $this->redux_auth->profile();
			if ($profile_var->group!="admin" && $profile_var->group!="super" ){
				$this->redux_auth->logout();
				redirect('admin');
			}
		}else{
			$this->redux_auth->logout();
			redirect('admin');
		}
	}
	function index()
	{
		$data['profile']= $this->redux_auth->profile();
		$data['head'] = $this->load->view('admin/head_view', $data, true);
		$data['navigation'] = $this->load->view('admin/main_menu_view', null, true);
		$data['content_menu'] = "";
		$data['content'] =""; 
		$data['foot'] = "";
		$this->load->view('templates/admin_view', $data);
	}
}


