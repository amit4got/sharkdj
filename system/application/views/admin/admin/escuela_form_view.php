<div id="cuerpo">

<?=$this->session->flashdata('message'); ?>

<?=validation_errors(); ?>

<?=form_open_multipart($accion_form); ?>

<div class="formularios_titulo">Campos requeridos</div>
<div class="campos">
    <div class="campos_titulo">Título en español</div>
    <div class="campos_introducir"><?=form_input('es_titulo',set_value('es_titulo')=="" ?  $registro['es_titulo']  :   set_value('es_titulo') ); ?></div>
</div>
<div class="campos">
    <div class="campos_titulo">Título en ingles</div>
    <div class="campos_introducir"><?=form_input('en_titulo', set_value('en_titulo')=="" ?  $registro['en_titulo']  :   set_value('en_titulo') ); ?></div>
</div>
<div class="campos">
    <div class="campos_titulo">Título en holandes</div>
    <div class="campos_introducir"><?=form_input('du_titulo', set_value('du_titulo')=="" ?  $registro['du_titulo']  :   set_value('du_titulo') ); ?></div>
</div>
<div class="campos">
    <div class="campos_titulo">Imagen</div>
    <div class="campos_introducir"><?=form_upload('imagen'); ?></div>
</div>
 <?php if ($this->uri->segment(3)=='modificar'): ?>
    <div class="campos">
        <img src="<?= $registro['imagen']; ?>" />
    </div>
<?php endif; ?>
<div class="campos_supertitulo">Detalle</div>
<div class="campos">
    <div class="campos_titulo">Subtítulo en español</div>
    <div class="campos_introducir"><?=form_input('es_subtitulo',set_value('es_subtitulo')=="" ?  $registro['es_subtitulo']  :   set_value('es_subtitulo') ); ?></div>
</div>
<div class="campos">
    <div class="campos_titulo">Subtítulo en ingles</div>
    <div class="campos_introducir"><?=form_input('en_subtitulo', set_value('en_subtitulo')=="" ?  $registro['en_subtitulo']  :   set_value('en_subtitulo') ); ?></div>
</div>
<div class="campos">
    <div class="campos_titulo">Subtítulo en holandes</div>
    <div class="campos_introducir"><?=form_input('du_subtitulo',set_value('du_subtitulo')=="" ?  $registro['du_subtitulo']  :   set_value('du_subtitulo') ); ?></div>
</div>
   
<div class="campos">
    <div class="campos_titulo">Texto en español</div>
    <div class="campos_introducir">
        <?= $ckeditor->editor('es_informacion', set_value('es_informacion')=="" ?  $registro['es_informacion']  :   set_value('es_informacion') ,$ckeditor->config ); ?>
    </div>
</div>
<div class="campos">
    <div class="campos_titulo">Texto en ingles</div>
    <div class="campos_introducir">
         <?= $ckeditor->editor('en_informacion', set_value('en_informacion')=="" ?  $registro['en_informacion']  :   set_value('en_informacion') ,$ckeditor->config ); ?>
    </div>
</div>
<div class="campos">
    <div class="campos_titulo">Texto en holandes</div>
    <div class="campos_introducir">
         <?= $ckeditor->editor('du_informacion', set_value('du_informacion')=="" ?  $registro['du_informacion']  :   set_value('du_informacion') ,$ckeditor->config ); ?>
    </div>
</div>   
   
<div id="btnsubmit"><?=form_submit('submit', 'Guardar'); ?></div>


<?=form_close(''); ?>
</div> 
