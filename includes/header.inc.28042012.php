<?php
/****************************
*  Created 02/11/11
*  Last update 04/12/11
****************************/
// ini_set ("display_errors","1" );
// error_reporting(E_ALL);

error_reporting(0);

session_start();

if ($_SESSION['status'] == "authorized") {
  $sesion = true;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="expires" content="Sat, 27 May 1978 12:00:00 GMT" />
<meta name="country" content="es" />
<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>SHARK DJ</title>
<link href="carousel/skin.css" rel="stylesheet" type="text/css" media="screen" />
<link type="text/css" href="css/custom-theme/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
<link href="shark.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="icon" type="image/png" href="images/logoSharkDJ.png" />
<script type="text/javascript" src="js/jquery-1.7.min.js"></script> 
<script type="text/javascript" src="js/jquery.jcarousel.min.js"></script>
<script type="text/javascript" src="js/functions.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript">
			$(function(){
				// Dialog			
				$('#dialog').dialog({
					autoOpen: true,
					height:100,
					hide:"fold",
					show:"fold",
					dialogClass: 'alert',
					closeOnEscape: true
				});
				
				// Dialog Link
				$('#dialog_link').click(function(){
					$('#dialog').dialog('open');
					return false;
				});
			});
		</script>

<script type="text/javascript"> 
jQuery(document).ready(function() {
    jQuery('#mycarousel').jcarousel({
        visible: 1
    });
});
</script> 
</head>
<body>
<div id="headerFull">
<div id="container">
  <div id="header">
    <div id="loginLinks">
      <a href="./inputLogin.php" class="login">Login</a>
      <span class="left fontGrey2 marginTop3">-</span>
      <a href="./inputSignup.php" class="login2">Sign up</a>
      <div id="flag"><img src="images/flags/<?php echo 'es'?>.png" alt="<?php echo 'Espa&ntilde;ol'?>" title="<?php echo 'Espa&ntilde;ol'?>" /></div>
      <?php include 'includes/socialLinks.inc.php'; ?>
    </div>
    <div id="menu">
      <div class="clear insideMenu font1">
        <?php 
          $file = explode("/",$_SERVER['SCRIPT_FILENAME']);
          $filename = $file[count($file)-1];            
        ?>
        <a href="index.php"><span <?php echo ($filename=="index.php") ? "class=\"fontRed hoverLargeRed\"" : "class=\"hoverLargeGrey\"";?>>INICIO</span></a>
        &bull;&nbsp;<a href="artistas.php"><span <?php echo ($filename=="artistas.php") ? "class=\"fontRed hoverLargeRed\"" : "class=\"hoverLargeGrey\"";?>>ARTISTAS</span></a>
        &bull;&nbsp;<a href="locales.php"><span <?php echo ($filename=="locales.php") ? "class=\"fontRed hoverLargeRed\"" : "class=\"hoverLargeGrey\"";?>>CLUBS</span></a>
        &bull;&nbsp;<a href="eventos.php"><span <?php echo ($filename=="eventos.php") ? "class=\"fontRed hoverLargeRed\"" : "class=\"hoverLargeGrey\"";?>>EVENTOS</span></a>
        &bull;&nbsp;<a href="escuela.php"><span <?php echo ($filename=="escuela.php") ? "class=\"fontRed hoverLargeRed\"" : "class=\"hoverLargeGrey\"";?>>ESCUELA</span></a>
        <?php if ($sesion && $_SESSION['idPerfil'] == 3){?>
          &bull;&nbsp;<a href="localBackend.php"><span <?php echo ($filename=="localBackend.php") ? "class=\"fontRed hoverLargeRed\"" : "class=\"hoverLargeGrey\"";?>>PERFIL</span></a>
        <?php }elseif ($sesion && $_SESSION['idPerfil'] == 2){?>
        &bull;&nbsp;<a href="artistaBackend.php"><span <?php echo ($filename=="artistaBackend.php") ? "class=\"fontRed hoverLargeRed\"" : "class=\"hoverLargeGrey\"";?>>PERFIL</span></a>
        <?php }?>
      </div>
    </div>
    <div id="logo"><a href="index.php"><img src="images/logoHeader.png"/></a></div>
  </div>
  <?php include 'carousel/carousel.php'; 
  
  function leerDirectorioPublicidad()
  {
    $dir=RUTA_ABSOLUTA.'/images/publicidad/';
    $directorio=opendir($dir);
    while ($archivo = readdir($directorio)){
      $imagenes[]=$archivo;
    }
    closedir($directorio);
    return $imagenes;
  }
  
  ?>
  <?php    
  	if (!class_exists("administradorDatos")) {
  		require_once RUTA_ABSOLUTA.'/administradorDatos/administradorDatos.class.php';
  	}
    $adminDatos = new administradorDatos();
	#Load languages
	$idioma = isset ($_GET['lang']) ? $GET['lang'] : 'es';
	$arrayIdiomas = $adminDatos->cargaIdioma($idioma);

    $arrayInfoArtistas = $adminDatos->obtenerArtistasInfo(1);    
    $nArtistas = count($arrayInfoArtistas);
    $i = rand(0, $nArtistas-1);
    if (!class_exists("imagenes")) {
      require_once RUTA_ABSOLUTA.'/administradorDatos/imagenes.class.php';
    }
    $publicidad = leerDirectorioPublicidad();
    $urlImagen = imagenes::obtenerImagenUsuario($arrayInfoArtistas[$i]['idUser']);
  ?>
  <div class="sideBar2">
    <div class="artistPhoto"><?php echo ($urlImagen!="") ? "<img height='140' width='140' src='".$urlImagen."' />" : ""; ?></div>
    <div id="artistInfoBox">
      <span class="fontRed"> <?php echo ($arrayInfoArtistas[$i]['nombre']!="") ? $arrayInfoArtistas[$i]['nombre'] : $arrayIdiomas['noName'];?></span>
      <br />
      <br />
      <span class="fontGrey"><?=$arrayIdiomas['style'];?>:</span><span class="fontBlack"> <?php echo ($arrayInfoArtistas[$i]['estilo']!="") ? $arrayInfoArtistas[$i]['estilo'] : " " . $arrayIdiomas['noStyle'];?></span>
      <br />
      <span class="fontGrey"><?=$arrayIdiomas['country'];?>:</span><span class="fontBlack"> <?php echo ($arrayInfoArtistas[$i]['pais']!="") ? $arrayInfoArtistas[$i]['pais'] : "-";?></span>
      <br />      
      <!--  SI ESTA -->      
      <span class="fontGrey"><?=$arrayIdiomas['club'];?>:</span><span class="fontBlack"> <?php echo ($arrayInfoArtistas[$i]['clubResidencia']!="") ? $arrayInfoArtistas[$i]['clubResidencia'] : "-";?></span>
      <br />
      <span class="fontGrey"><?=$arrayIdiomas['web'];?>:</span><span> <?php echo ($arrayInfoArtistas[$i]['web']!="") ? '<a href="'.$arrayInfoArtistas[$i]['web'].'" class="website">Website</a>' : "-";?></span>
    </div>
  </div>