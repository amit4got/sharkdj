<?php
/****************************
 *  Created 26/06/11
 *  Last update 25/12/12   
 ****************************/

#Esta parte detecta si el dispositivo es un móvil y redirige a la versión móvil de la web
function isMobile(){
  return preg_match('/ipod|iphone|android|opera mini|blackberry|palm os|windows ce|mobile/i', $_SERVER['HTTP_USER_AGENT'] );
}

if( isMobile() ){
  header('location: ./m/index.php');
  exit();
}
#Hasta aquí

if(!defined("RUTA_ABSOLUTA")){
	define("RUTA_ABSOLUTA",dirname(__FILE__));
}

require_once RUTA_ABSOLUTA.'/administradorDatos/administradorDatos.class.php';
$adminDatos = new administradorDatos();

#Load languages
include RUTA_ABSOLUTA.'/includes/lang.inc.php';
$arrayIdiomas = $adminDatos->cargaIdioma($idioma);
$langURL2 = '';
if ($idioma != 'es') {
  $langURL2 = '&lang=' . $idioma;
}
require_once RUTA_ABSOLUTA.'/administradorDatos/artist.class.php';
require_once RUTA_ABSOLUTA.'/administradorDatos/imagenes.class.php';
#INCLUDES
require_once 'includes/connect.ini.php';
require_once 'includes/functions.ini.php';

// session_start();

// if ($_SESSION['status'] != "authorized") {
//   header ("Location: index.php");
//   exit();
// }

$listaNoticias="";
$pagina = isset($_GET['p']) ? $_GET['p'] : 0;
$searchText = isset($_GET['s']) ? $_GET['s'] : "";
$enlacePag = "./index.php?";
if ($searchText!="") {
	$enlacePag .= "s=".$searchText."&";	
	$numTotalNoticias = count($adminDatos->buscarNoticias($searchText));
	$arrayNoticias = $adminDatos->buscarNoticias($searchText, 5, $pagina*5);	
}else{	
	$numTotalNoticias = count($adminDatos->obtenerNoticias(MAX,0, "", "", "", 1));
	$arrayNoticias = $adminDatos->obtenerNoticias(5, $pagina*5, "", "", "", 1);
}

$numNoticias = count($arrayNoticias);
if ($numNoticias>0){
	for($i=0; $i<$numNoticias && $i<3; $i++) {
		$idPost = $arrayNoticias[$i]['idPost'];
		$titulo = $arrayNoticias[$i]['titulo'];
		$contenidoLargo = $arrayNoticias[$i]['texto'];
		if (isset($_GET['lang']) && $_GET['lang'] == 'en') {
			$contenidoLargo = $arrayNoticias[$i]['lang_en'];
		}
		$contenido = preg_replace('/<img.+\/>/', ' ', $contenidoLargo);
		$contenido = substr($contenido, '0','730');
		if (strlen($contenido) == 730){
     	  $contenido .= '...';
		}
		//$contenido = preg_replace('/<div.*>/', ' ', $contenido);
		//$contenido = str_replace ('<div>', ' ', $contenido);
		//$contenido = str_replace ('</div>', ' ', $contenido);
		$contenido = strip_tags($contenido, '<span><b><p><strong><a>');
		$fecha = $arrayNoticias[$i]['fecha'];
		$categoria = $arrayNoticias[$i]['nombreCategoria'];
		$imagenUrl = imagenes::obtenerImagenPost($idPost);
		$imagenUrl .= '?nocache='.date("isu");
		$listaNoticias.=<<<EOF
    <div class="insideRow3">
      <div class="box4">
        <div id="news1" class="newsPhoto"><a href="./noticias.php?ip={$idPost}{$langURL2}"><img src='{$imagenUrl}' width="250" heigth="250" /></a></div>
        <div class="newsInfo">
          <div class="newsInfoTitle"><a href="./noticias.php?ip={$idPost}{$langURL2}" class="fontDarkGrey noDecoration">{$titulo}</a></div>
          <div  class="newsInfoText">{$contenido}</div>
        </div>
        <div class="newsCategoryDate">{$arrayIdiomas['date']}: {$fecha}<br />{$arrayIdiomas['category']}: {$categoria}</div>
        <div class="newsReadMore"><a href="./noticias.php?ip={$idPost}{$langURL2}" class="readMoreText fontDarkGrey noDecoration">{$arrayIdiomas['more']}</a></div>
      </div>
    </div>
EOF;
	}
	if (isset($arrayNoticias[3])) {
		$idPost = $arrayNoticias[3]['idPost'];
		$titulo = $arrayNoticias[3]['titulo'];
		$contenidoLargo = $arrayNoticias[3]['texto'];
		if (isset($_GET['lang']) && $_GET['lang'] == 'en') {
			$contenidoLargo = $arrayNoticias[$i]['lang_en'];
		}
		$contenido = preg_replace('/<img.+\/>/', ' ', $contenidoLargo);
		$contenido = substr($contenido, '0','660');
		if (strlen($contenido) == 660){
		  $contenido .= '...';
		}
		$contenido = strip_tags($contenido, '<span><b><p><strong><a>');
		//$contenido = preg_replace('/<div.*>/', ' ', $contenido);
		//$contenido = str_replace ('<div>', ' ', $contenido);
		//$contenido = str_replace ('</div>', ' ', $contenido);
		$fecha = $arrayNoticias[3]['fecha'];
		$categoria = $arrayNoticias[3]['nombreCategoria'];
		$listaNoticias.=<<<EOF
		  <div class="row">
		    <div class="insideRow">
		      <div class="box5 marginRight5">
		        <div class="newsInfo2">
		          <div class="newsInfoTitle2"><a href="./noticias.php?ip={$idPost}{$langURL2}" class="fontDarkGrey noDecoration">{$titulo}</a></div>
		          <div class="newsInfoText2">{$contenido}</div>
		        </div>
		        <div class="newsCategoryDate2">{$arrayIdiomas['date']}: {$fecha}<br />{$arrayIdiomas['category']}: {$categoria}</div>
		        <div class="newsReadMore2"><a href="./noticias.php?ip={$idPost}{$langURL2}" class="readMoreText fontDarkGrey noDecoration">{$arrayIdiomas['more']}</a></div>
		      </div>
		
EOF;
		if (isset($arrayNoticias[4])) {
			$idPost = $arrayNoticias[4]['idPost'];
			$titulo = $arrayNoticias[4]['titulo'];
			$contenidoLargo = $arrayNoticias[4]['texto'];
			if (isset($_GET['lang']) && $_GET['lang'] == 'en') {
				$contenidoLargo = $arrayNoticias[$i]['lang_en'];
			}
			$contenido = preg_replace('/<img.+\/>/', ' ', $contenidoLargo);
			$contenido = substr($contenido, '0','660');
			if (strlen($contenido) == 660){
			  $contenido .= '...';
			}
			$contenido = strip_tags($contenido, '<span><b><p><strong><a>');
			//$contenido = preg_replace('/<div.*>/', ' ', $contenido);
			//$contenido = str_replace ('<div>', ' ', $contenido);
			//$contenido = str_replace ('</div>', ' ', $contenido);
			$fecha = $arrayNoticias[4]['fecha'];
			$categoria = $arrayNoticias[4]['nombreCategoria'];
			$listaNoticias.=<<<EOF
			  <div class="box5">
			    <div class="newsInfo2">
			      <div class="newsInfoTitle2"><a href="./noticias.php?ip={$idPost}{$langURL2}" class="fontDarkGrey noDecoration">{$titulo}</a></div>
			      <div  class="newsInfoText2">{$contenido}</div>
			    </div>
			    <div class="newsCategoryDate2">{$arrayIdiomas['date']}: {$fecha}<br />{$arrayIdiomas['category']}: {$categoria}</div>
			    <div class="newsReadMore2"><a href="./noticias.php?ip={$idPost}{$langURL2}" class="readMoreText fontDarkGrey noDecoration">{$arrayIdiomas['more']}</a></div>
			  </div>
EOF;
		}
		$listaNoticias.=<<<EOF
		    </div>
        </div>		
EOF;
	}
} else {
    $listaNoticias='<div class="newsInfoTitle">No hemos encontrado resultados a la búsqueda.</div>
                    <br /><br /><br />
                    <div class="right"><a href="javascript:history.go(-1)" class="noDecoration fontRed fontBold hoverLargeGrey">Volver a la página anterior</a></div>';
}
include_once 'includes/header.inc.php';
srand();
$publicidades = $adminDatos->obtenerPublicidad();
if (count($publicidades) < 6){
	$claves = array_rand($publicidades,count($publicidades));	
}else{
	$claves = array_rand($publicidades,6);
	
}
	shuffle($claves);

?>
  <div id="main">
    <div id="searchBar">
      <span class="left fontWhite marginTop5 marginLeft5"><?=$arrayIdiomas['searchTitle'];?>:</span>
      <form method="get" action="index.php<?=$langURL;?>">
        <input type="image" name="submit" class="right marginRight7 marginTop2" id="searchButton" value="" />
        <input type="text" id="searchText" name="s" class="right searchText noBorder fontGrey fontBold" value="<?=$arrayIdiomas['searchText'];?>" onclick="javascript:valueReset('searchText');"/>
		<input type="hidden" name="lang" value="<?=$idioma?>" />
      </form>
    </div>
	<div class="column2">
	<?php
	echo $listaNoticias;
	?>
	</div>
	<div class="advertisementRow3"><?php
	 foreach($claves as $c){
	   echo '<br><a href="'.$publicidades[$c]['url'].'"><img src="http://www.sharkdj.com/'. $publicidades[$c]['imagen'] .'" alt="' . $arrayIdiomas['ads'] . '" title="' . $arrayIdiomas['ads'] . '" /></a><br>';
	}?></div>
    <div id="nextPreviousMenu" class="clear left insideRow">
      <div class="left width33"><?php if ($pagina>0) { ?><a href="<?php echo $enlacePag;?>p=<?php echo ($pagina-1) . $langURL2;?>" class="noDecoration fontGrey2 fontBold hoverLargeGrey">&laquo; <?=$arrayIdiomas['previousMenu'];?></a><?php }?><a class="noDecoration fontGrey2 fontBold hoverLargeGrey">&nbsp;</a></div>
      <div class="left width33 textCentered marginLeft5"><a href="<?php echo $enlacePag;?>p=0<?=$langURL2;?>" class="noDecoration fontGrey2 fontBold hoverLargeGrey"><?=$arrayIdiomas['homeMenu'];?></a></div>        
      <div class="right"><?php if (($pagina+1)*5 < $numTotalNoticias) {?><a href="<?php echo $enlacePag;?>p=<?php echo ($pagina+1) . $langURL2;?>" class="fontBold noDecoration fontGrey2 hoverLargeGrey"><?=$arrayIdiomas['nextMenu'];?> &raquo;</a><?php } ?></div>
    </div>
  </div>
<?php include_once 'includes/footer.inc.php'; ?>