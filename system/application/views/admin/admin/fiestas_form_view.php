<div id="cuerpo">
<script src="<?=base_url(); ?>js/jquery/jquery.js" type="text/javascript"></script>
<script src="<?=base_url(); ?>js/JSCal2-1.8/src/js/jscal2.js"></script>
<script src="<?=base_url(); ?>js/JSCal2-1.8/src/js/lang/es.js"></script>
<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAA7rkcgy9PNk8KUX29tySlehQ2gKpxVqWRT6wE0XsFNYvAiuTpzxTuLva2-syTq5I_J-decdqo2c8PPA"
      type="text/javascript"></script>
<script type="text/javascript">
	//<![CDATA[
	function TextualZoomControl() {
		}
		TextualZoomControl.prototype = new GControl();
		
		// Creates a one DIV for each of the buttons and places them in a container
		// DIV which is returned as our control element. We add the control to
		// to the map container and return the element for the map class to
		// position properly.
		TextualZoomControl.prototype.initialize = function(map) {
		  var container = document.createElement("div");
		
		  var btnMapa = document.createElement("div");
		  this.setButtonStyle_(btnMapa);
		  container.appendChild(btnMapa);
		  btnMapa.appendChild(document.createTextNode("MAPA"));
		  GEvent.addDomListener(btnMapa, "click", function() {
			var x= map.getMapTypes(); 
    		  map.setMapType(x[0]); 
		  });
		
		  var btnSatelite = document.createElement("div");
		  this.setButtonStyle_(btnSatelite);
		  container.appendChild(btnSatelite);
		  btnSatelite.appendChild(document.createTextNode("SATELITE"));
		  GEvent.addDomListener(btnSatelite, "click", function() {
			var x= map.getMapTypes(); 
    		 map.setMapType(x[1]); 
		  });
		  var btnMixto = document.createElement("div");
		  this.setButtonStyle_(btnMixto);
		  container.appendChild(btnMixto);
		  btnMixto.appendChild(document.createTextNode("MIXTO"));
		  GEvent.addDomListener(btnMixto, "click", function() {
			 var x= map.getMapTypes(); 
    		  map.setMapType(x[2]); 
		  });
		  map.getContainer().appendChild(container);
		  return container;
		}
		
		// By default, the control will appear in the top left corner of the
		// map with 7 pixels of padding.
		TextualZoomControl.prototype.getDefaultPosition = function() {
		  return new GControlPosition(G_ANCHOR_TOP_RIGHT, new GSize(7, 7));
		}
		
		// Sets the proper CSS for the given button element.
		TextualZoomControl.prototype.setButtonStyle_ = function(button) {
		  button.style.textDecoration = "none";
		  button.style.color = "#000000";
		  button.style.backgroundColor = "white";
		  button.style.font = "10px verdana";
		  button.style.border = "1px solid black";
		  button.style.padding = "2px";
		  button.style.marginBottom = "2px";
		  button.style.textAlign = "center";
		  button.style.width = "60px";
		  button.style.cursor = "pointer";
		}

	function load() {
	  if (GBrowserIsCompatible()) {
	   var mipoint = [];
	   var i=0;
	  var map = new GMap2(document.getElementById("map"));
		map.addControl(new GLargeMapControl());
       	map.addControl(new TextualZoomControl());

		map.setCenter(new GLatLng(document.forms.form1.posy.value,document.forms.form1.posx.value ),7 );
		
		 GEvent.addListener(map, "click", function(overlay, point) {
		 	 if (overlay) {
			 }else{
			map.removeOverlay(mipoint[i]);
			i++;
			mipoint[i]=new GMarker(point, icon)
			map.addOverlay(mipoint[i]);
			document.forms.form1.posx.value=point.x;
			document.forms.form1.posy.value=point.y;
			}
        });
		
		var icon = new GIcon();
			icon.image = "<?=base_url(); ?>file/entorno/marcador.png";
			icon.iconSize = new GSize(8, 8);
			//icon.shadowSize = new GSize(22, 20);
			icon.iconAnchor = new GPoint(4, 8);
			icon.infoWindowAnchor = new GPoint(0, 0);
			
		var point = new GLatLng(document.forms.form1.posy.value,document.forms.form1.posx.value );
		 mipoint[i]=new GMarker(point, icon)
		 map.addOverlay( mipoint[i]);
	  
	  
	  }
	}
	
	function Comprobar_chequeados()
	{
		var cadena_texto ="";
		var id_i=0;
		while (document.getElementById("check_list"+id_i)) {
			if (document.getElementById("check_list"+id_i).checked == true){
				cadena_texto+=document.getElementById("check_list"+id_i).value+",";
			}
			id_i++;
		}
		document.getElementById("estilos_id").value=cadena_texto;
	}
		 
	$(document).ready(function(){
		$("#select_pais").change(function(){
			//alert ("cambio"+$(this).val());
			$("#ciudad").load('<?=base_url(); ?>index.php/admin/fiestas/ajax_cargar_ciudades/'+$(this).val());
		});
	});
	
	function poner_punto(posx,posy)
	{
		document.getElementById("posx_flash").value=posx;
		document.getElementById("posy_flash").value=posy;
	}
	
	//]]>
	
</script>
    
<?=$this->session->flashdata('message'); ?>

<?=validation_errors(); ?>

<?=form_open_multipart($accion_form, array("id" => 'form1' ) ); ?>


 	<div class="formularios_titulo">Campos requeridos</div>
    <div class="campos_supertitulo">Información de contacto del productor de la fiesta</div>
    <div class="campos">
        <div class="campos_titulo">Nombre</div>
        <div class="campos_introducir"><?=form_input('pc_nombre',set_value('pc_nombre')=="" ?  $registro['pc_nombre']  :   set_value('pc_nombre') ); ?></div>
    </div>
    <div class="campos">
        <div class="campos_titulo">Teléfono</div>
        <div class="campos_introducir"><?=form_input('pc_telefono',set_value('pc_telefono')=="" ?  $registro['pc_telefono']  :   set_value('pc_telefono') ); ?></div>
    </div>
   	<div class="campos">
        <div class="campos_titulo">Mail</div>
        <div class="campos_introducir"><?=form_input('pc_mail',set_value('pc_mail')=="" ?  $registro['pc_mail']  :   set_value('pc_mail') ); ?></div>
    </div>
    <div class="campos_supertitulo">Información de la fiesta</div>
    <div class="campos">
        <div class="campos_titulo">Título en español</div>
        <div class="campos_introducir"><?=form_input('es_titulo',set_value('es_titulo')=="" ?  $registro['es_titulo']  :   set_value('es_titulo') ); ?></div>
    </div>
    <div class="campos">
        <div class="campos_titulo">Título en ingles</div>
        <div class="campos_introducir"><?=form_input('en_titulo', set_value('en_titulo')=="" ?  $registro['en_titulo']  :   set_value('en_titulo') ); ?></div>
    </div>
        <div class="campos">
        <div class="campos_titulo">Título en holandes</div>
        <div class="campos_introducir"><?=form_input('du_titulo', set_value('du_titulo')=="" ?  $registro['du_titulo']  :   set_value('du_titulo') ); ?></div>
    </div>
    <div class="campos">
        <div class="campos_titulo">Fecha de la fiesta</div>
        <div class="campos_introducir">
        	<div style="width:150px; float:left" id="cont"></div>
        	<?=form_input('f_date', set_value('f_date')=="" ?  $registro['f_date']  :   set_value('f_date'), ' id="f_date" style="text-align: center; width:100px;" readonly="true" ' ); ?>    
        </div>
    </div>
    <div class="campos">
        <div class="campos_titulo">Hora de la fiesta</div>
        <div class="campos_introducir">
        	<?=form_dropdown('hora' ,array('0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23'), 
										set_value('hora')=="" ?  $registro['hora']  :   set_value('hora'), 'style="width:60px;"' ); ?> :
            <?=form_dropdown('minutos' ,array('0','5','10','15','20','25','30','35','40','45','50','55'), 
										set_value('minutos')=="" ?  $registro['minutos']  :   set_value('minutos'), 'style="width:60px;"' ); ?>
        </div>
    </div>
    <div class="campos">
        <div class="campos_titulo">Imagen para la fiesta</div>
        <div class="campos_introducir"><?=form_upload('imagen'); ?></div>
    </div>
     <?php if ($this->uri->segment(3)=='modificar'): ?>
        <div class="campos">
            <img src="<?= $registro['imagen']; ?>" />
        </div>
    <?php endif; ?>
    <div class="campos">
        <div class="campos_titulo">Estilos</div>
        <div class="campos_introducir">
        	 <ul>
				<?php 
                    $cadena_estilo="";
                    $id_i=0;
                    if (count($qry_estilo)>0): 
                ?>
                    <?php foreach($qry_estilo as $qry_fila_estilo): ?>
                    <li>
                        <input onclick="Comprobar_chequeados();" type="checkbox"  name="check_list[]" id="check_list<?=$id_i ?>"  value="<?=$qry_fila_estilo['id']; ?>" <? 
                        if ( count($slct_estilo)>0 )
                        {
                           	foreach($slct_estilo as $slct_fila_estilo)    
                            {  
                                if ($qry_fila_estilo['id']==$slct_fila_estilo){
                                    echo 'checked="checked"';
                                    $cadena_estilo.=$qry_fila_estilo['id'].",";
                                }
                            }
                        }else{
                            echo 'checked="checked"';
                           $cadena_estilo.=$qry_fila_estilo['id'].",";
                        }
                        ?>><?=$qry_fila_estilo['titulo']; ?><input type="hidden" value="<?=$qry_fila_estilo['titulo']; ?>" id="titulo_eslito_<?=$qry_fila_estilo['id']; ?>"  />
                    </li>
                    <? $id_i+=1; ?>
                    <?php endforeach;?>
                <?php endif; ?>       
            </ul>
        </div>
    </div>
    	<input type="hidden" id="estilos_id" name="estilos_id" value="<?=$cadena_estilo; ?>"  /> 
    <div class="campos">
        <div class="campos_titulo">Lugar de la fiesta</div>
        <div class="campos_introducir">
        	<?=form_hidden('posx',set_value('posx')=="" ?  $registro['posx']  :   set_value('posx') ); ?>
			<?=form_hidden('posy',set_value('posy')=="" ?  $registro['posy']  :   set_value('posy') ); ?>
            <div style=" background-color:#777; padding: 5px 5px 5px 5px; width: 393px">
                <div id="map" style=" border:#777; width: 390px; height: 300px; overflow:hidden;"></div>
            </div>
        </div>
    </div>
     <div class="campos">
        <div class="campos_titulo">Pais</div>
        <div class="campos_introducir">
        	<select id="select_pais" name="select_pais"	>
			<?php if ($query->num_rows()>0): ?>
                <?php foreach($query->result() as $row): ?>
                    <option value="<?=$row->id; ?>" <?= ($row->id==$registro['id_pais'])?  'selected="selected"' : ''; ?>><?=$row->es_titulo; ?> / <?=$row->en_titulo; ?></option>
                <?php endforeach;?>
            <?php endif; ?>    
            </select>
        </div>
    </div>
     <div class="campos">
        <div class="campos_titulo">Ciudad</div>
        <div class="campos_introducir" id="ciudad" >
        	<?php if ($query_ciudad->num_rows()>0): ?>
                	<select id="select_ciudad" name="select_ciudad"  >
                    <?php foreach($query_ciudad->result() as $row): ?>
                        <option value="<?=$row->id; ?>" <?= ($row->id==$registro['id_ciudad'])?  'selected="selected"' : ''; ?> ><?=$row->es_titulo; ?> / <?=$row->en_titulo; ?></option>
                    <?php endforeach;?>
                    </select>
                <?php endif; ?>    
        </div>
    </div>
     <div class="campos">
        <div class="campos_titulo">Nueva ciudad</div>
        <div class="campos_introducir">Español: <?=form_input('es_new_ciudad', '' , 'style="width:150px;"' ); ?>  Ingles <?=form_input('en_new_ciudad', '', 'style="width:150px;"' ); ?></div>
    </div>
    <div class="campos_supertitulo">Detalle de la fiesta</div>
    <div class="campos">
        <div class="campos_titulo">Texto en español</div>
        <div class="campos_introducir">
			<?= $ckeditor->editor('es_informacion', set_value('es_informacion')=="" ?  $registro['es_informacion']  :   set_value('es_informacion') ,$ckeditor->config ); ?>
       	</div>
    </div>
    <div class="campos">
        <div class="campos_titulo">Texto en ingles</div>
        <div class="campos_introducir">
        	 <?= $ckeditor->editor('en_informacion', set_value('en_informacion')=="" ?  $registro['en_informacion']  :   set_value('en_informacion') ,$ckeditor->config ); ?>
        </div>
    </div>
    <div class="campos">
        <div class="campos_titulo">Texto en holandes</div>
        <div class="campos_introducir">
        	 <?= $ckeditor->editor('du_informacion', set_value('du_informacion')=="" ?  $registro['du_informacion']  :   set_value('du_informacion') ,$ckeditor->config ); ?>
        </div>
    </div>
     <div class="campos">
        <div class="campos_titulo">Texto de contacto en español</div>
        <div class="campos_introducir">
			<?= $ckeditor->editor('es_contacto', set_value('es_contacto')=="" ?  $registro['es_contacto']  :   set_value('es_contacto') ,$ckeditor->config ); ?>
        </div>
    </div>
     <div class="campos">
        <div class="campos_titulo">Texto de contacto en ingles</div>
        <div class="campos_introducir">
        	<?= $ckeditor->editor('en_contacto', set_value('en_contacto')=="" ?  $registro['en_contacto']  :   set_value('en_contacto') ,$ckeditor->config ); ?>
        </div>
    </div>
      <div class="campos">
        <div class="campos_titulo">Texto de contacto en holandes</div>
        <div class="campos_introducir">
        	<?= $ckeditor->editor('du_contacto', set_value('du_contacto')=="" ?  $registro['du_contacto']  :   set_value('du_contacto') ,$ckeditor->config ); ?>
        </div>
    </div>
   
   
   
   
    <div id="btnsubmit"><?=form_submit('submit', 'Guardar'); ?></div>

<?=form_close(''); ?>      

</div>  



<script type="text/javascript">
	//<![CDATA[
      // this handler is designed to work both for onSelect and onTimeChange
      // events.  It updates the input fields according to what's selected in
      // the calendar.
	  var cto_time=0;
      function updateFields(cal) {
              var date = cal.selection.get();
              if (date) {
                      date = Calendar.intToDate(date);
                      document.getElementById("f_date").value = Calendar.printDate(date, "%Y-%m-%d");
              }
      };
      Calendar.setup({
              cont         : "cont",
			  //selection     : 20100525,
              onSelect     : updateFields,
              onTimeChange : updateFields
      }); 	
    //]]>
</script> 