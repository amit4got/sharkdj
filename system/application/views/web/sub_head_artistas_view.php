 <script src="<?=base_url(); ?>js/jquery/jquery.js" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[	
	function buscar(){
		var id_i=0;
		var ct_check=false;
		while (document.getElementById("check_list"+id_i)) {
			if (document.getElementById("check_list"+id_i).checked == true){
				ct_check=true;
			}
			id_i++;
		}
		if (ct_check==true){
			document.form1.submit();
		}else{
			alert ("Por favor, elija algun estilo musical");
		}
	}
	function aparecer_select_estilos(){
		var obj=document.getElementById("sub_head_clubs_select");
		obj.style.display = "block";
	}
	function desaparecer_select_estilos(){
		var obj=document.getElementById("sub_head_clubs_select");
		obj.style.display = "none";
	}
	function checkAll()
	{
		var id_i=0;
		while (document.getElementById("check_list"+id_i)) {
			document.getElementById("check_list"+id_i).checked = true ;
			document.getElementById("estilos_text").value+=document.getElementById("titulo_eslito_"+document.getElementById("check_list"+id_i).value).value+",";
			id_i++;
		}
		
	}
	
	function uncheckAll()
	{
		var id_i=0;
		while (document.getElementById("check_list"+id_i)) {
			document.getElementById("check_list"+id_i).checked = false ;
			id_i++;
		}
		document.getElementById("estilos_text").value="";
	}
	
	function Comprobar_chequeados()
	{
		var field=document.form1.check_list;
		var cadena_texto ="";
		var id_i=0;
		while (document.getElementById("check_list"+id_i)) {
			if (document.getElementById("check_list"+id_i).checked == true){
				cadena_texto+=document.getElementById("titulo_eslito_"+document.getElementById("check_list"+id_i).value).value+",";
			}
			id_i++;
		}
		
		
		document.getElementById("estilos_text").value=cadena_texto;
		
	}
	function team_poner_off(){
		document.getElementById("team").value="0";
	}
	function team_poner_on(){
		document.getElementById("team").value="1";
	}
	
	
	$(document).ready(function(){
		$("#select_pais").change(function(){
			//alert ("cambio"+$(this).val());
			$("#ciudad").load('<?=base_url(); ?>index.php/web/fiestas/ajax_cargar_ciudades/'+$(this).val());
		});
	});
	
//]]>
</script> 
<form name="form1" id="form1" method="post" action="<?=base_url(); ?>index.php/web/artistas" target="_self" >
<input type="hidden" value="<?=$registro['team']; ?>" id="team" name="team"  />
<div id="sub_head_2" >
    <div id="sub_head_clubs_select">
    	<div id="sub_head_clubs_select_entorno">
     		<div id="sub_head_clubs_select_entorno_botones">
            <a href="#" onclick="checkAll(); return false;">check all</a>
            <a href="#" onclick="uncheckAll(); return false;">uncheck all</a>
            <a href="#" onclick="desaparecer_select_estilos(); return false;">Close</a>
            
    		</div>
            <ul>
                <?php 
					$cadena_estilo="";
					$id_i=0;
					if ($query_estilo->num_rows()>0): ?>
                    <?php foreach($query_estilo->result() as $row): ?>
                    <li>
                        <input onclick="Comprobar_chequeados();" type="checkbox"  name="check_list[]" id="check_list<?=$id_i ?>"  value="<?=$row->id; ?>" <? 
                        if ( isset($select_estilo) )
                        {
                            for ($i=0 ; $i<count($select_estilo) ; $i++)    
                            {  
                                if ($row->id==$select_estilo[$i]){
                                    echo 'checked="checked"';
									$cadena_estilo.=$row->titulo.",";
                                }
                            }
                        }else{
                            echo 'checked="checked"';
							$cadena_estilo.=$row->titulo.",";
                        }
                        ?>><?=$row->titulo; ?><input type="hidden" value="<?=$row->titulo; ?>" id="titulo_eslito_<?=$row->id; ?>"  />
                    </li>
                    <? $id_i+=1; ?>
                    <?php endforeach;?> 
                    <?php endif; ?>  
                
            </ul>
            
        </div>
    </div>
    
    <div id="sub_head_2_cuadro_sup1"><div class="txtbuscarclub">Buscar artistas por nombre</div>
    	<?=form_input('local', $registro['local']=='' ?  ''  :   $registro['local'] ); ?>
    </div>
    <div id="sub_head_2_cuadro_sup2"><div class="txtbuscarclubloca">Buscar artistas por localidad</div>
    	<div id="pais">
    	<select id="select_pais" name="select_pais"	>
			
            <option value="0" <?= (0==$registro['pais'])?  'selected="selected"' : ''; ?>>Seleccione un pais</option>
            
			<?php if ($query_pais->num_rows()>0): ?>
			<?php foreach($query_pais->result() as $row): ?>
                <option value="<?=$row->id; ?>" <?= ($row->id==$registro['pais'])?  'selected="selected"' : ''; ?>><?=$row->es_titulo; ?></option>
            <?php endforeach;?>
            <?php endif; ?>    
        </select>
        </div>
        <div id="ciudad">
            <select id="select_ciudad" name="select_ciudad"   >
            <option value="0">Seleccione una ciudad</option>
			<?php if ($query_ciudad->num_rows()>0): ?>
            <?php foreach($query_ciudad->result() as $row): ?>
                <option value="<?=$row->id; ?>" <?= ($row->id==$registro['ciudad'])?  'selected="selected"' : ''; ?> ><?=$row->es_titulo; ?></option>
            <?php endforeach;?> 
			<?php endif; ?> 
            </select>
        </div>  
    
    </div>
    <div id="sub_head_2_cuadro_inf1"><div class="txtbuscarclubstyle">Buscar artistas por estilo</div>
    	<input type="text" id="estilos_text" name="estilos_text" value="<?=$cadena_estilo; ?>"  disabled="disabled"/> 
        <a href="#" onclick="aparecer_select_estilos(); return false;"><img src="<?=base_url(); ?>file/entorno/select_btn.gif"  alt="select_btn"   ></a>
    		
    </div>
    <div id="sub_head_2_cuadro_inf2">
    	
        <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="287" height="92" id="FlashID" title="buscar">
          <param name="movie" value="<?=base_url(); ?>flash/btn_buscar_arexp.swf?pulsado=<?=$registro['team']; ?>">
          <param name="quality" value="high">
          <param name="wmode" value="opaque">
          <param name="swfversion" value="6.0.65.0">
          <param name="FlashVars" value="pulsado=<?=$registro['team']; ?>" />
          <!-- Esta etiqueta param indica a los usuarios de Flash Player 6.0 r65 o posterior que descarguen la versi�n m�s reciente de Flash Player. Elim�nela si no desea que los usuarios vean el mensaje. -->
          <param name="expressinstall" value="<?=base_url(); ?>js/Scripts/expressInstall.swf">
          <!-- La siguiente etiqueta object es para navegadores distintos de IE. Oc�ltela a IE mediante IECC. -->
          <!--[if !IE]>-->
          <object type="application/x-shockwave-flash" data="<?=base_url(); ?>flash/btn_buscar_arexp.swf?pulsado=<?=$registro['team']; ?>" width="287" height="92">
            <!--<![endif]-->
            <param name="quality" value="high">
            <param name="wmode" value="opaque">
            <param name="swfversion" value="6.0.65.0">
            <param name="FlashVars" value="pulsado=<?=$registro['team']; ?>" />
            <param name="expressinstall" value="<?=base_url(); ?>js/Scripts/expressInstall.swf">
            <!-- El navegador muestra el siguiente contenido alternativo para usuarios con Flash Player 6.0 o versiones anteriores. -->
            <div>
              <h4>El contenido de esta p&aacute;gina requiere una versi&oacute;n m&aacute;s reciente de Adobe Flash Player.</h4>
              <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Obtener Adobe Flash Player" width="112" height="33" /></a></p>
            </div>
            <!--[if !IE]>-->
          </object>
          <!--<![endif]-->
        </object>
        
    </div>
    <div id="sub_head_2_vacio">

		<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="185" height="92" id="FlashID5" title="buscar_artistas">
          <param name="movie" value="<?=base_url(); ?>flash/btn_buscar_artistas.swf">
          <param name="quality" value="high">
          <param name="wmode" value="opaque">
          <param name="swfversion" value="6.0.65.0">
          <!-- Esta etiqueta param indica a los usuarios de Flash Player 6.0 r65 o posterior que descarguen la versi�n m�s reciente de Flash Player. Elim�nela si no desea que los usuarios vean el mensaje. -->
          <param name="expressinstall" value="<?=base_url(); ?>js/Scripts/expressInstall.swf">
          <!-- La siguiente etiqueta object es para navegadores distintos de IE. Oc�ltela a IE mediante IECC. -->
          <!--[if !IE]>-->
          <object type="application/x-shockwave-flash" data="<?=base_url(); ?>flash/btn_buscar_artistas.swf" width="185" height="92">
            <!--<![endif]-->
            <param name="quality" value="high">
            <param name="wmode" value="opaque">
            <param name="swfversion" value="6.0.65.0">
            <param name="expressinstall" value="<?=base_url(); ?>js/Scripts/expressInstall.swf">
            <!-- El navegador muestra el siguiente contenido alternativo para usuarios con Flash Player 6.0 o versiones anteriores. -->
            <div>
              <h4>El contenido de esta p&aacute;gina requiere una versi&oacute;n m&aacute;s reciente de Adobe Flash Player.</h4>
              <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Obtener Adobe Flash Player" width="112" height="33" /></a></p>
            </div>
            <!--[if !IE]>-->
          </object>
          <!--<![endif]-->
        </object>

    </div>
    <div id="sub_head_2_radio">
    
        <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="185" height="92" id="FlashID3" title="radio">
          <param name="movie" value="<?=base_url(); ?>flash/btn_radio.swf">
          <param name="quality" value="high">
          <param name="wmode" value="opaque">
          <param name="swfversion" value="6.0.65.0">
          <!-- Esta etiqueta param indica a los usuarios de Flash Player 6.0 r65 o posterior que descarguen la versi�n m�s reciente de Flash Player. Elim�nela si no desea que los usuarios vean el mensaje. -->
          <param name="expressinstall" value="<?=base_url(); ?>js/Scripts/expressInstall.swf">
          <!-- La siguiente etiqueta object es para navegadores distintos de IE. Oc�ltela a IE mediante IECC. -->
          <!--[if !IE]>-->
          <object type="application/x-shockwave-flash" data="<?=base_url(); ?>flash/btn_radio.swf" width="185" height="92">
            <!--<![endif]-->
            <param name="quality" value="high">
            <param name="wmode" value="opaque">
            <param name="swfversion" value="6.0.65.0">
            <param name="expressinstall" value="<?=base_url(); ?>js/Scripts/expressInstall.swf">
            <!-- El navegador muestra el siguiente contenido alternativo para usuarios con Flash Player 6.0 o versiones anteriores. -->
            <div>
              <h4>El contenido de esta p&aacute;gina requiere una versi&oacute;n m&aacute;s reciente de Adobe Flash Player.</h4>
              <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Obtener Adobe Flash Player" width="112" height="33" /></a></p>
            </div>
            <!--[if !IE]>-->
          </object>
          <!--<![endif]-->
        </object>
    
    </div>
    <div id="sub_head_2_login">
   
   	
        <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="185" height="92" id="FlashID2" title="login">
          <param name="movie" value="<?=base_url(); ?>flash/btn_login.swf">
          <param name="quality" value="high">
          <param name="wmode" value="opaque">
          <param name="swfversion" value="6.0.65.0">
          <!-- Esta etiqueta param indica a los usuarios de Flash Player 6.0 r65 o posterior que descarguen la versi�n m�s reciente de Flash Player. Elim�nela si no desea que los usuarios vean el mensaje. -->
          <param name="expressinstall" value="<?=base_url(); ?>js/Scripts/expressInstall.swf">
          <!-- La siguiente etiqueta object es para navegadores distintos de IE. Oc�ltela a IE mediante IECC. -->
          <!--[if !IE]>-->
          <object type="application/x-shockwave-flash" data="<?=base_url(); ?>flash/btn_login.swf" width="185" height="92">
            <!--<![endif]-->
            <param name="quality" value="high">
            <param name="wmode" value="opaque">
            <param name="swfversion" value="6.0.65.0">
            <param name="expressinstall" value="<?=base_url(); ?>js/Scripts/expressInstall.swf">
            <!-- El navegador muestra el siguiente contenido alternativo para usuarios con Flash Player 6.0 o versiones anteriores. -->
            <div>
              <h4>El contenido de esta p&aacute;gina requiere una versi&oacute;n m&aacute;s reciente de Adobe Flash Player.</h4>
              <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Obtener Adobe Flash Player" width="112" height="33" /></a></p>
            </div>
            <!--[if !IE]>-->
          </object>
          <!--<![endif]-->
        </object>

   
    </div>
    <div id="sub_head_2_world">
    
    	<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="185" height="92" id="FlashID4" title="world">
          <param name="movie" value="<?=base_url(); ?>flash/btn_world.swf">
          <param name="quality" value="high">
          <param name="wmode" value="opaque">
          <param name="swfversion" value="6.0.65.0">
          <!-- Esta etiqueta param indica a los usuarios de Flash Player 6.0 r65 o posterior que descarguen la versi�n m�s reciente de Flash Player. Elim�nela si no desea que los usuarios vean el mensaje. -->
          <param name="expressinstall" value="Scripts/expressInstall.swf">
          <!-- La siguiente etiqueta object es para navegadores distintos de IE. Oc�ltela a IE mediante IECC. -->
          <!--[if !IE]>-->
          <object type="application/x-shockwave-flash" data="<?=base_url(); ?>flash/btn_world.swf" width="185" height="92">
            <!--<![endif]-->
            <param name="quality" value="high">
            <param name="wmode" value="opaque">
            <param name="swfversion" value="6.0.65.0">
            <param name="expressinstall" value="Scripts/expressInstall.swf">
            <!-- El navegador muestra el siguiente contenido alternativo para usuarios con Flash Player 6.0 o versiones anteriores. -->
            <div>
              <h4>El contenido de esta p&aacute;gina requiere una versi&oacute;n m&aacute;s reciente de Adobe Flash Player.</h4>
              <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Obtener Adobe Flash Player" width="112" height="33" /></a></p>
            </div>
            <!--[if !IE]>-->
          </object>
          <!--<![endif]-->
        </object>
    
    </div>
    
    <script type="text/javascript">
		<!--
		swfobject.registerObject("FlashID");
		swfobject.registerObject("FlashID2");
		swfobject.registerObject("FlashID3");
		swfobject.registerObject("FlashID4");
		swfobject.registerObject("FlashID5");
		//-->
	</script>
       
</div>
</form>