<?php
/****************************
*  Created 02/11/11
*  Last update 09/11/11
****************************/
ini_set("display_errors", "E_STRICT");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="expires" content="Sat, 27 May 1978 12:00:00 GMT" />
<meta name="country" content="es" />
<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>SHARK DJ</title>
<link href="shark.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/base/jquery-ui.css" id="theme">
<link rel="stylesheet" href="http://blueimp.github.com/jQuery-Image-Gallery/jquery.image-gallery.css">
<link rel="stylesheet" href="upload/jquery.fileupload-ui.css">
<link rel="stylesheet" href="upload/style.css">
<link rel="icon" type="image/png" href="images/logoSharkDJ.png" />
 
</head>
<body>
<div id="container">
  <div id="header">
    <div id="loginLinks">
      <a href="./inputLogin.php" class="login">Login</a>
      <span class="left fontGrey2 marginTop3">-</span>
      <a href="./inputSignup.php" class="login2">Sign up</a>
      <div id="flag"><img src="images/flags/<?php echo 'es'?>.png" alt="<?php echo 'Espa&ntilde;ol'?>" title="<?php echo 'Espa&ntilde;ol'?>" /></div>
      <?php include 'includes/socialLinks.inc.php'; ?>
    </div>
    <div id="menu">
      <div class="clear insideMenu font1">
        <a href="index.php"><span class="fontRed hoverLargeRed">INICIO</span></a>
        &bull;&nbsp;<a href="artistas.php"><span class="hoverLargeGrey">ARTISTAS</span></a>
        &bull;&nbsp;<a href="locales.php"><span class="hoverLargeGrey">CLUBS</span></a>
        &bull;&nbsp;<a href="eventos.php"><span class="hoverLargeGrey">EVENTOS</span></a>
        &bull;&nbsp;<a href="adminArtists.php"><span class="hoverLargeGrey">ESCUELA</span></a>
      </div>
    </div>
    <div id="logo"></div>
  </div>
