<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*$config['email']['protocol']  = 'smtp';
$config['email']['smtp_host'] = 'ssl://smtp.googlemail.com';
$config['email']['smtp_user'] = '';
$config['email']['smtp_pass'] = '';
$config['email']['smtp_port'] = '465';
$config['email']['mailtype']  = 'html';
$config['email']['charset']   = 'utf-8';*/

$config['email']['protocol'] = 'sendmail';
$config['email']['mailpath'] = '/usr/sbin/sendmail';
$config['email']['charset'] = 'utf-8';
$config['email']['wordwrap'] = TRUE;
$config['email']['mailtype']  = 'html';