<?php if ($query->num_rows()>0): ?>
<ul id="listado">
	<?php foreach($query->result() as $row): ?>
        <li> 
            <div class="username"><?=$row->es_titulo;?></div> 
            <div class="listado_botones"><?=anchor('panel_en/radio/borrar/'.$row->id, 'Delete');?></div>
            <div class="listado_botones"><?=anchor('panel_en/radio/bajar/'.$row->id, 'Down');?></div>
            <div class="listado_botones"><?=anchor('panel_en/radio/subir/'.$row->id, 'Up');?></div>
            <div class="listado_botones"><?=anchor('panel_en/radio/modificar/'.$row->id, 'Modify');?></div>
        </li>
    <?php endforeach;?>
</ul>
<?php endif; ?>
