<?php
class Radio extends Controller {
	function __construct() 
	{
		parent::Controller();
		if ($this->redux_auth->logged_in()==1 ){
			$profile_var = $this->redux_auth->profile();
			if ($profile_var->group!="artistas" && $profile_var->group!="admin"){
				$this->redux_auth->logout();
				redirect('web/login');
			}
		}else{
			$this->redux_auth->logout();
			redirect('web/login');
		}
		
		if ( !function_exists('version_compare') || version_compare( phpversion(), '5', '<' ) ){
			$this->load->library('php4/ckeditor');
			
		}else{
			$this->load->library('php5/ckeditor');
			
		}
		
		$this->load->helper('ckeditor');
		$this->load->helper('file'); 
		$this->load->model('web_fiestas_model');
	}
	function index()
	{
		redirect('panel_en/radio/listado/');
	}
	function listado()
	{
		$this->data['profile']= $this->redux_auth->profile();
		$this->data['head'] = $this->load->view('panel_en/head_view', $this->data, true);
		$this->data['navigation'] = $this->load->view('panel_en/main_menu_view', null, true);
		$this->data['content_menu'] = $this->load->view('panel_en/radio_menu_view', null, true);
		
		$this->db->where('id_artista', $this->data['profile']->id );
		$this->db->order_by("orden","DESC");
		$this->data['query']=$this->db->get('Artista_radio');
		$this->data['content'] =$this->load->view('panel_en/radio_listado_view', $this->data, true); 
		$this->data['foot'] = "";
		$this->load->view('templates/panel_view', $this->data);
		
	}
	function nuevo()
	{
		$this->data['profile']= $this->redux_auth->profile();
		$this->form_validation->set_rules('es_titulo', '"T�tulo en espa�ol"', 'required');
	  $this->form_validation->set_rules('en_titulo', '"T�tulo en ingles"', 'required');
	  $this->form_validation->set_rules('du_titulo', '"T�tulo en holandes"', 'required');
		$this->form_validation->set_rules('ruta_mp3', '"Ruta del mp3"', 'required');
		$this->form_validation->set_rules('es_subtitulo', '"Subtítulo en español"', '');
		$this->form_validation->set_rules('en_subtitulo', '"Subt�tulo en ingles"', '');
		$this->form_validation->set_rules('du_subtitulo', '"Subtt�tulo en holandes"', '');
		
	    $this->form_validation->set_error_delimiters('<p class="error">', '</p>');
	    if ($this->form_validation->run() == false)
	    { 
			
			$this->data['head'] = $this->load->view('panel_en/head_view', $this->data, true);
			$this->data['navigation'] = $this->load->view('panel_en/main_menu_view', null, true);
			$this->data['content_menu'] = $this->load->view('panel_en/radio_menu_view', null, true);
			$this->data['accion_form']='panel_en/radio/nuevo';
			
			$this->data['registro'] = array(
							"es_titulo" => '' , 
							"en_titulo" => '' ,
							"ruta_mp3" => '' , 
							"es_subtitulo" => '' , 
							"en_subtitulo" => '' ,
							"du_subtitulo" => '' );
			
			$ckeditor =  CK_Editor();
			$this->data['ckeditor']= $ckeditor; 
		
	        $this->data['content'] = $this->load->view('panel_en/radio_form_view', $this->data, true);
	       	$this->data['foot'] = "";
			$this->load->view('templates/panel_view', $this->data);
	    }
	    else
	    {
	    	$this->db->select_max('orden');
			$this->db->where('id_artista', $this->data['profile']->id );
			$consulta = $this->db->get('Artista_radio');
			if ($consulta){
				$row = $consulta->row_array();
				$orden= $row['orden']+1;
			} else{
				$orden= 1;
			}
		   
		   	$this->data['registro']['id_artista'] 		= $this->data['profile']->id;
			$this->data['registro']['orden'] 			= $orden;
			$this->data['registro']['es_titulo'] 		= $this->input->post('es_titulo');
			$this->data['registro']['en_titulo'] 		= $this->input->post('en_titulo');
			$this->data['registro']['du_titulo'] 		= $this->input->post('du_titulo');
			$this->data['registro']['es_subtitulo'] 	= $this->input->post('es_subtitulo');
			$this->data['registro']['en_subtitulo'] 	= $this->input->post('en_subtitulo');
			$this->data['registro']['du_subtitulo'] 	= $this->input->post('du_subtitulo');
			$this->data['registro']['ruta_mp3'] 		= $this->input->post('ruta_mp3');
			
		
			
			
			$this->db->set($this->data['registro']); 
			
			if ($this->db->insert('Artista_radio')){
				$this->session->set_flashdata('message', '<p class="notice">La información ha sido insertada.</p>');
	            redirect('panel_en/radio/nuevo/'.$this->uri->segment(4));
			} else
	        {
	            $this->session->set_flashdata('message', '<p class="error">No se ha podido guardar.</p>');
	            redirect('panel_en/radio/nuevo/'.$this->uri->segment(4));
	        }
	    }	
		
		
	}
	function modificar()
	{
		$this->form_validation->set_rules('es_titulo', '"T�tulo en espa�ol"', 'required');
	  $this->form_validation->set_rules('en_titulo', '"T�tulo en ingles"', 'required');
	  $this->form_validation->set_rules('du_titulo', '"T�tulo en holandes"', 'required');
		$this->form_validation->set_rules('ruta_mp3', '"Ruta del mp3"', 'required');
		$this->form_validation->set_rules('es_subtitulo', '"Subt�tulo en espa�ol"', '');
		$this->form_validation->set_rules('en_subtitulo', '"Subt�tulo en ingles"', '');
		$this->form_validation->set_rules('du_subtitulo', '"Subt�tulo en holandes"', '');
		$this->data['profile']= $this->redux_auth->profile();
	    $this->form_validation->set_error_delimiters('<p class="error">', '</p>');
	    if ($this->form_validation->run() == false)
	    { 
			
			
			$this->data['head'] = $this->load->view('panel_en/head_view', $this->data, true);
			$this->data['navigation'] = $this->load->view('panel_en/main_menu_view', null, true);
			$this->data['content_menu'] = $this->load->view('panel_en/radio_menu_view', null, true);
	        
			$this->data['accion_form']='panel_en/radio/modificar/'.$this->uri->segment(4);
			
			$this->db->where('id_artista', $this->data['profile']->id );
			$this->db->where('id', $this->uri->segment(4) );
			$query=$this->db->get('Artista_radio');
			$this->data['errores']='';
			$ckeditor =  CK_Editor();
			$this->data['ckeditor']= $ckeditor; 
			foreach ($query->result() as $row)
			{
				
				
				$this->data['registro'] = array(
											"es_titulo" 		=> $row->es_titulo , 
											"en_titulo" 		=> $row->en_titulo ,
											"du_titulo" 		=> $row->du_titulo ,
											"es_subtitulo" 		=> $row->es_subtitulo , 
											"en_subtitulo" 		=> $row->en_subtitulo ,
											"du_subtitulo" 		=> $row->du_subtitulo ,
											"ruta_mp3" 			=> $row->ruta_mp3 );
			}
			if (!isset($this->data['registro'])){
				 redirect('panel_en/radio/listado');
			}
			$this->data['content'] = $this->load->view('panel_en/radio_form_view', $this->data, true);
	       	$this->data['foot'] = "";
			$this->load->view('templates/panel_view', $this->data);
	    }
	    else
	    {
	        
			
			$update = array(
							"es_titulo" 		=> $this->input->post('es_titulo') , 
							"en_titulo" 		=> $this->input->post('en_titulo') ,
							"du_titulo" 		=> $this->input->post('du_titulo') ,
							"es_subtitulo" 		=> $this->input->post('es_subtitulo') , 
							"en_subtitulo" 		=> $this->input->post('en_subtitulo') ,
							"du_subtitulo" 		=> $this->input->post('du_subtitulo') ,
							"ruta_mp3" 			=> $this->input->post('ruta_mp3'));
			
			$this->db->where('id', $this->uri->segment(4) );
			$this->db->where('id_artista', $this->data['profile']->id );
			if ($this->db->update("Artista_radio",$update)){
				$this->session->set_flashdata('message', '<p class="notice">La información ha sido modifica.</p>');
	            redirect('panel_en/radio/modificar/'.$this->uri->segment(4));
			} else
	        {
	            $this->session->set_flashdata('message', '<p class="error">No se ha podido modificar.</p>');
	            redirect('panel_en/radio/modificar/'.$this->uri->segment(4));
	        }
	    }	
	}
	function borrar()
	{
		$this->data['profile']= $this->redux_auth->profile();
		$this->data['head'] = $this->load->view('panel_en/head_view', $this->data, true);
		$this->data['navigation'] = $this->load->view('panel_en/main_menu_view', null, true);
		$this->data['content_menu'] = $this->load->view('panel_en/radio_menu_view', null, true);
		
		
		$this->db->where('id', $this->uri->segment(4 ) );
		$this->db->where('id_artista', $this->data['profile']->id );
		$query=$this->db->get('Artista_radio');
		foreach ($query->result() as $row)
		{
			$this->data['es_titulo'] =$row->es_titulo;
		}
		
		$this->data['content'] =$this->load->view('panel_en/radio_borrar_view', $this->data, true); ;
		$this->data['foot'] = "";
		$this->load->view('templates/panel_view', $this->data);
	}
	function borrar_confirm()
	{
		$this->data['profile']= $this->redux_auth->profile();
		$this->db->where('id_artista', $this->data['profile']->id );
		$this->db->where('id', $this->uri->segment(4) );
		$query=$this->db->get('Artista_radio');
		if ($query->num_rows() > 0){
				$this->db->where('id', $this->uri->segment(4) );
				$this->db->delete('Artista_radio'); 
		}
		redirect('panel_en/radio/listado/');
	}
	function subir()
	{
		$this->data['profile']= $this->redux_auth->profile();
		$this->db->where('id_artista', $this->data['profile']->id );
		$this->db->where('id', $this->uri->segment(4) );
		$consulta = $this->db->get('Artista_radio');
		if ($consulta->num_rows() > 0){
			$row   = $consulta->row_array();
			$orden = $row['orden'];
			$this->db->where('orden >', $orden );	
			$this->db->order_by("orden","ASC");
			$consulta_cambio = $this->db->get('Artista_radio');
			if ($consulta_cambio->num_rows() > 0){
				$row_cambio   = $consulta_cambio->row_array();
				$orden_cambio = $row_cambio['orden'];
				$id_cambio    = $row_cambio['id'];
				
				$update = array("orden" => $orden_cambio );
				$this->db->where('id', $this->uri->segment(4) );
				$this->db->update("Artista_radio",$update);
				
				$update = array("orden" => $orden );
				$this->db->where('id', $id_cambio );
				$this->db->update("Artista_radio",$update);
					
			}
		} 
		redirect('panel_en/radio/listado/');
	}
	function bajar()
	{
		$this->data['profile']= $this->redux_auth->profile();
		$this->db->where('id_artista', $this->data['profile']->id );
		$this->db->where('id', $this->uri->segment(4) );
		$consulta = $this->db->get('Artista_radio');
		if ($consulta->num_rows() > 0){
			$row   = $consulta->row_array();
			$orden = $row['orden'];
			
			$this->db->where('orden <', $orden );	
			$this->db->order_by('orden','DESC');
			$consulta_cambio = $this->db->get('Artista_radio');
			if ($consulta_cambio->num_rows() > 0){
				$row_cambio   = $consulta_cambio->row_array();
				$orden_cambio = $row_cambio['orden'];
				$id_cambio    = $row_cambio['id'];
				
				$update = array("orden" => $orden_cambio );
				$this->db->where('id', $this->uri->segment(4) );
				$this->db->update("Artista_radio",$update);
				
				$update = array("orden" => $orden );
				$this->db->where('id', $id_cambio );
				$this->db->update("Artista_radio",$update);
					
			}
		} 
		redirect('panel_en/radio/listado/');
	}
	
}


