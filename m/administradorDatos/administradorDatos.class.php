<?php

if(!defined("RUTA_ABSOLUTA")){
  define("RUTA_ABSOLUTA",dirname(__FILE__));
}

require_once RUTA_ABSOLUTA.'/config/constants.php';
require_once RUTA_ABSOLUTA.'/database/insert.class.php';
require_once RUTA_ABSOLUTA.'/database/database.class.php';


class administradorDatos {

  private $insert;
  private $bd;
  
  public function __construct() {
    $this->insert = new insert();
    $this->bd = new database();
  }
  
  public function insertaUsuario($username, $password, $email, $idPerfil=2, $activo=0) {
    $idUser="";
    // idPerfil mayor que 1 porque no vamos a permitir añadir usuarios admin. Se hace manualmente ;)
    if ($username!="" && $password!="" && $email!="" && $idPerfil>1) {
      $idUser = $this->insert->inserta("INSERT INTO users (userName, password, email, idPerfil, activo) VALUES
      														('%s', MD5('%s'), '%s', %i, %i)", array($username, $password, $email, $idPerfil, $activo));
    }
    return $idUser;
  }
  
  public function  obtenerUsuario($idUser){
    if ($idUser>0) {
      $resultado = $this->bd->query("SELECT idUser, userName, email, idPerfil, activo FROM users WHERE idUser='%s'", array($idUser));
    }
    return isset($resultado[0]) ? $resultado[0] : 0;
  }
    
  public function existeUsuario($idUser) {
    if ($idUser!="" && $idUser>0) {
      $resultado = $this->bd->query("SELECT idUser FROM users WHERE idUser=%i", array($idUser));
    }
    return isset($resultado[0]['idUser']) ? $resultado[0]['idUser'] : 0;
  }
  
  
  public function existeUserName($username) {  	
  	if ($username!="") {
  		$resultado = $this->bd->query("SELECT idUser FROM users WHERE userName LIKE '%s'", array($username)); 
  	}
  	return isset($resultado[0]) ? $resultado[0]['idUser'] : 0;
  }

  public function existeEmail($email) {
    if ($email!="") {
    		$resultado = $this->bd->query("SELECT idUser FROM users WHERE email LIKE '%s'", array($email));
    }
    return isset($resultado[0]) ? $resultado[0]['idUser'] : 0;
  }
  
  public function cambiaEstadoActivo($idUser,$activo=0){
  	if ($idUser>0) {
  		$this->insert->inserta("UPDATE users SET activo=%i WHERE IDUser=%i", array($activo,$idUser));
  	}
  }
  
  public function existeCategoria($idCategoria) {
    if ($idCategoria>0) {
      $resultado = $this->bd->query("SELECT idCategoria FROM categorias WHERE idCategoria=%i", array($idCategoria));
    }
    return (isset($resultado[0]['idCategoria'])) ? $resultado[0]['idCategoria'] : 0;
  }
  
  public function existeRedSocial($nombre) {
  	if ($nombre!="") {
  		$resultado = $this->bd->query("SELECT idRed FROM redes_sociales WHERE nombre LIKE '%s'", array($nombre));
  	}
  	return (isset($resultado['idRed'])) ? $resultado['idRed'] : 0;
  }
  
  public function insertaRedSocial($nombre) {
  	$idRed="";
  	if ($nombre!="" && !$this->existeRedSocial($nombre)) {
  		$idRed = $this->insert->inserta("INSERT INTO redes_sociales (nombre) VALUES ('%s')", array($nombre));
  	}
  	return $idRed;
  }
  
  public function obtenerArtistas($visible="",$team="") {
  	return $this->bd->query("SELECT idUser FROM artistas WHERE 1=1 [AND visible=%I] [AND team=%I]", array($visible,$team));
  }
  
  public function obtenerArtistasInfo($numArtistas=999999, $offset=0, $visible="",$team="") {
  	return $this->bd->query("SELECT u.idUser, u.userName, u.password, u.email, u.idPerfil, u.activo, a.nombre, a.apellido1,
  		                           a.apellido2, a.direccion, a.cp, a.ciudad, a.pais, a.telefono, a.movil, a.lat, a.lng, a.bio, 
  		                           a.web, a.clubResidencia, a.visible, a.team 
  		                           FROM users u, artistas a WHERE u.idUser=a.idUser [AND a.visible=%I] [AND a.team=%I] ORDER BY a.team DESC", array($visible,$team), $offset, $numArtistas);
  }

  
  public function obtenerIdUsuario($email, $password) {  	
  	if ($email!="" && $password!="") {
  		$resultado = $this->bd->query("SELECT idUser, idPerfil FROM users WHERE email='%s' AND password=MD5('%s')", array($email, $password));  		
  	}
  	return (isset($resultado[0])) ? $resultado[0] : 0;
  }
  
  public function introduceNoticia($datos) {
  	$idUser = isset($datos['idUser']) ? $datos['idUser'] : 0;
  	$titulo = isset($datos['titulo']) ? $datos['titulo'] : "";
  	$texto = isset($datos['texto']) ? $datos['texto'] : "";
  	$fecha = isset($datos['fecha']) ? $datos['fecha'] : "";
  	$idCategoria = isset($datos['idCategoria']) ? $datos['idCategoria'] : 0;
  	
  	if ($this->existeUsuario($idUser) && $titulo!="" && $texto!="" && $fecha!="" && $this->existeCategoria($idCategoria)) {  	  
  	  $idRed = $this->insert->inserta("INSERT INTO post (idUser,titulo,texto,fecha,idCategoria) VALUES (%i,'%s','%s','%s',%i)", array($idUser,$titulo,$texto,$fecha,$idCategoria));
  	  
  	}
  }
  
  public function obtenerNoticias($numNoticias=999999, $offset=0, $idUser="", $idCategoria="",$fechaMaxima="") {
  	return $this->bd->query("SELECT p.idPost, p.idUser, u.userName as nombreUsuario, p.titulo, p.texto, DATE_FORMAT(p.fecha,'%s') as fecha, c.nombre as nombreCategoria 
  	                        FROM post p, categorias c, users u 
  	                        WHERE p.idCategoria=c.idCategoria AND p.idUser=u.idUser [AND p.idUser=%I] [AND c.idCategoria=%I] [(p.fecha <= NOW())=%I]
  	                        GROUP BY p.idPost ORDER BY p.fecha DESC "
  							, array('%d/%c/%Y',$idUser,$idCategoria, $fechaMaxima), $offset, $numNoticias);
  }
  
  public function obtenerNoticia($idPost){
    return $this->bd->query("SELECT p.idPost, p.idUser, u.userName as nombreUsuario, p.titulo, p.texto, DATE_FORMAT(p.fecha,'%s') as fecha, c.nombre as nombreCategoria
      	                        FROM post p, categorias c, users u 
      	                        WHERE p.idPost=%I AND p.idUser=u.idUser"
    , array('%d/%c/%Y',$idPost));
  }
  
  public function buscarNoticias($texto, $numNoticias=999999, $offset=0) {
  	return $this->bd->query("SELECT p.idPost, p.idUser, u.userName as nombreUsuario, p.titulo, p.texto, DATE_FORMAT(p.fecha,'%s') as fecha, c.nombre as nombreCategoria
  	  	                        FROM post p, categorias c, users u 
  	  	                        WHERE p.idCategoria=c.idCategoria AND p.idUser=u.idUser AND (p.titulo LIKE '%%s%' OR p.texto LIKE '%%s%')  
  	  	                        GROUP BY p.idPost ORDER BY p.fecha DESC "
  	, array('%d/%c/%Y',$texto, $texto), $offset, $numNoticias);
  }
  
  public function obtenerLocales($visible="") {
    return $this->bd->query("SELECT idUser FROM locales WHERE 1=1 [AND visible=%I]", array($visible));
  }
  
  public function obtenerLocalesInfo($numLocales=999999, $offset=0,$visible="") {
    return $this->bd->query("SELECT u.idUser, u.userName, u.password, u.email, u.idPerfil, u.activo, l.nombre,
    		                           l.direccion, l.ciudad, l.pais, l.lat, l.lng, l.visible, l.descripcion    		                           
    		                           FROM users u, locales l WHERE u.idUser=l.idUser [AND l.visible=%I]", array($visible), $offset, $numLocales);
  }
    
  public function buscarArtistas($nombre="",$lugar="",$estilo="",$numArtistas=999999, $offset=0) {
    return $this->bd->query("SELECT u.idUser, u.userName, u.password, u.email, u.idPerfil, u.activo, a.nombre, a.apellido1,
      		                           a.apellido2, a.direccion, a.cp, a.ciudad, a.pais, a.telefono, a.movil, a.lat, a.lng, a.bio, 
      		                           a.web, a.clubResidencia, a.visible, a.team 
      		                           FROM users u, artistas a WHERE u.idUser=a.idUser AND a.visible=1 AND u.activo=1
    																 [AND a.nombre='%S'] [AND a.direccion LIKE '%%S%'] [AND a.estilo LIKE '%%S%']", array($nombre,$lugar,$estilo), $offset, $numArtistas);
  }
  
  public function buscarLocales($nombre="",$lugar="",$estilo="",$numLocales=999999, $offset=0) {
    return $this->bd->query("SELECT u.idUser, u.userName, u.password, u.email, u.idPerfil, u.activo, l.nombre,
    		                           l.direccion, l.ciudad, l.pais, l.lat, l.lng, l.visible, l.descripcion    		                           
    		                           FROM users u, locales l WHERE u.idUser=l.idUser AND l.visible=1 AND u.activo=1
      														 [AND l.nombre='%S'] [AND l.direccion LIKE '%%S%'] [AND l.estilo LIKE '%%S%']", array($nombre,$lugar,$estilo), $offset, $numLocales);
  }
  
  public function obtenerEventos($numEventos=999999,$offset=0,$now="",$visible="") {
  	return $this->bd->query("SELECT e.idEvento, e.nombre, e.direccion, e.fechaInicio, e.visible, e.descripcion, e.precio
    	    		             FROM eventos e WHERE 1=1 [AND e.fechaInicio>=NOW()=%I] [AND e.visible=%I]
    							 ORDER BY e.fechaInicio DESC", array($now,$visible), $offset, $numEventos);
  }
  
  public function obtenerEvento($idEvento) {
  	$resultado = $this->bd->query("SELECT e.idEvento, e.idUser, e.nombre, e.direccion, e.fechaInicio, e.visible, e.descripcion, e.precio
      	    		             FROM eventos e WHERE idEvento=%i", array($idEvento)); 
  	return (isset($resultado[0])) ? $resultado[0] : array();
  }
}
