<?php

if(!defined("RUTA_ABSOLUTA")){
  define("RUTA_ABSOLUTA",dirname(__FILE__));
}

require_once RUTA_ABSOLUTA.'/config/constants.php';
require_once RUTA_ABSOLUTA.'/database/insert.class.php';
require_once RUTA_ABSOLUTA.'/database/database.class.php';


class user {
	
	protected $insert;
	protected $bd;
	
	public function __construct() {	
		$this->insert = new insert();
		$this->bd = new database();
	}
	
	public function existeUser($idUser) {
		if ($idUser!="" && $idUser>0) {
			$resultado = $this->bd->query("SELECT idUser FROM users WHERE idUser=%i", array($idUser));
		}
		return isset($resultado[0]['idUser']) ? $resultado[0]['idUser'] : 0;
	}
	
	public function existeRedSocial($idUser,$idRed) {
		if ($idUser>0 && $idRed>0) {
			$resultado = $this->bd->query("SELECT idUser,idRed FROM redes_users WHERE idUser=%i AND idRed=%i", array($idUser,$idRed));
		}
		return isset($resultado[0]) ? $resultado[0] : array();
	}
	
	public function insertaRedSocial($idUser, $idRed, $url, $token="") {
		if ($this->existeUser($idUser) && $idRed>0 && $url!="") {
			$existeRed = $this->bd->query("SELECT idRed FROM redes_sociales WHERE idRed=%i", array($idRed));
			if (count($existeRed)>0) {
				if ($this->existeRedSocial($idUser, $idRed)) {
					$this->insert->inserta("UPDATE redes_users SET url='%s',token='%s' WHERE idUser=%i AND idRed=%i",
					array($url,$token,$idUser,$idRed));
				}else{
					$this->insert->inserta("INSERT INTO redes_users (idUser,idRed,url,token) VALUES (%i,%i,'%s','%s')",
					array($idUser,$idRed,$url,$token));
				}
			}
		}
	}
	
	public function obtenerRedesSociales($idUser, $idRed="") {
		if ($this->existeUser($idUser)) {
			$resultado = $this->bd->query("SELECT idUser, idRed, url, token FROM redes_users
	  									WHERE idUser=%i [AND idRed=%I]", array($idUser,$idRed));  		
		}
		return isset($resultado[0]) ? $resultado : array();
	}
	
	public function cambiaPassword($idUser, $password) {
		if ($this->existeUser($idUser) && $password!="") {
			$resultado = $this->insert->inserta("UPDATE users SET password = MD5('%s') WHERE idUser=%i", array($password,$idUser));
		}
	}
	
	public function cambiaEmail($idUser, $email) {
		if ($this->existeUser($idUser) && $email!="") {
			$resultado = $this->insert->inserta("UPDATE users SET email='%s' WHERE idUser=%i", array($email,$idUser));
		}
	}
	
	public function insertaEvento($datos) {
		
	}
	
	public function obtenerEventos($idUser, $fecha="") {
		
	}
	
	public function obtenerEvento($idEvento) {
		
	}
}