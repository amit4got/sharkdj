<?php
/****************************
 *  Created 26/06/11
 *  Last update 25/12/11   
 ****************************/

#INCLUDES
if(!defined("RUTA_ABSOLUTA")){
	define("RUTA_ABSOLUTA",dirname(__FILE__));
}

require_once RUTA_ABSOLUTA.'/administradorDatos/administradorDatos.class.php';
require_once RUTA_ABSOLUTA.'/administradorDatos/artist.class.php';
require_once RUTA_ABSOLUTA.'/administradorDatos/imagenes.class.php';

$pagina = isset($_GET['p']) ? $_GET['p'] : 0;
$searchNombre = isset($_GET['n']) ? $_GET['n'] : "";
$searchLugar = isset($_GET['l']) ? $_GET['l'] : "";
$searchEstilo = isset($_GET['e']) ? $_GET['e'] : "";
$enlacePag = "./artistas.php?";

$listaArtistas="";
$adminDatos = new administradorDatos();
#Load languages
include RUTA_ABSOLUTA.'/includes/lang.inc.php';
$arrayIdiomas = $adminDatos->cargaIdioma($idioma);
$langURL = '';
if ($idioma != 'es') {
  $langURL = '&lang=' . $idioma;
}
if ($searchNombre!="" || $searchLugar!="" || $searchEstilo!="") {
  $enlacePag .= "n=".$searchNombre."&l=".$searchLugar."&e=".$searchEstilo."&";
  $numTotalArtistas = count($adminDatos->buscarArtistas($searchNombre,$searchLugar,$searchEstilo));
  $arrayArtistas = $adminDatos->buscarArtistas($searchNombre,$searchLugar,$searchEstilo, 15, $pagina*15);
}else{
  $numTotalArtistas = count($adminDatos->obtenerArtistas(1));
  $arrayArtistas = $adminDatos->obtenerArtistasInfo(15, $pagina*15, 1);
}
$numArtistas = count($arrayArtistas);
if ($numArtistas>0){
	$i=0;
	foreach ($arrayArtistas as $artista) {
		$i = ($i>=3) ? 0 : $i;
		if ($i==0) {
			$listaArtistas.=<<<EOF
    		<div class="row">
      		<div class="insideRow">  
EOF;
		}
		$idUser = $artista['idUser'];
		$imagenUrl = imagenes::obtenerImagenUsuario($idUser);
		$noCache = '?nocache='.date("isu");
		$imagenShark = ($artista['team']) ? "<img class=\"artistSharkImg\" src=\"images/emblema_sharkdj_artistas.png\" width=\"42px\" height=\"41px\" />" : "";
		$listaArtistas.=<<<EOF
		    <a class="noDecoration" href="perfilArtista.php?id={$idUser}{$langURL}">
        	  <div class="box6">
          	    <img src="{$imagenUrl}{$noCache}" />
                <div class="left artistName">
                  <span class="left marginTop7 marginLeft5 font1 fontDarkGrey">{$artista['nombre']} {$artista['apellido1']}</span>
                  {$imagenShark}
                </div>
              </div>
            </a>
EOF;
		if ($i==2) {
			$listaArtistas.=<<<EOF
          </div>
        </div>
EOF;
		}
		$i++;
	}
	$i = ($i>=3) ? 0 : $i;
	if ($i>0) {
		for($i; $i<3; $i++) {
			$listaArtistas.=<<<EOF
                  <div class="box6">
                    <a href="#"></a>                    
                  </div>
EOF;
		}
		$listaArtistas.=<<<EOF
              </div>              
            </div>
EOF;

	}
} else {
    $listaArtistas='<div class="newsInfoTitle">No hemos encontrado resultados a la búsqueda.</div>
                    <br /><br /><br />
                    <div class="right"><a href="javascript:history.go(-1)" class="noDecoration fontRed fontBold hoverLargeGrey">Volver a la página anterior</a></div>';
}

include_once 'includes/header.inc.php';
srand();
$publicidades = $adminDatos->obtenerPublicidad();
if (count($publicidades) < 6){
  $claves = array_rand($publicidades,count($publicidades)); 
}else{
  $claves = array_rand($publicidades,6); 

}
  shuffle($claves);
?>
  <div id="main">
    <div id="searchBar">
      <form method="get" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <label class="left fontWhite marginTop5 marginLeft5"><?=$arrayIdiomas['name'];?>:</label>
        <input type="text" name="n" class="left searchText2 noBorder fontGrey fontBold marginLeft3" value="" />
        <label class="left fontWhite marginTop5 marginLeft5"><?=$arrayIdiomas['place'];?>:</label>
        <input type="text" name="l" class="left searchText2 noBorder fontGrey fontBold marginLeft3" value="" />
        <label class="left fontWhite marginTop5 marginLeft5"><?=$arrayIdiomas['style'];?>:</label>
        <input type="text" name="e" class="left searchText2 noBorder fontGrey fontBold marginLeft3" value="" />
        <input type="image" name="submit" class="right marginTop2 marginRight7" id="searchButton" value="" />
		<input type="hidden" name="lang" value="<?=$idioma;?>" />
      </form>
    </div>
    <div class="advertisementRow3"><?php
      foreach($claves as $c){
        echo '<br><a href="'.$publicidades[$c]['url'].'"><img src="http://www.sharkdj.com/'. $publicidades[$c]['imagen'] .'" alt="' . $arrayIdiomas['ads'] . '" title="' . $arrayIdiomas['ads'] . '" /></a><br>';
      }?>
    </div>
    <div class="column2">
    <?php echo $listaArtistas;?>
    </div>
    <div id="nextPreviousMenu" class="clear left insideRow">
      <div class="left width33"><?php if ($pagina>0) { ?><a href="<?php echo $enlacePag;?>p=<?php echo ($pagina-1).$langURL;?>" class="noDecoration fontGrey2 fontBold hoverLargeGrey">&laquo; <?=$arrayIdiomas['previousMenu'];?></a><?php }?><a class="noDecoration fontGrey2 fontBold hoverLargeGrey">&nbsp;</a></div>
      <div class="left width33 textCentered marginLeft5"><a href="<?php echo $enlacePag;?>p=0<?=$langURL;?>" class="noDecoration fontGrey2 fontBold hoverLargeGrey"><?=$arrayIdiomas['homeMenu'];?></a></div>        
      <div class="right"><?php if (($pagina+1)*15 < $numTotalArtistas) {?><a href="<?php echo $enlacePag;?>p=<?php echo ($pagina+1).$langURL;?>" class="fontBold noDecoration fontGrey2 hoverLargeGrey"><?=$arrayIdiomas['nextMenu'];?> &raquo;</a><?php } ?></div>
    </div>
  </div>

<?php include_once 'includes/footer.inc.php'; ?>
