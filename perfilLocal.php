<?php
/****************************
 *  Created 11/11/11
 *  Last update 11/11/11   
 ****************************/
if(!defined("RUTA_ABSOLUTA")){
  define("RUTA_ABSOLUTA",dirname(__FILE__));
}

require_once RUTA_ABSOLUTA.'/administradorDatos/administradorDatos.class.php';
require_once RUTA_ABSOLUTA.'/administradorDatos/local.class.php';
require_once RUTA_ABSOLUTA.'/administradorDatos/imagenes.class.php';
#INCLUDES
require_once 'includes/connect.ini.php';
require_once 'includes/functions.ini.php';

$adminDatos = new administradorDatos();
#Load languages
include RUTA_ABSOLUTA.'/includes/lang.inc.php';
$arrayIdiomas = $adminDatos->cargaIdioma($idioma);
$langURL = '';
if ($idioma != 'es') {
  $langURL = '&lang=' . $idioma;
}

// session_start();

// if ($_SESSION['status'] != "authorized") {
//   header ("Location: login.php");
//   exit();
// }
include_once 'includes/headerArtista.inc.php';

#funciones
function leerDirectorio($idUser)
{
  $dir='upload/php/files/'.$idUser.'/thumbnails/';
  $directorio=opendir($dir);
  while ($archivo = readdir($directorio)){
    $imagenes[]=$archivo;
  }
  closedir($directorio);
  return $imagenes;
}

function limpiaLTGT($string){
  $string = preg_replace('/\&lt;/','<', $string);
  $string = preg_replace('/\&gt;/', '>', $string);
  return $string;
}

?>
<div id="fb-root"></div>
<script style="text/javascript">(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/es_LA/all.js#xfbml=1&appId=142309935866555";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
</script>
<?php 
$idUser = $_GET['iu'];
$imagenUrl = imagenes::obtenerImagenUsuario($idUser);
$local = new local();
$datos = $local->obtenerDatosLocal($idUser);
$redes = $local->obtenerRedesSociales($idUser);



foreach ($redes as $red){
  switch ($red['idRed']) {
    case '1':
      $facebook = $red['url'];
      break;
    case '2':
      $twitter = $red['url'];
      break;
    case '3':
      $soundcloud = $red['url'];
      break;
    case '4':
      $youtube = $red['url'];
      break;
    case '5':
      $myspace = $red['url'];
      break;
    default:
      ;
      break;
  }
}

srand();
$publicidades = $adminDatos->obtenerPublicidad();
if (count($publicidades) < 6){
  $claves = array_rand($publicidades,count($publicidades)); 
}else{
  $claves = array_rand($publicidades,6); 
 
}
  shuffle($claves);
?>
  <div id="main">
    <div class="insideRow3 bkgdGrey fontBold">
      <div class="box7">
        <div class="artistPhoto2"><img src="<?php echo $imagenUrl;?>" width="140" heigth="140"/></div>
        <span class="fontRed font3"><?php echo $datos['nombre']?></span>
        <br />
        <br />
        <span class="fontWhite"><?=$arrayIdiomas['country'];?>:<br></span>
        <span class="fontBlack"><?php echo $campo = isset($datos['pais'])? $datos['pais']:''?></span>
        <br />
<!--    <span class="fontWhite">Contacto:</span>
        <span class="fontBlack">XXX</span>
        <br /> -->
        <?php $web = isset($datos['web'])? str_replace('http://','',$datos['web']):''?>
				<span class="fontWhite">Website:<br /></span>
        <span><br><a href="http://<?=$web;?>" class="website"><?echo str_replace('www.','',$web);?></a></span>
        <div class="clear left">
          <div id="socialLinks">
            <a href="http://<?php echo $facebook;?>" class="left width24px marginLeft5" id="linkFA"></a>
            <a href="http://soundcloud.com/<?php echo $soundcloud;?>" class="left width24px marginLeft5" id="linkTW"></a>
            <a href="http://www.twitter.com/<?php echo $twitter;?>" class="left width24px marginLeft5" id="linkMY"></a>
            <a href="http://www.myspace.com/<?php echo $myspace; ?>" class="left width24px marginLeft5" id="linkSO"></a>
            <a href="http://www.youtube.com/<?php echo $youtube;?>" class="left width24px marginLeft5" id="linkYO"></a>
          </div>
        </div>
        <?php $imagenes = leerDirectorio($datos['idUser']);?>
        <div class="box8"><?php 
          foreach ($imagenes as $imagen){
            if (preg_match('/(gif|jpe?g|png)$/i', $imagen)){
              echo '<div class="thumb"><a class="group1" href="upload/php/files/'.$datos['idUser'].'/'.$imagen.'"><img src="upload/php/files/'.$datos['idUser'].'/thumbnails/'.$imagen.'" /></a></div>';
            }
          }
        ?></div>
      </div>
      <div class="box7">
      	<div id="map_canvas" style="height:280px;top:2px">
          <script type="text/javascript">
          function placeMarketArtist() {
            <?php if ($datos['lat'] !="" && $datos['lng'] !="") {?>
        	  placeMarkerFromLatLng('<?php echo $datos['lat'];?>','<?php echo $datos['lng'];?>');
        	  <?php } ?>
          }
      		</script>
      	</div>
      </div>
     </div>
     <div class="advertisementRow3"><?php
    foreach($claves as $c){
     echo '<br><a href="'.$publicidades[$c]['url'].'"><img src="http://www.sharkdj.com/'. $publicidades[$c]['imagen'] .'" alt="' . $arrayIdiomas['ads'] . '" title="' . $arrayIdiomas['ads'] . '" /></a><br>';
  }?></div>
     <div class="box16 textJustified">
        <span class="fontRed font3"><?=$arrayIdiomas['descriptions'];?></span>
        <br />
        <br />
        <span class="fontGrey"><?php echo strip_tags(limpiaLTGT($datos['bio']),'<br><b><p>');?></span>
        <br />
        <br />
      </div>

      <div class="insideRow2 fontBold bkgrdBlueRepeat">
                <div class="box13"><!-- BEGIN: Twitter for Web widget (http://twitterforweb.com) -->
<div style="width:350px;font-size:8px;text-align:right;"><script src="http://widgets.twimg.com/j/2/widget.js"></script>
<script>
new TWTR.Widget({
  version: 2,
  type: 'profile',
  rpp: 4,
  interval: 30000,
  width: 350,
  height: 200,
  theme: {
    shell: {
        background: '#cccccc',
        color: '#080808'
      },
      tweets: {
        background: '#ededed',
        color: '#000000',
        links: '#e01616'
      }
  },
  features: {
    scrollbar: true,
    loop: false,
    live: false,
    behavior: 'all'
  }
}).render().setUser('<?echo $twitter;?>').start();
</script></div>



<!-- END: Twitter for Web widget (http://twitterforweb.com) --></div>
        <div class="box13"><div class="fb-like-box" data-href="http://<?php echo $facebook?>" data-width="350" data-height="280" data-show-faces="true" data-stream="false" data-header="false"></div></div>
      </div>
    </div>
<?php include_once 'includes/footer.inc.php'; ?>
