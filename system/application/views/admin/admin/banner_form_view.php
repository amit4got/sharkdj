<div id="cuerpo">

<?=$this->session->flashdata('message'); ?>

<?=validation_errors(); ?>

<?=form_open_multipart($accion_form, array("id" => 'form1' ) ); ?>

<div class="formularios_titulo">Campos requeridos</div>

<div class="campos">
    <div class="campos_titulo">Banner para expañoles</div>
    <div class="campos_introducir">
        <?= $ckeditor->editor('es_informacion', set_value('es_informacion')=="" ?  $registro['es_informacion']  :   set_value('es_informacion') ,$ckeditor->config ); ?>
    </div>
</div>
<div class="campos">
    <div class="campos_titulo">Banner para ingleses</div>
    <div class="campos_introducir">
         <?= $ckeditor->editor('en_informacion', set_value('en_informacion')=="" ?  $registro['en_informacion']  :   set_value('en_informacion') ,$ckeditor->config ); ?>
    </div>
</div>
<div class="campos">
    <div class="campos_titulo">Banner para holandeses</div>
    <div class="campos_introducir">
        <?= $ckeditor->editor('du_informacion', set_value('du_informacion')=="" ?  $registro['du_informacion']  :   set_value('du_informacion') ,$ckeditor->config ); ?>
    </div>
</div>   
<div id="btnsubmit"><?=form_submit('submit', 'Guardar'); ?></div>

<?=form_close(''); ?>
</div> 