<?php

if(!defined("RUTA_ABSOLUTA")){
  define("RUTA_ABSOLUTA",dirname(__FILE__));
}

require_once RUTA_ABSOLUTA.'/database/SafeSQL.class.php';
   
  class SafeSQLSharkDJ extends SafeSQL_MySQL {
  
    /**
     * Constructor de la clase
     * Al ser una clase sobrecargada solo tenemos de momento los parametros de SafeSQL
     * @param string $link_id referirse al manual de SafeSQL para consultar este parametro     
     */
    function __construct($link_id = null)    
    {
      // Llamamos al constructor de la clase SafeSQL
      SafeSQL_MySQL::SafeSQL_MySQL($link_id);
      
    } 
    
    /**
     * Sobrecarga de la funcion query para meter el desplazamiento y limite
     * @param string $query_string la consulta a escapar correctamente
     * @param string $query_vars los parametros de la consulta en formato SafeSQL
     * @param integer $offset Desplazamiento de la consulta
     * @param integer $numRow Numero maximo de la consulta
     */
    function query($query_string, $query_vars,$offset=0,$numRow=50)
    {
    	$query=SafeSQL_MySQL::query($query_string,$query_vars);
      
      	if ($numRow>0) {
      		$query=$query." limit $offset,$numRow";
      	}      	
      	#echo $query;
      	return $query;
    }      
  }
?>
