<h1>Olvidó su password?</h1>
<p>Introduzca su mail.</p>

<?=$this->session->flashdata('message'); ?>

<?=validation_errors(); ?>

<?=form_open('panel/recuperar_password'); ?>

<div class="formularios_titulo">RECUPERAR PASSWORD</div>
<div class="campos">
    <div class="campos_titulo">Email</div>
    <div class="campos_introducir"><?=form_input('email', set_value('email')); ?></div>
</div>
<div id="btnsubmit"><?=form_submit('submit', 'Renovar password'); ?></div>

<?=form_close(''); ?>


<?=form_open('panel/recuperar_password/activar'); ?>

<div class="formularios_titulo">CLAVE PARA RECUPERAR PASSWORD</div>
<div class="campos">
    <div class="campos_titulo">Codigo de recuperación</div>
    <div class="campos_introducir"><?=form_input('code', set_value('code')); ?></div>
</div>
<div id="btnsubmit"><?=form_submit('submit', 'Enviar codigo'); ?></div>

<?=form_close(''); ?>