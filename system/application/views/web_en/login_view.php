<div id="cuerpo_1">
    <div id="cuerpo_noticias">
    
        <div id="Identificarse" >
        <?=form_open('web/login', array('id' => 'form1' )); ?>
        <?=form_hidden('acc', 'identificarse'); ?>
            	<div class="campos_txt">
            	  <p>-	This section is reserved for Djs, Producers, Promoters, Bands or Groups, if you are a club, agency, etc and you want your name to appear in the club section or if you wish to contact SHARK for parties or promotions, you should send an email to <a href="mailto:marketing@sharkiberia.sl">marketing@sharkiberia.sl</a></p>      	
       	  </div>
            <div class="formularios_titulo">Validate</div>
            <div class="campos">
                <div class="campos_titulo">Email</div>
                <div class="campos_introducir"><?=form_input('email_iden', set_value('email_iden')); ?></div>
            </div>
           
            <div class="campos">
                <div class="campos_titulo">Password</div>
                <div class="campos_introducir"><?=form_password('password_iden'); ?></div>
            </div>
            <div class="formularios_sumbit"><?=form_submit('submit', 'Validate'); ?></div>
            <?php 
				if (isset($error_login)){
					echo ' <div class="campos">'.$error_login.'</div>';
				}
			?>
            
            <div class="formularios_sumbit"><?=anchor('panel/recuperar_password/' , 'Forgotten password' ); ?></div>
        <?=form_close(''); ?>

        </div>
        <div id="Registrarse" >
        <?=form_open('web/login' , array('id' => 'form2' )); ?>
        <?=form_hidden('acc', 'registrarse'); ?>
        	<div class="formularios_titulo">Register</div>
            <div class="campos">
                <div class="campos_titulo">Artist alias</div>
                <div class="campos_introducir"><?=form_input('username', set_value('username')); ?></div>
            </div>
            <div class="campos">
                <div class="campos_titulo">Email</div>
                <div class="campos_introducir"><?=form_input('email', set_value('email')); ?></div>
            </div>
            <div class="campos">
                <div class="campos_titulo">Password</div>
                <div class="campos_introducir"><?=form_password('password'); ?></div>
            </div>
            <div class="campos">
                <div class="campos_titulo">Name</div>
                <div class="campos_introducir"><?=form_input('first_name', set_value('first_name')); ?></div>
            </div>
            <div class="campos">
                <div class="campos_titulo">Surname</div>
                <div class="campos_introducir"><?=form_input('last_name', set_value('last_name')); ?></div>
            </div>
            <div class="campos">
                <div class="campos_titulo">Telephone</div>
                <div class="campos_introducir"><?=form_input('telefono', set_value('telefono')); ?></div>
            </div>
            <div class="campos">
            	<?php
					$atts = array(
					  'width'      => '800',
					  'height'     => '600',
					  'scrollbars' => 'yes',
					  'status'     => 'yes',
					  'resizable'  => 'no',
					  'screenx'    => '0',
					  'screeny'    => '0'
					);

					echo anchor_popup('web/legal_en', 'Legal information', $atts);
				?>
            </div>
            <div class="formularios_sumbit"><?=form_submit('submit', 'Save'); ?></div>
        <?=form_close(''); ?>
        	<?php 
				if (isset($error_registro)){
					echo ' <div class="campos">'.$error_registro.'</div>';
				}
			?>
        	
            <div class="campos">
                *You will receive a joining email; please check your spam folder 
            </div>
        </div>            
  
    </div>
    <div id="banner">
   	  <?= $this->banner_model->cargar_banner_es (); ?>
    </div>
	
</div>
