<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAA7rkcgy9PNk8KUX29tySlehQ2gKpxVqWRT6wE0XsFNYvAiuTpzxTuLva2-syTq5I_J-decdqo2c8PPA"
      type="text/javascript"></script>
<script type="text/javascript">
	//<![CDATA[
	function TextualZoomControl() {
		}
		TextualZoomControl.prototype = new GControl();
		
		// Creates a one DIV for each of the buttons and places them in a container
		// DIV which is returned as our control element. We add the control to
		// to the map container and return the element for the map class to
		// position properly.
		TextualZoomControl.prototype.initialize = function(map) {
		  var container = document.createElement("div");
		
		  var btnMapa = document.createElement("div");
		  this.setButtonStyle_(btnMapa);
		  container.appendChild(btnMapa);
		  btnMapa.appendChild(document.createTextNode("MAPA"));
		  GEvent.addDomListener(btnMapa, "click", function() {
			var x= map.getMapTypes(); 
    		  map.setMapType(x[0]); 
		  });
		
		  var btnSatelite = document.createElement("div");
		  this.setButtonStyle_(btnSatelite);
		  container.appendChild(btnSatelite);
		  btnSatelite.appendChild(document.createTextNode("SATELITE"));
		  GEvent.addDomListener(btnSatelite, "click", function() {
			var x= map.getMapTypes(); 
    		 map.setMapType(x[1]); 
		  });
		  var btnMixto = document.createElement("div");
		  this.setButtonStyle_(btnMixto);
		  container.appendChild(btnMixto);
		  btnMixto.appendChild(document.createTextNode("MIXTO"));
		  GEvent.addDomListener(btnMixto, "click", function() {
			 var x= map.getMapTypes(); 
    		  map.setMapType(x[2]); 
		  });
		  map.getContainer().appendChild(container);
		  return container;
		}
		
		// By default, the control will appear in the top left corner of the
		// map with 7 pixels of padding.
		TextualZoomControl.prototype.getDefaultPosition = function() {
		  return new GControlPosition(G_ANCHOR_TOP_RIGHT, new GSize(7, 7));
		}
		
		// Sets the proper CSS for the given button element.
		TextualZoomControl.prototype.setButtonStyle_ = function(button) {
		  button.style.textDecoration = "none";
		  button.style.color = "#000000";
		  button.style.backgroundColor = "white";
		  button.style.font = "10px verdana";
		  button.style.border = "1px solid black";
		  button.style.padding = "2px";
		  button.style.marginBottom = "2px";
		  button.style.textAlign = "center";
		  button.style.width = "60px";
		  button.style.cursor = "pointer";
		}

	function load() {
	  if (GBrowserIsCompatible()) {
	   var mipoint = [];
	   var i=0;
	  var map = new GMap2(document.getElementById("map"));
		map.addControl(new GLargeMapControl());
       	map.addControl(new TextualZoomControl());

		map.setCenter(new GLatLng(<?=$registro['posy']; ?>,<?=$registro['posx']; ?> ),7 );

				
		var icon = new GIcon();
			icon.image = "<?=base_url(); ?>file/entorno/marcador.png";
			icon.iconSize = new GSize(8, 8);
			//icon.shadowSize = new GSize(22, 20);
			icon.iconAnchor = new GPoint(4, 8);
			icon.infoWindowAnchor = new GPoint(0, 0);
			
		var point = new GLatLng(<?=$registro['posy']; ?>,<?=$registro['posx']; ?> );
		 mipoint[i]=new GMarker(point, icon)
		 map.addOverlay( mipoint[i]);
	  
	  
	  }
	}
		 
	
	//]]>
	
</script>
<div id="cuerpo_1">
    <div id="cuerpo_noticias">
        <div id="noticias_detalle">
        	<div id="map" ></div>
            <div id="clubs_localizacion_info" ><?=$registro['es_contacto']; ?></div>
        </div>
    </div>
    <div id="banner">
    	<?= $this->banner_model->cargar_banner_es (); ?>
    </div>
	
</div>
