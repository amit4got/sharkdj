<?php 
if(!defined("RUTA_ABSOLUTA")){
  define("RUTA_ABSOLUTA",dirname(__FILE__));
}


require_once RUTA_ABSOLUTA.'/administradorDatos/administradorDatos.class.php';
require_once RUTA_ABSOLUTA.'/includes/functions.ini.php';

session_start();

#Out of session

if (isset($_GET['logout'])) {
  $_SESSION['statusAdmin'] = $_GET['logout'];
  header ('location: index.php');
  sesion_destroy();
  exit;
}
if ($_POST){
   if($_POST['email'] != '' && $_POST['password'] != ''){
    $usuario = new administradorDatos();
    $user = $usuario->obtenerIdUsuario($_POST['email'],$_POST['password']);
    $_SESSION['idUser'] = $user['idUser'];
    $_SESSION['idPerfil'] = $user['idPerfil'];
    if ($_SESSION['idUser'] == 0){
      $_SESSION['error'] = 1;
    }
    elseif ($_SESSION['idPerfil'] == 1) {
      $_SESSION['statusAdmin'] = 'authorized';
      header ('location: adminBackend.php');
      exit;
    }
    else{
      $_SESSION['error'] = 1;
    }  
  }
  else{
    $_SESSION['error'] = 1;
  }
}

include_once 'includes/headerAdmin.inc.php';
if ($_SESSION['error'] == 1){
  echo '<div id="dialog" title="Error de Login">
  	      <p>Usuario o contraseña incorrectos.</p>
  	    </div>';
  session_destroy();
}
elseif ($_SESSION['error'] == 2){
  echo '<div id="dialog" title="Logout">
    	      <p>Se ha cerrado su sesión.</p>
    	    </div>';
  session_destroy();
}
?>



<div id="centerLoginAdmin">
	<div id="boxLoginAdmin">
    			<form action="<?php $_SERVER['PHP_SELF']?>" method="post" id="form">
      			<div id="label1" class="labelFormLogin">Email</div>
      			<div class="rounded_input">
      				<input id="field1" name="email" type="email" value>
      			</div>
      			<div id="label1" class="labelFormLogin">Password</div>
      			<div class="rounded_input">
      				<input id="field1" name="password" type="password">
      			</div>
						<input class="buttonLogin" type="submit" value="Log In" />
						<div id="forgot1" class="forgotLogin">
							<a href="forgotPassword.php">¿Olvidaste tu contraseña?</a>
						</div>
					</form>
	</div>
</div>