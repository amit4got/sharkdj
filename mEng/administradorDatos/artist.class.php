<?php

if(!defined("RUTA_ABSOLUTA")){
  define("RUTA_ABSOLUTA",dirname(__FILE__));
}

require_once RUTA_ABSOLUTA.'/config/constants.php';
require_once RUTA_ABSOLUTA.'/database/insert.class.php';
require_once RUTA_ABSOLUTA.'/database/database.class.php';
require_once RUTA_ABSOLUTA.'/administradorDatos/user.class.php';


class artist extends user{
  
  public function __construct() {
  	parent::__construct();
  }
  
  public function existeArtista($idUser) {  	
  	if ($idUser!="" && $idUser>0) {
  		$resultado = $this->bd->query("SELECT idUser FROM artistas WHERE idUser=%i", array($idUser));  		
  	}
  	return isset($resultado[0]['idUser']) ? $resultado[0]['idUser'] : 0;
  }  
  
  
  public function insertaDatosArtista($datos) {
  	$idUser = isset($datos['idUser']) ? $datos['idUser'] : 0;
  	$nombre = isset($datos['nombre']) ? $datos['nombre'] : "";
  	$apellido1 = isset($datos['apellido1']) ? $datos['apellido1'] : "";
  	$apellido2 = isset($datos['apellido2']) ? $datos['apellido2'] : ""; 
  	$direccion = isset($datos['direccion']) ? $datos['direccion'] : "";
  	$cp = isset($datos['cp']) ? $datos['cp'] : "";
  	$ciudad = isset($datos['ciudad']) ? $datos['ciudad'] : 0;
  	$pais = isset($datos['pais']) ? $datos['pais'] : 0;
  	$telefono = isset($datos['telefono']) ? $datos['telefono'] : "";
  	$movil = isset($datos['movil']) ? $datos['movil'] : "";
  	$lat = isset($datos['lat']) ? $datos['lat'] : "";
  	$lng = isset($datos['lng']) ? $datos['lng'] : "";
  	$bio = isset($datos['bio']) ? $datos['bio'] : "";
  	$web = isset($datos['web']) ? $datos['web'] : "";
  	$clubResidencia = isset($datos['clubResidencia']) ? $datos['clubResidencia'] : "";
  	$visible = isset($datos['visible']) ? 1 : 0; 
  	$team=0; 	

  	
  	if ($idUser>0 && $nombre!="" && $apellido1!="" && /*$ciudad>0 &&*/ $pais>0 && $bio!="") {  		
	  	if ($this->existeArtista($idUser)) {	
	  		$this->insert->inserta("UPDATE artistas SET nombre='%s',apellido1='%s',apellido2='%s',direccion='%s',cp='%s',ciudad=%i,
	  		               pais=%i,telefono='%s',movil='%s',lat=%f,lng=%f,bio='%s',web='%s',clubResidencia='%s',visible=%i WHERE idUser=%i", 
	  		               array($nombre,$apellido1,$apellido2,$direccion,$cp,$ciudad,$pais,$telefono,$movil,$lat,$lng,$bio,$web,$clubResidencia,$visible,$idUser));
	  	}else{	  		
		  	$this->insert->inserta("INSERT INTO artistas (idUser,nombre,apellido1,apellido2,direccion,cp,ciudad,pais,telefono,movil,lat,lng,bio,web,clubResidencia,visible,team) VALUES
		  	   						             (%i, '%s', '%s', '%s', '%s', '%s', %i, %i, '%s', '%s', %f, %f, '%s', '%s','%s',%i, %i)", 
		  	                                     array($idUser,$nombre,$apellido1,$apellido2,$direccion,$cp,$ciudad,$pais,$telefono,$movil,$lat,$lng,$bio,$web,$clubResidencia,$visible,$team));
	  	}
  	}
  	return $idUser;
  }

  public function cambiaEstadoTeam($idUser, $team=0){
  	if ($this->existeArtista($idUser)) {
  		$this->insert->inserta("UPDATE artistas SET team=%i WHERE idUser=%i", array($team, $idUser));
  	}
  }
  
  public function obtenerDatosArtista($idUser) {  	
  	if ($this->existeArtista($idUser)) {
  		$resultado = $this->bd->query("SELECT u.idUser, u.userName, u.password, u.email, u.idPerfil, u.activo, a.nombre, a.apellido1,
  		                           a.apellido2, a.direccion, a.cp, a.ciudad, a.pais, a.telefono, a.movil, a.lat, a.lng, a.bio, 
  		                           a.web, a.clubResidencia, a.visible, a.team 
  		                           FROM users u, artistas a WHERE u.idUser=%i AND u.idUser=a.idUser",
  		                           array($idUser));
  	}
  	return isset($resultado[0]) ? $resultado[0] : array();
  } 
}