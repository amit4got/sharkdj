<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Shark dj legal</title>
<style type="text/css">
<!--
.negrita {
	font-weight: bold;
}
-->
</style>
</head>

<body>
<p>Aviso Legal<br />
  <span class="negrita">1. Objeto del Aviso legal.</span><br />
  El Aviso legal regula las condiciones, el acceso y la utilización del Portal, sus contenidos y servicios, de pago o gratuitos, puestos a disposición de los usuarios; término este que incluye tanto a los clientes, como a los propietarios de los alojamientos y servicios que conforman el Portal. </p>
<p><br />
  <span class="negrita">2. Aceptación de las Condiciones del Aviso Legal.</span><br />
  2.1. Condición de Usuario.<br />
  El mero acceso, utilización y/o del Portal, o de todo o parte de los contenidos y servicios que en él se incorporan, atribuye la condición de &quot;usuario&quot; y supone la adhesión plena y sin reservas a las Condiciones del Aviso Legal que se exponen en la versión publicada desde el momento en el que el usuario acceda a los mismos. Se entenderán aceptadas por el simple hecho de visitar las páginas del Portal y por descontado si se utilizasen los servicios ofertados en el Portal.<br />
  El Usuario declara ser mayor de edad y que dispone de la capacidad legal para obligarse y asumir las condiciones del Aviso Legal que aquí se detallan.<br />
  2.2 Normas de Uso Generales.<br />
  El usuario se obliga a usar el Portal de forma diligente y correcta. El usuario deberá comunicarse con respeto con otros usuarios en el Portal.<br />
  El usuario se obliga a título meramente enunciativo a no remitir publicidad de cualquier clase y comunicaciones con fines de naturaleza comercial o publicitaria, o de cualquier otro tipo de envíos masivos no autorizados.<br />
  2.3 Modificación del presente Aviso Legal<br />
  El presente aviso legal podrá ser objeto de periódica actualización o modificación. En dicho caso, el usuario estará sujeto a las nuevas condiciones para el uso del Portal.</p>
<p><br />
  <span class="negrita">3. Contenidos y servicios prestados a través del Portal.</span><br />
  SHARKDJ.COM es una/s pagina/s meramente informativa/s que ofrece, a través de de internet, compilaciones de contenidos y servicios relacionados con el mundo del ocio y la música electrónica, además de prestaciones y facilidades interactivas complementarias accesibles merced a Internet. SHARKDJ.COM se reserva la facultad de modificar, aumentar o cancelar, en cualquier momento, y sin aviso previo, la presentación y configuración del Portal, así como los contenidos y servicios que en él se incorporan.</p>
<p><br />
  <span class="negrita">4. Cookies y otros Dispositivos.</span><br />
  El usuario autoriza a SHARKDJ.COM al uso de cookies. El Portal utiliza cookies para facilitarle la navegación, personalizar la información y analizar la efectividad y audiencia del sitio web. En su caso, si no deseara usted recibir cookies de SHARKDJ.COM le invitamos a configurar su navegador para que le pida su aceptación antes de almacenar en su terminal un nuevo cookie.</p>
<p><br />
  <span class="negrita">5. Responsabilidad por el acceso y uso de los contenidos y servicios de SHARKDJ.COM.</span><br />
  Tanto el acceso al Portal , como el uso que pueda hacer el Usuario de la misma, es de la exclusiva responsabilidad de quien lo realiza. Por ello, el usuario acepta responder de las consecuencias, daños y/o perjuicios que pudieran derivarse de dicho acceso o uso de los servicios ofrecidos por SHARKDJ.COM.<br />
  SHARKDJ.COM pondrá todos los medios a su alcance para prestar un buen servicio a través de su página web al usuario. No obstante lo anterior, SHARKDJ.COM no se hará responsable por daños y perjuicios o por cualquier otro concepto de los siguientes aspectos:<br />
  i. Disponibilidad y continuidad.<br />
  SHARKDJ.COM no garantiza la disponibilidad y continuidad del funcionamiento de la página y de sus servicios en todo momento.<br />
  ii. Veracidad, exactitud y actualización.<br />
  SHARKDJ.COM no garantiza la veracidad, exactitud y actualización, en todo momento, de los contenidos y servicios de la página. Toda la información acerca de los servicios o alojamientos que ofrece el Portal a los Usuarios se facilita sólo a efectos informativos. La información facilitada por SHARKDJ.COM ha sido elaborada en base a fuentes disponibles al público, de los datos facilitados por los propios clubs, de los datos facilitados por otros proveedores de servicios y de los propios usuarios del Portal. SHARKDJ.COM no se hace responsable del contenido, veracidad y actualización de dicha información.<br />
  Los usuarios se abstendrán de publicar contenidos que atenten contra derechos de terceros.<br />
  iii. Privacidad y seguridad.<br />
  El usuario se abstendrá de producir por cualquier medio cualquier destrucción, alteración, inutilización o daños de los datos, programas o documentos electrónicos de SHARKDJ.COM, de sus proveedores o de terceros, así como de introducir o difundir en la Red programas, virus o cualquier instrumento físico o electrónico que cause o sea susceptible de causar cualquier tipo de alteración en la Red, en el sistema, o en equipos de SHARKDJ.COM o de terceros. Queda expresamente prohibido llevar a cabo cualquier tipo de actividad o práctica que transgreda los principios de buena conducta aceptados generalmente entre los usuarios de Internet. SHARKDJ.COM no garantiza la privacidad y seguridad de la página, no haciéndose responsable de intervenciones de terceros no autorizados en la misma, ni de la aparición de virus y similares en su sistema o soportes informáticos.<br />
  iv. Opiniones y otros<br />
  Así mismo, el usuario se abstendrá de llevar a cabo a través de SHARKDJ.COM cualquier conducta que atente contra los derechos de propiedad intelectual o industrial de SHARKDJ.COM o de terceros, o que vulnere o transgreda el honor, la intimidad personal o familiar o la imagen de terceros, manteniendo indemne a SHARKDJ.COM frente a reclamaciones, judiciales o extrajudiciales, que se presenten contra ella como consecuencia de dicha conducta.<br />
  Los &quot;usuarios&quot; podrán emitir opiniones, positivas o negativas. SHARKDJ.COM no publicará los comentarios u opiniones que atenten manifiestamente el honor, la intimidad personal o familiar de terceros. SHARKDJ.COM no se hace responsable, ni garantiza, de la veracidad y exactitud de las opiniones que puedan exponerse por los usuarios en su Página.<br />
  v. Varios<br />
  El usuario se compromete a no utilizar los contenidos y servicios de SHARKDJ.COM para realizar o sugerir actividades prohibidas por la moral o el ordenamiento jurídico, aceptando expresamente dejar exenta a SHARKDJ.COM de cualquier responsabilidad relacionada con la calidad, exactitud, fiabilidad, corrección o moralidad de los datos, programas, informaciones u opiniones, cualquiera que sea su origen, incluidas en la Página web.</p>
<p><br />
  <span class="negrita">6. Propiedad intelectual e industrial</span><br />
  Los contenidos, software, imágenes, sonidos, textos y fotografías de SHARKDJ.COM u otros terceros reflejados en la Página web se encuentran protegidos por el Real Decreto Legislativo 1/1996, de la Ley de Propiedad Intelectual. Los contenidos se encuentran protegidos en especial, según el Artículo 12 del citado Real Decreto, como las colecciones de datos. Queda terminantemente prohibida la reproducción, distribución, comunicación pública y transformación de dicha propiedad salvo autorización expresa y escrita de SHARKDJ.COM.<br />
  Una utilización no autorizada de está propiedad intelectual por terceros dará lugar a las responsabilidades, de carácter penal y/o civil y otras, legalmente establecidas. SHARKDJ.COM no será responsable de las infracciones del usuario de la Página web que afecten a los derechos de terceros.</p>
<p><br />
  <span class="negrita">8. Protección de datos personales</span><br />
  SHARKDJ.COM pone a disposición del usuario el presente aviso legal para que el usuario pueda decidir sobre la conveniencia de registrarse en cualquiera de las bases de datos recogidas en ficheros automatizados incluidas en las páginas de SHARKDJ.COM.<br />
  Los datos personales de los usuarios que decidan registrarse en la correspondiente base de datos serán utilizados, entre otros, con fines estadísticos, para la redefinición de los servicios que ofrece la página, para el mantenimiento y gestión de la relación con el usuario, así como las labores de información, formación y/o para el envío de información técnica, operativa, y comercial acerca de productos y servicios relacionados con la página.<br />
  SHARKDJ.COM cumple con la normativa de Protección de datos. En este sentido, SHARKDJ.COM ha adoptado las medidas de seguridad requeridas y tiene inscrita su base de datos en el correspondiente Registro de la Agencia Española de Protección de Datos. </p>
<p><span class="negrita">9. Terminación</span><br />
SHARKDJ.COM podrá interrumpir o cancelar el Portal o cualquiera de los servicios ofrecidos a los usuarios, en cualquier momento y sin previo aviso. En todo caso, se podrá interrumpir el servicio a cualquier usuario que haga una utilización no ética, ofensiva, ilegal, o incorrecta de los contenidos o servicios de SHARKDJ.COM y/o contraria a los intereses de este portal.</p>
<p><br />
  <span class="negrita">10. Fuerza Mayor</span><br />
  Se considera Fuerza Mayor cualquier suceso o circunstancia no sujeta a control de las partes o cualquier otra contingencia que no pueda ser prevista, o, si fuera previsible, sea inevitable de acuerdo con las estipulaciones y jurisprudencia del Articulo 1.105 del Código Civil. Si por causas de Fuerza Mayor se interrumpiese el servicio objeto del presente contrato, dicha obligación sería suspendida durante el tiempo y en la extensión que fuese justificable.</p>
<p><br />
  <span class="negrita">11. Comunicaciones</span><br />
  SHARKDJ.COM podrá dirigir al usuario sus comunicaciones bien por correo electrónico o correo postal; de igual forma, el usuario podrá dirigir sus comunicaciones a la Página web por correo electrónico a info@sharkdj.com</p>
<p><br />
  <span class="negrita">12. Ley y foro aplicables</span><br />
  Las partes, con renuncia a su fuero propio, si ello fuera posible, se someten a los Juzgados y Tribunales de Santa Cruz de Tenerife (España). La Ley aplicable será la española.</p>
<p></p>
</body>
</html>
