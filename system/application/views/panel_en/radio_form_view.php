<div id="cuerpo">


<?=$this->session->flashdata('message'); ?>

<?=validation_errors(); ?>

<?=form_open($accion_form); ?>
<div class="formularios_titulo">Required field</div>
<div class="campos">
    <div class="campos_titulo">Title in Spanish</div>
    <div class="campos_introducir"><?=form_input('es_titulo',set_value('es_titulo')=="" ?  $registro['es_titulo']  :   set_value('es_titulo') ); ?></div>
</div>
<div class="campos">
    <div class="campos_titulo">Title in English</div>
    <div class="campos_introducir"><?=form_input('en_titulo', set_value('en_titulo')=="" ?  $registro['en_titulo']  :   set_value('en_titulo') ); ?></div>
</div>
<div class="campos">
    <div class="campos_titulo">Title in Dutch</div>
    <div class="campos_introducir"><?=form_input('du_titulo', set_value('du_titulo')=="" ?  $registro['du_titulo']  :   set_value('du_titulo') ); ?></div>
</div>
<div class="campos">
    <div class="campos_titulo">URL del mp3 (soundcloud: añadir &quot;/download.mp3&quot; al link del track)</div>
    <div class="campos_introducir"><?=form_input('ruta_mp3', set_value('ruta_mp3')=="" ?  $registro['ruta_mp3']  :   set_value('ruta_mp3') ); ?></div>
</div>
<div class="campos_supertitulo">Details</div>
<div class="campos">
    <div class="campos_titulo">Subtitles in Spanish</div>
    <div class="campos_introducir"><?=form_input('es_subtitulo',set_value('es_subtitulo')=="" ?  $registro['es_subtitulo']  :   set_value('es_subtitulo') ); ?></div>
</div>
<div class="campos">
    <div class="campos_titulo">Subtitles in English</div>
    <div class="campos_introducir"><?=form_input('en_subtitulo', set_value('en_subtitulo')=="" ?  $registro['en_subtitulo']  :   set_value('en_subtitulo') ); ?></div>
</div>
<div class="campos">
    <div class="campos_titulo">Subtitles in Dutch</div>
    <div class="campos_introducir"><?=form_input('du_subtitulo', set_value('du_subtitulo')=="" ?  $registro['du_subtitulo']  :   set_value('du_subtitulo') ); ?></div>
</div>   
   
<div id="btnsubmit"><?=form_submit('submit', 'Save'); ?></div>


<?=form_close(''); ?>
</div> 