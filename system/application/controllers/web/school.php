<?php
class School extends Controller {
	function __construct() 
	{
		parent::Controller();
		$this->load->model('banner_model');
		$this->load->library('pagination');
	}
	function index()
	{
		redirect('web/school/tutorials');
		
	}
	function courses()
	{
		$config['base_url'] = 'http://www.sharkdj.com/index.php/web/school/courses';
		
		$query=$this->db->get('Escuela_tutoriales_texto');
			
		$config["total_rows"] = $query->num_rows();
		$config['per_page'] = '5';
		$config['cur_page']=$this->uri->segment(4);
		
		$this->data['pulsado']='escuela';
		$this->data['sub_pulsado']='tutoriales';
		$this->data['sub_head'] = $this->load->view('web_en/sub_head_2_view', $this->data, true);
		$this->db->order_by("orden","DESC");
		$this->data['query']=$this->db->get('Escuela_tutoriales_texto');
		$this->data['cuerpo'] =   $this->load->view('web_en/tutoriales_listado_view', $this->data, true);
		$this->load->view('templates/en_web_view',  $this->data);
	}
	function courses_detail()
	{
		$this->data['pulsado']='escuela';
		$this->data['sub_pulsado']='tutoriales';
		$this->data['sub_head'] = $this->load->view('web_en/sub_head_2_view', $this->data, true);
		
		$this->db->where('id', $this->uri->segment(4) );
		$query=$this->db->get('Escuela_tutoriales_texto');
		foreach ($query->result() as $row)
		{
			$this->data['registro'] = array(
										"es_titulo" 		=> $row->en_titulo , 
										
										"es_informacion" 	=> htmlspecialchars_decode($row->en_informacion) 
										);
		}
		if (!isset($this->data['registro'])){
			 redirect('web/school/courses');
		}
		
		$this->data['cuerpo'] = $this->load->view('web_en/tutoriales_detalle_view', $this->data, true);
		$this->load->view('templates/en_web_view',  $this->data);
	}
	function tutorials()
	{
		$config['base_url'] = 'http://www.sharkdj.com/index.php/web/school/tutorials';
		
		$query=$this->db->get('Escuela_tutoriales_video');
			
		$config["total_rows"] = $query->num_rows();
		$config['per_page'] = '5';
		$config['cur_page']=$this->uri->segment(4);
		
		$this->data['pulsado']='escuela';
		$this->data['sub_pulsado']='videos';
		$this->data['sub_head'] = $this->load->view('web_en/sub_head_2_view', $this->data, true);
		$this->db->order_by("orden","DESC");
		$this->data['query']=$this->db->get('Escuela_tutoriales_video');
		$this->data['cuerpo'] =   $this->load->view('web_en/videos_listado_view', $this->data, true);
		$this->load->view('templates/en_web_view',  $this->data);
	}
	function tutorials_detail()
	{
		$this->data['pulsado']='escuela';
		$this->data['sub_pulsado']='videos';
		$this->data['sub_head'] = $this->load->view('web_en/sub_head_2_view', $this->data, true);
		
		$this->db->where('id', $this->uri->segment(4) );
		$query=$this->db->get('Escuela_tutoriales_video');
		foreach ($query->result() as $row)
		{
			$this->data['registro'] = array(
										"es_titulo" 		=> $row->en_titulo , 
										
										"es_informacion" 	=> htmlspecialchars_decode($row->en_informacion) 
										);
		}
		if (!isset($this->data['registro'])){
			 redirect('web/school/tutorials');
		}
		
		$this->data['cuerpo'] = $this->load->view('web_en/videos_detalle_view', $this->data, true);
		$this->load->view('templates/en_web_view',  $this->data);
	}
}


