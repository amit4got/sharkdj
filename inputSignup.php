<?php
/****************************
 *  Created 24/11/11
 *  Last update 24/11/11   
 ****************************/

if(!defined("RUTA_ABSOLUTA")){
  define("RUTA_ABSOLUTA",dirname(__FILE__));
}

#INCLUDES
require_once RUTA_ABSOLUTA.'/includes/connect.ini.php';
require_once RUTA_ABSOLUTA.'/includes/functions.ini.php';
require_once RUTA_ABSOLUTA.'/administradorDatos/administradorDatos.class.php';

// session_start();

// if ($_SESSION['status'] != "authorized") {
//   header ("Location: login.php");
//   exit();
// }

  if (isset($_POST['enviado'])){
    
    $bd = new administradorDatos();

    #Load languages
    include RUTA_ABSOLUTA.'/includes/lang.inc.php';
    $arrayIdiomas = $bd->cargaIdioma($idioma);
	$langURL = '?';
    if ($idioma != 'es') {
      $langURL = '?lang=' . $idioma;
    }

    $user = (string) $_POST['userName'];
    $email = (string) $_POST['email'];
    $perfil = $_POST['perfil'];
    

//     if ($perfil == 3){
//        session_start();
      
//       if ($_SESSION['status'] != "authorized" || $_SESSION['idUser'] != 1) {
//         $_SESSION['error'] = 3;
//         header ("Location: inputLogin.php" . $langURL);
//         exit();
//       }
//     }
    
    if ($user != '' && $email != ''){
      if ($bd->existeUserName($user)){
        header('Location: inputSignup.php' . $langURL . '&e=3&n='.$_POST['userName'].'&em='.$_POST['email']);
        exit;
      }
      if ($bd->existeEmail($email)){
        header('Location: inputSignup.php' . $langURL . '&e=4&n='.$_POST['userName'].'&em='.$_POST['email']);
        exit;
      }
    }
    elseif ($user == '' || $email == ''){
      header('Location: inputSignup.php' . $langURL . '&e=5&n='.$_POST['userName'].'&em='.$_POST['email']);
      exit;
    }
    
    
    if (strcmp((string) $_POST['password'],(string) $_POST['password2']) == 0 && $_POST['password'] != ''){
      $idUser = $bd->insertaUsuario($_POST['userName'],$_POST['password'],$_POST['email'],$perfil);
      if ($idUser && $perfil == 2){
        session_start();

        $_SESSION['idUser'] = $idUser;
	  	  $_SESSION['idPerfil'] = $perfil;
		    $_SESSION['status'] = 'authorized';
        header('Location: artistaBackend.php' . $langURL);
        exit;
      }
      elseif($idUser && $perfil == 3) {
        session_start(); 
        
        $_SESSION['idUser'] = $idUser;
		    $_SESSION['idPerfil'] = $perfil;
		    $_SESSION['status'] = 'authorized';
        header ("Location: localBackend.php" . $langURL);
        exit();
      }
      else{
        header('Location: inputSignup.php' . $langURL . '&e=2&n='.$_POST['userName'].'&em='.$_POST['email']);
        exit;
      }
    } 
    else{
      header('Location: inputSignup.php' . $langURL . '&e=1&n='.$_POST['userName'].'&em='.$_POST['email']);
      exit;
    }
  } 


include_once 'includes/header.inc.php';


?>
  	<div class="rowSignup">
    		<div id="boxSignup">
    			<form action="<?php echo $_SERVER['PHP_SELF'] . $langURL; ?>" method="post" id="form">
      			<div id="label1" class="labelFormLogin"><?=$arrayIdiomas['user'];?></div>
      			<div class="rounded_input">
      				<input id="field1" name="userName" type="text" value="<?php echo $_GET['n']; ?>">
      			</div>
      			<div id="label1" class="labelFormLogin"><?=$arrayIdiomas['email'];?></div>
      			<div class="rounded_input">
      				<input id="field1" name="email" type="email" value="<?php echo $_GET['em']; ?>">
      			</div>
      			<?php if ($_GET['e'] == 1){?>
      				<div id="label1" class="labelFormLogin"><?=$arrayIdiomas['password'];?></div>
      				<div class="rounded_input_red">
      					<input class="inputRed" id="field1" name="password" type="password">
      				</div>
      				<div id="label1" class="labelFormLogin"><?=$arrayIdiomas['repeatPassword'];?></div>
      				<div class="rounded_input_red">
      					<input class="inputRed" id="field1" name="password2" type="password">
      				</div>
      			<?php }
      			else {?>
        			<div id="label1" class="labelFormLogin"><?=$arrayIdiomas['password'];?></div>
        			<div class="rounded_input">
        				<input id="field1" name="password" type="password">
        			</div>
        			<div id="label1" class="labelFormLogin"><?=$arrayIdiomas['repeatPassword'];?></div>
        			<div class="rounded_input">
        				<input id="field1" name="password2" type="password">
        			</div>
        			<div align="center"><br>
        			<div id="label1" class="radioFormLogin">
								<input type="radio" name="perfil" value="2" checked>  Dj
							</div>
							<div id="label1" class="radioFormLogin">
								<input type="radio" name="perfil" value="3"> <?=$arrayIdiomas['establishment'];?>
							</div><br>
        			</div>
        		<?php }?>
        		<input type="hidden" name="enviado" value="enviado"> 
						<input class="buttonLogin" type="submit" value="Sign Up" />
						<div id="forgot1" class="forgotLogin">
							<a href="inputLogin.php<?=$langURL;?>"><?=$arrayIdiomas['alreadyUser'];?></a>
						</div>
					</form>
    		</div>
  	</div>
  	
  	<?php if ($_GET['e'] == '1'){?>
  		  <div id="dialog" title="<?=$arrayIdiomas['err4'];?>">
  				<p><?=$arrayIdiomas['err4'];?></p>
  			</div>
  		<?php }
  		elseif ($_GET['e'] == '2'){?>
  		  <div id="dialog" title="Error">
  		  <p><?=$arrayIdiomas['err5'];?></p>
  		  </div>
  		<?php }
  		elseif ($_GET['e'] == '3'){
  		  ?>
  	    <div id="dialog" title="<?php echo $arrayIdiomas['err6a'] . ' ' . $arrayIdiomas['err6b'];?>">
  	      <p><?=$arrayIdiomas['err6a'];?> <b><?php echo $_GET['n'];?></b> <?=$arrayIdiomas['err6b'];?></p>
  	    </div>
  	
  	    <?php }
  	    elseif ($_GET['e'] == '4'){
  	      ?>
  	      	    <div id="dialog" title="<?php echo $arrayIdiomas['err7a'] . ' ' . $arrayIdiomas['err7b'];?>">
  	      	      <p><?=$arrayIdiomas['err7a'];?> <b><?php echo $_GET['em'];?></b> <?=$arrayIdiomas['err7b'];?></p>
  	      	    </div>
  	    <?php }
  	    elseif ($_GET['e'] == '5'){
  	      ?>
  	    <div id="dialog" title="<?=$arrayIdiomas['err8b'];?>">
  	      <p><?=$arrayIdiomas['err8'];?></p>
  	    </div>
  	    
  		<?php }
  		  ?>
  	
<?php include_once 'includes/footer.inc.php'; ?>