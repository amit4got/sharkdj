<?php
/****************************
 *  Created 24/11/11
 *  Last update 24/11/11   
 ****************************/
error_reporting(E_ALL);
ini_set("display_errors", 1);


if(!defined("RUTA_ABSOLUTA")){
  define("RUTA_ABSOLUTA",dirname(__FILE__));
}


#INCLUDES
require_once RUTA_ABSOLUTA.'/administradorDatos/administradorDatos.class.php';
require_once RUTA_ABSOLUTA.'/includes/functions.ini.php';

session_start();

#Load languages
include RUTA_ABSOLUTA.'/includes/lang.inc.php';
$user = new administradorDatos();
$arrayIdiomas = $user->cargaIdioma($idioma);
$langURL  = '';
$langURL2 = '';
if ($idioma != 'es') {
  $langURL  = '?lang=' . $idioma;
  $langURL2 = '&lang=' . $idioma;
}


#Out of session

if (isset($_GET['logout'])) {
  $_SESSION['status'] = $_GET['logout'];
  header ('location: index.php'.$langURL);
  exit;
}
if ($_POST){
   if($_POST['email'] != '' && $_POST['password'] != ''){
    $usuario = new administradorDatos();
    #Load languages
    #include RUTA_ABSOLUTA.'/includes/lang.inc.php';
    #$arrayIdiomas = $usuario->cargaIdioma($idioma);

    $user = $usuario->obtenerIdUsuario($_POST['email'],$_POST['password']);
    $_SESSION['idUser'] = $user['idUser'];
    $_SESSION['idPerfil'] = $user['idPerfil'];
    if ($_SESSION['idUser'] == 0){
      $_SESSION['error'] = 1;
    }
    elseif ($user['idPerfil'] == 2 || $user['idPerfil'] == 1) {
      $_SESSION['status'] = 'authorized';
      header ('location: artistaBackend.php'.$langURL);
      exit;
    }  
    elseif ($user['idPerfil'] == 3) {
      $_SESSION['status'] = 'authorized';
      header ('location: localBackend.php'.$langURL);
      exit;
    }
  }
  
  
}

include_once 'includes/header.inc.php';
if ($_SESSION['error'] == 1){
  echo '<div id="dialog" title="Error Login">
  	      <p>' . $arrayIdiomas['err1'] . '</p>
  	    </div>';
  session_destroy();
}
elseif ($_SESSION['error'] == 2){
  echo '<div id="dialog" title="Logout">
    	      <p>' . $arrayIdiomas['err2'] . '</p>
    	    </div>';
  session_destroy();
}
elseif ($_SESSION['error'] == 3){
  echo '<div id="dialog" title="Logout">
    	      <p>' . $arrayIdiomas['err3'] . '</p>
    	    </div>';
  session_destroy();
}


?>
  
  	<div class="rowLogin">
    		<div id="boxLogin">
    			<form action="<?php echo $_SERVER['PHP_SELF'] . $langURL; ?>" method="post" id="form">
      			<div id="label1" class="labelFormLogin"><?=$arrayIdiomas['email'];?></div>
      			<div class="rounded_input">
      				<input id="field1" name="email" type="email" value>
      			</div>
      			<div id="label1" class="labelFormLogin"><?=$arrayIdiomas['password'];?></div>
      			<div class="rounded_input">
      				<input id="field1" name="password" type="password">
      			</div>
						<input class="buttonLogin" type="submit" value="<?=$arrayIdiomas['log-in'];?>" />
						<div id="forgot1" class="forgotLogin">
							<a href="forgotPassword.php<?=$langURL;?>"><?=$arrayIdiomas['lostPassword'];?></a>
						</div>
					</form>
    		</div>
  	</div>
<?php include_once 'includes/footer.inc.php'; ?>