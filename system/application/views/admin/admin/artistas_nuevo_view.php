<div id="cuerpo">
<?=$this->session->flashdata('message'); ?>
<?=validation_errors(); ?>
<?=form_open('admin/artistas/nuevo'); ?>
<div class="formularios_titulo">Campos requeridos</div>
    <div class="campos">
    	<div class="campos_titulo">Alias del artista</div>
		<div class="campos_introducir"><?=form_input('username', set_value('username')); ?></div>
   	</div>
	<div class="campos">
    	<div class="campos_titulo">Email</div>
		<div class="campos_introducir"><?=form_input('email', set_value('email')); ?></div>
   	</div>
    <div class="campos">
    	<div class="campos_titulo">Password</div>
		<div class="campos_introducir"><?=form_password('password'); ?></div>
   	</div>
	<div class="formularios_titulo">Detalle del artista</div>
	  <div class="campos">
    	<div class="campos_titulo">Nombre</div>
		<div class="campos_introducir"><?=form_input('first_name', set_value('first_name')); ?></div>
   	</div>
    <div class="campos">
    	<div class="campos_titulo">Apellidos</div>
		<div class="campos_introducir"><?=form_input('last_name', set_value('last_name')); ?></div>
   	</div>
    <div class="campos">
    	<div class="campos_titulo">Teléfono</div>
		<div class="campos_introducir"><?=form_input('telefono', set_value('telefono')); ?></div>
   	</div>
	<div class="formularios_sumbit"><?=form_submit('submit', 'Guardar'); ?></div>
<?=form_close(''); ?>
</div>