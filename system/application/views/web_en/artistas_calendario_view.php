<div id="cuerpo_1">
    <div id="cuerpo_noticias">
        <ul id="noticias">
            <?php if ($query->num_rows()>0): ?>
			<?php foreach($query->result() as $row): ?>
            <li>
                <div class="noticias_imagen"><img src="<?=base_url(); ?>uploads/calendario/<? 
						if ($row->imagen!=""){
							$ar_imagen=explode(".",$row->imagen);
							echo $ar_imagen[0].'p.'.$ar_imagen[1];
						}else{
							echo 'espacio.gif';
						}
				?>"  alt="shark" ></div>
                <div class="noticias_info">
                    <h2><?=$row->en_titulo;?></h2>
                    <p><?=substr(strip_tags($row->en_informacion),0,200).'...';?></p>
                </div>
                <div class="noticias_enlace"><?=anchor('web/artists/event/'.$this->uri->segment(4).'/'.$row->id.'/'.$row->en_titulo	, 'Read more' ); ?></div>
            </li> 
            <?php endforeach;?>
			<?php endif; ?>  
        </ul>
    </div>
    <div id="banner">
   	  <?= $this->banner_model->cargar_banner_es (); ?>
    </div>
</div>