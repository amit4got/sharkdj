<div id="cuerpo">
<?=$this->session->flashdata('message'); ?>
<?=validation_errors(); ?>
<?=$errores; ?>
<?=form_open('admin/artistas/modificar/'.$this->uri->segment(4)); ?>
  <div class="formularios_titulo">Campos requeridos</div>
    <div class="campos">
    	<div class="campos_titulo">Alias del artista</div>
		<div class="campos_introducir"><?=form_input('username', set_value('username')=="" ?  $profile_edit->username :   set_value('username') ); ?></div>
   	</div>
	<div class="campos">
    	<div class="campos_titulo">Email</div>
		<div class="campos_introducir"><?=form_input('email', set_value('email')=="" ?  $profile_edit->email :   set_value('email')); ?></div>
   	</div>
	<div class="formularios_titulo">Detalle del artista</div>
	  <div class="campos">
    	<div class="campos_titulo">Nombre</div>
		<div class="campos_introducir"><?=form_input('first_name', set_value('first_name')=="" ?  $profile_edit->first_name :   set_value('first_name') ); ?></div>
   	</div>
    <div class="campos">
    	<div class="campos_titulo">Apellidos</div>
		<div class="campos_introducir"><?=form_input('last_name', set_value('last_name')=="" ?  $profile_edit->last_name :   set_value('last_name') ); ?></div>
   	</div>
    <div class="campos">
    	<div class="campos_titulo">Teléfono</div>
		<div class="campos_introducir"><?=form_input('telefono', set_value('telefono')=="" ?  $profile_edit->telefono :   set_value('telefono') ); ?></div>
   	</div>
	<div class="formularios_sumbit"><?=form_submit('submit', 'Guardar'); ?></div>
<?=form_close(''); ?>
</div>