<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Web_fiestas_model extends Model
{
	
	
	public function __construct()
	{
		parent::__construct();
	}
	/**
	Casos para cargar fiestas
	 		//// por fecha /////
			//// por fecha , pais /////
			//// por fecha , ciudad /////
			//// por fecha , estilos /////
			//// por fecha , local /////
			
			//// por fecha , pais 	, estilos 			/////
			//// por fecha , pais 	, local    			/////
			//// por fecha , pais 	, local , estilos	/////
			
			//// por fecha , ciudad , estilos 			/////
			//// por fecha , ciudad , local    			/////
			//// por fecha , ciudad , local , estilos	/////
	**/
	public function cargar_fiestas ($var_fecha, $var_pais,$var_ciudad,$var_local,$arr_estilo)
	{
	    $sql = 'SELECT ';
		$sql.= 'F.id AS "id", ';
		$sql.= 'F.fecha AS "fecha", ';
		$sql.= 'F.imagen AS "imagen", ';
		$sql.= 'F.es_titulo AS "titulo", ';
		$sql.= 'F.es_informacion AS "informacion", ';
		$sql.= 'F.es_contacto AS "contacto", ';
		$sql.= 'F.estilos , ';
		$sql.= 'P.es_titulo AS "pais", ';
		$sql.= 'C.es_titulo AS "ciudad" ';
		$sql.= 'FROM Fiestas F, Pais P  ,Ciudad C ';// WHERE word = ? AND ip_address = ? AND captcha_time > ?";
		$sql.= 'WHERE P.id = F.id_pais ';
		$sql.= 'AND C.id = F.id_ciudad ';
		if ($var_pais!='' && $var_pais!='0'){
			$sql.= 'AND F.id_pais = '.$var_pais.' ';
		}
		if ($var_ciudad!='' && $var_ciudad!='0'){
			$sql.= 'AND F.id_ciudad = '.$var_ciudad.' ';
		}
		if ($var_local!=''){
			$sql.= 'AND F.es_titulo LIKE "%'.$var_local.'%" ';	
		}
		$sql.= 'AND fecha >= "'.$var_fecha.'%" ';
		if (count($arr_estilo)>0){	
			$sql.= ' AND (';
			$ct_caso=true;
			foreach ($arr_estilo as $fila_estilo)    
			{   
				if ($ct_caso==true){
					$sql.= ' F.estilos LIKE "%,'.$fila_estilo.',%" ';
					$ct_caso=false;
				}else{
					$sql.= ' OR F.estilos LIKE "%,'.$fila_estilo.',%" ';
				}
			}
			$sql.= ' ) ';
		}
		$sql.=' ORDER BY fecha ASC ';
		$query=$this->db->query($sql);
		return $query->result_array();
	}
	public function cargar_fiestas_en ($var_fecha, $var_pais,$var_ciudad,$var_local,$arr_estilo)
	{
	    $sql = 'SELECT ';
		$sql.= 'F.id AS "id", ';
		$sql.= 'F.fecha AS "fecha", ';
		$sql.= 'F.imagen AS "imagen", ';
		$sql.= 'F.en_titulo AS "titulo", ';
		$sql.= 'F.en_informacion AS "informacion", ';
		$sql.= 'F.en_contacto AS "contacto", ';
		$sql.= 'F.estilos , ';
		$sql.= 'P.en_titulo AS "pais", ';
		$sql.= 'C.en_titulo AS "ciudad" ';
		$sql.= 'FROM Fiestas F, Pais P  ,Ciudad C ';// WHERE word = ? AND ip_address = ? AND captcha_time > ?";
		$sql.= 'WHERE P.id = F.id_pais ';
		$sql.= 'AND C.id = F.id_ciudad ';
		if ($var_pais!='' && $var_pais!='0'){
			$sql.= 'AND F.id_pais = '.$var_pais.' ';
		}
		if ($var_ciudad!='' && $var_ciudad!='0'){
			$sql.= 'AND F.id_ciudad = '.$var_ciudad.' ';
		}
		if ($var_local!=''){
			$sql.= 'AND F.es_titulo LIKE "%'.$var_local.'%" ';	
		}
		$sql.= 'AND fecha >= "'.$var_fecha.'%" ';
		if (count($arr_estilo)>0){	
			$sql.= ' AND (';
			$ct_caso=true;
			foreach ($arr_estilo as $fila_estilo)    
			{   
				if ($ct_caso==true){
					$sql.= ' F.estilos LIKE "%,'.$fila_estilo.',%" ';
					$ct_caso=false;
				}else{
					$sql.= ' OR F.estilos LIKE "%,'.$fila_estilo.',%" ';
				}
			}
			$sql.= ' ) ';
		}
		$sql.=' ORDER BY fecha ASC ';
		$query=$this->db->query($sql);
		return $query->result_array();
	}
	public function cargar_estilos()
	{
			$query = $this->db->get('Estilo');
			return $query->result_array();
	}
	public function cargar_pais()
	{
			$query = $this->db->get('Pais');
			return $query->result_array();
	}
	public function cargar_ciudades($var_pais)
	{
			$query = $this->db->where( 'id_pais', $var_pais )->get( 'Ciudad' );
			return $query->result_array();
	}
	
}
