<?php 

error_reporting(E_ALL);
ini_set("display_errors", 1);

	include_once 'header.php';
	
	if(!defined("RUTA_ABSOLUTA")){
	  define("RUTA_ABSOLUTA",dirname(__FILE__));
	}
	
	require_once RUTA_ABSOLUTA.'/administradorDatos/administradorDatos.class.php';
	require_once RUTA_ABSOLUTA.'/administradorDatos/artist.class.php';
	require_once RUTA_ABSOLUTA.'/administradorDatos/imagenes.class.php';
	#INCLUDES
	require_once '../includes/connect.ini.php';
	require_once '../includes/functions.ini.php';
	
	
	$listaNoticias="";
	$pagina = isset($_GET['p']) ? $_GET['p'] : 0;
	$enlacePag = "./index.php?";
 	$adminDatos = new administradorDatos();

	$numTotalNoticias = count($adminDatos->obtenerNoticias());
	$arrayNoticias = $adminDatos->obtenerNoticias(3, $pagina*3);
	$numNoticias = count($arrayNoticias);
	if ($numNoticias>0){
	  for($i=0; $i<$numNoticias && $i<3; $i++) {
	    $idPost = $arrayNoticias[$i]['idPost'];
	    $titulo = $arrayNoticias[$i]['titulo'];
	    $contenidoLargo = $arrayNoticias[$i]['texto'];
      $contenidoLargo = strip_tags($contenidoLargo, '<span><p><a><img>');
	    $contenido = preg_replace('/<img.+\/>/', ' ', $contenidoLargo);
	    $contenido = substr($contenido, '0','650');
	    if (strlen($contenido) == 650){
	      $contenido .= '...';
	    }
      $contenido = strip_tags($contenido, '<span><b><p><strong><a>');
	    $fecha = $arrayNoticias[$i]['fecha'];
	    $categoria = $arrayNoticias[$i]['nombreCategoria'];
	    $imagenUrl = imagenes::obtenerImagenPost($idPost);
	    $listaNoticias.=<<<EOF
	    <h3>{$titulo}</h3>
	    <p><div align="center"><a href="index.php#noticia{$i}"><img src='{$imagenUrl}' width="250" heigth="250" /></a></div><div style="text-align:justify;"> {$contenido}</div></p>
	    <p><a href="index.php#noticia{$i}">Leer mas...</a></p>
EOF;
        switch ($i) {
          case 0:
          $noticia1 =<<<EOF
           <h3>{$titulo}</h3>
	    <p>{$contenidoLargo}</p>
EOF;
          break;
          case 1:
            $noticia2 =<<<EOF
                     <h3>{$titulo}</h3>
          	    <p>{$contenidoLargo}</p>
EOF;
            break;
            case 2:
              $noticia3 =<<<EOF
                       <h3>{$titulo}</h3>
            	    <p>{$contenidoLargo}</p>
EOF;
              break;          
          default:
            ;
          break;
        }
	  }
	}
	
?>
<div id="pagina"> 
<div data-role="page" data-theme="c" data-content-theme="c" id="home">
    <?php echo $header;?>
    <div data-role="content" data-filter="true">
      <a name="up"></a>
      <?php echo $listaNoticias;?>
     <div align="center">   
      <div data-role="controlgroup" data-type="horizontal">
        <?php if ($pagina>0) { ?><a href="index.php?p=<?php echo $pagina-1?>" data-role="button" data-icon="arrow-l" data-iconpos="notext" data-ajax="false">Anterior</a>  <?php }?>
      	<a href="index.php" data-role="button" id="buttonUp" data-icon="home" data-iconpos="notext" data-ajax="false">Up</a>
        <?php if (($pagina+1)*5 < $numTotalNoticias) {?><a href="index.php?p=<?php echo $pagina+1?>" data-role="button" data-icon="arrow-r" data-iconpos="notext" data-ajax="false">Siguiente</a><?php } ?>
      </div>
      </div>
    </div>
    <?php echo $footer;?>
</div><!-- /page -->

<div data-role="page" data-theme="c" data-content-theme="c" id="noticia0">
    <?php echo $header;?>
    <div data-role="content" data-filter="true">
      <a name="up"></a>
      <?php echo $noticia1;?>
     <div align="center">   
      <div data-role="controlgroup" data-type="horizontal">
        <a href="index.php" data-role="button" id="buttonUp" data-icon="home" data-iconpos="notext" data-ajax="false">Home</a>
      </div>
      </div>
    </div>
    <?php echo $footer;?>
</div><!-- /page -->

<div data-role="page" data-theme="c" data-content-theme="c" id="noticia1">
    <?php echo $header;?>
    <div data-role="content" data-filter="true">
      <a name="up"></a>
      <?php echo $noticia2;?>
     <div align="center">   
      <div data-role="controlgroup" data-type="horizontal">
        <a href="index.php" data-role="button" id="buttonUp" data-icon="home" data-iconpos="notext" data-ajax="false">Home</a>
      </div>
      </div>
    </div>
    <?php echo $footer;?>
</div><!-- /page -->

<div data-role="page" data-theme="c" data-content-theme="c" id="noticia2">
    <?php echo $header;?>
    <div data-role="content" data-filter="true">
      <a name="up"></a>
      <?php echo $noticia3;?>
     <div align="center">   
      <div data-role="controlgroup" data-type="horizontal">
        <a href="index.php" data-role="button" id="buttonUp" data-icon="home" data-iconpos="notext" data-ajax="false">Home</a>
      </div>
      </div>
    </div>
    <?php echo $footer;?>
</div><!-- /page -->

</div>
</body>
</html>


