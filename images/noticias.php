<?php
/****************************
 *  Created 26/06/11
 *  Last update 08/11/11   
 ****************************/

#Esta parte detecta si el dispositivo es un móvil y redirige a la versión móvil de la web
function isMobile(){
  return preg_match('/ipod|iphone|android|opera mini|blackberry|palm os|windows ce|mobile/i', $_SERVER['HTTP_USER_AGENT'] );
}

if( isMobile() ){
  header('location: ./m/index.php');
  exit();
}
#Hasta aquí

if(!defined("RUTA_ABSOLUTA")){
	define("RUTA_ABSOLUTA",dirname(__FILE__));
}

require_once RUTA_ABSOLUTA.'/administradorDatos/administradorDatos.class.php';
require_once RUTA_ABSOLUTA.'/administradorDatos/artist.class.php';
require_once RUTA_ABSOLUTA.'/administradorDatos/imagenes.class.php';
#INCLUDES
require_once 'includes/connect.ini.php';
require_once 'includes/functions.ini.php';

// session_start();

// if ($_SESSION['status'] != "authorized") {
//   header ("Location: index.php");
//   exit();
// }

$noticia = '';
$adminDatos = new administradorDatos();
$arrayNoticia = $adminDatos->obtenerNoticia($_GET['ip']);

$titulo = utf8_encode($arrayNoticia['titulo']);
$contenido = utf8_encode($arrayNoticia['texto']);
$fecha = $arrayNoticia['fecha'];
$categoria = $arrayNoticia['nombreCategoria'];

include_once 'includes/header.inc.php';
?>
  <div id="main">
		<div id="searchBar">
      <span class="left fontWhite marginTop5 marginLeft5">Buscador de noticias:</span>
      <form>
        <input type="image" name="submit" class="right marginRight7 marginTop2" id="searchButton" value="" />
        <input type="text" id="searchText" name="searchText" class="right searchText noBorder fontGrey fontBold" value="Campo de b&uacute;squeda..." onclick="javascript:valueReset('searchText');"/>
      </form>
    </div>
    <div class="advertisementRow3"></div>
    <div class="noticia">
    	<div class="cuerpoNoticia">
      	<div class="newsInfoTitle2"><?php echo $titulo?></div>
      	<div class="fechaCategoria">Fecha: <?php echo $fecha;?>&nbsp;&nbsp;Categor&iacute;a: <?php echo $categoria;?></div>
      	<div class="newsInfoText2"><?php echo $contenido;?></div>
    	</div>
  	</div>
  	<div id="nextPreviousMenu" class="clear left insideRow">
    	<div class="left width33"><a href="./index.php?p=<?php echo ($pagina>0) ? $pagina-1 : 0;?>" class="noDecoration fontGrey2 fontBold hoverLargeGrey">&laquo; Anterior</a></div>
    	<div class="left width33 textCentered marginLeft5"><a href="./index.php?p=0" class="noDecoration fontGrey2 fontBold hoverLargeGrey">Inicio</a></div>        
    	<div class="right"><a href="./index.php?p=<?php echo (($pagina+1)*5 < $numTotalNoticias) ? $pagina+1 : $pagina;?>" class="fontBold noDecoration fontGrey2 hoverLargeGrey">Siguiente &raquo;</a></div>
  	</div>
	</div>
	
<?php include_once 'includes/footer.inc.php'; ?>