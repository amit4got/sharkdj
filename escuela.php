<?php
/****************************
 *  Created 08/12/11
 *  Last update 25/12/12
 ****************************/

#INCLUDES
require_once 'includes/connect.ini.php';
require_once 'includes/functions.ini.php';

define('USERSHARK','adminshark'); #identificador del canal de youtube
if(!defined("RUTA_ABSOLUTA")){
	define("RUTA_ABSOLUTA",dirname(__FILE__));
}

include_once 'includes/header.inc.php';
srand();
$publicidades = $adminDatos->obtenerPublicidad();
if (count($publicidades) < 6){
  $claves = array_rand($publicidades,count($publicidades)); 
}else{
  $claves = array_rand($publicidades,6);
    
}
  shuffle($claves);
define('USERSHARK','adminshark');
$enlacePag = "./escuela.php?";

?>
  <div id="main">
  <div id="searchBar">
    <form>
      <label class="left fontWhite marginTop5 marginLeft5"><?=$arrayIdiomas['name'];?>:</label>
      <input type="text" name="searchText" class="left searchText2 noBorder fontGrey fontBold" value="" />
      <label class="left fontWhite marginTop5 marginLeft5"><?=$arrayIdiomas['place'];?>:</label>
      <input type="text" name="searchText" class="left searchText2 noBorder fontGrey fontBold" value="" />
      <label class="left fontWhite marginTop5 marginLeft5"><?=$arrayIdiomas['style'];?>:</label>
      <input type="image" name="submit" class="right marginRight7 marginTop2" id="searchButton" value="" />
      <input type="text" name="searchText" class="right searchText2 noBorder fontGrey fontBold" value="" />
	  <input type="hidden" name="lang" value="<?=$idioma?>" />
    </form>
  </div>
  <div class="column2 fontBold marginTop5 fontDarkGrey">
    <?php 
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'http://gdata.youtube.com/feeds/api/videos?author='.USERSHARK);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
    $xml = curl_exec($ch);
    $error = curl_error($ch);
    curl_close($ch);
    
    $object = @simplexml_load_string($xml);
    
    $i = 1;
    foreach ($object->entry as $video){
      $aux = explode("/", (string) $video->id);
      $id = $aux[count($aux) - 1];
      $title = str_replace('.mp4', '', $video->title);
      echo '<div id="eventBoxClose' . $i . '" class="boxEvents" onclick="javascript:showElement(\'eventBoxOpen' . $i . '\');javascript:hideElement(\'eventBoxClose' . $i . '\');">
              <div class="infoEscuela">
                <div class="width100">
                  <span class="font3 marginTop5 marginBottom5 left">'. $title .'</span>
                </div>
                <span class="clear left marginTop10">'. $video->content .'</span>
              </div>
            </div>';
      echo '<div id="eventBoxOpen' . $i . '" class="boxEventsOpenEscuela marginBottom10 displayNone">
          <div style="position:relative;float:right;right:5px;top:5px;cursor:pointer;color:#fff" onclick="javascript:showElement(\'eventBoxClose' . $i . '\');javascript:hideElement(\'eventBoxOpen' . $i . '\');">X</div>
              <div class="escuelaYouTube marginTop10 marginLeft10">'. $video->content .'</div>
              <div class="box11 textJustified">
                <div class="infoEscuelaOpen fontWhite">
					<div class="width100">
            			<span class="left">'. $title .'</span>
            		</div>
                  <div class="clear left marginTop15">
                     <object style="height: 250px; width: 500px">
                     <param name="movie" value="http://www.youtube.com/v/'.$id.'?version=3&feature=player_embedded">
                     <param name="allowFullScreen" value="true">
                     <param name="allowScriptAccess" value="always">
                     <embed src="http://www.youtube.com/v/'.$id.'?version=3&feature=player_embedded&autoplay=0" type="application/x-shockwave-flash" allowfullscreen="true" allowScriptAccess="always" width="500" height="250"></object>
                  </div>
                </div>
              </div>
            </div>';
      $i++;
    }
    ?>
  </div>
  <div class="advertisementRow3"><?php
		foreach($claves as $c){
	   echo '<br><a href="'.$publicidades[$c]['url'].'"><img src="http://www.sharkdj.com/'. $publicidades[$c]['imagen'] .'" alt="' . $arrayIdiomas['ads'] . '" title="' . $arrayIdiomas['ads'] . '" /></a><br>';
	}?>
	  </div>
  <div id="nextPreviousMenu" class="clear left insideRow">
      <div id="nextPreviousMenu" class="clear left insideRow">
      	<div class="left width33"><?php if ($pagina>0) { ?><a href="<?php echo $enlacePag;?>p=<?php echo ($pagina-1).$langURL;?>" class="noDecoration fontGrey2 fontBold hoverLargeGrey">&laquo; <?=$arrayIdiomas['previousMenu'];?></a><?php }?><a class="noDecoration fontGrey2 fontBold hoverLargeGrey">&nbsp;</a></div>
      	<div class="left width33 textCentered marginLeft5"><a href="/<?=$langURL;?>" class="noDecoration fontGrey2 fontBold hoverLargeGrey"><?=$arrayIdiomas['homeMenu'];?></a></div>        
      	<div class="right"><?php if (($pagina+1)*15 < $numTotalNoticias) {?><a href="<?php echo $enlacePag;?>p=<?php echo ($pagina+1).$langURL;?>" class="fontBold noDecoration fontGrey2 hoverLargeGrey"><?=$arrayIdiomas['nextMenu'];?> &raquo;</a><?php } ?></div>
      </div>
    </div>
</div>
<?php include_once 'includes/footer.inc.php'; ?>