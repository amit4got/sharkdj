<?php
class Escuela extends Controller {
	function __construct() 
	{
		parent::Controller();
		if ($this->redux_auth->logged_in()==1 ){
			$profile_var = $this->redux_auth->profile();
			if ($profile_var->group!="admin" && $profile_var->group!="super" ){
				$this->redux_auth->logout();
				redirect('admin');
			}
		}else{
			$this->redux_auth->logout();
			redirect('admin');
		}
		
		if ( !function_exists('version_compare') || version_compare( phpversion(), '5', '<' ) ){
			$this->load->library('php4/ckeditor');
			$this->load->library('php4/ckfinder');
		}else{
			$this->load->library('php5/ckeditor');
			$this->load->library('php5/ckfinder');
		}
		$this->load->helper('ckeditor');
	}
	function index()
	{
		redirect('admin/escuela/listado/');
	}
	function listado()
	{
		
		
		$this->data['profile']= $this->redux_auth->profile();
		$this->data['head'] = $this->load->view('admin/head_view', $this->data, true);
		$this->data['navigation'] = $this->load->view('admin/main_menu_view', null, true);
		$this->data['content_menu'] = $this->load->view('admin/escuela_menu_view', null, true);
	
		$this->db->order_by("orden","DESC");
		$this->data['query']=$this->db->get('Escuela_tutoriales_texto');
		$this->data['content'] =$this->load->view('admin/escuela_listado_view', $this->data, true); 
		$this->data['foot'] = "";
		$this->load->view('templates/admin_view', $this->data);
		
	}
	function nuevo()
	{
		$this->form_validation->set_rules('es_titulo', '"Título en español"', 'required');
	  $this->form_validation->set_rules('en_titulo', '"Título en ingles"', 'required');
	  $this->form_validation->set_rules('du_titulo', '"Título en holandes"', 'required');
		$this->form_validation->set_rules('es_subtitulo', '"Subtítulo en español"', '');
		$this->form_validation->set_rules('en_subtitulo', '"Subtítulo en ingles"', '');
		$this->form_validation->set_rules('du_subtitulo', '"Subtítulo en holandes"', '');
		$this->form_validation->set_rules('es_informacion', '"Texto en español"', '');
		$this->form_validation->set_rules('en_informacion', '"Texto en ingles"', '');
		$this->form_validation->set_rules('du_informacion', '"Texto en holandes"', '');
	    $this->form_validation->set_error_delimiters('<p class="error">', '</p>');
	    if ($this->form_validation->run() == false)
	    { 
			$this->data['profile']= $this->redux_auth->profile();
			$this->data['head'] = $this->load->view('admin/head_view', $this->data, true);
			$this->data['navigation'] = $this->load->view('admin/main_menu_view', null, true);
			$this->data['content_menu'] = $this->load->view('admin/escuela_menu_view', null, true);
			$this->data['accion_form']='admin/escuela/nuevo';
			
			$this->data['registro'] = array(
							"es_titulo" => '' , 
							"en_titulo" => '' ,
			"du_titulo" => '' ,
							"es_subtitulo" => '' , 
							"en_subtitulo" => '' ,
			"du_subtitulo" => '' ,
							"es_informacion" => '' , 
							"en_informacion" => '' ,
			"du_informacion" => '' );
			
			$ckeditor =  CK_Editor();
			$this->data['ckeditor']= $ckeditor; 
		
	        $this->data['content'] = $this->load->view('admin/escuela_form_view', $this->data, true);
	       	$this->data['foot'] = "";
			$this->load->view('templates/admin_view', $this->data);
	    }
	    else
	    {
	    	$config_upload['upload_path']    = './uploads/tutoriales/'; 
			$config_upload['allowed_types']  = 'gif|jpg|png'; 
			$config_upload['max_size']       = '1000'; 
			$config_upload['max_width']      = '2048'; 
			$config_upload['max_height']     = '1536'; 
			$this->load->library('upload', $config_upload);
			if ( $this->upload->do_upload('imagen')) 
			{ 
				$imagen=$this->upload->data();
				$this->data['registro']['imagen'] = $imagen['file_name'];
				$config_resize['image_library']   = 'GD2'; 
				$config_resize['source_image']    = './uploads/tutoriales/'.$imagen['file_name']; 
				$config_resize['thumb_marker']       = 'p';
				//$config_resize['new_image']       = './uploads/fiestas/'.$imagen['raw_name'].'p'.$imagen['file_ext']; 
				$config_resize['create_thumb']    = TRUE; 
				$config_resize['maintain_ratio']  = TRUE; 
				$config_resize['width']           = 175; 
				$config_resize['height']          = 175; 
				$this->load->library('image_lib', $config_resize); 
				$this->image_lib->resize(); 

			} 
			
			
			$this->db->select_max('orden');
			$consulta = $this->db->get('Escuela_tutoriales_texto');
			if ($consulta){
				$row = $consulta->row_array();
				$orden= $row['orden']+1;
			} else{
				$orden= 1;
			}
		   
			$this->data['registro']['orden'] 			= $orden;
			$this->data['registro']['es_titulo'] 		= $this->input->post('es_titulo');
			$this->data['registro']['en_titulo'] 		= $this->input->post('en_titulo');
			$this->data['registro']['du_titulo'] 		= $this->input->post('du_titulo');
			$this->data['registro']['es_subtitulo'] 	= $this->input->post('es_subtitulo');
			$this->data['registro']['en_subtitulo'] 	= $this->input->post('en_subtitulo');
			$this->data['registro']['du_subtitulo'] 	= $this->input->post('du_subtitulo');
			$this->data['registro']['es_informacion'] 	= $this->input->post('es_informacion');
			$this->data['registro']['en_informacion'] 	= $this->input->post('en_informacion');
			$this->data['registro']['du_informacion'] 	= $this->input->post('du_informacion');
			
			
			$this->db->set($this->data['registro']); 
			
			if ($this->db->insert('Escuela_tutoriales_texto')){
				$this->session->set_flashdata('message', '<p class="notice">La información ha sido insertada.</p>');
	            redirect('admin/escuela/nuevo/'.$this->uri->segment(4));
			} else
	        {
	            $this->session->set_flashdata('message', '<p class="error">No se ha podido guardar.</p>');
	            redirect('admin/escuela/nuevo/'.$this->uri->segment(4));
	        }
	    }	
		
		
	}
	function modificar()
	{
		$this->form_validation->set_rules('es_titulo', '"Título en español"', 'required');
	  $this->form_validation->set_rules('en_titulo', '"Título en ingles"', 'required');
	  $this->form_validation->set_rules('du_titulo', '"Título en holades"', 'required');
		$this->form_validation->set_rules('es_subtitulo', '"Subtítulo en español"', '');
		$this->form_validation->set_rules('en_subtitulo', '"Subtítulo en ingles"', '');
		$this->form_validation->set_rules('du_subtitulo', '"Subtítulo en holandes"', '');
		$this->form_validation->set_rules('es_informacion', '"Texto en español"', '');
		$this->form_validation->set_rules('en_informacion', '"Texto en ingles"', '');
		$this->form_validation->set_rules('du_informacion', '"Texto en holandes"', '');
	    $this->form_validation->set_error_delimiters('<p class="error">', '</p>');
	    if ($this->form_validation->run() == false)
	    { 
			
			$this->data['profile']= $this->redux_auth->profile();
			$this->data['head'] = $this->load->view('admin/head_view', $this->data, true);
			$this->data['navigation'] = $this->load->view('admin/main_menu_view', null, true);
			$this->data['content_menu'] = $this->load->view('admin/escuela_menu_view', null, true);
	        
			$this->data['accion_form']='admin/escuela/modificar/'.$this->uri->segment(4);
			
			$this->db->where('id', $this->uri->segment(4) );
			$query=$this->db->get('Escuela_tutoriales_texto');
			$this->data['errores']='';
			$ckeditor =  CK_Editor();
			$this->data['ckeditor']= $ckeditor; 
			foreach ($query->result() as $row)
			{
				if ($row->imagen!=""){
					$ar_imagen=explode(".",$row->imagen);
					$imagen=base_url().'uploads/tutoriales/'.$ar_imagen[0].'p.'.$ar_imagen[1];
				}else{
					$imagen=base_url().'uploads/tutoriales/espacio.gif';
				}
				
				$this->data['registro'] = array(
											"es_titulo" 		=> $row->es_titulo , 
											"en_titulo" 		=> $row->en_titulo ,
				"du_titulo" 		=> $row->du_titulo ,
											"imagen"			=> $imagen ,
											"es_subtitulo" 		=> $row->es_subtitulo , 
											"en_subtitulo" 		=> $row->en_subtitulo ,
				"du_subtitulo" 		=> $row->du_subtitulo ,
											"es_informacion" 	=> htmlspecialchars_decode($row->es_informacion) , 
											"en_informacion" 	=> htmlspecialchars_decode($row->en_informacion) ,
				"du_informacion" 	=> htmlspecialchars_decode($row->du_informacion) );
			}
			if (!isset($this->data['registro'])){
				 redirect('admin/escuela/listado');
			}
			$this->data['content'] = $this->load->view('admin/escuela_form_view', $this->data, true);
	       	$this->data['foot'] = "";
			$this->load->view('templates/admin_view', $this->data);
	    }
	    else
	    {
	        $config_upload['upload_path']    = './uploads/tutoriales/'; 
			$config_upload['allowed_types']  = 'gif|jpg|png'; 
			$config_upload['max_size']       = '1000'; 
			$config_upload['max_width']      = '2048'; 
			$config_upload['max_height']     = '1536'; 
			$update = array(
							"es_titulo" 		=> $this->input->post('es_titulo') , 
							"en_titulo" 		=> $this->input->post('en_titulo') ,
			"du_titulo" 		=> $this->input->post('du_titulo') ,
							"es_subtitulo" 		=> $this->input->post('es_subtitulo') , 
							"en_subtitulo" 		=> $this->input->post('en_subtitulo') ,
			"du_subtitulo" 		=> $this->input->post('du_subtitulo') ,
							"es_informacion" 	=> $this->input->post('es_informacion'),
							"en_informacion" 	=> $this->input->post('en_informacion'),
			"du_informacion" 	=> $this->input->post('du_informacion'));
			$this->load->library('upload', $config_upload);
			if ( $this->upload->do_upload('imagen')) 
			{ 
				$imagen=$this->upload->data();
				$update['imagen'] = $imagen['file_name'];
				$config_resize['image_library']   = 'GD2'; 
				$config_resize['source_image']    = './uploads/tutoriales/'.$imagen['file_name']; 
				$config_resize['thumb_marker']       = 'p';
				//$config_resize['new_image']       = './uploads/fiestas/'.$imagen['raw_name'].'p'.$imagen['file_ext']; 
				$config_resize['create_thumb']    = TRUE; 
				$config_resize['maintain_ratio']  = TRUE; 
				$config_resize['width']           = 175; 
				$config_resize['height']          = 175; 
				$this->load->library('image_lib', $config_resize); 
				$this->image_lib->resize(); 
				
				$this->db->where('id', $this->uri->segment(4) );
				
				$query=$this->db->get('Escuela_tutoriales_texto');
				foreach ($query->result() as $row)
				{
					if ($row->imagen!=''){
						$ar_imagen=explode(".",$row->imagen);
						$imagen=PUBPATH.'uploads/tutoriales/'.$ar_imagen[0].'p.'.$ar_imagen[1];
						unlink($imagen);
						$imagen=PUBPATH.'uploads/tutoriales/'.$ar_imagen[0].'.'.$ar_imagen[1];
						unlink($imagen);
					}
				}
			} 
			
			
			
			$this->db->where('id', $this->uri->segment(4) );
			
			if ($this->db->update("Escuela_tutoriales_texto",$update)){
				$this->session->set_flashdata('message', '<p class="notice">La información ha sido modifica.</p>');
	            redirect('admin/escuela/modificar/'.$this->uri->segment(4));
			} else
	        {
	            $this->session->set_flashdata('message', '<p class="error">No se ha podido modificar.</p>');
	            redirect('admin/escuela/modificar/'.$this->uri->segment(4));
	        }
	    }	
	}
	function borrar()
	{
		$this->data['profile']= $this->redux_auth->profile();
		$this->data['head'] = $this->load->view('admin/head_view', $this->data, true);
		$this->data['navigation'] = $this->load->view('admin/main_menu_view', null, true);
		$this->data['content_menu'] = $this->load->view('admin/escuela_menu_view', null, true);
		
		
		$this->db->where('id', $this->uri->segment(4 ) );
		
		$query=$this->db->get('Escuela_tutoriales_texto');
		foreach ($query->result() as $row)
		{
			$this->data['es_titulo'] =$row->es_titulo;
		}
		
		$this->data['content'] =$this->load->view('admin/escuela_borrar_view', $this->data, true); ;
		$this->data['foot'] = "";
		$this->load->view('templates/admin_view', $this->data);
	}
	function borrar_confirm()
	{
		$this->data['profile']= $this->redux_auth->profile();
		$this->db->where('id', $this->uri->segment(4) );
		$query=$this->db->get('Escuela_tutoriales_texto');
		if ($query->num_rows() > 0){
				$this->db->where('id', $this->uri->segment(4) );
				$this->db->delete('Escuela_tutoriales_texto'); 
		}
		redirect('admin/escuela/listado/');
	}
	function subir()
	{
		$this->db->where('id', $this->uri->segment(4) );
		$consulta = $this->db->get('Escuela_tutoriales_texto');
		if ($consulta->num_rows() > 0){
			$row   = $consulta->row_array();
			$orden = $row['orden'];
			$this->db->where('orden >', $orden );	
			$this->db->order_by("orden","ASC");
			$consulta_cambio = $this->db->get('Escuela_tutoriales_texto');
			if ($consulta_cambio->num_rows() > 0){
				$row_cambio   = $consulta_cambio->row_array();
				$orden_cambio = $row_cambio['orden'];
				$id_cambio    = $row_cambio['id'];
				
				$update = array("orden" => $orden_cambio );
				$this->db->where('id', $this->uri->segment(4) );
				$this->db->update("Escuela_tutoriales_texto",$update);
				
				$update = array("orden" => $orden );
				$this->db->where('id', $id_cambio );
				$this->db->update("Escuela_tutoriales_texto",$update);
					
			}
		} 
		redirect('admin/escuela/listado/');
	}
	function bajar()
	{
		$this->db->where('id', $this->uri->segment(4) );
		$consulta = $this->db->get('Escuela_tutoriales_texto');
		if ($consulta->num_rows() > 0){
			$row   = $consulta->row_array();
			$orden = $row['orden'];
			
			$this->db->where('orden <', $orden );	
			$this->db->order_by('orden','DESC');
			$consulta_cambio = $this->db->get('Escuela_tutoriales_texto');
			if ($consulta_cambio->num_rows() > 0){
				$row_cambio   = $consulta_cambio->row_array();
				$orden_cambio = $row_cambio['orden'];
				$id_cambio    = $row_cambio['id'];
				
				$update = array("orden" => $orden_cambio );
				$this->db->where('id', $this->uri->segment(4) );
				$this->db->update("Escuela_tutoriales_texto",$update);
				
				$update = array("orden" => $orden );
				$this->db->where('id', $id_cambio );
				$this->db->update("Escuela_tutoriales_texto",$update);
					
			}
		} 
		redirect('admin/escuela/listado/');
	}
	
}


