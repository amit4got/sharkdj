<?php
class News extends Controller {
	function __construct() 
	{
		parent::Controller();
		$this->load->model('banner_model');
		$this->load->library('pagination');
	}
	function index()
	{
		redirect('web/news/listing/0');
	}
	function listing()
	{
		$config['base_url'] = 'http://www.sharkdj.com/index.php/web/news/listing';
		
		$query=$this->db->where('es_clave','Noticias')->get('Textos');
			
		$config["total_rows"] = $query->num_rows();
		$config['per_page'] = '5';
		$config['cur_page']=$this->uri->segment(4);
	
		
		$this->pagination->initialize($config);
		
		$this->data['pulsado']='noticias';
		$this->data['sub_head'] = $this->load->view('web_en/sub_head_1_view', $this->data, true);
		$this->db->where('es_clave', 'noticias');
		$this->db->order_by('id','DESC');
		$this->data['query']=$this->db->get('Textos', $config['per_page'] , $this->uri->segment(4) );
		$this->data['cuerpo'] = $this->load->view('web_en/noticias_listado_view', $this->data, true);
		$this->load->view('templates/en_web_view',  $this->data);
	
	}
	
	function detail()
	{
		$this->data['pulsado']='noticias';
		$this->data['sub_head'] = $this->load->view('web_en/sub_head_1_view', $this->data, true);
		$this->db->where('es_clave',  'noticias');
		$this->db->where('id', $this->uri->segment(4) );
		$query=$this->db->get('Textos');
		foreach ($query->result() as $row)
		{
			if ($row->imagen!=""){
				$imagen=base_url().'uploads/Noticias/'.$row->imagen;
			}else{
				$imagen=base_url().'uploads/Noticias/espacio.gif';
			}
			$this->data['registro'] = array(
										"es_titulo" 		=> $row->en_titulo , 
										"imagen"			=> $imagen ,
										"es_informacion" 	=> htmlspecialchars_decode($row->en_informacion) 
										);
		}
		if (!isset($this->data['registro'])){
			 redirect('web/news');
		}
		
		$this->data['cuerpo'] = $this->load->view('web_en/noticias_detalle_view', $this->data, true);
		$this->load->view('templates/en_web_view',  $this->data);
		
	}
}


