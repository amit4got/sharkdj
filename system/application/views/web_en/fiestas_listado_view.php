<script src="<?=base_url(); ?>js/JSCal2-1.8/src/js/jscal2.js"></script>
<script src="<?=base_url(); ?>js/JSCal2-1.8/src/js/lang/es.js"></script>

<div id="cuerpo_1">
    <div id="cuerpo_noticias">
        <div id="noticias_detalle">
        	<div id="calendario_fiestas" ></div>
            <ul id="listado_fiestas" >
            	<?php if (count($arr_fiestas)>0): ?>
				<?php foreach($arr_fiestas as $fila_fiesta): ?>
                <li>
                	<div class="listad_fiestas_titulo"><?=$fila_fiesta['titulo']; ?></div>
                    <div class="listad_fiestas_informacion"><?=substr(strip_tags($fila_fiesta['informacion']),0,200).'...';?></div>
                    
                    <div class="listad_fiestas_localizacion"><b>Localización: </b><?=$fila_fiesta['ciudad']; ?>, <?=$fila_fiesta['pais']; ?></div>
					<div class="listad_fiestas_hora">
                    <b>Fecha: </b>
						<? 
                            list($f_date,$f_hora)=explode(" ",$fila_fiesta['fecha']);
                            list($ano,$mes,$dia)=explode("-",$f_date);
                           
                            echo ($dia.'/'.$mes.'/'.$ano);
                        ?>
                    
                    <b>Hora: </b>
						<? 
                            
                            list($hora,$minutos,$segundos)=explode(":",$f_hora);
                            substr($hora,0,1)==0 ? $hora=substr($hora,1,2):$hora;
                            echo ($hora.':'.$minutos);
                        ?>
                    </div>
                	<div class="fiestas_enlace"><?=anchor('web/parties/information/'.$fila_fiesta['id'].'/'.$fila_fiesta['titulo']	, 'Read more' ); ?></div>
            	</li>
               	<?php endforeach;?>
				<?php endif; ?> 
            </ul>
        </div>
    </div>
    <div id="banner">
    	<?= $this->banner_model->cargar_banner_es (); ?>
    </div>
	
</div>
<script type="text/javascript">
	//<![CDATA[
      // this handler is designed to work both for onSelect and onTimeChange
      // events.  It updates the input fields according to what's selected in
      // the calendar.
	  var cto_time=0;
      function updateFields(cal) {
              var date = cal.selection.get();
              if (date) {
                      date = Calendar.intToDate(date);
                      document.getElementById("fecha").value = Calendar.printDate(date, "%Y-%m-%d");
					  buscar();
              }
      };
      Calendar.setup({
              cont         : "calendario_fiestas",
			  selection    : <?=$fecha_corta ?>,
              onSelect     : updateFields,
              onTimeChange : updateFields
      }); 	
    //]]>
</script> 