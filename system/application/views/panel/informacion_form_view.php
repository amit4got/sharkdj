<div id="cuerpo">
<script src="<?=base_url(); ?>js/jquery/jquery.js" type="text/javascript"></script>
<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAA7rkcgy9PNk8KUX29tySlehQ2gKpxVqWRT6wE0XsFNYvAiuTpzxTuLva2-syTq5I_J-decdqo2c8PPA"
      type="text/javascript"></script>
<script type="text/javascript">
	//<![CDATA[
	function TextualZoomControl() {
		}
		TextualZoomControl.prototype = new GControl();
		
		// Creates a one DIV for each of the buttons and places them in a container
		// DIV which is returned as our control element. We add the control to
		// to the map container and return the element for the map class to
		// position properly.
		TextualZoomControl.prototype.initialize = function(map) {
		  var container = document.createElement("div");
		
		  var btnMapa = document.createElement("div");
		  this.setButtonStyle_(btnMapa);
		  container.appendChild(btnMapa);
		  btnMapa.appendChild(document.createTextNode("MAPA"));
		  GEvent.addDomListener(btnMapa, "click", function() {
			var x= map.getMapTypes(); 
    		  map.setMapType(x[0]); 
		  });
		
		  var btnSatelite = document.createElement("div");
		  this.setButtonStyle_(btnSatelite);
		  container.appendChild(btnSatelite);
		  btnSatelite.appendChild(document.createTextNode("SATELITE"));
		  GEvent.addDomListener(btnSatelite, "click", function() {
			var x= map.getMapTypes(); 
    		 map.setMapType(x[1]); 
		  });
		  var btnMixto = document.createElement("div");
		  this.setButtonStyle_(btnMixto);
		  container.appendChild(btnMixto);
		  btnMixto.appendChild(document.createTextNode("MIXTO"));
		  GEvent.addDomListener(btnMixto, "click", function() {
			 var x= map.getMapTypes(); 
    		  map.setMapType(x[2]); 
		  });
		  map.getContainer().appendChild(container);
		  return container;
		}
		
		// By default, the control will appear in the top left corner of the
		// map with 7 pixels of padding.
		TextualZoomControl.prototype.getDefaultPosition = function() {
		  return new GControlPosition(G_ANCHOR_TOP_RIGHT, new GSize(7, 7));
		}
		
		// Sets the proper CSS for the given button element.
		TextualZoomControl.prototype.setButtonStyle_ = function(button) {
		  button.style.textDecoration = "none";
		  button.style.color = "#000000";
		  button.style.backgroundColor = "white";
		  button.style.font = "10px verdana";
		  button.style.border = "1px solid black";
		  button.style.padding = "2px";
		  button.style.marginBottom = "2px";
		  button.style.textAlign = "center";
		  button.style.width = "60px";
		  button.style.cursor = "pointer";
		}

	function load() {
	  if (GBrowserIsCompatible()) {
	   var mipoint = [];
	   var i=0;
	  var map = new GMap2(document.getElementById("map"));
		map.addControl(new GLargeMapControl());
       	map.addControl(new TextualZoomControl());

		map.setCenter(new GLatLng(document.forms.form1.posy_flash.value,document.forms.form1.posx_flash.value ),7 );
		
		 GEvent.addListener(map, "click", function(overlay, point) {
		 	 if (overlay) {
			 }else{
			map.removeOverlay(mipoint[i]);
			i++;
			mipoint[i]=new GMarker(point, icon)
			map.addOverlay(mipoint[i]);
			document.forms.form1.posx_flash.value=point.x;
			document.forms.form1.posy_flash.value=point.y;
			}
        });
		
		var icon = new GIcon();
			icon.image = "<?=base_url(); ?>file/entorno/marcador.png";
			icon.iconSize = new GSize(8, 8);
			//icon.shadowSize = new GSize(22, 20);
			icon.iconAnchor = new GPoint(4, 8);
			icon.infoWindowAnchor = new GPoint(0, 0);
			
		var point = new GLatLng(document.forms.form1.posy_flash.value,document.forms.form1.posx_flash.value );
		 mipoint[i]=new GMarker(point, icon)
		 map.addOverlay( mipoint[i]);
	  
	  
	  }
	}
	function Comprobar_chequeados()
	{
		var cadena_texto ="";
		var id_i=0;
		while (document.getElementById("check_list"+id_i)) {
			if (document.getElementById("check_list"+id_i).checked == true){
				cadena_texto+=document.getElementById("check_list"+id_i).value+",";
			}
			id_i++;
		}
		document.getElementById("estilos_id").value=cadena_texto;
	}
		 
	$(document).ready(function(){
		$("#select_pais").change(function(){
			//alert ("cambio"+$(this).val());
			//$("#ciudad").load('<?=base_url(); ?>index.php/admin/fiestas/ajax_cargar_ciudades/'+$(this).val());
			$("#ciudad").load('<?=base_url(); ?>index.php/panel/informacion/ajax_cargar_ciudades/'+$(this).val());
		});
	});	 
	
	//]]>
	
</script>


<?=$this->session->flashdata('message'); ?>
<?=validation_errors(); ?>

<?=form_open_multipart($accion_form, array("id" => 'form1' ) ); ?>


<div class="campos_supertitulo">Información del artista</div>
<div class="campos">
    <div class="campos_titulo">Estado del artista</div>
    <div class="campos_introducir">
        <select id="visible" name="visible"	>
            <option value="0" <?= ($registro['visible']=='0')?  'selected="selected"' : ''; ?>>NO VISIBLE EN LA WEB</option>
            <option value="1" <?= ($registro['visible']=='1')?  'selected="selected"' : ''; ?>>VISIBLE EN LA WEB</option>
        </select>
    </div>
</div>


<div class="campos">
    <div class="campos_titulo">Alias</div>
    <div class="campos_introducir"><?=form_input('alias',set_value('alias')=="" ?  $registro['alias']  :   set_value('alias') ); ?></div>
</div>
<div class="campos">
    <div class="campos_titulo">Imagen para el artista</div>
    <div class="campos_introducir"><?=form_upload('imagen'); ?></div>
</div>
 <?php if ($registro['imagen']!=''): ?>
    <div class="campos">
        <img src="<?= $registro['imagen']; ?>" />
    </div>
<?php endif; ?>
<div class="campos">
    <div class="campos_titulo">Estilos</div>
    <div class="campos_introducir">
         <ul>
            <?php 
                $cadena_estilo="";
                $id_i=0;
                if (count($qry_estilo)>0): 
            ?>
                <?php foreach($qry_estilo as $qry_fila_estilo): ?>
                <li>
                    <input onclick="Comprobar_chequeados();" type="checkbox"  name="check_list[]" id="check_list<?=$id_i ?>"  value="<?=$qry_fila_estilo['id']; ?>" <? 
                    if ( count($slct_estilo)>0 )
                    {
                        foreach($slct_estilo as $slct_fila_estilo)    
                        {  
                            if ($qry_fila_estilo['id']==$slct_fila_estilo){
                                echo 'checked="checked"';
                                $cadena_estilo.=$qry_fila_estilo['id'].",";
                            }
                        }
                    }else{
                        echo 'checked="checked"';
                       $cadena_estilo.=$qry_fila_estilo['id'].",";
                    }
                    ?>><?=$qry_fila_estilo['titulo']; ?><input type="hidden" value="<?=$qry_fila_estilo['titulo']; ?>" id="titulo_eslito_<?=$qry_fila_estilo['id']; ?>"  />
                </li>
                <? $id_i+=1; ?>
                <?php endforeach;?>
            <?php endif; ?>       
        </ul>
    </div>
</div>
<input type="hidden" id="estilos_id" name="estilos_id" value="<?=$cadena_estilo; ?>"  /> 

 <div class="campos">
    <div class="campos_titulo">Pais</div>
    <div class="campos_introducir">
        <select id="select_pais" name="select_pais"	>
        <?php if ($query->num_rows()>0): ?>
            <?php foreach($query->result() as $row): ?>
                <option value="<?=$row->id; ?>" <?= ($row->id==$registro['id_pais'])?  'selected="selected"' : ''; ?>><?=$row->es_titulo; ?> / <?=$row->en_titulo; ?></option>
            <?php endforeach;?>
        <?php endif; ?>    
        </select>
    </div>
</div>
 <div class="campos">
    <div class="campos_titulo">Ciudad</div>
    <div class="campos_introducir" id="ciudad" >
        <?php if ($query_ciudad->num_rows()>0): ?>
                <select id="select_ciudad" name="select_ciudad"  >
                <?php foreach($query_ciudad->result() as $row): ?>
                    <option value="<?=$row->id; ?>" <?= ($row->id==$registro['id_ciudad'])?  'selected="selected"' : ''; ?> ><?=$row->es_titulo; ?> / <?=$row->en_titulo; ?></option>
                <?php endforeach;?>
                </select>
            <?php endif; ?>    
    </div>
</div>
 <div class="campos">
    <div class="campos_titulo">Nueva ciudad</div>
    <div class="campos_introducir">Español: <?=form_input('es_new_ciudad', '' , 'style="width:150px;"' ); ?>  Ingles <?=form_input('en_new_ciudad', '', 'style="width:150px;"' ); ?></div>
</div>
 
       
         
  <div class="campos_supertitulo">Detalle del artista</div>
    <div class="campos">
        <div class="campos_titulo">Texto en Español</div>
        <div class="campos_introducir">
			<?= $ckeditor->editor('es_informacion', set_value('es_informacion')=="" ?  $registro['es_informacion']  :   set_value('es_informacion') ,$ckeditor->config ); ?>
       	</div>
    </div>
    <div class="campos">
        <div class="campos_titulo">Texto en ingles</div>
        <div class="campos_introducir">
        	 <?= $ckeditor->editor('en_informacion', set_value('en_informacion')=="" ?  $registro['en_informacion']  :   set_value('en_informacion') ,$ckeditor->config ); ?>
        </div>
    </div>
    <div class="campos">
        <div class="campos_titulo">Texto en dutch</div>
        <div class="campos_introducir">
        	 <?= $ckeditor->editor('du_informacion', set_value('du_informacion')=="" ?  $registro['du_informacion']  :   set_value('du_informacion') ,$ckeditor->config );?>
        </div>
    </div>
    <div class="campos">
    <div class="campos_titulo">Localizacion</div>
    <div class="campos_introducir">
        <?=form_hidden('posx_flash',set_value('posx_flash')=="" ?  $registro['posx_flash']  :   set_value('posx_flash') ); ?>
        <?=form_hidden('posy_flash',set_value('posy_flash')=="" ?  $registro['posy_flash']  :   set_value('posy_flash') ); ?>
        <div style=" background-color:#777; padding: 5px 5px 5px 5px; width: 393px">
            <div id="map" style=" border:#777; width: 390px; height: 300px; overflow:hidden;"></div>
        </div>
    </div>
</div>
    
    <div id="btnsubmit"><?=form_submit('submit', 'Guardar'); ?></div>
<?=form_close(''); ?>
</div> 
<script type="text/javascript">
	<!--
	swfobject.registerObject("FlashID");
	
	//-->
</script>
