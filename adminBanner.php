<?php

if(!defined("RUTA_ABSOLUTA")){
  define("RUTA_ABSOLUTA",dirname(__FILE__));
}

#INCLUDES
require_once RUTA_ABSOLUTA.'/administradorDatos/artist.class.php';
require_once RUTA_ABSOLUTA.'/administradorDatos/administradorDatos.class.php';
include	RUTA_ABSOLUTA.'/editor/spaw2/spaw.inc.php';

session_start();

if ($_SESSION['statusAdmin'] != "authorized") {
  header ("Location: index.php");
  exit();
}
include_once 'includes/headerAdmin.inc.php';

function leerDirectorioBanner()
{
  $dir=RUTA_ABSOLUTA.'/images/carousel/';
  $directorio=opendir($dir);
  while ($archivo = readdir($directorio)){
    $imagenes[]=$archivo;
  }
  closedir($directorio);
  return $imagenes;
}

$status = "";
if ($_POST["action"] == "upload") {
  // obtenemos los datos del archivo
  $tamano = $_FILES["archivo"]['size'];
  $tipo = $_FILES["archivo"]['type'];
  $archivo = $_FILES["archivo"]['name'];
  $prefijo = explode(" ",microtime());
  $prefijo = (string)$prefijo[0].'_'.(string)$prefijo[1];
   
  if ($archivo != "") {
    // guardamos el archivo a la carpeta files
    $destino =  "images/carousel/".$prefijo."_".$archivo;
    if (copy($_FILES['archivo']['tmp_name'],$destino)) {
      $status = "Archivo subido: <b>".$archivo."</b>";
    } else {
      $status = "Error al subir el archivo";
    }
  } else {
    $status = "Error al subir archivo";
  }
}

if ($_POST["action"] == "delete") {
  $filename = './images/carousel/'.$_POST['nombreArchivo'];
  if (!unlink($filename)){
    $status = 'No se pudo borrar el archivo';
  }
  else{
    $status = 'Archivo Borrado';
  }
}


$imagenes = leerDirectorioBanner();
?>

  <div id="content">
    <div id="container">
      <?php echo $status.'<br><br>';
            echo 'Seleccione un archivo de banner (795px x 298px):<br>';?>
      
      <form action="<?php echo $_SERVER["PHP_SELF"];?>" method="post" enctype="multipart/form-data">
        <input name="archivo" type="file" size="35" />
        <input name="enviar" type="submit" value="Upload File" />
        <input name="action" type="hidden" value="upload" />     
      </form>
      
      <?php 
      for($i=2;$i <= count($imagenes)-1;$i++){
        echo '<img src="images/carousel/'.$imagenes[$i].'">';  
        echo '<form name="form'.$i.'" action="'.$_SERVER['PHP_SELF'].'" method="POST">';
          echo '<input type="hidden" name="nombreArchivo" value="'.$imagenes[$i].'"/>';
          echo '<input name="action" type="hidden" value="delete" />';
        echo '<div style="position:relative;"><input type="submit" name="submit" value="Borrar"/></div>';
        echo '</form> <br><br>';
      }?>
      
    </div>
  </div>
    
  <div id="menu">
    <img src="images/logoHeader.png" width="150">
    <br><br>
    <ul>
      <li><a href="adminBackend.php?o=3">Noticias</a></li><br><br>
      <li><a href="adminBackendEventos.php?o=3">Eventos</a></li><br><br>
      <li><a href="adminBackendUser.php?o=1">DJ's</a></li><br><br>
      <li><a href="adminBackendLocal.php?o=1">Locales</a></li><br><br>
      <li><a href="adminPublicidad.php">Publicidad</a></li><br><br>
      <li><a href="adminBanner.php">Banner</a></li><br><br>
      <li><a href="admin.php?logout" style="color:red;">Logout</a></li>
    </ul> 
  </div>