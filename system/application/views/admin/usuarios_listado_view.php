<?php if ($query->num_rows()>0): ?>
<ul id="listado">
	<?php foreach($query->result() as $row): ?>
        <li> 
            <div class="username"><?=$row->username;?></div> 
            <div class="mail"><?=$row->email;?></div>
            <div class="listado_botones"><?=anchor('admin/usuarios/borrar/'.$row->id, 'Borrar');?></div>
            <div class="listado_botones"><?=anchor('admin/usuarios/modificar_pw/'.$row->id, 'Modificar password');?></div>
            <div class="listado_botones"><?=anchor('admin/usuarios/modificar/'.$row->id, 'Modificar información');?></div>     
        </li>
    <?php endforeach;?>
</ul>
<?php endif; ?>
