<?php
class Escuela extends Controller {
	function __construct() 
	{
		parent::Controller();
		$this->load->model('banner_model');
		$this->load->library('pagination');
	}
	function index()
	{
		redirect('web/escuela/tutoriales');
		
	}
	function tutoriales()
	{
		$config['base_url'] = 'http://www.sharkdj.com/index.php/web/escuela/tutoriales';
		
		$query=$this->db->get('Escuela_tutoriales_texto');
			
		$config["total_rows"] = $query->num_rows();
		$config['per_page'] = '5';
		$config['cur_page']=$this->uri->segment(4);
		
		$this->data['pulsado']='escuela';
		$this->data['sub_pulsado']='tutoriales';
		$this->data['sub_head'] = $this->load->view('web/sub_head_2_view', $this->data, true);
		$this->db->order_by("orden","DESC");
		$this->data['query']=$this->db->get('Escuela_tutoriales_texto');
		$this->data['cuerpo'] =   $this->load->view('web/tutoriales_listado_view', $this->data, true);
		$this->load->view('templates/es_web_view',  $this->data);
	}
	function tutoriales_detalle()
	{
		$this->data['pulsado']='escuela';
		$this->data['sub_pulsado']='tutoriales';
		$this->data['sub_head'] = $this->load->view('web/sub_head_2_view', $this->data, true);
		
		$this->db->where('id', $this->uri->segment(4) );
		$query=$this->db->get('Escuela_tutoriales_texto');
		foreach ($query->result() as $row)
		{
			$this->data['registro'] = array(
										"es_titulo" 		=> $row->es_titulo , 
										
										"es_informacion" 	=> htmlspecialchars_decode($row->es_informacion) 
										);
		}
		if (!isset($this->data['registro'])){
			 redirect('web/escuela/tutoriales');
		}
		
		$this->data['cuerpo'] = $this->load->view('web/tutoriales_detalle_view', $this->data, true);
		$this->load->view('templates/es_web_view',  $this->data);
	}
	function videos()
	{
		$config['base_url'] = 'http://www.sharkdj.com/index.php/web/escuela/videos';
		
		$query=$this->db->get('Escuela_tutoriales_video');
			
		$config["total_rows"] = $query->num_rows();
		$config['per_page'] = '5';
		$config['cur_page']=$this->uri->segment(4);
		
		$this->data['pulsado']='escuela';
		$this->data['sub_pulsado']='videos';
		$this->data['sub_head'] = $this->load->view('web/sub_head_2_view', $this->data, true);
		$this->db->order_by("orden","DESC");
		$this->data['query']=$this->db->get('Escuela_tutoriales_video');
		$this->data['cuerpo'] =   $this->load->view('web/videos_listado_view', $this->data, true);
		$this->load->view('templates/es_web_view',  $this->data);
	}
	function videos_detalle()
	{
		$this->data['pulsado']='escuela';
		$this->data['sub_pulsado']='videos';
		$this->data['sub_head'] = $this->load->view('web/sub_head_2_view', $this->data, true);
		
		$this->db->where('id', $this->uri->segment(4) );
		$query=$this->db->get('Escuela_tutoriales_video');
		foreach ($query->result() as $row)
		{
			$this->data['registro'] = array(
										"es_titulo" 		=> $row->es_titulo , 
										
										"es_informacion" 	=> htmlspecialchars_decode($row->es_informacion) 
										);
		}
		if (!isset($this->data['registro'])){
			 redirect('web/escuela/tutoriales');
		}
		
		$this->data['cuerpo'] = $this->load->view('web/videos_detalle_view', $this->data, true);
		$this->load->view('templates/es_web_view',  $this->data);
	}
}


