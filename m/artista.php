<?php 
	include_once 'header.php';
	
	function leerDirectorio($idUser)
	{
	  $dir='../upload/php/files/'.$idUser.'/thumbnails/';
	  $directorio=opendir($dir);
	  while ($archivo = readdir($directorio)){
	    $imagenes[]=$archivo;
	  }
	  closedir($directorio);
	  return $imagenes;
	}
	
	if(!defined("RUTA_ABSOLUTA")){
	  define("RUTA_ABSOLUTA",dirname(__FILE__));
	}
	
	require_once RUTA_ABSOLUTA.'/administradorDatos/administradorDatos.class.php';
	require_once RUTA_ABSOLUTA.'/administradorDatos/artist.class.php';
	require_once RUTA_ABSOLUTA.'/administradorDatos/imagenes.class.php';
	#INCLUDES
	require_once '../includes/connect.ini.php';
	require_once '../includes/functions.ini.php';
	
	$idUser = isset($_GET['id']) ? $_GET['id'] : 0;
	$adminArtist = new artist();
	$artista = $adminArtist->obtenerDatosArtista($idUser);
	$facebook = $adminArtist->obtenerRedesSociales($idUser,'1');
	$twitter = $adminArtist->obtenerRedesSociales($idUser,'2');
	$myspace = $adminArtist->obtenerRedesSociales($idUser,'5');
	

  $imagen = '../images/perfil_masculino_sinfoto.png';
  if (file_exists('../images/profiles/'.$artista['idUser'].'_crop.jpeg')){
    $imagen = 'http://sharkdj.com/images/profiles/'.$artista['idUser'].'_crop.jpeg';
  }
?>
	<div data-role="page" data-theme="c" data-content-theme="c" id="home">
		
  <?php echo $header;?>
  <div data-role="content">
    <div class="artist">
      <div class="ui-grid-a">
        <div class="ui-block-a">
    			<div class="image">
    				<img src="<?php echo $imagen; ?>">
    			</div>
        </div>
        <div class="ui-block-b">
          <div class="artistname"><?php echo $artista['nombre']; ?></div>
          <p><a href="http://maps.google.es/maps?q=<?php echo $artista['direccion'];?>"><img src="./imagen/icons/103-map.png"></a></p>
          <div class="iconSocial">
            <?php if ($twitter[0]['url'] !=""){?>
            <a href="http://www.twitter.com/<?php echo $twitter[0]['url']; ?>"><img src="./imagen/icons/twitter.png" width="35"></a>
            <?php }?>
            <?php if ($facebook[0]['url'] !=""){?>
            <a href="<?php echo 'http://'.$facebook[0]['url'];?>"><img src="./imagen/icons/facebook.png" width="35"></a>
            <?php }?>
            <?php if ($myspace[0]['url'] !=""){?>
            <a href="<?php echo 'http://www.myspace.com/'.$myspace[0]['url']?>"><img src="./imagen/icons/myspace.png" width="35"></a>
            <?php }?>
          </div>
        </div> 
      </div>
		</div>
	<?php if ($artista['estilo'] != ''){?>
   	<p><b>Estilo:</b><?php echo $artista['estilo'];?></p>
    <?php }?>
    <?php if ($artista['clubResidencia'] != ''){?>
    <p><b>Residencia:</b><?php echo $artista['clubResidencia']; ?></p>
    <?php }?>
    <?php if ($artista['web'] != ''){?>
    <p><b>Web:</b><a href='<?php echo $artista['web']; ?>'><?php echo str_replace('http://','',$artista['web']); ?></a></p>
    <?php }?>
    <div data-role="collapsible">
      <h3>Bio</h3>
      <p><?php echo $artista['bio']; ?></p>
    </div>
    <div data-role="collapsible">
      <h3>Fotos</h3>
      <div align="center">
      <?php $imagenes = leerDirectorio($artista['idUser']);
          foreach ($imagenes as $imagen){
            if (preg_match('/(gif|jpe?g|png)$/i', $imagen)){?>
              <img src="../upload/php/files/<?php echo $artista['idUser'];?>/<?php echo $imagen;?>">
<?php       }
          }?>
      </div>
    </div>
  </div>
		
		
		
		
    <?php echo $footer?>
    </div>