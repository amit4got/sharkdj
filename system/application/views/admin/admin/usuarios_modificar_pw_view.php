<div id="cuerpo">

<div class="campos_supertitulo">Cambiar password del usuario: <b><?=$username ?></b></div>

<?=validation_errors(); ?>

<?=$errores; ?>

<?=form_open('admin/usuarios/modificar_pw/'.$this->uri->segment(4)); ?>

<div class="formularios_titulo">Campos requeridos</div>

<div class="campos">
    <div class="campos_titulo">Password antiguo</div>
    <div class="campos_introducir"><?=form_password('old', set_value('old')) ?></div>
</div>
<div class="campos">
    <div class="campos_titulo">Nuevo password</div>
    <div class="campos_introducir"><?=form_password('new', set_value('new')) ?></div>
</div>
<div class="campos">
    <div class="campos_titulo">Repita el nuevo password</div>
    <div class="campos_introducir"><?=form_password('new_repeat', set_value('new_repeat')) ?></div>
</div>

<div id="btnsubmit"><?=form_submit('submit', 'Guardar'); ?></div>

<?=form_close(); ?>
</div>