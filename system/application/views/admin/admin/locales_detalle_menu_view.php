<div id="titulo_seccion">Locales</div>
<ul id="submenu">
	<li><?=anchor('admin/locales/listado/', 'Listado'); ?></li>
    <li><?=anchor('admin/locales/nuevo/', 'Nuevo local'); ?></li>
</ul>
<ul id="submenu_detalle">
	<li><b><?=$local; ?></b></li>
	<li><?=anchor('admin/locales/modificar/'.$this->uri->segment(4).'/'.$this->uri->segment(5), 'Detalle'); ?></li>
    <li><?=anchor('admin/local_fiestas/nuevo/'.$this->uri->segment(4).'/'.$local, 'Nueva fiesta'); ?></li>
    <li><?=anchor('admin/local_fiestas/listado/'.$this->uri->segment(4).'/'.$local, 'Listado fiestas'); ?></li>
</ul>