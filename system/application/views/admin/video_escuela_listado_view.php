<?php if ($query->num_rows()>0): ?>
<ul id="listado">
	<?php foreach($query->result() as $row): ?>
        <li> 
            <div class="username"><?=$row->es_titulo;?></div> 
            <div class="listado_botones"><?=anchor('admin/video_escuela/borrar/'.$row->id, 'Borrar');?></div>
            <div class="listado_botones"><?=anchor('admin/video_escuela/bajar/'.$row->id, 'Bajar');?></div>
            <div class="listado_botones"><?=anchor('admin/video_escuela/subir/'.$row->id, 'Subir');?></div>
            <div class="listado_botones"><?=anchor('admin/video_escuela/modificar/'.$row->id, 'Modificar');?></div>
        </li>
    <?php endforeach;?>
</ul>
<?php endif; ?>
