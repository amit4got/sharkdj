<?php
class Kunstenaar extends Controller {
	function __construct() 
	{
		parent::Controller();
		$this->load->model('web_fiestas_model');
		$this->load->model('banner_model');
	}
	function index()
	{
		
		
		$this->data['registro']['local']   ='';
		$this->data['registro']['ciudad']  =0;
		$this->data['registro']['pais']    =0;
		$this->data['registro']['team']    =0;

		$comp_form=$this->input->post('select_pais');
		$sql = 'SELECT * FROM meta as m, groups as g , users as u ';// WHERE word = ? AND ip_address = ? AND captcha_time > ?";
		$sql.= ' WHERE ';
		$sql.= ' g.name = "artistas" ';
		$sql.= ' AND m.visible = "1" ';
		$sql.= ' AND g.id = u.group_id ';
		$sql.= ' AND u.id = m.user_id ';
		$ct_where=0;
		if ($comp_form!='')
		{	
			$this->data['registro']['team']    =$this->input->post('team');
			if ($this->data['registro']['team']=='1'){
				$sql.= ' AND m.team = "1" ';
			
			}
			
			if ( $this->input->post('select_pais')!='0' )
			{
				$this->data['registro']['pais'] 	=  $this->input->post('select_pais') ;
				$this->data['registro']['ciudad'] 	= $this->input->post('select_ciudad');
				$ct_where=1;
				
				
				$sql.= '  AND m.id_pais = '.$this->data['registro']['pais'];
				
				
				if ( $this->data['registro']['ciudad']!='0' )
				{
					$sql.= ' AND m.id_ciudad = '.$this->data['registro']['ciudad'];
					
				}
			}
			if ( $this->input->post('local')!='' )
			{
				$sql.= ' AND ';
				$this->data['registro']['local']   = $this->input->post('local');
				$sql.= ' m.alias LIKE "%'.$this->data['registro']['local'].'%" ';
				
				
			}
			$this->data['select_estilo']=$this->input->post('check_list'); 
			if (count($this->data['select_estilo'])>0){
				$sql.= ' AND (';
				for ($i=0 ; $i< count($this->data['select_estilo']) ; $i++)    
				{   
					
					if ($i==0){
						$sql.= ' m.estilos LIKE "%,'.$this->data['select_estilo'][$i].',%" ';
						
					}else{
						$sql.= ' OR m.estilos LIKE "%,'.$this->data['select_estilo'][$i].',%" ';
					}
				}
				$sql.= ' )';
			}
		}
		$sql.= ' ORDER BY m.alias ';
		$this->data['query']=$this->db->query($sql);
		$this->data['query_pais']=$this->db->get('Pais');
		$this->db->where('id_pais', $this->data['registro']['pais'] );
		$this->data['query_ciudad']=$this->db->get('Ciudad');
		
		$this->data['query_estilo']=$this->db->get('Estilo');
		
		$this->data['pulsado']='artistas';
		$this->data['sub_head'] = $this->load->view('web_du/sub_head_artistas_view', $this->data, true);
		
		$this->data['cuerpo'] = $this->load->view('web_du/artistas_listado_view', $this->data, true);
		$this->load->view('templates/du_web_view',  $this->data);
		
	}
	
	
	function informatie()
	{
		$this->data['pulsado']='artistas';
		$this->data['sub_pulsado']='info';
		$this->db->where('user_id', $this->uri->segment(4) );
		$query=$this->db->get('meta');
		foreach ($query->result() as $row)
		{
			if ($row->imagen!=""){
				$ar_imagen=explode(".",$row->imagen);
				$imagen=base_url().'uploads/informacion/'.$ar_imagen[0].'m.'.$ar_imagen[1];
			}else{
				$imagen=base_url().'uploads/informacion/espacio.gif';
			}
			$this->data['registro'] = array(
										
										
										"alias" 			=> $row->alias , 
										"team" 				=> $row->team ,
										"imagen"			=> $imagen ,
										"es_informacion" 	=> htmlspecialchars_decode($row->du_informacion)
										
				
										);
			$this->db->where('id', $row->id_ciudad );
			$query_ciudad=$this->db->get('Ciudad');
			if ($query_ciudad->num_rows() > 0){
				foreach ($query_ciudad->result() as $row_ciudad)
				{
					$this->data['registro']['ciudad']=$row_ciudad->du_titulo;
				}
			}
			
			$this->db->where('id', $row->id_pais );
			$query_pais=$this->db->get('Pais');
			if ($query_pais->num_rows() > 0){
				foreach ($query_pais->result() as $row_pais)
				{
					$this->data['registro']['pais']=$row_pais->du_titulo;
				}
			}
			
		}
		if (!isset($this->data['registro'])){
			 redirect('web/kunstenaar');
		}
		$this->data['sub_head'] = $this->load->view('web_du/sub_head_artistas_detalle_view', $this->data, true);
		$this->data['cuerpo'] = $this->load->view('web_du/artistas_info_view', $this->data, true);
		$this->load->view('templates/du_web_view',  $this->data);
		
	}
	
	
	function kalender()
	{
		$this->data['pulsado']='artistas';
		$this->data['sub_pulsado']='calendario';
		$this->db->where('user_id', $this->uri->segment(4) );
		$query=$this->db->get('meta');
		foreach ($query->result() as $row)
		{
			if ($row->imagen!=""){
				$ar_imagen=explode(".",$row->imagen);
				$imagen=base_url().'uploads/informacion/'.$ar_imagen[0].'m.'.$ar_imagen[1];
			}else{
				$imagen=base_url().'uploads/informacion/espacio.gif';
			}
			
			
			
			
			
			$this->data['registro'] = array(
										
										"id" 				=> $row->id ,
										"alias" 			=> $row->alias , 
										"team" 				=> $row->team ,
										"imagen"			=> $imagen ,
										"es_informacion" 	=> htmlspecialchars_decode($row->du_informacion)
				
										);
			$this->db->where('id', $row->id_ciudad );
			$query_ciudad=$this->db->get('Ciudad');
			if ($query_ciudad->num_rows() > 0){
				foreach ($query_ciudad->result() as $row_ciudad)
				{
					$this->data['registro']['ciudad']=$row_ciudad->du_titulo;
				}
			}
			
			$this->db->where('id', $row->id_pais );
			$query_pais=$this->db->get('Pais');
			if ($query_pais->num_rows() > 0){
				foreach ($query_pais->result() as $row_pais)
				{
					$this->data['registro']['pais']=$row_pais->du_titulo;
				}
			}
			
		}
		if (!isset($this->data['registro'])){
			 redirect('web/kunstenaar');
		}
		$this->db->where( 'id_artista' , $this->uri->segment(4) );
		$this->db->order_by('fecha','DESC');
		$this->data['query']=$this->db->get('Artista_calendario');
		$this->data['sub_head'] = $this->load->view('web_du/sub_head_artistas_detalle_view', $this->data, true);
		$this->data['cuerpo'] = $this->load->view('web_du/artistas_calendario_view', $this->data, true);
		$this->load->view('templates/du_web_view',  $this->data);
		
	}
	
	function evenement()
	{
		$this->data['pulsado']='artistas';
		$this->data['sub_pulsado']='calendario';
		$this->db->where('user_id', $this->uri->segment(4) );
		$query=$this->db->get('meta');
		foreach ($query->result() as $row)
		{
			if ($row->imagen!=""){
				$ar_imagen=explode(".",$row->imagen);
				$imagen=base_url().'uploads/informacion/'.$ar_imagen[0].'m.'.$ar_imagen[1];
			}else{
				$imagen=base_url().'uploads/informacion/espacio.gif';
			}
			$this->data['registro'] = array(
					
										"id" 				=> $row->id ,
										"alias" 			=> $row->alias , 
										"team" 				=> $row->team ,
										"imagen"			=> $imagen ,
										"es_informacion" 	=> htmlspecialchars_decode($row->du_informacion)
				
										);
			$this->db->where('id', $row->id_ciudad );
			$query_ciudad=$this->db->get('Ciudad');
			if ($query_ciudad->num_rows() > 0){
				foreach ($query_ciudad->result() as $row_ciudad)
				{
					$this->data['registro']['ciudad']=$row_ciudad->du_titulo;
				}
			}
			
			$this->db->where('id', $row->id_pais );
			$query_pais=$this->db->get('Pais');
			if ($query_pais->num_rows() > 0){
				foreach ($query_pais->result() as $row_pais)
				{
					$this->data['registro']['pais']=$row_pais->du_titulo;
				}
			}
			
		}
		if (!isset($this->data['registro'])){
			 redirect('web/kunstenaar');
		}
		
		$this->db->where('id', $this->uri->segment(5) );
		$query=$this->db->get('Artista_calendario');
		foreach ($query->result() as $row)
		{
			list($f_date,$f_hora)=explode(" ",$row->fecha);
			list($hora,$minutos,$segundos)=explode(":",$f_hora);
			if ($row->imagen!=""){
				$ar_imagen=explode(".",$row->imagen);
				$imagen=base_url().'uploads/calendario/'.$ar_imagen[0].'p.'.$ar_imagen[1];
			}else{
				$imagen=base_url().'uploads/calendario/espacio.gif';
			}
			$this->data['registro_fiesta'] = array(
						
										"es_titulo" 		=> $row->du_titulo , 
										"f_date" 			=> $f_date , 
										"hora" 				=> $hora ,
										"minutos" 			=> $minutos/5 ,
										"imagen"		=> $imagen ,
										"es_informacion" 	=> htmlspecialchars_decode($row->du_informacion)  
										
										
										);
		}
		
		
		$this->data['sub_head'] = $this->load->view('web_du/sub_head_artistas_detalle_view', $this->data, true);
		$this->data['cuerpo'] = $this->load->view('web_du/artistas_evento_detalle_view', $this->data, true);
		$this->load->view('templates/du_web_view',  $this->data);
		
	}
	
	
	function radio()
	{
		$this->data['pulsado']='artistas';
		$this->data['sub_pulsado']='radio';
		$this->db->where('user_id', $this->uri->segment(4) );
		$query=$this->db->get('meta');
		foreach ($query->result() as $row)
		{
			
			if ($row->imagen!=""){
				$ar_imagen=explode(".",$row->imagen);
				$imagen=base_url().'uploads/informacion/'.$ar_imagen[0].'m.'.$ar_imagen[1];
			}else{
				$imagen=base_url().'uploads/informacion/espacio.gif';
			}
			
			$this->data['registro'] = array(
										
										"id" 				=> $row->id ,
										"alias" 			=> $row->alias , 
										"team" 				=> $row->team ,
										"imagen"			=> $imagen ,
										"es_informacion" 	=> htmlspecialchars_decode($row->du_informacion)
				
										);
			$this->db->where('id', $row->id_ciudad );
			$query_ciudad=$this->db->get('Ciudad');
			if ($query_ciudad->num_rows() > 0){
				foreach ($query_ciudad->result() as $row_ciudad)
				{
					$this->data['registro']['ciudad']=$row_ciudad->du_titulo;
				}
			}
			
			$this->db->where('id', $row->id_pais );
			$query_pais=$this->db->get('Pais');
			if ($query_pais->num_rows() > 0){
				foreach ($query_pais->result() as $row_pais)
				{
					$this->data['registro']['pais']=$row_pais->du_titulo;
				}
			}
			
		}
		if (!isset($this->data['registro'])){
			 redirect('web/artistas');
		}
		$this->db->where( 'id_artista' , $this->uri->segment(4) );
		$this->db->order_by('orden','DESC');
		$this->data['query']=$this->db->get('Artista_radio');
		$this->data['sub_head'] = $this->load->view('web_du/sub_head_artistas_detalle_view', $this->data, true);
		$this->data['cuerpo'] = $this->load->view('web_du/artistas_radio_view', $this->data, true);
		$this->load->view('templates/du_web_view',  $this->data);
		
	}
	
	
}


