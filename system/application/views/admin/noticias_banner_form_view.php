<div id="cuerpo">

<?=$this->session->flashdata('message'); ?>

<?=validation_errors(); ?>

<?=form_open_multipart($accion_form, array("id" => 'form1' ) ); ?>

<div class="formularios_titulo">Campos requeridos</div>
<div class="campos">
    <div class="campos_titulo">Título en español</div>
    <div class="campos_introducir"><?=form_input('es_titulo',set_value('es_titulo')=="" ?  $registro['es_titulo']  :   set_value('es_titulo') ); ?></div>
</div>
<div class="campos">
    <div class="campos_titulo">Título en ingles</div>
    <div class="campos_introducir"><?=form_input('en_titulo', set_value('en_titulo')=="" ?  $registro['en_titulo']  :   set_value('en_titulo') ); ?></div>
</div>
<div class="campos">
    <div class="campos_titulo">Título en holandes</div>
    <div class="campos_introducir"><?=form_input('du_titulo', set_value('du_titulo')=="" ?  $registro['du_titulo']  :   set_value('du_titulo') ); ?></div>
</div>
<div class="campos">
    <div class="campos_titulo">Imagen para la noticia banner</div>
    <div class="campos_introducir"><?=form_upload('imagen'); ?> <i>tamaño: 488px × 265px</i></div>
</div>
 <?php if ($this->uri->segment(3)=='modificar'): ?>
    <div class="campos">
        <img src="<?= $registro['imagen']; ?>" />
    </div>
<?php endif; ?>
<div class="campos">
    <div class="campos_titulo">Link para español</div>
    <div class="campos_introducir">
        <?=form_input('es_link', set_value('es_link')=="" ?  $registro['es_link']  :   set_value('es_link') ); ?>
    </div>
</div>
<div class="campos">
    <div class="campos_titulo">Link para ingles</div>
    <div class="campos_introducir">
         <?=form_input('en_link', set_value('en_link')=="" ?  $registro['en_link']  :   set_value('en_link') ); ?>
    </div>
</div>
<div class="campos">
    <div class="campos_titulo">Link para holandes</div>
    <div class="campos_introducir">
         <?=form_input('du_link', set_value('du_link')=="" ?  $registro['du_link']  :   set_value('du_link') ); ?>
    </div>
</div>   
   
<div id="btnsubmit"><?=form_submit('submit', 'Guardar'); ?></div>

<?=form_close(''); ?>
</div> 