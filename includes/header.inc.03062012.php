<?php
/****************************
*  Created 02/11/11
*  Last update 28/04/12
****************************/
// ini_set ("display_errors","1" );
// error_reporting(E_ALL);

error_reporting(0);

session_start();

if ($_SESSION['status'] == "authorized") {
  $sesion = true;
}

if (!class_exists("administradorDatos")) {
  require_once RUTA_ABSOLUTA.'/administradorDatos/administradorDatos.class.php';
}
$adminDatos = new administradorDatos();

#Load languages
include RUTA_ABSOLUTA.'/includes/lang.inc.php';
$langURL = '';
if ($idioma != 'es') {
  $langURL = '?lang=' . $idioma;
}
$arrayIdiomas = $adminDatos->cargaIdioma($idioma);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="expires" content="Sat, 27 May 1978 12:00:00 GMT" />
<meta name="country" content="es" />
<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>SHARK DJ</title>
<link href="carousel/skin.css" rel="stylesheet" type="text/css" media="screen" />
<link type="text/css" href="css/custom-theme/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
<link href="shark.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="icon" type="image/png" href="images/logoSharkDJ.png" />
<script type="text/javascript" src="js/jquery-1.7.min.js"></script> 
<script type="text/javascript" src="js/jquery.jcarousel.min.js"></script>
<script type="text/javascript" src="js/functions.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript">
			$(function(){
				// Dialog			
				$('#dialog').dialog({
					autoOpen: true,
					height:100,
					hide:"fold",
					show:"fold",
					dialogClass: 'alert',
					closeOnEscape: true
				});
				
				// Dialog Link
				$('#dialog_link').click(function(){
					$('#dialog').dialog('open');
					return false;
				});
			});
		</script>

<script type="text/javascript"> 
jQuery(document).ready(function() {
    jQuery('#mycarousel').jcarousel({
        visible: 1
    });
});
</script> 
</head>
<body>
<div id="headerFull">
<div id="container">
  <div id="header">
    <div id="loginLinks">
      <a href="./inputLogin.php<?=$langURL;?>" class="login"><?=$arrayIdiomas['login'];?></a>
      <span class="left fontGrey2 marginTop3">-</span>
      <a href="./inputSignup.php<?=$langURL;?>" class="login2"><?=$arrayIdiomas['signup'];?></a>
      <div id="flag">
	    <?php
		#SHOW FLAGS
        foreach ($arrayFlagLanguages as $key => $value) {
          echo '<a href="' . $_SERVER['PHP_SELF'] . '?lang=' . $key . '">';
		  echo   '<img class="marginLeft3" src="images/flags/' . $key . '.png" alt="' . $value . '" title="' . $value . '" />';
          echo '</a>';
	    }
	    ?>
	  </div>
      <?php include 'includes/socialLinks.inc.php'; ?>
    </div>
    <div id="menu">
      <div class="clear insideMenu font1">
        <?php 
          $file = explode("/",$_SERVER['SCRIPT_FILENAME']);
          $filename = $file[count($file)-1];            
        ?>
        <a href="index.php<?=$langURL;?>"><span <?php echo ($filename=="index.php") ? "class=\"fontRed hoverLargeRed\"" : "class=\"hoverLargeGrey\"";?>><?=$arrayIdiomas['home'];?></span></a>
        &bull;&nbsp;<a href="artistas.php<?=$langURL;?>"><span <?php echo ($filename=="artistas.php") ? "class=\"fontRed hoverLargeRed\"" : "class=\"hoverLargeGrey\"";?>><?=$arrayIdiomas['artists'];?></span></a>
        &bull;&nbsp;<a href="locales.php<?=$langURL;?>"><span <?php echo ($filename=="locales.php") ? "class=\"fontRed hoverLargeRed\"" : "class=\"hoverLargeGrey\"";?>><?=$arrayIdiomas['clubs'];?></span></a>
        &bull;&nbsp;<a href="eventos.php<?=$langURL;?>"><span <?php echo ($filename=="eventos.php") ? "class=\"fontRed hoverLargeRed\"" : "class=\"hoverLargeGrey\"";?>><?=$arrayIdiomas['events'];?></span></a>
        &bull;&nbsp;<a href="escuela.php<?=$langURL;?>"><span <?php echo ($filename=="escuela.php") ? "class=\"fontRed hoverLargeRed\"" : "class=\"hoverLargeGrey\"";?>><?=$arrayIdiomas['school'];?></span></a>
        <?php if ($sesion && $_SESSION['idPerfil'] == 3){?>
          &bull;&nbsp;<a href="localBackend.php<?=$langURL;?>"><span <?php echo ($filename=="localBackend.php") ? "class=\"fontRed hoverLargeRed\"" : "class=\"hoverLargeGrey\"";?>><?=$arrayIdiomas['profile'];?></span></a>
        <?php }elseif ($sesion && $_SESSION['idPerfil'] == 2){?>
        &bull;&nbsp;<a href="artistaBackend.php<?=$langURL;?>"><span <?php echo ($filename=="artistaBackend.php") ? "class=\"fontRed hoverLargeRed\"" : "class=\"hoverLargeGrey\"";?>><?=$arrayIdiomas['profile'];?></span></a>
        <?php }?>
      </div>
    </div>
    <div id="logo"><a href="index.php<?=$langURL;?>"><img src="images/logoHeader.png"/></a></div>
  </div>
  <?php include 'carousel/carousel.php'; 
  
  function leerDirectorioPublicidad()
  {
    $dir=RUTA_ABSOLUTA.'/images/publicidad/';
    $directorio=opendir($dir);
    while ($archivo = readdir($directorio)){
      $imagenes[]=$archivo;
    }
    closedir($directorio);
    return $imagenes;
  }

  $arrayInfoArtistas = $adminDatos->obtenerArtistasInfo(99999,0,1,'');     
  $nArtistas = count($arrayInfoArtistas);
  $i = rand(0, $nArtistas-1);
  if (!class_exists("imagenes")) {
    require_once RUTA_ABSOLUTA.'/administradorDatos/imagenes.class.php';
  }
  $publicidad = leerDirectorioPublicidad();
  $urlImagen = imagenes::obtenerImagenUsuario($arrayInfoArtistas[$i]['idUser']);
  ?>
  <div class="sideBar2">
    <div class="artistPhoto"><?php echo ($urlImagen!="") ? "<a href='perfilArtista.php?id=".$arrayInfoArtistas[$i]['idUser'].$langURL2."'><img height='140' width='140' src='".$urlImagen."' alt='' title='' /></a>" : ""; ?></div>
    <div id="artistInfoBox">
      <span class="fontRed"> <?php echo ($arrayInfoArtistas[$i]['nombre']!="") ? $arrayInfoArtistas[$i]['nombre'] : $arrayIdiomas['noName'];?></span>
      <br />
      <br />
      <span class="fontGrey"><?=$arrayIdiomas['style'];?>:</span><span class="fontBlack"> <?php echo ($arrayInfoArtistas[$i]['estilo']!="") ? $arrayInfoArtistas[$i]['estilo'] : " " . $arrayIdiomas['noStyle'];?></span>
      <br />
      <span class="fontGrey"><?=$arrayIdiomas['country'];?>:</span><span class="fontBlack"> <?php echo ($arrayInfoArtistas[$i]['pais']!="") ? $arrayInfoArtistas[$i]['pais'] : "-";?></span>
      <br />      
      <!--  SI ESTA -->      
      <span class="fontGrey"><?=$arrayIdiomas['club'];?>:</span><span class="fontBlack"> <?php echo ($arrayInfoArtistas[$i]['clubResidencia']!="") ? $arrayInfoArtistas[$i]['clubResidencia'] : "-";?></span>
      <br />
      <span class="fontGrey"><?=$arrayIdiomas['web'];?>:</span><span> <?php echo ($arrayInfoArtistas[$i]['web']!="") ? '<a href="'.$arrayInfoArtistas[$i]['web'].'" class="website">Website</a>' : "-";?></span>
    </div>
  </div>