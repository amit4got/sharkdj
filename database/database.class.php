<?php

if(!defined("RUTA_ABSOLUTA")){
  define("RUTA_ABSOLUTA",dirname(__FILE__));
}

require_once RUTA_ABSOLUTA.'/config/constants.php';
require_once RUTA_ABSOLUTA.'/database/SafeSQLSharkDJ.class.php';

class database {
	private $connection;	
	private $result;
	
	public function __construct() {
		$this->connection = mysql_connect(HOST_DB, USER_DB, PASS_DB) or die	('Error connecting to mysql');		
		mysql_select_db(NAME_DB);
	}
	
	protected function select($query,$parameters="",$offset=0,$numRow=50)
    {
      if(!$this->connection)
      {
        trigger_error("Se ha invocado un m�todo select y no hay conexi�n a base de datos activa.",E_USER_NOTICE);
        return false;
      }
              
      // CREAMOS LA CLASE SAFESQL
      $safeSQL =new SafeSQLSharkDJ(); 
      // PARSEAMOS LA SENTENCIA PARA QUE SEA SEGURA Y LA EJECUTAMOS      
      $safeQuery = $safeSQL->query($query,$parameters,$offset,$numRow);      
      $this->result=null;
      $this->result=mysql_query($safeQuery,$this->connection) or die (mysql_error($this->connection));
      return;
    }
    
    /**
     * Funcion fieldsSelect
     * Este metodo devuelve una matriz con los nombres de los campos de una consulta.
     * 
     * @return array de campos para uso en php
     */
    protected function fieldsSelect()
    {
      // COMPROBAMOS QUE TENGAMOS UN RESULTADO CORRECTO
      $matrix = null;
      if($this->result) {
	      $rows=0;      
	      while ($rows < mysql_num_fields($this->result)) {
	        $matrix[$rows]=mysql_fetch_field($this->result,$rows)->name;        
	        $rows++;        
	      }
      }
            
      return $matrix;
    }
    
    /**
     * matrix     
     * Devuelve el resultado de un select convertida en una matriz manejable por php.     
     * @return array matriz convertida para uso en PHP. Null en caso de no resultados.
     */    
    protected function matrix() {
          
      // COMPROBAMOS QUE TENGAMOS UN RESULTADO CORRECTO
      if($this->result)
      {
	      // COGEMOS LOS NOMBRES DE LOS CAMPOS  
	      $keys=$this->fieldsSelect($this->result);                  
	      $rows=0;
	      while ($row = mysql_fetch_assoc($this->result)) {        
	        $col=0;                          
	        foreach($row as $f) {        
	          $matrix[$rows][$keys[$col]]=$f;                    
	          $col++;                    
	        }                     
	        $rows++;                        
	      }
      }
      //var_dump($matriz);
      // DEVOLVEMOS LA MATRIZ PREPARADA
            
      return (isset($matrix)) ? $matrix : null;
      
    }
	
	public function query($query, $parameters='',$offset=0,$numRow=50) {
		if($query=="")
      	{
        	trigger_error("No se ha encontrado la consulta. $consulta");
        	return null;
      	}
	    //ejecutamos la select
    	$this->select($query, $parameters,$offset,$numRow);      
      	return $this->matrix();
	}
	
	public function getCount() {
		return ($this->result) ? mysql_num_rows($this->result) : 0;
	}

	public function runQuery ($query) {
		return mysql_query ($query);
	}
}
?>