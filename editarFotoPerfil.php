<?php
/****************************
 *  Created 11/11/11
 *  Last update 11/11/11   
 ****************************/
error_reporting(E_ALL);
ini_set('display_errors', 0);
if(!defined("RUTA_ABSOLUTA")){
  define("RUTA_ABSOLUTA",dirname(__FILE__));
}

session_start();

if ($_SESSION['status'] == "authorized") {
  $sesion = true;
}

if (!class_exists("administradorDatos")) {
  require_once RUTA_ABSOLUTA.'/administradorDatos/administradorDatos.class.php';
}
require_once 'includes/connect.ini.php';
require_once 'includes/functions.ini.php';
$adminDatos = new administradorDatos();   

include RUTA_ABSOLUTA.'/includes/lang.inc.php';
$arrayIdiomas = $adminDatos->cargaIdioma($idioma);
$langURL  = '';
$langURL2 = '';
if ($idioma != 'es') {
  $langURL  = '?lang=' . $idioma;
  $langURL2 = '&lang=' . $idioma;
}

function is_alpha_png($fn){
  return (ord(@file_get_contents($fn, NULL, NULL, 25, 1)) == 6);
}
if ($_POST['crop'])
{
  $targ_w = $targ_h = 240;
  $jpeg_quality = 90;

  $nombreImagen = explode('/',$_POST['imagen']);
  $nombreImagen = $nombreImagen[count($nombreImagen) - 1];
  
  $directorio = 'images/profiles/';
  $imagen = $directorio.$_POST['idUser'].'.jpeg';  
  $src = $imagen;
  $img_r = @imagecreatefromjpeg($src);
  $dst_r = @ImageCreateTrueColor( $targ_w, $targ_h );

  @imagecopyresampled($dst_r,$img_r,0,0,$_POST['x'],$_POST['y'],$targ_w,$targ_h,$_POST['w'],$_POST['h']);

//   header('Content-type: image/jpeg');
  $imagen = $directorio.$_POST['idUser'].'_crop.jpeg';
  @imagejpeg($dst_r,$imagen);

#  header('refresh:0; artistaBackend.php'.$langURL);
  if (isset($_POST['local'])){
    header('Location: localBackend.php'.$langURL);
  }
  else {
    header('Location: artistaBackend.php'.$langURL.$langURL2);
  }
}
elseif ($_POST['imagenSubida']){
  
    // Datos de conexión a configurar
    //     $user = "usuario_bbdd";
    //     $pass = "clave_bbdd";
    //     $bbdd = "nombre_bbdd";
  
    // Ruta donde se guardarán las imágenes
    $directorio = 'images/profiles/';
  
    // Conecto a la BBDD
    //     $dbh = mysql_connect("localhost", $user, $pass);
    //     $db = mysql_select_db($bbdd);
  
    // Recibo los datos de la imagen
    $name = $_FILES['imagen']['name'];
    $tipo = $_FILES['imagen']['type'];
    $tamano = $_FILES['imagen']['size'];
    $prefijo = substr(md5(uniqid(rand())),0,20);

    if(isset($_GET['l'])){
      $local ='&l';
    } 
    if ($_FILES['imagen']['error'] == 1){
       header('Location: editarFotoPerfil.php?e=2&i='.$_POST['idUser'].$local.$langURL2);
      exit;
    }
    if ($tamano > 1048576){
      header('Location: editarFotoPerfil.php?e=3&s='.$tamano.'&i='.$_POST['idUser'].$local.$langURL2);
      exit;
    }
    // guardamos el archivo a la carpeta files
    $destino =  $directorio.$prefijo."_".$name;
    move_uploaded_file($_FILES['imagen']['tmp_name'],$directorio.$prefijo.'_'.$name);
  
  
    $anchura=500;
    $hmax=500;
    $nombre=$directorio.$prefijo.'_'.$name;
    
    if (is_alpha_png($nombre)){
      header('Location: editarFotoPerfil.php?e=4'.$local.$langURL2);
      exit;
    }
    
    $datos = getimagesize($nombre);
    if($datos[2]==1){
      $img = @imagecreatefromgif($nombre);
    }
    elseif($datos[2]==2){
      $img = @imagecreatefromjpeg($nombre);
    }
    elseif($datos[2]==3){
      $img = @imagecreatefrompng($nombre);
    }
    else{
       header('Location: editarFotoPerfil.php?e=1'.$local.$langURL2);
       exit;
    }
    if ($datos[0] < 250 || $datos[1] < 250){
      unlink($directorio.$prefijo.'_'.$name);
      header('Location: editarFotoPerfil.php?e=5&i='.$_POST['idUser'].$local.$langURL2);
      exit;
    }
    if ($datos[0] > 500 || $datos[1] > 500){
      $ratio = ($datos[0] / $anchura);
      $altura = ($datos[1] / $ratio);
      if($altura>$hmax){
        $anchura2=$hmax*$anchura/$altura;
        $altura=$hmax;
        $anchura=$anchura2;
      }
      $thumb = imagecreatetruecolor($anchura,$altura);
      imagecopyresampled($thumb, $img, 0, 0, 0, 0, $anchura, $altura, $datos[0], $datos[1]);

      if(file_exists($directorio.$_POST['idUser'].'.jpeg')){
        unlink($directorio.$_POST['idUser'].'.jpeg');
      }
      imagejpeg($thumb,$directorio.$_POST['idUser'].'.jpeg');
      imagedestroy($thumb);
      unlink($directorio.$prefijo.'_'.$name);
    }
    else{
      if(file_exists($directorio.$_POST['idUser'].'.jpeg')){
         unlink($directorio.$_POST['idUser'].'.jpeg');
       }
       rename($directorio.$prefijo.'_'.$name, $directorio.$_POST['idUser'].'.jpeg');
    }
  
      header('refresh:0; editarFotoPerfil.php'.$langURL);
      header('Location: editarFotoPerfil.php?e=0&i='.$_POST['idUser'].$local.$langURL2);
    // Muevo la imagen desde su ubicación
    // temporal al directorio definitivo
  }

else{
include_once 'includes/headerBackendFoto.inc.php';

?>

<?php 
$idUser = $_SESSION['idUser'];
$directorio = 'images/profiles/';

$idImage = ($_GET['im'] == '0') ? '0' : $idUser;
$imagen = $directorio.$idImage.'.jpeg';
?>

<div id="mainFotoPerfil">
	<div class="ui-widget-content boxEditarFoto">
		<div class="imagenEditar">
			<img src="<?php echo $imagen; echo '?nocache='.date("isu"); ?>" id="cropbox" />
		</div>
				<!-- This is the form that our event handler fills -->
		<form action="<?php $_SERVER['PHP_SELF']?>" enctype="multipart/form-data" method="post">
			<fieldset>
				<label for="file"><?=$arrayIdiomas['selectFoto'];?></label><br><br> 
				<input type="file" name="imagen" id="file" />
				<input type="hidden" id="idUser" name="idUser" value="<?php echo $idUser?>" />
				<br><br><label><?=$arrayIdiomas['subeFoto'];?></label> 
				<input type="submit" name="imagenSubida" id="upload" value="<?=$arrayIdiomas['subirFoto'];?>" />
			</fieldset>
		</form>
		
		<form action="editarFotoPerfil.php<?php echo $langURL;?>" method="post" onSubmit="return checkCoords();">
			<label><?=$arrayIdiomas['regionFoto'];?></label><br><br>
			<input type="hidden" id="x" name="x" />
			<input type="hidden" id="y" name="y" />
			<input type="hidden" id="w" name="w" />
			<input type="hidden" id="h" name="h" />
			<input type="hidden" id="idUser" name="idUser" value="<?php echo $idUser?>" />
			<?php if (isset($_GET['l'])){?>
				<input type="hidden" id="local" name="local" value="l" /><?php 
      }?>
			<input type="submit" name="crop" value="<?=$arrayIdiomas['actualizarFoto'];?>" />
		</form>
        
      
        
        
	<?php if ($_GET['e'] == '1'){?>
	  <div id="dialog" title="Formato incorrecto">
			<p><?=$arrayIdiomas['formatoNoPermitido'];?></p>
		</div>
	<?php }
	elseif ($_GET['e'] == '2'){?>
	  <div id="dialog" title="Error subiendo imagen">
	  <p><?=$arrayIdiomas['errorFoto'];?></p>
	  </div>
	<?php }
	elseif ($_GET['e'] == '3'){
	  $size = round($_GET['s']/1024,0);
	  ?>
    <div id="dialog" title="Archivo muy grande">
      <p>
        <?=$arrayIdiomas['pesoFoto'];?>
        <?php echo '<b>'.$size.'</b>'?>
        <?=$arrayIdiomas['kbPermitido'];?>
      </p>
    </div>

    <?php }
    elseif ($_GET['e'] == '4'){
      ?>
    <div id="dialog" title="Archivo con transparencia">
      <p><?=$arrayIdiomas['canalAlpha'];?></p>
    </div>
	<?php }
	elseif ($_GET['e'] == '5'){
	  ?>
	    <div id="dialog" title="Archivo muy pequeño">
	      <p><?=$arrayIdiomas['anchoAltura'];?></p>
	    </div>
	    
		<?php }
	elseif ($_GET['e'] == '0') {
	  ?>
    <div id="dialog" title="Archivo subido correctamente">
      <p><?=$arrayIdiomas['subidaCorrectamente'];?></p>
    </div>

    <?php }
	?>

  </div>
</div>
<div class="ui-widget-content boxTexto">
	<?=$arrayIdiomas['ayudaFoto'];?>
</div>
<?php 
}
include_once 'includes/footer.inc.php'; ?>