<div id="cuerpo">
<script src="<?=base_url(); ?>js/JSCal2-1.8/src/js/jscal2.js"></script>
<script src="<?=base_url(); ?>js/JSCal2-1.8/src/js/lang/es.js"></script>
<script type="text/javascript">
	//<![CDATA[
	
	function Comprobar_chequeados()
	{
		var cadena_texto ="";
		var id_i=0;
		while (document.getElementById("check_list"+id_i)) {
			if (document.getElementById("check_list"+id_i).checked == true){
				cadena_texto+=document.getElementById("check_list"+id_i).value+",";
			}
			id_i++;
		}
		document.getElementById("estilos_id").value=cadena_texto;
	}
		 
	//]]>
	
</script>
    
<?=$this->session->flashdata('message'); ?>

<?=validation_errors(); ?>

<?=form_open_multipart($accion_form, array("id" => 'form1' ) ); ?>
 	<div class="formularios_titulo">Campos requeridos</div>
    <div class="campos_supertitulo">Información de la fiesta</div>
    <div class="campos">
        <div class="campos_titulo">Título en español</div>
        <div class="campos_introducir"><?=form_input('es_titulo',set_value('es_titulo')=="" ?  $registro['es_titulo']  :   set_value('es_titulo') ); ?></div>
    </div>
    <div class="campos">
        <div class="campos_titulo">Título en ingles</div>
        <div class="campos_introducir"><?=form_input('en_titulo', set_value('en_titulo')=="" ?  $registro['en_titulo']  :   set_value('en_titulo') ); ?></div>
    </div>
    <div class="campos">
        <div class="campos_titulo">Título en holandess</div>
        <div class="campos_introducir"><?=form_input('du_titulo', set_value('du_titulo')=="" ?  $registro['du_titulo']  :   set_value('du_titulo') ); ?></div>
    </div>
    <div class="campos">
        <div class="campos_titulo">Fecha de la fiesta</div>
        <div class="campos_introducir">
        	<div style="width:150px; float:left" id="cont"></div>
        	<?=form_input('f_date', set_value('f_date')=="" ?  $registro['f_date']  :   set_value('f_date'), ' id="f_date" style="text-align: center; width:100px;" readonly="true" ' ); ?>    
        </div>
    </div>
    <div class="campos">
        <div class="campos_titulo">Hora de la fiesta</div>
        <div class="campos_introducir">
        	<?=form_dropdown('hora' ,array('0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23'), 
										set_value('hora')=="" ?  $registro['hora']  :   set_value('hora'), 'style="width:60px;"' ); ?> :
            <?=form_dropdown('minutos' ,array('0','5','10','15','20','25','30','35','40','45','50','55'), 
										set_value('minutos')=="" ?  $registro['minutos']  :   set_value('minutos'), 'style="width:60px;"' ); ?>
        </div>
    </div>
    <div class="campos">
        <div class="campos_titulo">Imagen para la fiesta</div>
        <div class="campos_introducir"><?=form_upload('imagen'); ?></div>
    </div>
     <?php if ($this->uri->segment(3)=='modificar'): ?>
        <div class="campos">
            <img src="<?= $registro['imagen']; ?>" />
        </div>
    <?php endif; ?>
    <div class="campos">
        <div class="campos_titulo">Estilos</div>
        <div class="campos_introducir">
        	 <ul>
				<?php 
                    $cadena_estilo="";
                    $id_i=0;
                    if (count($qry_estilo)>0): 
                ?>
                    <?php foreach($qry_estilo as $qry_fila_estilo): ?>
                    <li>
                        <input onclick="Comprobar_chequeados();" type="checkbox"  name="check_list[]" id="check_list<?=$id_i ?>"  value="<?=$qry_fila_estilo['id']; ?>" <? 
                        if ( count($slct_estilo)>0 )
                        {
                           	foreach($slct_estilo as $slct_fila_estilo)    
                            {  
                                if ($qry_fila_estilo['id']==$slct_fila_estilo){
                                    echo 'checked="checked"';
                                    $cadena_estilo.=$qry_fila_estilo['id'].",";
                                }
                            }
                        }else{
                            echo 'checked="checked"';
                           $cadena_estilo.=$qry_fila_estilo['id'].",";
                        }
                        ?>><?=$qry_fila_estilo['titulo']; ?><input type="hidden" value="<?=$qry_fila_estilo['titulo']; ?>" id="titulo_eslito_<?=$qry_fila_estilo['id']; ?>"  />
                    </li>
                    <? $id_i+=1; ?>
                    <?php endforeach;?>
                <?php endif; ?>       
            </ul>
        </div>
    </div>
    	<input type="hidden" id="estilos_id" name="estilos_id" value="<?=$cadena_estilo; ?>"  /> 
   
     
  
    <div class="campos_supertitulo">Detalle de la fiesta</div>
    <div class="campos">
        <div class="campos_titulo">Texto en español</div>
        <div class="campos_introducir">
					<?= $ckeditor->editor('es_informacion', set_value('es_informacion')=="" ?  $registro['es_informacion']  :   set_value('es_informacion') ,$ckeditor->config ); ?>
       	</div>
    </div>
    <div class="campos">
        <div class="campos_titulo">Texto en ingles</div>
        <div class="campos_introducir">
        	 <?= $ckeditor->editor('en_informacion', set_value('en_informacion')=="" ?  $registro['en_informacion']  :   set_value('en_informacion') ,$ckeditor->config ); ?>
        </div>
    </div>   
        <div class="campos">
        <div class="campos_titulo">Texto en holandes</div>
        <div class="campos_introducir">
					<?= $ckeditor->editor('du_informacion', set_value('du_informacion')=="" ?  $registro['du_informacion']  :   set_value('du_informacion') ,$ckeditor->config ); ?>
       	</div>
    </div>
    
    <div id="btnsubmit"><?=form_submit('submit', 'Guardar'); ?></div>

<?=form_close(''); ?>      

</div>  
<script type="text/javascript">
	//<![CDATA[
      // this handler is designed to work both for onSelect and onTimeChange
      // events.  It updates the input fields according to what's selected in
      // the calendar.
	  var cto_time=0;
      function updateFields(cal) {
              var date = cal.selection.get();
              if (date) {
                      date = Calendar.intToDate(date);
                      document.getElementById("f_date").value = Calendar.printDate(date, "%Y-%m-%d");
              }
      };
      Calendar.setup({
              cont         : "cont",
			  //selection     : 20100525,
              onSelect     : updateFields,
              onTimeChange : updateFields
      }); 	
    //]]>
</script> 