<p>Welcome@ to SHARK DJ, your account has been successfully activated, this will be your administration page where you can manage your information, biography, photos, dates of events and bring up your sessions and themes.</p>
<p>If you are interested in promoting SHARK, you can send an email to <a href="mailto:marketing@sharkiberia.sl">marketing@sharkiberia.sl</a> <br />
  <br />
  If you have any problems with your account, send an email to&nbsp;<a href="mailto:info@sharkdj.com">info@sharkdj.com</a> <br />
  <br />
  Please note that until you enter information and post a photo you are not visible on the web, in this case we reserve the right to change your profile to invisible.<br />
  <br />
Your profile photo should be as square as possible in format, photos with larger formats may be cut.</p>
<p>&nbsp;</p>
<p><br />
</p>

