<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"

    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html>

    <head>

        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	
        <title>Shark DJ - Map</title>

        <?= link_tag('assets/css/radio.css')."\n"; ?>
<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAA7rkcgy9PNk8KUX29tySlehQ2gKpxVqWRT6wE0XsFNYvAiuTpzxTuLva2-syTq5I_J-decdqo2c8PPA"
      type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
		var clubsArray = [];
		var fiestasArray = [];
		var djsArray = [];
		var map ='';
 function initialize() {
      if (GBrowserIsCompatible()) {
		
        map = new GMap2(document.getElementById("map_canvas"));
        map.setCenter(new GLatLng(28.2633,-16.6178), 3);
        map.setUIToDefault();

        // Create a base icon for all of our markers that specifies the
        // shadow, icon dimensions, etc.
        var baseIcon = new GIcon(G_DEFAULT_ICON);
        baseIcon.shadow = "http://www.sharkdj.com/images/espacio.gif";
        baseIcon.iconSize = new GSize(26, 34);
        baseIcon.shadowSize = new GSize(37, 34);
        baseIcon.iconAnchor = new GPoint(9, 34);
        baseIcon.infoWindowAnchor = new GPoint(9, 2);

        // Creates a marker whose info window displays the letter corresponding
        // to the given index.
        function createMarker_locales(point, name, id ) {
          // Create a lettered icon for this point using our icon class
          
          var letteredIcon = new GIcon(baseIcon);
          letteredIcon.image = "http://www.sharkdj.com/images/club.png";

          // Set up our GMarkerOptions object
          markerOptions = { icon:letteredIcon };
          var marker = new GMarker(point, markerOptions);
		  
          GEvent.addListener(marker, "click", function() {
            marker.openInfoWindowHtml("<a href='http://www.sharkdj.com/index.php/web/club_s/information/" + id + "' target='_blank' >" + name + "</a><br />Club");
          });
          return marker;
        }
		function createMarker_fiestas(point, name, id ) {
          // Create a lettered icon for this point using our icon class
          var letteredIcon = new GIcon(baseIcon);
          letteredIcon.image = "http://www.sharkdj.com/images/bar.png";

          // Set up our GMarkerOptions object
          markerOptions = { icon:letteredIcon };
          var marker = new GMarker(point, markerOptions);

          GEvent.addListener(marker, "click", function() {
            marker.openInfoWindowHtml("<a href='http://www.sharkdj.com/index.php/web/parties/information/" + id + "' target='_blank' >" + name + "</a><br />Party");
          });
          return marker;
        }
		function createMarker_dj(point, name, id ) {
          // Create a lettered icon for this point using our icon class
          
          var letteredIcon = new GIcon(baseIcon);
          letteredIcon.image = "http://www.sharkdj.com/images/music.png";

          // Set up our GMarkerOptions object
          markerOptions = { icon:letteredIcon };
          var marker = new GMarker(point, markerOptions);

          GEvent.addListener(marker, "click", function() {
            marker.openInfoWindowHtml("<a href='http://www.sharkdj.com/index.php/web/artists/information/" + id + "' target='_blank' >" + name + "</a><br />Artist");
          });
          return marker;
        }
		
		
		<?php if ($locales->num_rows()>0): ?>
		<?php foreach($locales->result() as $row): ?>
			var latlng = new GLatLng(<?php echo $row->posy; ?>, <?php echo $row->posx; ?>  );
			clubsArray.push(createMarker_locales(latlng, "<?php echo $row->local; ?>",<?php echo $row->id; ?>));
			
			
		<?php endforeach;?>
		<?php endif; ?> 
		if (clubsArray) {
			clubs_show_Array()
		}
		
		<?php if ($dj->num_rows()>0): ?>
		<?php foreach($dj->result() as $row): ?>
			var latlng = new GLatLng(<?php echo $row->posy_flash; ?>, <?php echo $row->posx_flash; ?>  );
			djsArray.push(createMarker_dj(latlng, "<?php echo $row->alias; ?>",<?php echo $row->user_id; ?>));
			
		<?php endforeach;?>
		<?php endif; ?> 
		if (djsArray) {
			djs_show_Array()
		}
		
		
		<?php if ($fiestas->num_rows()>0): ?>
		<?php foreach($fiestas->result() as $row): ?>
			 var latlng = new GLatLng(<?php echo $row->posy; ?>, <?php echo $row->posx; ?>  );
			 fiestasArray.push(createMarker_fiestas(latlng, "<?php echo $row->es_titulo; ?>",<?php echo $row->id; ?>));
			
		<?php endforeach;?>
		<?php endif; ?> 
        if (fiestasArray) {
			fiestas_show_Array()
		}

      
      }
    }
	// Removes the overlays from the map, but keeps them in the array
function fiestas_ocultar_Array() {
  if (fiestasArray) {
    for (i in fiestasArray) {
      map.removeOverlay(fiestasArray[i]);
    }
  }
}

// Shows any overlays currently in the array
function fiestas_show_Array() {
  if (fiestasArray) {
    for (i in fiestasArray) {
      map.addOverlay(fiestasArray[i]);
    }
  }
}	
	
	// Removes the overlays from the map, but keeps them in the array
function djs_ocultar_Array() {
  if (djsArray) {
    for (i in djsArray) {
      map.removeOverlay(djsArray[i]);
    }
  }
}

// Shows any overlays currently in the array
function djs_show_Array() {
  if (djsArray) {
    for (i in djsArray) {
      map.addOverlay(djsArray[i]);
    }
  }
}
	
	
	// Removes the overlays from the map, but keeps them in the array
function clubs_ocultar_Array() {
  if (clubsArray) {
    for (i in clubsArray) {
      map.removeOverlay(clubsArray[i]);
    }
  }
}

// Shows any overlays currently in the array
function clubs_show_Array() {
  if (clubsArray) {
    for (i in clubsArray) {
      map.addOverlay(clubsArray[i]);
    }
  }
}

function check_artist (){
	if (document.getElementById("ck_artist").checked == true ){
		djs_show_Array()
	}else {
		djs_ocultar_Array()
	}
}
function check_club (){
	if (document.getElementById("ck_club").checked == true ){
		clubs_show_Array()
	}else {
		clubs_ocultar_Array()
	}
}
function check_fiesta (){
	if (document.getElementById("ck_fiestas").checked == true ){
		fiestas_show_Array()
	}else {
		fiestas_ocultar_Array()
	}
}
	//]]>
    </script>
  </head>

  <body onload="initialize()" onunload="GUnload()">
  	<div id="cabecera" ><img src="http://www.sharkdj.com/images/cabecera_mapa.jpg"  alt="cabecera" /></div>
    <div id="map_canvas" style="width: 758px; height: 450px" ></div>
    <ul>
        <li><img src="http://www.sharkdj.com/images/music.png"  alt="Artists" /> Artists <input type="checkbox" name="ck_artist" id="ck_artist" onchange="check_artist ()" checked="checked"></li> 
        <li><img src="http://www.sharkdj.com/images/club.png"  alt="Clubs" /> Clubs <input type="checkbox" name="ck_club" id="ck_club" onchange="check_club ()" checked="checked"></li>  
        <li><img src="http://www.sharkdj.com/images/bar.png"  alt="Parties" /> Parties <input type="checkbox" name="ck_fiestas" id="ck_fiestas" onchange="check_fiesta ()" checked="checked"></li>  
    </ul>
  </body>
</html>
