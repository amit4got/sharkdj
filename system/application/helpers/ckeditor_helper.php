<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
function CK_Editor (){
	$ckeditor = new CKEditor();
	$ckeditor->basePath = base_url() . 'js/ckeditor/';
	$ckeditor->config['startupOutlineBlocks']=true;
	$ckeditor->config['htmlEncodeOutput']=false;
	$ckeditor->config['toolbar'] = array(
	array('Source', '-','Cut','Copy','Paste','PasteText','PasteFromWord','-' , 'Scayt'),
    array('Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'),
	array( 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-' ,'Table' ),
	array(  'Bold', 'Italic', 'Underline', 'Strike', 'TextColor','BGColor','Format','Font','FontSize' ),
	array( 'Image', 'Flash', 'Link', 'Unlink', 'Anchor' )
	); 
	
	//CKFinder::SetupCKEditor( $ckeditor, base_url() . 'js/ckfinder/' ) ;
	
	
	$ckeditor->config['filebrowserBrowseUrl'] =      base_url() . 'js/ckfinder/ckfinder.php';
	$ckeditor->config['filebrowserImageBrowseUrl'] = base_url() . 'js/ckfinder/ckfinder.html?type=Images';
	$ckeditor->config['filebrowserFlashBrowseUrl'] = base_url() . 'js/ckfinder/ckfinder.html?type=Flash';
	$ckeditor->config['filebrowserUploadUrl'] =      base_url() . 'js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
	$ckeditor->config['filebrowserImageUploadUrl'] = base_url() . 'js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
	$ckeditor->config['filebrowserFlashUploadUrl'] = base_url() . 'js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';
	
	return $ckeditor;
}
function CK_Editor_artistas (){
	$ckeditor = new CKEditor();
	$ckeditor->basePath = base_url() . 'js/ckeditor/';
	$ckeditor->config['startupOutlineBlocks']=true;
	$ckeditor->config['htmlEncodeOutput']=false;
	$ckeditor->config['toolbar'] = array(
	array('Source', '-','Cut','Copy','Paste','PasteText','PasteFromWord','-' , 'Scayt'),
    array('Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'),
	array( 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-' ,'Table' ),
	array(  'Bold', 'Italic', 'Underline', 'Strike', 'TextColor','BGColor','Format','Font','FontSize' ),
	array( 'Image', 'Flash', 'Link', 'Unlink', 'Anchor' )
	); 
	
	
	return $ckeditor;
}
