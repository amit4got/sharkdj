<?php
/****************************
 *  Created 11/11/11
 *  Last update 25/12/12   
 ****************************/

if(!defined("RUTA_ABSOLUTA")){
  define("RUTA_ABSOLUTA",dirname(__FILE__));
}

require_once RUTA_ABSOLUTA.'/administradorDatos/administradorDatos.class.php';
require_once RUTA_ABSOLUTA.'/administradorDatos/artist.class.php';
require_once RUTA_ABSOLUTA.'/administradorDatos/imagenes.class.php';
#INCLUDES
require_once RUTA_ABSOLUTA.'/includes/connect.ini.php';
require_once RUTA_ABSOLUTA.'/includes/functions.ini.php';

// session_start();

// if ($_SESSION['status'] != "authorized") {
//   header ("Location: login.php");
//   exit();
// }
include_once RUTA_ABSOLUTA.'/includes/headerArtista.inc.php';

function soundcloud ($user)
{
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, 'http://soundcloud.com/oembed?format=xml&maxwidth=725&maxheight=395&url=http://soundcloud.com/'.$user);
  curl_setopt($ch, CURLOPT_HEADER, false);
  curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
  $xml = curl_exec($ch);
  $error = curl_error($ch);
  curl_close($ch);
  
  $html = explode('&lt;![CDATA[', $xml);
  $html = explode(']]&gt;', $html[1]);
  $html = $html[0];
  
  $html = str_replace('&lt;', '<', $html);
  $html = str_replace('&gt;', '>', $html);
 
//  $objectSoundCloud = @simplexml_load_string($xml);

   
  if ($html == ''){
    $html = '<img src="http://www.sharkdj.com/images/perfil_sin_soundcloud.jpg" alt="soundcloud" title="soundcloud" />';
  }
  /*
  else{

    
    $version = (string) $objectSoundCloud->version;
    $html = (string) $objectSoundCloud->html;
    $html = explode('<span>', $html);
    $html = $html[0];
    $html = preg_replace('/height="\d*"/','height="280"',$html);

  }
*/

  return $html;
}

function limpiaLTGT($string){
  $string = preg_replace('/\&lt;/','<', $string);
  $string = preg_replace('/\&gt;/', '>', $string);
  return $string;
}

function leerDirectorio($idUser)
{
  $dir='upload/php/files/'.$idUser.'/thumbnails/';
  $directorio=opendir($dir);
  while ($archivo = readdir($directorio)){
    $imagenes[]=$archivo;
  }
  closedir($directorio);
  return $imagenes;  
}

?>
<div id="fb-root"></div>
<script type="text/javascript">(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/es_LA/all.js#xfbml=1&appId=142309935866555";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
</script>

<?php 
$idUser = $_GET['id'];
$imagenUrl = imagenes::obtenerImagenUsuario($idUser);
$artista = new artist();
$datos = $artista->obtenerDatosArtista($idUser);
$redes = $artista->obtenerRedesSociales($idUser);

$idArtistaAnterior = $artista->obtenerIdAnterior($idUser);
$idArtistaSiguiente = $artista->obtenerIdSiguiente($idUser);

foreach ($redes as $red){
  switch ($red['idRed']) {
    case '1':
    $facebook = $red['url'];
    break;
    case '2':
      $twitter = $red['url'];
    break;
    case '3':
      $soundcloud = $red['url'];
    break;
    case '4':
      $youtube = $red['url'];
    break;
    case '5':
      $myspace = $red['url'];
    break;
    default:
      ;
    break;
  }
}

srand();
$publicidades = $adminDatos->obtenerPublicidad();
if (count($publicidades) < 6){
  $claves = array_rand($publicidades,count($publicidades)); 
}else{
  $claves = array_rand($publicidades,6);

}
  shuffle($claves);
?>

  <div id="main">
    <div id="searchBar" class="marginBottom10">
        <div class="right fontRed noDecoration font1 fontGrey2 marginTop15 marginRight10">
            <?php if($idArtistaAnterior): ?>
            <a href="<?=$_SERVER['PHP_SELF'];?>?id=<?=$idArtistaAnterior["idUser"];?>" class="fontGrey noDecoration hoverLargeGrey">Anterior</a>
            <?php endif; ?>
            <?php if($idArtistaAnterior && $idArtistaSiguiente): ?>
            |
            <?php endif; ?>
            <?php if($idArtistaSiguiente): ?>
            <a href="<?=$_SERVER['PHP_SELF'];?>?id=<?=$idArtistaSiguiente["idUser"];?>" class="fontGrey noDecoration hoverLargeGrey">Siguiente</a>
            <?php endif; ?>
        </div>
    </div>
    <div class="insideRow3 bkgdGrey fontBold">
      <div class="box7">
        <div class="artistPhoto2"><img src="<?php echo $imagenUrl;echo '?nocache='.date("isu");?>" width="140" height="140" alt="" title="" /></div>
        <span class="fontRed font3"><?php echo $datos['nombre'].' '.$datos['apellido1'];?></span>
        <br />
        <br />
        <span class="fontWhite"><?=$arrayIdiomas['country'];?>:<br /></span>
        <span class="fontBlack"><?php echo $campo = isset($datos['pais'])? $datos['pais']:''?></span>
        <br />
        <span class="fontWhite"><?=$arrayIdiomas['style'];?>:<br /></span>
        <span class="fontBlack"><?php echo $campo = isset($datos['estilo'])? $datos['estilo']:''?></span>
        <br />
        <span class="fontWhite"><?=$arrayIdiomas['residencia'];?>:<br /></span>
        <span class="fontBlack"><?php echo $campo = isset($datos['clubResidencia'])? $datos['clubResidencia']:''?></span>
        <br />
<!--    <span class="fontWhite">Contacto:</span>
        <span class="fontBlack">XXX</span>
        <br /> -->
		<?php $web = isset($datos['web'])? str_replace('http://','',$datos['web']):''?>
		<span class="fontWhite">Website:<br /></span>
        <span><a href="http://<?=$web;?>" class="website"><?echo str_replace('www.','',$web);?></a></span>
        <div class="clear left">
          <div id="socialLinks2">
            <?php if ($facebook != ''){?>
              <a href="http://<?php echo $facebook;?>" class="left width24px marginLeft5" id="linkFA"></a>
            <?php } 
            if ($soundcloud != ''){?>
            <a href="http://soundcloud.com/<?php echo $soundcloud;?>" class="left width24px marginLeft5" id="linkTW"></a>
            <?php } 
            if ($twitter != ''){?>
            <a href="http://www.twitter.com/<?php echo $twitter;?>" class="left width24px marginLeft5" id="linkMY"></a>
            <?php } 
            if ($myspace != ''){?>
            <a href="http://www.myspace.com/<?php echo $myspace; ?>" class="left width24px marginLeft5" id="linkSO"></a>
            <?php } 
            if ($youtube != ''){?>
            <a href="http://www.youtube.com/<?php echo $youtube;?>" class="left width24px marginLeft5" id="linkYO"></a>
            <?php } ?>
          </div>
        </div>
        <?php $imagenes = leerDirectorio($datos['idUser']);?>
        <div class="box8"><?php 
          foreach ($imagenes as $imagen){
            if (preg_match('/(gif|jpe?g|png)$/i', $imagen)){
              echo '<div class="thumb"><a class="group1" href="upload/php/files/'.$datos['idUser'].'/'.$imagen.'"><img src="upload/php/files/'.$datos['idUser'].'/thumbnails/'.$imagen.'" alt="" title="" /></a></div>';
            }
          }
        ?></div>
      </div>
      <div class="box15">
        <span class="fontRed font3"><?=$arrayIdiomas['biography'];?></span>
        <br />
        <span><?php echo strip_tags(limpiaLTGT($datos['bio']),'<br><b><p>');?></span>
      </div>
      </div>
      <div class="advertisementRow3"><?php
  foreach($claves as $c){
     echo '<br><a href="'.$publicidades[$c]['url'].'"><img src="http://www.sharkdj.com/'. $publicidades[$c]['imagen'] .'" alt="' . $arrayIdiomas['ads'] . '" title="' . $arrayIdiomas['ads'] . '" /></a><br>';
  }?></div>
      <div class="box9">
        <div id="soundCloud"><?php echo soundcloud($soundcloud)?></div>
      </div>

      <div class="insideRow2 fontBold bkgdGrey">
        <div class="box13"><!-- BEGIN: Twitter for Web widget (http://twitterforweb.com) -->
<div style="width:350px;font-size:8px;text-align:right;"><script type="text/javascript" src="http://widgets.twimg.com/j/2/widget.js"></script>
<script type="text/javascript">
new TWTR.Widget({
  version: 2,
  type: 'profile',
  rpp: 4,
  interval: 30000,
  width: 350,
  height: 200,
  theme: {
    shell: {
        background: '#cccccc',
        color: '#080808'
      },
      tweets: {
        background: '#ededed',
        color: '#000000',
        links: '#e01616'
      }
  },
  features: {
    scrollbar: true,
    loop: false,
    live: false,
    behavior: 'all'
  }
}).render().setUser('<?echo $twitter;?>').start();
</script></div>



<!-- END: Twitter for Web widget (http://twitterforweb.com) --></div>
        <div class="box13">
          <?php if ($facebook != ''){?>
          <div class="fb-like-box" data-href="http://<?php echo $facebook;?>" data-width="350" data-show-faces="true" data-stream="false" data-header="true"></div>
          <?php } ?>
    </div> 
  </div>

	<div class="insideRow2 bkgdGrey fontBold">
        <div class="box14"><div id="map_canvas" style="width:720px;height:280px;top:5px">
          <script type="text/javascript">
          function placeMarketArtist() {
            <?php if ($datos['lat'] !="" && $datos['lng'] !="") {?>
        	  placeMarkerFromLatLng('<?php echo $datos['lat'];?>','<?php echo $datos['lng'];?>');
        	  <?php } ?>
          }
      </script></div></div>
      </div>
    </div>
<?php include_once 'includes/footer.inc.php'; ?>