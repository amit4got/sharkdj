<?php
class Activar extends Controller {
	function __construct() 
	{
		parent::Controller();
	}
	function index()
	{
		$this->form_validation->set_rules('code', 'Codigo de verificación', 'required');
	    $this->form_validation->set_error_delimiters('<p class="error">', '</p>');
	    if ($this->form_validation->run() == false)
	    { 
			$data['head'] = "";
			$data['navigation'] = "";
			$data['content_menu'] = "";
			$data['content'] = $this->load->view('admin/activar_view', null, true);
			$data['foot'] = "";
	    	$this->load->view('templates/admin_view', $data);
	    }
	    else
	    {
	        $code = $this->input->post('code');
			$activate = $this->redux_auth->activate($code);
		    
			if ($activate)
			{
				//$this->session->set_flashdata('message', '<p class="success">Tu cuenta ha sido activada, ya puedes ingresar en el panel de control.</p>');
	            redirect('admin'); 
			}
			else
			{
				$this->session->set_flashdata('message', '<p class="error">Tu cuenta ya ha sido activada.</p>');
	            redirect('admin/activar'); 
			}
	    }
		
	}
}


