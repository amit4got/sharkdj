<p>Bienvenid@ a SHARK DJ, tu cuenta ha sido activada con &eacute;xito,  este ser&aacute; tu panel de administraci&oacute;n donde podr&aacute;s gestionar tu informaci&oacute;n,  bio,&nbsp; fotos, fechas de eventos y subir  tus sesiones o temas.</p>
<p>Si est&aacute;s interesado en promocionar SHARK, debes dirigirte  con un email a <a href="mailto:marketing@sharkiberia.sl">marketing@sharkiberia.sl</a></p>
<p>Si tienes problemas con tu cuenta dir&iacute;gete a <a href="mailto:info@sharkdj.com">info@sharkdj.com</a></p>
<p>Por favor hasta que no  introduzcas informaci&oacute;n y una foto no te hagas visible en la web, en caso de  hacerlo nos reservamos el derecho de cambiar tu perfil a no visible.</p>
<p>Tu foto de perfil debe tener un formato lo mas  cuadrado posible, las fotos con formatos alargados pueden recortarse.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><br />
</p>
