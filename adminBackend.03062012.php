<?php

if(!defined("RUTA_ABSOLUTA")){
  define("RUTA_ABSOLUTA",dirname(__FILE__));
}

#INCLUDES
require_once RUTA_ABSOLUTA.'/administradorDatos/artist.class.php';
require_once RUTA_ABSOLUTA.'/administradorDatos/administradorDatos.class.php';
include	RUTA_ABSOLUTA.'/editor/spaw2/spaw.inc.php';

session_start();

if ($_SESSION['statusAdmin'] != "authorized") {
  header ("Location: index.php");
  exit();
}
include_once 'includes/headerAdmin.inc.php';

$texto="";
$spaw = new SpawEditor("texto");
$spaw->addPage(new SpawEditorPage("texto","Español",$texto));
$spaw->addPage(new SpawEditorPage("lang_en","Ingles",$lang_en));
$spaw->setConfigValue('default_toolbarset','standard');
$idUser = $_SESSION['idUser'];

?>
<div class="body">

<?php
$datos = new administradorDatos();
if ($_POST){
  if (isset($_POST['modificar'])) {    
    $idPost = isset($_POST['idPost']) ? $_POST['idPost'] : 0;
    $idUser = isset($_POST['idUser']) ? $_POST['idUser'] : 0;
    $idCategoria = isset($_POST['idCategoria']) ? $_POST['idCategoria'] : 0;
    $visible = isset($_POST['visible']) ? 1 : 0;
    $titulo = isset($_POST['titulo']) ? $_POST['titulo'] : "";
    $fecha = isset($_POST['fecha']) ? $_POST['fecha'] : date('Y-m-d');
    // texto noticia.
    $texto = $_POST['texto'];
    $texto = str_replace("\\", '', $texto);
    $texto = preg_replace('/<img border=\"0\" style=\"display: none; \" src=\"http:\/\/visit\.webhosting\.yahoo\.com\/visit\.gif.*<\/noscript>/im', '', $texto);
    $texto = preg_replace('/<noscript>.*?<\/noscript>/i','',$texto);
    //texto noticia inglés.
    $lang_en = $_POST['lang_en'];
    $lang_en = str_replace("\\", '', $lang_en);
    $lang_en = preg_replace('/<img border=\"0\" style=\"display: none; \" src=\"http:\/\/visit\.webhosting\.yahoo\.com\/visit\.gif.*<\/noscript>/im', '', $lang_en);
    $lang_en = preg_replace('/<noscript>.*?<\/noscript>/i','',$lang_en);
    // $idUser,$titulo,$texto,$fecha,$idCategoria,$idPost
    $datosNoticia = array("idUser"=>$idUser,"titulo"=>$titulo,"texto"=>$texto,"lang_en"=>$lang_en,"fecha"=>$fecha,"idCategoria"=>$idCategoria,"visible"=>$visible,"idPost"=>$idPost);    
    $datos->actualizaNoticia($datosNoticia);
  } else {
    $texto = $_POST['texto'];    
    $texto = str_replace("\\", '', $texto);
    $texto = preg_replace('/<img border=\"0\" style=\"display: none; \" src=\"http:\/\/visit\.webhosting\.yahoo\.com\/visit\.gif.*<\/noscript>/im', '', $texto);
    $texto = preg_replace('/<noscript>.*?<\/noscript>/i','',$texto);
    $_POST['Texto'] = $texto;
    //texto noticia inglés.
    $lang_en = $_POST['lang_en'];
    $lang_en = str_replace("\\", '', $lang_en);
    $lang_en = preg_replace('/<img border=\"0\" style=\"display: none; \" src=\"http:\/\/visit\.webhosting\.yahoo\.com\/visit\.gif.*<\/noscript>/im', '', $lang_en);
    $lang_en = preg_replace('/<noscript>.*?<\/noscript>/i','',$lang_en);
    $_POST['lang_en'] = $lang_en;
    
    $noticia['texto'] = $texto;
    $noticia['lang_en'] = $lang_en;
    $noticia['idUser'] = $idUser;
    $noticia['idCategoria'] = '1';
    $noticia['titulo'] = $_POST['titulo'];
    $noticia['fecha'] = $_POST['fecha'];
    $noticia['visible'] = isset($_POST['visible']);
  }    
} 

$option = (isset($_GET['o'])) ? $_GET['o'] : 0;
if ($option == '1'){  
  $imgPost = $datos->proximaNoticia(); 
  $idPost = $imgPost;
?>

  <div id='content'>
    <div class='container'>
    <form action="adminBackend.php?o=2" method="post" id="form">
  		<div id="label1">Titulo</div>
  		<input id="field1" name="titulo" type="text" value=""><br><br>
  		<div id="label1">Fecha</div>
  		<input id="datepicker" name="fecha" type="text" value="<?php echo date('Y-m-d')?>"><br><br>
  		<div id="label1">Visible<input name="visible" type="checkbox" <?php echo $visibleCheck;?> /></div><br><br>
  		<?php $imagen = file_exists('images/posts/'.$imgPost.'_c.jpeg') ? 'images/posts/'.$imgPost.'_c.jpeg' : 'images/posts/0.jpeg';?>  		     
  		  					<div>
  		  						<img src="<?php echo $imagen;  echo '?nocache='.date("isu"); ?>" width="250" height="250">
  		  						<a href="editarFotoNoticia.php?im=<?php echo $idPost;?>&i=<?php echo $idPost?>&p='<?php echo $_SERVER['argv'][0]?>'"><button>Editar Foto</button></a>
  		  				</div>
           <?php $spaw->show();?><br><br>
      	<p><input class="buttonLogin" type="submit" value="Guardar" /></p>
  	</form>
    </div>
  </div>

<?php   
}
elseif($option == '2'){  
  $datos->introduceNoticia($noticia);

}elseif($option == '3'){
?>

  <div id='content'>
    <div class='container'>
    	<b><a href='adminBackend.php?o=1'>Crear noticia nueva</a></b><br/><br/>

<?php   
  $tabla="";
  $noticias = $datos->obtenerNoticias();  
  if (count($noticias)>0){
    $tabla="<table>";    
    foreach($noticias as $noticia){
      $tabla.="<tr>";
      $tabla.="<td>".$noticia['titulo']." -</td>";
      $tabla.="<td><a href='adminBackend.php?o=4&n=".$noticia['idPost']."'>Editar</a></td>";
      $tabla.="</tr>";
    }
    $tabla.="</table>";
  }
  echo $tabla;
}elseif($option == '4'){  
  $idPost = (isset($_GET['n'])) ? $_GET['n'] : 0;
  if ($idPost>0) {
    $noticia = $datos->obtenerNoticia($idPost);    
    $idPost = $noticia['idPost'];
    $idUser = $noticia['idUser'];
    $idCategoria = $noticia['idCategoria'];
    $visible = $noticia['visible'];
    $visibleCheck = ($visible) ? "checked='checked'" : "";
    $titulo = $noticia['titulo'];
    $fecha = $noticia['fecha'];
    $aux =  explode('/', $fecha);
    $fecha = $aux[2].'-'.$aux[1].'-'.$aux[0];
    $texto = $noticia['texto'];
    $texto = str_replace("\\", '', $texto);
    $texto = preg_replace('/<img border=\"0\" style=\"display: none; \" src=\"http:\/\/visit\.webhosting\.yahoo\.com\/visit\.gif.*<\/noscript>/im', '', $texto);
    $texto = preg_replace('/<noscript>.*?<\/noscript>/i','',$texto);
    $lang_en = $noticia['lang_en'];
    $lang_en = str_replace("\\", '', $lang_en);
    $lang_en = preg_replace('/<img border=\"0\" style=\"display: none; \" src=\"http:\/\/visit\.webhosting\.yahoo\.com\/visit\.gif.*<\/noscript>/im', '', $lang_en);
    $lang_en = preg_replace('/<noscript>.*?<\/noscript>/i','',$lang_en);
    $spaw = new SpawEditor("texto");
    $spaw->addPage(new SpawEditorPage("texto","Español",$texto));
    $spaw->addPage(new SpawEditorPage("lang_en","Ingles",$lang_en));
?>
    
  <div id='content'>
    <div class='container'>
    <form action="adminBackend.php?o=3" method="post" id="form">
  		<div id="label1">Titulo</div>
  		<input id="field1" name="titulo" type="text" value="<?php echo $titulo;?>"><br><br>
  		<div id="label1">Fecha</div>  		
  		<input id="datepicker" name="fecha" type="text" value="<?php echo $fecha;?>"><br><br>
  		<div id="label1">Visible<input name="visible" type="checkbox" <?php echo $visibleCheck;?> /></div><br><br>
  		<input type="hidden" name="idPost" value="<?php echo $idPost;?>" />
  		<input type="hidden" name="idUser" value="<?php echo $idUser;?>" />
  		<input type="hidden" name="idCategoria" value="<?php echo $idCategoria;?>" />
  		<?php $imagen = file_exists('images/posts/'.$idPost.'_c.jpeg') ? $idPost.'_c.jpeg' : 'images/posts/0.jpeg';?>  		      
  		  					<div>
  		  						<img src="images/posts/<?php echo $idPost;?>_c.jpeg<?php  echo '?nocache='.date("isu");?>" width="250" height="250">
  		  						<a href="editarFotoNoticia.php?im=<?php echo $idPost;?>&i=<?php echo $idPost?>&p='<?php echo $_SERVER['argv'][0]?>'"><button>Editar Foto</button></a>
  		  				</div>
           <?php $spaw->show();?><br><br>
      	<p><input type="hidden" name="modificar" value=""/><input class="buttonLogin" type="submit" value="Guardar" /></p>
  	</form>
    </div>
  </div>
      
<?php  
        
  }
}
?>

  	</div>
  </div>

  <div id="menu">
    <img src="images/logoHeader.png" width="150">
    <br><br>
    <ul>

      <li><a href="adminBackend.php?o=3">Noticias</a></li><br><br>
      <li><a href="adminBackendEventos.php?o=3">Eventos</a></li><br><br>      
      <li><a href="adminBackendUser.php?o=1">DJ's</a></li><br><br>
      <li><a href="adminBackendLocal.php?o=1">Locales</a></li><br><br>
      <li><a href="adminPublicidad.php">Publicidad</a></li><br><br>
      <li><a href="adminBanner.php">Banner</a></li><br><br>
      <li><a href="admin.php?logout" style="color:red;">Logout</a></li>
    </ul> 
  </div>
</div>