<div id="cuerpo">
    
<?=$this->session->flashdata('message'); ?>

<?=validation_errors(); ?>

<?=form_open_multipart($accion_form, array("id" => 'form1' ) ); ?>
 	<div class="formularios_titulo">Campos requeridos</div>
    <div class="campos_supertitulo">Información del pais</div>
    <div class="campos">
        <div class="campos_titulo">Nombre del pais (español)</div>
        <div class="campos_introducir"><?=form_input('es_titulo',set_value('es_titulo')=="" ?  $registro['es_titulo']  :   set_value('es_titulo') ); ?></div>
    </div>
    <div class="campos">
        <div class="campos_titulo">Nombre del pais (ingles)</div>
        <div class="campos_introducir"><?=form_input('en_titulo',set_value('en_titulo')=="" ?  $registro['en_titulo']  :   set_value('en_titulo') ); ?></div>
    </div>
    <div class="campos">
        <div class="campos_titulo">Nombre del pais (holandes)</div>
        <div class="campos_introducir"><?=form_input('du_titulo',set_value('du_titulo')=="" ?  $registro['du_titulo']  :   set_value('du_titulo') ); ?></div>
    </div>
    <div id="btnsubmit"><?=form_submit('submit', 'Guardar'); ?></div>
<?=form_close(''); ?>      
</div>  
