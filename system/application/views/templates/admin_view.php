<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"

    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html>

    <head>

        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<script type="text/javascript" src="<?=base_url(); ?>js/ckeditor/ckeditor.js"></script>
        <title>Shark Dj - Administración</title>

        <?= link_tag('assets/css/admin.css')."\n"; ?>

      	<link rel="stylesheet" type="text/css" href="<?=base_url(); ?>js/JSCal2-1.8/src/css/jscal2.css" />
        <link rel="stylesheet" type="text/css" href="<?=base_url(); ?>js/JSCal2-1.8/src/css/reduce-spacing.css" />
        <link rel="stylesheet" type="text/css" href="<?=base_url(); ?>js/JSCal2-1.8/src/css/border-radius.css" />

    </head>

    <body <? if (isset($body_carga)) { echo $body_carga; } ?>>

        <div id="wrapper">

            <div id="head">

                <?= $head."\n" ?>

            </div>

            <ul id="navigation">

                <?= $navigation."\n" ?>

            </ul>

            <div id="content">
				<?= $content_menu."\n" ?>
                <?= $content."\n" ?>

            </div>

            <div id="foot">

                <?= $foot."\n" ?>

            </div>

        </div>

    </body>

</html>