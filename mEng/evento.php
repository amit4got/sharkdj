<?php 
	include_once 'header.php';
	
	if(!defined("RUTA_ABSOLUTA")){
	  define("RUTA_ABSOLUTA",dirname(__FILE__));
	}
	
	require_once RUTA_ABSOLUTA.'/administradorDatos/administradorDatos.class.php';
	require_once RUTA_ABSOLUTA.'/administradorDatos/artist.class.php';
	require_once RUTA_ABSOLUTA.'/administradorDatos/imagenes.class.php';
	#INCLUDES
	require_once '../includes/connect.ini.php';
	require_once '../includes/functions.ini.php';
	
	$idEvento= isset($_GET['id']) ? $_GET['id'] : 0;
	$adminEvento = new administradorDatos();
	$evento = $adminEvento->obtenerEvento($idEvento);

  if (file_exists('../images/eventos/'.$idEvento.'_c.jpeg')){
    $imagen = 'http://sharkdj.com/images/eventos/'.$idEvento.'_c.jpeg';
    $imagenGrande = 'http://sharkdj.com/images/eventos/'.$idEvento.'.jpeg';
  }
?>
	<div data-role="page" data-theme="c" data-content-theme="c" id="evento">
		
		
  <?php echo $header;?>
  <div data-role="content">
    <div class="artist">
      <div class="ui-grid-a">
        <div class="ui-block-a">
    			<div class="image">
    				<a href="<?php echo $imagenGrande;?>" rel="external"><img src="<?php echo $imagen; ?>"></a>
    			</div>
        </div>
        <div class="ui-block-b">
          <div class="artistname"><?php echo $evento['nombre']; ?></div>
          <p><strong><?php echo $evento['fechaInicio']; ?></strong></p>
          <p><?php echo $evento['direccion']; ?></p>
        </div> 
      </div>
		</div>
   <p><?php echo $evento['descripcion']?></p>

  </div>
		
    <?php echo $footer?>
</div>
