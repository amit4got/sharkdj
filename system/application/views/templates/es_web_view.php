<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<html>  
  <head>
    <title>Shark</title>
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="copyright" content="">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="<?=base_url(); ?>assets/css/web.css" >
    <?php if (isset($calendario)): ?>
    	<link rel="stylesheet" type="text/css" href="<?=base_url(); ?>js/JSCal2-1.8/src/css/jscal2.css" />
        
        <link rel="stylesheet" type="text/css" href="<?=base_url(); ?>js/JSCal2-1.8/src/css/border-radius.css" /> 
    <?php endif; ?> 
    <script src="<?=base_url(); ?>Scripts/swfobject_modified.js" type="text/javascript"></script>
    <script type="text/javascript">
//<![CDATA[	
	function abrir_radio_general(){
		window.open('<?=base_url(); ?>index.php/web/radio','Radio','width=460,height=380,resizable=yes ,top=0,left=0')	
	}
	function abrir_mapa_general(){
		window.open('<?=base_url(); ?>index.php/web/mapa','Mapa','width=758.8,height=582.05,resizable=yes ,top=0,left=0')	
	}
//]]>
</script>
<link rel="icon" type="image/gif" href="http://www.sharkdj.com/images/favicon.gif" />
  </head>

<body <?= isset($body_carga)?  $body_carga : ''   ?>>
    <div id="centrar">
        <div id="head">
             <div id="head_logo"><img src="<?=base_url(); ?>file/entorno/logo_1.png"  alt="shark"  ></div>
             <div style="float:right;">
             	<a href="http://www.sharkdj.com/index.php/web/noticias" target="_self" >Español</a> · 
              <a href="http://www.sharkdj.com/index.php/web/news" target="_self" >English</a> ·
              <a href="http://www.sharkdj.com/index.php/web/nieuws" target="_self" >Nederlands</a> 
            	<a href="http://www.facebook.com/pages/Shark-DJ/121047887943034?v=info&ref=ts" target="_blank" ><img src="<?=base_url(); ?>file/entorno/facebook.png"></a>
        		<a href="http://twitter.com/sharkenergia" target="_blank" ><img src="<?=base_url(); ?>file/entorno/twitter.png"></a>
        		<a href="http://www.myspace.com/SHARKdeejay" target="_blank" ><img src="<?=base_url(); ?>file/entorno/myspace.png"></a> 
                
             </div>
             <ul id="head_menu">
             
                    <li<?= $pulsado=='artistas'? ' class="head_menu_pulsado"' : ''  ?>><?=anchor('web/artistas/' , 'Artistas' ,$pulsado=='artistas'? 'style="color:#FFF"' : ''); ?></li><li>·</li>
                    <li<?= $pulsado=='clubs'   ? ' class="head_menu_pulsado"' : ''  ?>><?=anchor('web/clubs/'	 , 'Clubs'    ,$pulsado=='clubs'   ? 'style="color:#FFF"' : ''); ?></li><li>·</li>
                    <li<?= $pulsado=='fiestas' ? ' class="head_menu_pulsado"' : ''  ?>><?=anchor('web/fiestas/'	 , 'Fiestas'  ,$pulsado=='fiestas' ? 'style="color:#FFF"' : ''); ?></li><li>·</li>
                    <li<?= $pulsado=='escuela' ? ' class="head_menu_pulsado"' : ''  ?>><?=anchor('web/escuela/'	 , 'Escuela'  ,$pulsado=='escuela' ? 'style="color:#FFF"' : ''); ?></li><li>·</li>
                    <li<?= $pulsado=='foro'    ? ' class="head_menu_pulsado"' : ''  ?>><a href=" http://www.sharkdj.com/foro/" target="_self" >Foro</a></li><li>·</li>
                    <li<?= $pulsado=='noticias'? ' class="head_menu_pulsado"' : ''  ?>><?=anchor('web/noticias/' , 'Noticias' ,$pulsado=='noticias'? 'style="color:#FFF"' : ''); ?></li>
             </ul>             
        </div>
		
		<?= $sub_head."\n" ?>
		<?= $cuerpo."\n" ?>
		<div id="foot">
            <ul id="footmenu_der">
              
                <li><a href="mailto:info@sharkdj.com" target="_blank" >info@sharkdj.com</a></li>
            </ul>
		</div>
    </div>
    <script type="text/javascript">
      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-24660306-1']);
      _gaq.push(['_trackPageview']);
      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();
    </script>
</body>
</html>