<?php
/****************************
 *  Created 11/11/11
 *  Last update 11/11/11   
 ****************************/

#INCLUDES
require_once 'includes/connect.ini.php';
require_once 'includes/functions.ini.php';

session_start();

if ($_SESSION['statusAdmin'] != "authorized") {
  header ("Location: index.php");
  exit();
}

function is_alpha_png($fn){
  return (ord(@file_get_contents($fn, NULL, NULL, 25, 1)) == 6);
}

if ($_POST['crop'])
{
  $targ_w = 200;
  $targ_h = 280;
  $jpeg_quality = 90;

  $nombreImagen = explode('/',$_POST['imagen']);
  $nombreImagen = $nombreImagen[count($nombreImagen) - 1];
  
  $directorio = 'images/eventos/';
    
  $src = $directorio.$_POST['idEvento'].'.jpeg';  
  $img_r = @imagecreatefromjpeg($src);
  $dst_r = @ImageCreateTrueColor( $targ_w, $targ_h );
  
  @imagecopyresampled($dst_r,$img_r,0,0,$_POST['x'],$_POST['y'],$targ_w,$targ_h,$_POST['w'],$_POST['h']);

//   header('Content-type: image/jpeg');
  $imagen = $directorio.$_POST['idEvento'].'_c.jpeg';
  @imagejpeg($dst_r,$imagen,$jpeg_quality);
  
  
  $parametros = str_replace("\\", "",$_POST['p']);  
  $parametros = str_replace("'", "",$parametros);
  $parametros = str_replace("_", "&",$parametros);
  $parametros = str_replace("o", "&o",$parametros);
  

  header('refresh:0; url=adminBackendEventos.php');
  header('Location: adminBackendEventos.php?'.$parametros);
  
}
elseif ($_POST['imagenSubida']){
  
    // Datos de conexión a configurar
    //     $user = "usuario_bbdd";
    //     $pass = "clave_bbdd";
    //     $bbdd = "nombre_bbdd";
  
    // Ruta donde se guardarán las imágenes
    $directorio = 'images/eventos/';
    $parametros = str_replace('\\', '',$_POST['p']);
    
    // Conecto a la BBDD
    //     $dbh = mysql_connect("localhost", $user, $pass);
    //     $db = mysql_select_db($bbdd);
  
    // Recibo los datos de la imagen
    $name = $_FILES['imagen']['name'];
    $tipo = $_FILES['imagen']['type'];
    $tamano = $_FILES['imagen']['size'];
    $prefijo = substr(md5(uniqid(rand())),0,20);

    if ($_FILES['imagen']['error'] == 1){
       header("Location: editarFotoEvento.php?e=2&i=".$_POST['idEvento']."&pp=".$parametros);
      exit;
    }
    if ($tamano > 1048576){
      header("Location: editarFotoEvento.php?e=3&s=".$tamano."&i=".$_POST['idEvento']."&pp=".$parametros);
      exit;
    }
    // guardamos el archivo a la carpeta files
    $destino =  $directorio.$prefijo."_".$name;
    move_uploaded_file($_FILES['imagen']['tmp_name'],$directorio.$prefijo.'_'.$name);
  
    $anchura=400;
    $hmax=660;
    $nombre=$directorio.$prefijo.'_'.$name;
    
    if (is_alpha_png($nombre)){
      header("Location: editarFotoEvento.php?e=4&pp=".$parametros);
      exit;
    }
    
    $datos = getimagesize($nombre);
    if($datos[2]==1){
      $img = @imagecreatefromgif($nombre);
    }
    elseif($datos[2]==2){
      $img = @imagecreatefromjpeg($nombre);
    }
    elseif($datos[2]==3){
      $img = @imagecreatefrompng($nombre);
    }
    else{
       header("Location: editarFotoEvento.php?e=1&pp=".$parametros);
       exit;
    }
    if ($datos[0] < 200 || $datos[1] < 280){
      unlink($directorio.$prefijo.'_'.$name);
      header("Location: editarFotoEvento.php?e=5&i=".$_POST['idEvento']."&pp=".$parametros);
      exit;
    }
    if ($datos[0] > 400 || $datos[1] > 660){
      $ratio = ($datos[0] / $anchura);
      $altura = ($datos[1] / $ratio);
      if($altura>$hmax){
        $anchura2=$hmax*$anchura/$altura;
        $altura=$hmax;
        $anchura=$anchura2;
      }
      $thumb = imagecreatetruecolor($anchura,$altura);
      imagecopyresampled($thumb, $img, 0, 0, 0, 0, $anchura, $altura, $datos[0], $datos[1]);

      if(file_exists($directorio.$_POST['idEvento'].'.jpeg')){
        unlink($directorio.$_POST['idEvento'].'.jpeg');
      }
      imagejpeg($thumb,$directorio.$_POST['idEvento'].'.jpeg');
      imagedestroy($thumb);
      unlink($directorio.$prefijo.'_'.$name);
    }
    else{
      if(file_exists($directorio.$_POST['idEvento'].'.jpeg')){
         unlink($directorio.$_POST['idEvento'].'.jpeg');
       }
       rename($directorio.$prefijo.'_'.$name, $directorio.$_POST['idEvento'].'.jpeg');
    }
  
      header('refresh:0; editarFotoEvento.php');
      header("Location: editarFotoEvento.php?im=".$_POST['idEvento']."&i=".$_POST['idEvento']."&pp=".$parametros);
    // Muevo la imagen desde su ubicación
    // temporal al directorio definitivo
  }

else{
include_once 'includes/headerBackendFotoEvento.inc.php';

?>

<?php 
$idEvento = $_GET['i']; #en realidad es el id del post
$directorio = 'images/eventos/';

$idImage = ($_GET['im'] == '0') ? '0' : $_GET['im'];
$parametros = (isset($_GET['pp'])) ? $_GET['pp'] : "'".str_replace("&", "_",$_SERVER['argv'][0])."'";
$imagen = $directorio.$idEvento.'.jpeg';
?>

<div id="mainFotoPerfil">
	<div class="ui-widget-content boxEditarFoto">
		<div class="imagenEditar">
			<img src="<?php echo $imagen;  echo '?nocache='.date("isu"); ?>" id="cropbox" />
		</div>
				<!-- This is the form that our event handler fills -->

        
      <form action="<?php $_SERVER['PHP_SELF']?>" enctype="multipart/form-data" method="post">
        <fieldset>
        <label for="file">Primero selecciona una foto de tu disco duro...</label><br><br>
        <input type="file" name="imagen" id="file" />
        <input type="hidden" id="idEvento" name="idEvento" value="<?php echo $idEvento?>" />
        <input type="hidden" name="p" value="<?php echo $parametros;?>" />
        <br><br><label>Una vez hayas seleccionado la foto, dale a subir imagen.</label>
        <input type="submit" name="imagenSubida" id="upload" value="Subir Imagen" />
        </fieldset>
      </form>
      <form action="<?php $_SERVER['PHP_SELF']?>" method="post" onSubmit="return checkCoords();">
        <label>Una vez hayas subido la imagen, selecciona la región de la foto que quieres usar y haz click en Actualizar imagen.</label><br><br>
        <input type="hidden" id="x" name="x" />
        <input type="hidden" id="y" name="y" />
        <input type="hidden" id="w" name="w" />
        <input type="hidden" id="h" name="h" />
        <input type="hidden" id="idEvento" name="idEvento" value="<?php echo $idEvento?>" />
        <input type="hidden" name="p" value="<?php echo $parametros;?>" />
        <input type="submit" name="crop" value="Actualizar imagen" />
      </form>
        
        
	<?php if ($_GET['e'] == '1'){?>
	  <div id="dialog" title="Formato incorrecto">
			<p>Formato de archivo no permitido, por favor comprueba que sea jpg, gif o png.</p>
		</div>
	<?php }
	elseif ($_GET['e'] == '2'){?>
	  <div id="dialog" title="Error subiendo imagen">
	  <p>Se ha producido un error subiendo la imagen, posiblemnte se deba al tamaño del archivo, recuerde que solo se permiten imagenes de menos de 600Kb.</p>
	  </div>
	<?php }
	elseif ($_GET['e'] == '3'){
	  $size = round($_GET['s']/1024,0);
	  ?>
    <div id="dialog" title="Archivo muy grande">
      <p>
        Su archivo pesa
        <?php echo '<b>'.$size.'</b>'?>
        Kb y el máximo permitido es 600 Kb
      </p>
    </div>

    <?php }
    elseif ($_GET['e'] == '4'){
      ?>
    <div id="dialog" title="Archivo con transparencia">
      <p>Su archivo tiene canal alpha, no se perrmite este tipo de
        imagen.</p>
    </div>
    
	<?php }
	elseif ($_GET['e'] == '5'){
	  ?>
	    <div id="dialog" title="Archivo muy pequeño">
	      <p>La imagen tiene que tener un ancho y una altura de mas de 240px</p>
	    </div>
	    
		<?php }
	elseif ($_GET['e'] == '0') {
	  ?>
    <div id="dialog" title="Archivo subido correctamente">
      <p>Su imagen se actualizó correctamente, sino la ve pulse F5 para refrescar su navegador.</p>
    </div>

    <?php }
	?>

  </div>
</div>
<div class="ui-widget-content boxTexto">
	Selecciona la foto y arrastra las esquinas de la siguiente caja transparente y recorta esta foto para usarla como foto de perfil.
</div>
<?php 
}
include_once 'includes/footer.inc.php'; ?>