<?php 
class Informacion extends Controller {
	function __construct() 
	{
		parent::Controller();
		if ($this->redux_auth->logged_in()==1 ){
			$profile_var = $this->redux_auth->profile();
			if ($profile_var->group!="artistas" && $profile_var->group!="admin"){
				$this->redux_auth->logout();
				redirect('web/login');
			}
		}else{
			$this->redux_auth->logout();
			redirect('web/login');
		}
		
		if ( !function_exists('version_compare') || version_compare( phpversion(), '5', '<' ) ){
			$this->load->library('php4/ckeditor');
			
		}else{
			$this->load->library('php5/ckeditor');
			
		}
		
		$this->load->helper('ckeditor');
		$this->load->helper('file'); 
		$this->load->model('web_fiestas_model');
		
		function hacer_thumb($nombre_del_archivo,$clave_thumb, $nuevo_ancho , $nuevo_alto){
				
				$config_resize['image_library']   = 'GD2'; 
				$config_resize['source_image']    = './uploads/informacion/'.$nombre_del_archivo; 
				$config_resize['thumb_marker']       = $clave_thumb;
				$config_resize['create_thumb']    = TRUE; 
				$config_resize['maintain_ratio']  = TRUE; 
				$config_resize['width']           = $nuevo_ancho; 
				$config_resize['height']          = $nuevo_alto; 
				return $config_resize;
		}
		function nombrenuevo(){
			$fecha = time (); 
			$lafecha=date("y_m_j_H_i_s",$fecha);
			return $lafecha;
		}
		
		
		
	}
	function ajax_cargar_ciudades (){
		$this->data['qry_ciudad']	= $this->web_fiestas_model->cargar_ciudades( $this->uri->segment(4) );
		$this->load->view('modulos/aj_select_ciudades_form_view',  $this->data);
	}
	
	function index()
	{
		
		$this->data['body_carga']=' onload="load();" onUnload="GUnload()" ';
		$this->data['profile']= $this->redux_auth->profile();
		$this->form_validation->set_rules('alias', '"Alias"', 'required');
		//$this->form_validation->set_rules('imagen', '"Imagen"', '');
		
		$this->form_validation->set_rules('es_informacion', '"Texto en espa�ol"', '');
		$this->form_validation->set_rules('en_informacion', '"Texto en ingles"', '');
		$this->form_validation->set_rules('du_informacion', '"Texto en holandes"', '');

	    $this->form_validation->set_error_delimiters('<p class="error">', '</p>');
	    if ($this->form_validation->run() == false)
	    { 
	
			$this->data['accion_form']='panel_en/informacion';
			
			$this->db->where('user_id', $this->data['profile']->id );
			$query=$this->db->get('meta');
			$this->data['errores']='';
			$ckeditor =  CK_Editor_artistas();
			$this->data['ckeditor']= $ckeditor; 
			foreach ($query->result() as $row)
			{
				
				if ($row->imagen!=""){
					$ar_imagen=explode(".",$row->imagen);
					$imagen=base_url().'uploads/informacion/'.$ar_imagen[0].'p.'.$ar_imagen[1];
				}else{
					$imagen=base_url().'uploads/informacion/espacio.gif';
				}
				$this->data['registro'] = array(
											"visible" 			=> $row->visible , 
											"alias" 			=> $row->alias , 
											"id_ciudad" 		=> $row->id_ciudad ,
											"id_pais" 			=> $row->id_pais , 
											"posx_flash" 		=> $row->posx_flash ,
											"posy_flash" 		=> $row->posy_flash , 
											"imagen"			=> $imagen ,
											"es_informacion" 	=> htmlspecialchars_decode($row->es_informacion) , 
											"en_informacion" 	=> htmlspecialchars_decode($row->en_informacion) ,
											"du_informacion" 	=> htmlspecialchars_decode($row->du_informacion) ,
											
											);
				$this->data['alias'] 	= $row->alias;
				$this->data['slct_estilo'] 	= explode(",",$row->estilos);
				$this->data['qry_estilo']  = $this->web_fiestas_model->cargar_estilos();
				$this->data['query']=$this->db->get('Pais');
				if ($row->id_pais!=''){
					$this->db->where('id_pais', $row->id_pais );
				}else{
					$this->db->where('id_pais', '1' );
				}
				$this->data['query_ciudad']=$this->db->get('Ciudad');
			}
			
			$this->data['head'] = $this->load->view('panel_en/head_view', $this->data, true);
			$this->data['navigation'] = $this->load->view('panel_en/main_menu_view', null, true);
			$this->data['content_menu'] = $this->load->view('panel_en/informacion_menu_view', $this->data, true);
			$this->data['content'] = $this->load->view('panel_en/informacion_form_view', $this->data, true);
	       	$this->data['foot'] = "";
			$this->load->view('templates/panel_view', $this->data);
	    }
	    else
	    {
	        
			$update = array(
							"visible" 			=> $this->input->post('visible') ,
							"alias" 			=> $this->input->post('alias') , 
							"estilos" 			=> $this->input->post('estilos_id'),
							"posx_flash" 		=> $this->input->post('posx_flash') , 
							"posy_flash" 		=> $this->input->post('posy_flash') , 
							"es_informacion" 	=> $this->input->post('es_informacion'),
							"en_informacion" 	=> $this->input->post('en_informacion'),
							"du_informacion" 	=> $this->input->post('du_informacion')
							);
			
			
			$config_upload['upload_path']    = './uploads/informacion/'; 
			$config_upload['allowed_types']  = 'gif|jpg|png'; 
			$config_upload['max_size']       = '1000'; 
			$config_upload['max_width']      = '2048'; 
			$config_upload['max_height']     = '1536'; 
			
			$this->load->library('upload', $config_upload);
			if ( $this->upload->do_upload('imagen')) 
			{ 
				$imagen=$this->upload->data();
			
				
				$nuevo_nombre=nombrenuevo();
				$ar_imagen_ant=explode(".",$imagen['file_name']);
				$update['imagen'] = $nuevo_nombre.'.'.$ar_imagen_ant[1];
				rename('./uploads/informacion/'.$imagen['file_name'],'./uploads/informacion/'.$update['imagen']); 
		
				$this->load->library('image_lib', hacer_thumb($update['imagen'], 'p', 180 , 1000 ) ); 
				$this->image_lib->resize(); 	
				$this->image_lib->clear(); 	
				$this->image_lib->initialize(hacer_thumb($update['imagen'], 'm', 220 , 1600 ) ); 
				$this->image_lib->resize(); 	
				
				
				$this->db->where('user_id', $this->data['profile']->id );
				$query=$this->db->get('meta');
				foreach ($query->result() as $row)
				{
					if ($row->imagen!=''){
						$ar_imagen=explode(".",$row->imagen);
						$imagen=PUBPATH.'uploads/informacion/'.$ar_imagen[0].'m.'.$ar_imagen[1];
						if (file_exists($imagen)){ 
							unlink($imagen);
						}
						$imagen=PUBPATH.'uploads/informacion/'.$ar_imagen[0].'p.'.$ar_imagen[1];
						if (file_exists($imagen)){ 
							unlink($imagen);
						}
						$imagen=PUBPATH.'uploads/informacion/'.$ar_imagen[0].'.'.$ar_imagen[1];
						if (file_exists($imagen)){ 
							unlink($imagen);
						}
					}
				}
			} 
			$update['id_pais'] 		= $this->input->post('select_pais');
		   	if ($this->input->post('es_new_ciudad')!=''){
				$this->data['registro_ciudad']['es_titulo'] 		= $this->input->post('es_new_ciudad');
				$this->data['registro_ciudad']['en_titulo'] 		= $this->input->post('en_new_ciudad');
				$this->data['registro_ciudad']['du_titulo'] 		= $this->input->post('du_new_ciudad');
				$this->data['registro_ciudad']['id_pais'] 			= $this->input->post('select_pais');
				$this->db->set($this->data['registro_ciudad']); 
				$this->db->insert('Ciudad');
				$update['id_ciudad'] =$this->db->insert_id();
			}else{
				$update['id_ciudad']= $this->input->post('select_ciudad');
			}
			
			$this->db->where('user_id', $this->data['profile']->id );
			
			if ($this->db->update("meta",$update)){
				//$this->session->set_flashdata('message', '<p class="notice">La informaci�n ha sido modifica.</p>');
	            redirect('panel_en/informacion');
			} else
	        {
	            //$this->session->set_flashdata('message', '<p class="error">No se ha podido modificar.</p>');
	            redirect('panel_en/informacion');
	        }
			
	    }	
	}
}


