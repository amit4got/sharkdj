<?php
class Login extends Controller {
	function __construct() 
	{
		parent::Controller();
		$this->load->model('banner_model');
	}
	function index()
	{
		$this->form_validation->set_rules('password_iden', 'password_iden', '');
	    $this->form_validation->set_rules('email_iden', 'email_iden', '');
		$this->form_validation->set_rules('username', 'username', '');
	    $this->form_validation->set_rules('email', 'email', '');
		$this->form_validation->set_rules('first_name', 'first_name', '');
	    $this->form_validation->set_rules('last_name', 'last_name', '');
		$this->form_validation->set_rules('telefono', 'telefono', '');
	  	if ($this->form_validation->run() == false)
	    {
			$this->data['pulsado']='';
			$this->data['sub_head'] = $this->load->view('web/sub_head_1_view', $this->data, true);
			$this->data['cuerpo'] = $this->load->view('web/login_view', $this->data, true);
			$this->load->view('templates/es_web_view',  $this->data);
	    }
	    else
	    {
			if ($this->input->post('acc')=='identificarse')
			{
				$email    = $this->input->post('email_iden');
				$password = $this->input->post('password_iden');
				$login = $this->redux_auth->login($email, $password, 'artistas');
				if ($login)
				{
					redirect('panel/portada');
				}
				else
				{
					$this->data['error_login']='Los datos de acceso no son correctos';
					$this->data['pulsado']='';
					$this->data['sub_head'] = $this->load->view('web/sub_head_1_view', $this->data, true);
					$this->data['cuerpo'] = $this->load->view('web/login_view', $this->data, true);
					$this->load->view('templates/es_web_view',  $this->data);
				}
			}
			else
			{
				$this->data['error_registro']="";
				$username = $this->input->post('username');
				$email    = $this->input->post('email');
				$password = $this->input->post('password');
				$ct_register=false;
				$register=false;
				if ($username!="" && $email!="" && $password!=""){
					$ct_register=true;
					if ($this->redux_auth_model->username_check($username))
					{
						$this->data['error_registro'].='<p>El nombre de usuario "'.$username.'" ya existe.</p>';
						$ct_register=false;
					}
					if ($this->redux_auth_model->email_check($email)){
						$this->data['error_registro'].='<p>El email "'.$email.'" ya existe.</p>';
						$ct_register=false;
					}
					if ($ct_register==true){
						if ($this->redux_auth->register($username, $password, $email , 'artistas'))
						{
							
							$register=true;
						}
					}
				}
				if ($register==true)
				{
					$this->session->set_flashdata('message', '<p class="notice">El usuario ha sido registrado, revise su correo</p>');
					redirect('web/login');
				}
				else
				{
					$this->data['error_registro'].='<p>No se ha podido registrar el ususario</p>';
					$this->data['pulsado']='';
					$this->data['sub_head'] = $this->load->view('web/sub_head_1_view', $this->data, true);
					$this->data['cuerpo'] = $this->load->view('web/login_view', $this->data, true);
					$this->load->view('templates/es_web_view',  $this->data);
				}
			}
	    }
	}
	

	
	
}


