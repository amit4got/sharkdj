<?php
/****************************
 *  Created 26/06/11
 *  Last update 25/12/12   
 ****************************/

#Esta parte detecta si el dispositivo es un móvil y redirige a la versión móvil de la web
function isMobile(){
  return preg_match('/ipod|iphone|android|opera mini|blackberry|palm os|windows ce|mobile/i', $_SERVER['HTTP_USER_AGENT'] );
}

if( isMobile() ){
  header('location: ./m/index.php');
  exit();
}
#Hasta aquí

if(!defined("RUTA_ABSOLUTA")){
	define("RUTA_ABSOLUTA",dirname(__FILE__));
}

require_once RUTA_ABSOLUTA.'/administradorDatos/administradorDatos.class.php';
require_once RUTA_ABSOLUTA.'/administradorDatos/artist.class.php';
require_once RUTA_ABSOLUTA.'/administradorDatos/imagenes.class.php';
#INCLUDES
require_once 'includes/connect.ini.php';
require_once 'includes/functions.ini.php';

// session_start();

// if ($_SESSION['status'] != "authorized") {
//   header ("Location: index.php");
//   exit();
// }

$noticia = '';
$adminDatos = new administradorDatos();

#Load languages
include RUTA_ABSOLUTA.'/includes/lang.inc.php';
$arrayIdiomas = $adminDatos->cargaIdioma($idioma);

$arrayNoticia = $adminDatos->obtenerNoticia($_GET['ip']);
$idNoticiaAnterior = $adminDatos->obtenerIdAnterior($_GET['ip']);
$idNoticiaSiguiente = $adminDatos->obtenerIdSiguiente($_GET['ip']);

$titulo = $arrayNoticia['titulo'];
if (isset($_GET['lang']) && $_GET['lang'] == 'en') {
  $contenido = $arrayNoticia['lang_en'];
}
else {
  $contenido = $arrayNoticia['texto'];
}
$fecha = $arrayNoticia['fecha'];
$categoria = $arrayNoticia['nombreCategoria'];

include_once 'includes/header.inc.php';

srand();
$publicidades = $adminDatos->obtenerPublicidad();
if (count($publicidades) < 6){
  $claves = array_rand($publicidades,count($publicidades)); 
}else{
  $claves = array_rand($publicidades,6);  

}
  shuffle($claves);

?>
  <div id="main">
    <div id="searchBar">
        <div class="right fontRed noDecoration font1 fontGrey2 marginTop15 marginRight10">
            <?php if($idNoticiaAnterior): ?>
            <a href="<?=$_SERVER['PHP_SELF'];?>?ip=<?=$idNoticiaAnterior["idPost"];?>" class="fontGrey noDecoration hoverLargeGrey">Anterior</a>
            <?php endif; ?>
            <?php if($idNoticiaAnterior && $idNoticiaSiguiente): ?>
            |
            <?php endif; ?>
            <?php if($idNoticiaSiguiente): ?>
            <a href="<?=$_SERVER['PHP_SELF'];?>?ip=<?=$idNoticiaSiguiente["idPost"];?>" class="fontGrey noDecoration hoverLargeGrey">Siguiente</a>
            <?php endif; ?>
        </div>
    </div>
    <div class="advertisementRow3"><?php
    foreach($claves as $c){
     echo '<br><a href="'.$publicidades[$c]['url'].'"><img src="http://www.sharkdj.com/'. $publicidades[$c]['imagen'] .'" alt="' . $arrayIdiomas['ads'] . '" title="' . $arrayIdiomas['ads'] . '" /></a><br>';
  }?></div>
    <div class="noticia">
    	<div class="cuerpoNoticia">
      	<div class="newsInfoTitle2"><?php echo $titulo?></div>
      	<div class="fechaCategoria"><?=$arrayIdiomas['date'];?>: <?php echo $fecha;?>&nbsp;&nbsp;<?=$arrayIdiomas['category'];?>: <?php echo $categoria;?></div>
      	<div class="newsInfoText2"><?php echo $contenido;?></div>
    	</div>
  	</div>
  	<div id="nextPreviousMenu" class="clear left insideRow">
    	<div class="left width33"><a href="./index.php?p=<?php echo ($pagina>0) ? $pagina-1 : 0;?>" class="noDecoration fontGrey2 fontBold hoverLargeGrey">&nbsp;</a></div>
    	<div class="left width33 textCentered marginLeft5"><a href="./index.php?p=0" class="noDecoration fontGrey2 fontBold hoverLargeGrey"><?=$arrayIdiomas['homeMenu'];?></a></div>        
    	<div class="right"><a href="./index.php?ip=<?php echo (($pagina+1)*5 < $numTotalNoticias) ? $pagina+1 : $pagina;?>" class="fontBold noDecoration fontGrey2 hoverLargeGrey">&nbsp;</a></div>
  	</div>
	</div>
	
<?php include_once 'includes/footer.inc.php'; ?>
