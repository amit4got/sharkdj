<?php
//$arrayFlagLanguages = array ('es' => 'Espa&ntilde;ol', 'en' => 'English', 'fr' => 'Fran&ccedil;ais', 
//                             'it' => 'Italiano', 'pt' => 'Portugues', 'de' => 'Deutsche');
$arrayFlagLanguages = array ('es' => 'Espa&ntilde;ol', 'en' => 'English');
#Load languages
$idioma = isset ($_GET['lang']) ? $_GET['lang'] : 'es';

if (!array_key_exists ($idioma, $arrayFlagLanguages)) {
  $idioma = 'es';
}