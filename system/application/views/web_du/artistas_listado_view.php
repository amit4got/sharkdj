<div id="cuerpo_1">
    <div id="cuerpo_lista_float">
        <ul id="lista_float">
            <?php if ($query->num_rows()>0): ?>
			<?php foreach($query->result() as $row): ?>
            <li>
                <div class="lista_float_imagen">
                			<?php
							if ($row->imagen!=""){
									$ar_imagen=explode(".",$row->imagen);
									$imagen= '<img src="'.base_url().'uploads/informacion/'.$ar_imagen[0].'p.'.$ar_imagen[1].'"  alt="'.$row->alias.'" >';
								}else{
									$imagen= '<img src="'.base_url().'uploads/informacion/espacio.gif"  alt="'.$row->alias.'" >';
								}
							
							?>
                			<?=anchor('web/kunstenaar/informatie/'.$row->id.'/'.$row->alias , $imagen ); ?>
                
               </div>
                <div class="lista_float_info"><?=anchor('web/kunstenaar/informatie/'.$row->id.'/'.$row->alias , $row->alias ); ?></div>
            </li>
            <?php endforeach;?>
			<?php endif; ?> 
        </ul>
    </div>
    <div id="banner">
    	<?= $this->banner_model->cargar_banner_es (); ?>
    </div>
</div>
