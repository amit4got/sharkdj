<h1>Activar cuenta.</h1>

<p>Por favor escriba su clave de activación para validar el email de su cuenta.</p>

<?=$this->session->flashdata('message'); ?>

<?=validation_errors(); ?>

<?=form_open('admin/activar'); ?>

<div class="formularios_titulo">Campos requeridos</div>
<div class="campos">
    <div class="campos_titulo">Clave de activación</div>
    <div class="campos_introducir"><?=form_input('code', set_value('code')); ?></div>
</div>

<div id="btnsubmit"><?=form_submit('submit', 'Activar'); ?></div>

<?=form_close(''); ?>