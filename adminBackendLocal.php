<?php

if(!defined("RUTA_ABSOLUTA")){
  define("RUTA_ABSOLUTA",dirname(__FILE__));
}

#INCLUDES
require_once RUTA_ABSOLUTA.'/administradorDatos/local.class.php';
require_once RUTA_ABSOLUTA.'/administradorDatos/administradorDatos.class.php';
require_once RUTA_ABSOLUTA.'/administradorDatos/imagenes.class.php';

include	RUTA_ABSOLUTA.'/editor/spaw2/spaw.inc.php';

session_start();

if ($_SESSION['statusAdmin'] != "authorized") {
  header ("Location: index.php");
  exit();
}
include_once 'includes/headerAdmin.inc.php';


function soundcloud ($user)
{
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, 'http://soundcloud.com/oembed?format=xml&maxwidth=350&maxheight=280&url=http://soundcloud.com/'.$user);
  curl_setopt($ch, CURLOPT_HEADER, false);
  curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
  $xml = curl_exec($ch);
  $error = curl_error($ch);
  curl_close($ch);


  $objectSoundCloud = @simplexml_load_string($xml);

  if (!$objectSoundCloud){
    $html = '<img src="'.RUTA_ABSOLUTA.'/images/perfil_sin_soundcloud.jpg" alt="soundcloud" title="soundcloud" />';
  }
  else{


    $version = (string) $objectSoundCloud->version;
    $html = (string) $objectSoundCloud->html;
    $html = explode('<span>', $html);
    $html = $html[0];
    $html = preg_replace('/height="\d*"/','height="280"',$html);

  }


  return $html;
}

function leerDirectorio($idUser)
{
  $dir='upload/php/files/'.$idUser.'/thumbnails/';
  $directorio=opendir($dir);
  while ($archivo = readdir($directorio)){
    $imagenes[]=$archivo;
  }
  closedir($directorio);
  return $imagenes;
}



$spaw = new SpawEditor("texto", $texto);
$spaw->setConfigValue('default_toolbarset','standard');
$idUser = $_SESSION['idUser'];

?>
<div class="body">

<?php
$datos = new administradorDatos();


$option = (isset($_GET['o'])) ? $_GET['o'] : 0;
if ($option == '1'){
?>  <div id='content'>
  <div class='container'>
  <?php 
  $locales = $datos->obtenerLocalesInfo();
//   var_dump($artistas);
  $tabla="";
  if (count($locales)>0){
    $tabla="<table>";
    $tabla .= '<tr> 
       <th>Nombre</th> 
       <th>Visible</th>       
		</tr> ';
    $i=0;
    /*
     * u.idUser, u.userName, u.password, u.email, u.idPerfil, u.activo, l.nombre,
	 * l.direccion, l.ciudad, l.pais, l.lat, l.lng, l.visible, l.descripcion  
	 */
    foreach($locales as $local){
      $tabla.= '<form action="adminBackendLocal.php?o=2&l='.$local['idUser'].'" method="post" id="form'.$i.'">';
      $tabla.="<tr>";
      $color = ($local['visible'] == '1')?'black':'red';
      $tabla.='<td><a href="adminBackendLocal.php?o=3&l='.$local['idUser'].'" style="color:'.$color.';">'.$local['nombre'].'</a> -</td>';
      $checked = ($local['visible'] == '1')?'checked':'';
      $tabla.='<td><input name="activo" type="checkbox" '.$checked.'/></td>';      
      $tabla.='<td><input class="buttonLogin" type="submit" value="Actualizar" /></td>';
	  $tabla.='<td><input class="buttonLogin" type="submit" name="Eliminar" value="Eliminar" /></td>';
      $tabla.="</tr>";
      $tabla.="</form>";
      $i++;
    }
    $tabla.="</table>";
  }
  
  echo $tabla; 
  ?></div>
  </div>

<?php   
}
elseif ($option == '2'){
  ?><div id='content'>
  <div class='container'><?php
   if (isset ($_POST['Eliminar'])) {
	  $datos->eliminaFila($table='locales',$columnName='idUser',$id=$_GET['l']);
	  $datos->eliminaFila($table='users',$columnName='idUser',$id=$_GET['l']);
	  echo 'Se ha eliminado el club. ';
    }
    $visible = isset($_POST['activo'])? 1 : 0;    
    $idUser = $_GET['l'];
    $local = new local();
    $local->cambiaVisible($idUser,$visible);
    ?>Local actualizado <a href="adminBackendLocal.php?o=1"><button>Volver</button></a>
  </div>
  </div><?php 
}
elseif($option == '3'){
?><div id='content'>
  <div class='container'>  
  
  <div id="fb-root"></div>
  <script type="text/javascript">(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
      return;
    }
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/es_LA/all.js#xfbml=1&appId=142309935866555";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
  
  
  <?php
  $idUser = $_GET['l'];
	$imagenUrl = imagenes::obtenerImagenUsuario($idUser);
	$local = new local();
	$datos = $local->obtenerDatosLocal($idUser);
	$redes = $local->obtenerRedesSociales($idUser);



foreach ($redes as $red){
  switch ($red['idRed']) {
    case '1':
      $facebook = $red['url'];
      break;
    case '2':
      $twitter = $red['url'];
      break;
    case '3':
      $soundcloud = $red['url'];
      break;
    case '4':
      $youtube = $red['url'];
      break;
    case '5':
      $myspace = $red['url'];
      break;
    default:
      ;
      break;
  }
}
?>
</script>
  <div id="main">
    <div class="insideRow3 bkgdGrey fontBold">
      <div class="box7">
        <div class="artistPhoto2"><img src="<?php echo $imagenUrl;?>" width="140" heigth="140"/></div>
        <span class="fontRed font3"><?php echo $datos['nombre']?></span>
        <br />
        <br />
        <span class="fontWhite">Pais:<br></span>
        <span class="fontBlack"><?php echo $campo = isset($datos['pais'])? $datos['pais']:''?></span>
        <br />
        <span class="fontWhite">Estilo:<br></span>
        <span class="fontBlack"><?php echo $campo = isset($datos['estilo'])? $datos['estilo']:''?></span>
        <br />
        <span class="fontWhite">Residencia:<br></span>
        <span class="fontBlack"><?php echo $campo = isset($datos['clubResidencia'])? $datos['clubResidencia']:''?></span>
        <br />
<!--    <span class="fontWhite">Contacto:</span>
        <span class="fontBlack">XXX</span>
        <br /> -->
        <span><br><a href="<?php echo $web = isset($datos['web'])? $datos['web']:''?>" class="website">Website</a></span>
        <div class="clear left">
          <div id="socialLinks">
            <a href="http://<?php echo $facebook;?>" class="left width24px marginLeft5" id="linkFA"></a>
            <a href="http://soundcloud.com/<?php echo $soundcloud;?>" class="left width24px marginLeft5" id="linkTW"></a>
            <a href="http://www.twitter.com/<?php echo $twitter;?>" class="left width24px marginLeft5" id="linkMY"></a>
            <a href="http://www.myspace.com/<?php echo $myspace; ?>" class="left width24px marginLeft5" id="linkSO"></a>
            <a href="http://www.youtube.com/<?php echo $youtube;?>" class="left width24px marginLeft5" id="linkYO"></a>
          </div>
        </div>
        <?php $imagenes = leerDirectorio($datos['idUser']);?>
        <div class="box8"><?php 
          foreach ($imagenes as $imagen){
            if (preg_match('/(gif|jpe?g|png)$/i', $imagen)){
              echo '<div class="thumb"><a class="group1" href="upload/php/files/'.$datos['idUser'].'/'.$imagen.'"><img src="upload/php/files/'.$datos['idUser'].'/thumbnails/'.$imagen.'" /></a></div>';
            }
          }
        ?></div>
      </div>
      <div class="box7">
      	<div id="map_canvas" style="height:280px;top:2px">
          <script type="text/javascript">
          function placeMarketArtist() {
            <?php if ($datos['lat'] !="" && $datos['lng'] !="") {?>
        	  placeMarkerFromLatLng('<?php echo $datos['lat'];?>','<?php echo $datos['lng'];?>');
        	  <?php } ?>
          }
      		</script>
      	</div>
      </div>
     </div>     
     <div class="box9 textJustified">
        <span class="fontRed font3">Descripción</span>
        <br />
        <br />
        <span class="fontGrey"><?php echo $datos['descripcion']?></span>
        <br />
        <br />
      </div>
      <div class="insideRow2 bkgdGrey fontBold">
        <div id="accordion">
						<h3><a href="#section1">MOVEMBER ESPAÑA 03/12 free</a></h3>
						<div>
							<p>SHARK Energy Drink, SHARK Rock, Banda Aparte y Movember te invitan a que vengas a la primera edición en Tenerife.
Dj Vidi y Dj Carrie serán las Dj´s bigotudas de la Mo Party 2011!</p>
						</div>
						<h3><a href="#section2">Section 2</a></h3>
						<div>
							<p>Sed non urna. Donec et ante. Phasellus eu ligula. Vestibulum sit amet purus. Vivamus hendrerit, dolor at aliquet laoreet, mauris turpis porttitor velit, faucibus interdum tellus libero ac justo. Vivamus non quam. In suscipit faucibus urna. </p>
						</div>
						<h3><a href="#section3">Section 3</a></h3>
						<div>
          		<p>Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis. Phasellus pellentesque purus in massa. Aenean in pede. Phasellus ac libero ac tellus pellentesque semper. Sed ac felis. Sed commodo, magna quis lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui. </p>
          		<ul>
          			<li>List item</li>
          			<li>List item</li>
          			<li>List item</li>
          			<li>List item</li>
          			<li>List item</li>
          			<li>List item</li>
          			<li>List item</li>
          		</ul>
          		<a href="#othercontent">Link to other content</a>
						</div>
					</div>
      </div>
      <div class="insideRow2 fontBold bkgrdBlueRepeat">
                <div class="box13"><!-- BEGIN: Twitter for Web widget (http://twitterforweb.com) -->
<div style="width:350px;font-size:8px;text-align:right;"><script src="http://widgets.twimg.com/j/2/widget.js"></script>
<script>
new TWTR.Widget({
  version: 2,
  type: 'profile',
  rpp: 4,
  interval: 30000,
  width: 350,
  height: 200,
  theme: {
    shell: {
        background: '#cccccc',
        color: '#080808'
      },
      tweets: {
        background: '#ededed',
        color: '#000000',
        links: '#e01616'
      }
  },
  features: {
    scrollbar: true,
    loop: false,
    live: false,
    behavior: 'all'
  }
}).render().setUser('<?echo $twitter;?>').start();
</script></div>



<!-- END: Twitter for Web widget (http://twitterforweb.com) --></div>
        <div class="box13"><div class="fb-like-box" data-href="http://<?php echo $facebook?>" data-width="350" data-height="280" data-show-faces="true" data-stream="false" data-header="false"></div></div>
      </div>
    <br><br><div style="float: right;padding: 15px 0px;"><a href="adminBackendLocal.php?o=1"><button>Volver</button></a></div>
    </div>
        </div>
    </div><?php 

}
?>

	</div>
</div>
  
  <div id="menu">
    <img src="images/logoHeader.png" width="150">
    <br><br>
    <ul>
      <li><a href="adminBackend.php?o=3">Noticias</a></li><br><br>
      <li><a href="adminBackendEventos.php?o=3">Eventos</a></li><br><br>
      <li><a href="adminBackendUser.php?o=1">DJ's</a></li><br><br>
      <li><a href="adminBackendLocal.php?o=1">Locales</a></li><br><br>
      <li><a href="adminPublicidad.php">Publicidad</a></li><br><br> 
      <li><a href="adminBanner.php">Banner</a></li><br><br>     
      <li><a href="admin.php?logout" style="color:red;">Logout</a></li>
    </ul> 
  </div>
</div>
