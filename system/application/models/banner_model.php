<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banner_model extends Model
{
	
	
	public function __construct()
	{
		parent::__construct();
	}
	/**
	Casos para cargar fiestas
	 		//// por fecha /////
			//// por fecha , pais /////
			//// por fecha , ciudad /////
			//// por fecha , estilos /////
			//// por fecha , local /////
			
			//// por fecha , pais 	, estilos 			/////
			//// por fecha , pais 	, local    			/////
			//// por fecha , pais 	, local , estilos	/////
			
			//// por fecha , ciudad , estilos 			/////
			//// por fecha , ciudad , local    			/////
			//// por fecha , ciudad , local , estilos	/////
	**/
	public function cargar_banner_es ()
	{
	    $this->db->where('es_clave', 'banner');
		$query=$this->db->get('Textos');
		foreach ($query->result() as $row)
		{
			$informacion =  htmlspecialchars_decode($row->es_informacion);
		}
		return $informacion;
	}
	
	
}
