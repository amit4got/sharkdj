<?php
/****************************
 *  Created 26/06/11
 *  Last update 24/07/11  
 ****************************/

function md5Crypt ($data)
{
  return md5 ($data);
} #END function md5Crypt ($data)


/******* DIFERENTES NIVELES DE USUARIOS *******/
#Returns true if user level is in the the ones indicated
function checkIfPermit ($arrayAllowedLevels, $currentLevel)
{
  foreach ($arrayAllowedLevels as $value) {
    if ($value == $currentLevel) {
      return true; #ACCESS ALLOWED
    }
  }
  return false; #ACCESS NOT ALLOWED
} #END function checkIfPermit ($arrayLevels)