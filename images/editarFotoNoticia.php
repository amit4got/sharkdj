<?php
/****************************
 *  Created 11/11/11
 *  Last update 11/11/11   
 ****************************/

#INCLUDES
require_once 'includes/connect.ini.php';
require_once 'includes/functions.ini.php';

session_start();

if ($_SESSION['statusAdmin'] != "authorized") {
  header ("Location: index.php");
  exit();
}

function is_alpha_png($fn){
  return (ord(@file_get_contents($fn, NULL, NULL, 25, 1)) == 6);
}

if ($_POST['crop'])
{
  $targ_w = $targ_h = 250;
  $jpeg_quality = 90;

  $nombreImagen = explode('/',$_POST['imagen']);
  $nombreImagen = $nombreImagen[count($nombreImagen) - 1];
  
  $directorio = 'images/posts/';
    
  $src = $directorio.$_POST['idUser'].'.jpeg';
  $img_r = @imagecreatefromjpeg($src);
  $dst_r = @ImageCreateTrueColor( $targ_w, $targ_h );
  
  @imagecopyresampled($dst_r,$img_r,0,0,$_POST['x'],$_POST['y'],$targ_w,$targ_h,$_POST['w'],$_POST['h']);

//   header('Content-type: image/jpeg');
  $imagen = $directorio.$_POST['idUser'].'.jpeg';
  @imagejpeg($dst_r,$imagen);
  
  $parametros = $_POST['p'];

  header('refresh:0; adminBackend.php');
  if (isset($_GET['l'])){
    header('Location: localBackend.php');
  }
  else {
    header('Location: adminBackend.php?'.$parametros);
  }
}
elseif ($_POST['imagenSubida']){
  
    // Datos de conexión a configurar
    //     $user = "usuario_bbdd";
    //     $pass = "clave_bbdd";
    //     $bbdd = "nombre_bbdd";
  
    // Ruta donde se guardarán las imágenes
    $directorio = 'images/posts/';
  
    // Conecto a la BBDD
    //     $dbh = mysql_connect("localhost", $user, $pass);
    //     $db = mysql_select_db($bbdd);
  
    // Recibo los datos de la imagen
    $name = $_FILES['imagen']['name'];
    $tipo = $_FILES['imagen']['type'];
    $tamano = $_FILES['imagen']['size'];
    $prefijo = substr(md5(uniqid(rand())),0,20);

    if(isset($_GET['l'])){
      $local ='&l';
    } 
    if ($_FILES['imagen']['error'] == 1){
       header('Location: editarFotoNoticia.php?e=2&i='.$_POST['idUser'].$local);
      exit;
    }
    if ($tamano > 1048576){
      header('Location: editarFotoNoticia.php?e=3&s='.$tamano.'&i='.$_POST['idUser'].$local);
      exit;
    }
    // guardamos el archivo a la carpeta files
    $destino =  $directorio.$prefijo."_".$name;
    move_uploaded_file($_FILES['imagen']['tmp_name'],$directorio.$prefijo.'_'.$name);
  
  
    $anchura=500;
    $hmax=500;
    $nombre=$directorio.$prefijo.'_'.$name;
    
    if (is_alpha_png($nombre)){
      header('Location: editarFotoNoticia.php?e=4'.$local);
      exit;
    }
    
    $datos = getimagesize($nombre);
    if($datos[2]==1){
      $img = @imagecreatefromgif($nombre);
    }
    elseif($datos[2]==2){
      $img = @imagecreatefromjpeg($nombre);
    }
    elseif($datos[2]==3){
      $img = @imagecreatefrompng($nombre);
    }
    else{
       header('Location: editarFotoNoticia.php?e=1'.$local);
       exit;
    }
    if ($datos[0] < 250 || $datos[1] < 250){
      unlink($directorio.$prefijo.'_'.$name);
      header('Location: editarFotoNoticia.php?e=5&i='.$_POST['idUser'].$local);
      exit;
    }
    if ($datos[0] > 500 || $datos[1] > 500){
      $ratio = ($datos[0] / $anchura);
      $altura = ($datos[1] / $ratio);
      if($altura>$hmax){
        $anchura2=$hmax*$anchura/$altura;
        $altura=$hmax;
        $anchura=$anchura2;
      }
      $thumb = imagecreatetruecolor($anchura,$altura);
      imagecopyresampled($thumb, $img, 0, 0, 0, 0, $anchura, $altura, $datos[0], $datos[1]);

      if(file_exists($directorio.$_POST['idUser'].'.jpeg')){
        unlink($directorio.$_POST['idUser'].'.jpeg');
      }
      imagejpeg($thumb,$directorio.$_POST['idUser'].'.jpeg');
      imagedestroy($thumb);
      unlink($directorio.$prefijo.'_'.$name);
    }
    else{
      if(file_exists($directorio.$_POST['idUser'].'.jpeg')){
         unlink($directorio.$_POST['idUser'].'.jpeg');
       }
       rename($directorio.$prefijo.'_'.$name, $directorio.$_POST['idUser'].'.jpeg');
    }
  
      header('refresh:0; editarFotoNoticia.php');
      header('Location: editarFotoNoticia.php?im='.$_POST['idUser'].'&i='.$_POST['idUser'].$local);
    // Muevo la imagen desde su ubicación
    // temporal al directorio definitivo
  }

else{
include_once 'includes/headerBackendFoto.inc.php';

?>

<?php 
$idUser = $_GET['i']; #en realidad es el id del post
echo $idUser;
$directorio = 'images/posts/';

$idImage = ($_GET['im'] == '0') ? '0' : $_GET['im'];
$parametros = (isset($_SERVER['argv'][0])) ? $_SERVER['argv'][0] : "";
$imagen = $directorio.$idImage.'.jpeg';
?>

<div id="mainFotoPerfil">
	<div class="ui-widget-content boxEditarFoto">
		<div class="imagenEditar">
			<img src="<?php echo $imagen ?>" id="cropbox" />
		</div>
				<!-- This is the form that our event handler fills -->
		<form action="<?php $_SERVER['PHP_SELF']?>" method="post" onSubmit="return checkCoords();">
			<input type="hidden" id="x" name="x" />
			<input type="hidden" id="y" name="y" />
			<input type="hidden" id="w" name="w" />
			<input type="hidden" id="h" name="h" />
			<input type="hidden" id="idUser" name="idUser" value="<?php echo $idUser?>" />
			<input type="hidden" name="p" value="<?php echo $parametros;?>" />
			<input type="submit" name="crop" value="Crop Image" />
		</form>
		<form action="<?php $_SERVER['PHP_SELF']?>" enctype="multipart/form-data" method="post">
          <fieldset> 
            <label for="file">Elegir nueva foto...</label> 
            <input type="file" name="imagen" id="file" /> 
      	    <input type="hidden" id="idUser" name="idUser" value="<?php echo $idUser?>" />
            <input type="submit" name="imagenSubida" id="upload" value="Subir Imagen" /> 
          </fieldset>
        </form>
        
      
        
        
	<?php if ($_GET['e'] == '1'){?>
	  <div id="dialog" title="Formato incorrecto">
			<p>Formato de archivo no permitido, por favor comprueba que sea jpg, gif o png.</p>
		</div>
	<?php }
	elseif ($_GET['e'] == '2'){?>
	  <div id="dialog" title="Error subiendo imagen">
	  <p>Se ha producido un error subiendo la imagen, posiblemnte se deba al tamaño del archivo, recuerde que solo se permiten imagenes de menos de 600Kb.</p>
	  </div>
	<?php }
	elseif ($_GET['e'] == '3'){
	  $size = round($_GET['s']/1024,0);
	  ?>
    <div id="dialog" title="Archivo muy grande">
      <p>
        Su archivo pesa
        <?php echo '<b>'.$size.'</b>'?>
        Kb y el máximo permitido es 600 Kb
      </p>
    </div>

    <?php }
    elseif ($_GET['e'] == '4'){
      ?>
    <div id="dialog" title="Archivo con transparencia">
      <p>Su archivo tiene canal alpha, no se perrmite este tipo de
        imagen.</p>
    </div>
    
	<?php }
	elseif ($_GET['e'] == '5'){
	  ?>
	    <div id="dialog" title="Archivo muy pequeño">
	      <p>La imagen tiene que tener un ancho y una altura de mas de 240px</p>
	    </div>
	    
		<?php }
	elseif ($_GET['e'] == '0') {
	  ?>
    <div id="dialog" title="Archivo subido correctamente">
      <p>Su imagen se actualizó correctamente, sino la ve pulse F5 para refrescar su navegador.</p>
    </div>

    <?php }
	?>

  </div>
</div>
<div class="ui-widget-content boxTexto">
	Selecciona la foto y arrastra las esquinas de la siguiente caja transparente y recorta esta foto para usarla como foto de perfil.
</div>
<?php 
}
include_once 'includes/footer.inc.php'; ?>