<h1>Login</h1>

<?=validation_errors(); ?>

<?=form_open('panel'); ?>

<div class="formularios_titulo">Campos requeridos</div>
<div class="campos">
    <div class="campos_titulo">Email</div>
    <div class="campos_introducir"><?=form_input('email', set_value('email')); ?></div>
</div>
<div class="campos">
    <div class="campos_titulo">Password</div>
    <div class="campos_introducir"><?=form_password('password'); ?></div>
</div>

<div id="btnsubmit"><?=form_submit('submit', 'Validar'); ?></div>

<?=form_close(''); ?>

<div class="formularios_titulo">Olvido su password?</div>
<div class="campos"><?=anchor('panel/recuperar_password', 'Recuperar password'); ?></div>
