<?php
/****************************
 *  Created 11/11/11
 *  Last update 11/11/11   
 ****************************/

#INCLUDES
//require_once 'includes/connect.ini.php';
//require_once 'includes/functions.ini.php';

//error_reporting(E_ALL);
//ini_set("display_errors", 1);

if(!defined("RUTA_ABSOLUTA")){
	define("RUTA_ABSOLUTA",dirname(__FILE__));
}

require_once RUTA_ABSOLUTA.'/administradorDatos/administradorDatos.class.php';
require_once RUTA_ABSOLUTA.'/administradorDatos/imagenes.class.php';

$listaEventos="";
$pagina = isset($_GET['p']) ? $_GET['p'] : 0;
$searchNombre = isset($_GET['n']) ? $_GET['n'] : "";
$searchLugar = isset($_GET['l']) ? $_GET['l'] : "";
$searchEstilo = isset($_GET['e']) ? $_GET['e'] : "";
$enlacePag = "./eventos.php?";
$adminDatos = new administradorDatos();

#Load languages
include RUTA_ABSOLUTA.'/includes/lang.inc.php';
$arrayIdiomas = $adminDatos->cargaIdioma($idioma);
$langURL = '';
if ($idioma != 'es') {
  $langURL = '&lang=' . $idioma;
}

if ($searchNombre!="" || $searchLugar!="" || $searchEstilo!="") {
	$enlacePag .= "n=".$searchNombre."&l=".$searchLugar."&e=".$searchEstilo."&";
	$numTotalEventos = count($adminDatos->buscarEventos($searchNombre,$searchLugar,$searchEstilo));
	$arrayEventos = $adminDatos->buscarEventos($searchNombre,$searchLugar,$searchEstilo, 15, $pagina*15);
}else{
	$numTotalEventos = count($adminDatos->obtenerEventos(MAX,0,1));
	$arrayEventos = $adminDatos->obtenerEventos(15,$pagina*15);
}

$numEventos = count($arrayEventos);
if ($numEventos>0){
	foreach ($arrayEventos as $evento) {
		$idEvento = $evento['idEvento'];		
		$nombre = $evento['nombre'];		
		$fechaInicio = strtotime($evento['fechaInicio']);
		$mes = date("M", $fechaInicio);
		$dia = date("d", $fechaInicio);
		$hora = date("H:i", $fechaInicio);
		$direccion = $evento['direccion'];
		$transporte = $evento['transporte'];
		$precio = $evento['precio'];
		$descripcion = $evento['descripcion'];
		$imagenUrl = imagenes::obtenerImagenEvento($idEvento);
		$imagenTocha = str_replace('_c', '', $imagenUrl);
		$listaEventos.=<<<EOF
			<div id="eventBoxClose{$idEvento}" class="boxEvents" onclick="javascript:showElement('eventBoxOpen{$idEvento}');javascript:hideElement('eventBoxClose{$idEvento}');">
              <div class="calendarEvent marginTop10 marginLeft10">
                <div class="monthWhiteEvent textCentered fontBold">{$mes}</div>
                <div class="dayEvent textCentered fontBold">{$dia}</div>
              </div>
              <div class="infoEvent">
                <div class="width100">
                  <span class="font3 marginTop5 marginBottom5 left">{$nombre}</span>
      			</div>
                <span class="clear left marginTop10">{$arrayIdiomas['time']}: {$hora}<br />{$arrayIdiomas['place']}: {$direccion}</span>
                <span class="right marginTop25 marginRight5">{$arrayIdiomas['price']}: {$precio} &euro;</span>
              </div>
            </div>
      		<div id="eventBoxOpen{$idEvento}" class="boxEventsOpen marginBottom10 displayNone">
			  <div style="position:relative;float:right;right:5px;top:5px;cursor:pointer;color:#fff" onclick="javascript:showElement('eventBoxClose{$idEvento}');javascript:hideElement('eventBoxOpen{$idEvento}');">X</div>
              <div class="eventPhoto marginTop10 marginLeft10"><a class="group1" href='{$imagenTocha}'><img src='{$imagenUrl}' width="200" heigth="280"/></a></div>
              <div class="box11">
                <div class="calendarEventOpen marginLeft5">
                  <div class="monthEventOpen textCentered fontBold">{$mes}</div>
                  <div class="dayEventOpen textCentered fontBold">{$dia}</div>
                </div>
                <div class="infoEventOpen fontWhite">
                  <div class="width100">
                    <span class="font3 left">{$nombre}</span>              
      			  </div>
                  <span class="clear left">{$arrayIdiomas['time']}: {$hora}<br />{$arrayIdiomas['place']}: {$direccion}</span>
                  <span class="right marginTop15 marginRight5">{$arrayIdiomas['price']}: {$precio} &euro;</span>
                  <span class="clear left">Transporte: {$transporte}</span>
                </div>
                <div class="box12 textJustified fontWhite">{$descripcion}</div>
              </div>
            </div>
EOF;
	}
}

include_once 'includes/header.inc.php';
srand();
$publicidades = $adminDatos->obtenerPublicidad();
if (count($publicidades) < 6){
	$claves = array_rand($publicidades,count($publicidades));	
}else{
	$claves = array_rand($publicidades,6);	

}
	shuffle($claves);
?>
  <div id="main">
	  <div id="searchBar">
	    <form>
	      <label class="left fontWhite marginTop5 marginLeft5"><?=$arrayIdiomas['name'];?>:</label>
	      <input type="text" name="n" class="left searchText2 noBorder fontGrey fontBold" value="" />
	      <label class="left fontWhite marginTop5 marginLeft5"><?=$arrayIdiomas['place'];?>:</label>
	      <input type="text" name="l" class="left searchText2 noBorder fontGrey fontBold" value="" />
	      <label class="left fontWhite marginTop5 marginLeft5"><?=$arrayIdiomas['style'];?>:</label>
	      <input type="image" name="submit" class="right marginRight7 marginTop2" id="searchButton" value="" />
	      <input type="text" name="e" class="right searchText2 noBorder fontGrey fontBold" value="" />
	      <input type="hidden" name="lang" value="<?=$idioma?>" />
	    </form>
	  </div>
	  <div class="advertisementRow3"><?php
		foreach($claves as $c){
	   echo '<br><a href="'.$publicidades[$c]['url'].'"><img src="http://www.sharkdj.com/'. $publicidades[$c]['imagen'] .'" alt="' . $arrayIdiomas['ads'] . '" title="' . $arrayIdiomas['ads'] . '" /></a><br>';
	}?>
	  </div>
	  <div class="column2">
	    <?php 
	    	echo $listaEventos;
	    ?>
	  </div>  
  	  <div id="nextPreviousMenu" class="clear left insideRow">
      	<div class="left width33"><?php if ($pagina>0) { ?><a href="<?php echo $enlacePag;?>p=<?php echo ($pagina-1).$langURL;?>" class="noDecoration fontGrey2 fontBold hoverLargeGrey">&laquo; <?=$arrayIdiomas['previousMenu'];?></a><?php }?><a class="noDecoration fontGrey2 fontBold hoverLargeGrey">&nbsp;</a></div>
      	<div class="left width33 textCentered marginLeft5"><a href="<?php echo $enlacePag;?>p=0<?=$langURL;?>" class="noDecoration fontGrey2 fontBold hoverLargeGrey"><?=$arrayIdiomas['homeMenu'];?></a></div>        
      	<div class="right"><?php if (($pagina+1)*15 < $numTotalNoticias) {?><a href="<?php echo $enlacePag;?>p=<?php echo ($pagina+1).$langURL;?>" class="fontBold noDecoration fontGrey2 hoverLargeGrey"><?=$arrayIdiomas['nextMenu'];?> &raquo;</a><?php } ?></div>
      </div>
  </div>
<?php include_once 'includes/footer.inc.php'; ?>