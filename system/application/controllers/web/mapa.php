<?php
class Mapa extends Controller {
	function __construct() 
	{
		parent::Controller();
	}
	function index()
	{
		$this->data['locales']=$this->db->get('Locales');
		$this->db->where('visible', '1'); 
		$this->data['dj']=$this->db->get('meta');
		$fecha=date('Y-m-d H:i:s');
		$this->db->where('fecha >=', $fecha); 
		$this->data['fiestas']=$this->db->get('Fiestas');
		$this->load->view('templates/mapa_view',  $this->data);
	}
	
}


