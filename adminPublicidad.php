<?php

if(!defined("RUTA_ABSOLUTA")){
  define("RUTA_ABSOLUTA",dirname(__FILE__));
}

#INCLUDES
require_once RUTA_ABSOLUTA.'/administradorDatos/artist.class.php';
require_once RUTA_ABSOLUTA.'/administradorDatos/administradorDatos.class.php';
include	RUTA_ABSOLUTA.'/editor/spaw2/spaw.inc.php';

session_start();

if ($_SESSION['statusAdmin'] != "authorized") {
  header ("Location: index.php");
  exit();
}
include_once 'includes/headerAdmin.inc.php';

function leerDirectorioPublicidad()
{
  $dir=RUTA_ABSOLUTA.'/images/publicidad/';
  $directorio=opendir($dir);
  while ($archivo = readdir($directorio)){
    $imagenes[]=$archivo;
  }
  closedir($directorio);
  return $imagenes;
}

$status = "";
$datos = new administradorDatos();
if ($_POST["action"] == "upload") {
  // obtenemos los datos del archivo
  $tamano = $_FILES["archivo"]['size'];
  $tipo = $_FILES["archivo"]['type'];
  $archivo = $_FILES["archivo"]['name'];
  $prefijo = explode(" ",microtime());
  $prefijo = (string)$prefijo[0].'_'.(string)$prefijo[1];
   
  if ($archivo != "") {
    // guardamos el archivo a la carpeta files
    $destino =  "images/publicidad/".$prefijo."_".$archivo;
    if (copy($_FILES['archivo']['tmp_name'],$destino)) {
      $status = "Archivo subido: <b>".$archivo."</b>";
      $publicidad['imagen']= $destino;
      $publicidad['url'] = $_POST['url'];
      $datos->introducePublicidad($publicidad);

    } else {
      $status = "Error al subir el archivo";
    }
  } else {
    $status = "Error al subir archivo";
  }
}

if ($_POST["action"] == "delete") {
  $filename = $_POST['nombreArchivo'];
  if (!unlink($filename)){
    $status = 'No se pudo borrar el archivo';
  }
  else{
    $datos->eliminaFila('publicidad','imagen',$_POST['nombreArchivo']);
    $status = 'Archivo Borrado';
  }
}

if ($_POST["action"] == "modificar") {
  $status = "URL Modificada";
  $publicidad['id']= $_POST['id'];
  $publicidad['url']=$_POST['url'];
  $datos->actualizaPublicidad($publicidad);
  
}


// $imagenes = leerDirectorioPublicidad();
 $publicidades = $datos->obtenerPublicidad();


?>

  <div id="content">
    <div id="container">
      <?php echo $status.'<br><br>';
            echo 'Seleccione un archivo de publicidad (max 150px ancho):<br>';?>
      
      <form action="<?php echo $_SERVER["PHP_SELF"];?>" method="post" enctype="multipart/form-data">
        <input name="archivo" type="file" size="35" />
        <br />URL: <input name="url" size="100" value="http://" />
        <input name="enviar" type="submit" value="Guardar" />
        <input name="action" type="hidden" value="upload" />     
      </form>
      <br><br>
      <?php 
      for($i=0;$i < count($publicidades);$i++){
        echo '<a href="'.$publicidades[$i]['url'].'"><img src="'.$publicidades[$i]['imagen'].'"></a>';  
        echo '<form name="form'.$i.'" action="'.$_SERVER['PHP_SELF'].'" method="POST">';
          echo '<input type="hidden" name="nombreArchivo" value="'.$publicidades[$i]['imagen'].'"/>';
          echo '<input name="action" type="hidden" value="delete" />';
        echo '<div style="position:relative;"><input type="submit" name="submit" value="Borrar"/></div>';
        echo '</form> ';
          echo '<form name="form'.$i.'" action="'.$_SERVER['PHP_SELF'].'" method="POST">';
          echo '<input name="url" value="'.$publicidades[$i]['url'].'" size="100" />';
          echo '<input type="hidden" name="id" value="'.$publicidades[$i]['id'].'"/>';
          echo '<input name="action" type="hidden" value="modificar" />';
        echo '<div style="position:relative;"><input type="submit" name="submit" value="Modificar"/></div>';
        echo '</form><br><br>';
      }?>
      
    </div>
  </div>
    
  <div id="menu">
    <img src="images/logoHeader.png" width="150">
    <br><br>
    <ul>
      <li><a href="adminBackend.php?o=3">Noticias</a></li><br><br>
      <li><a href="adminBackendEventos.php?o=3">Eventos</a></li><br><br>
      <li><a href="adminBackendUser.php?o=1">DJ's</a></li><br><br>
      <li><a href="adminBackendLocal.php?o=1">Locales</a></li><br><br>
      <li><a href="adminPublicidad.php">Publicidad</a></li><br><br>
      <li><a href="adminBanner.php">Banner</a></li><br><br>
      <li><a href="admin.php?logout" style="color:red;">Logout</a></li>
    </ul> 
  </div>