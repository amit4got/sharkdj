<div id="cuerpo">

<?=validation_errors(); ?>

<?=$errores; ?>

<?=form_open('panel_en/usuarios/modificar_pw/'.$this->uri->segment(4)); ?>

<div class="formularios_titulo">Required field</div>

<div class="campos">
    <div class="campos_titulo">Old Password </div>
    <div class="campos_introducir"><?=form_password('old', set_value('old')) ?></div>
</div>
<div class="campos">
    <div class="campos_titulo">New password</div>
    <div class="campos_introducir"><?=form_password('new', set_value('new')) ?></div>
</div>
<div class="campos">
    <div class="campos_titulo">Repeat new password</div>
    <div class="campos_introducir"><?=form_password('new_repeat', set_value('new_repeat')) ?></div>
</div>

<div id="btnsubmit"><?=form_submit('submit', 'save'); ?></div>

<?=form_close(); ?>
</div>