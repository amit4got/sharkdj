<!DOCTYPE html>
<html>
<head>
<meta name="SharkDJ Mobile"  content="text/html;" http-equiv="content-type" charset="utf-8">
<title>SharkDJ Mobile</title>


<meta name="viewport" content="width=device-width, initial-scale=1"> 

  <link rel="stylesheet" href="http://code.jquery.com/mobile/1.0/jquery.mobile-1.0.min.css" />
  <link rel="stylesheet" href="./css/mobile.css">
  <script type="text/javascript" src="http://code.jquery.com/jquery-1.6.4.min.js"></script>
  <script type="text/javascript" src="http://code.jquery.com/mobile/1.0/jquery.mobile-1.0.min.js"></script>

</head>
<body>

<?php $header = '<div data-role="header" class="nav-glyphish-example" data-theme="c">
      <h1><a href="index.php"><img src="./imagen/logo_1.png"></a></h1>
      <div data-role="navbar" class="nav-glyphish-example" data-theme="a">
        <ul>
          <li><a href="indexArtista.php" id="artistasIcon" data-icon="custom">Artistas</a></li>
          <li><a href="indexLocal.php" id="localesIcon" data-icon="custom">Locales</a></li>
          <li><a href="indexEvento.php" id="eventosIcon" data-icon="custom">Eventos</a></li>
          <li><a href="indexEscuela.php" id="tutorialesIcon" data-icon="custom">Tutoriales</a></li>
        </ul>
      </div>
    </div><!-- /header -->';

$footer = '<div data-role="footer">
        <h4><a href="http://www.sharkenergy.com" target="_blank"><img src="./imagen/logo_pie.png"></a></h4>
    </div>';
?>

  <style> 
    .nav-glyphish-example .ui-btn .ui-btn-inner { padding-top: 40px !important; }
    .nav-glyphish-example .ui-btn .ui-icon { width: 30px!important; height: 30px!important; margin-left: -15px !important; box-shadow: none!important; -moz-box-shadow: none!important; -webkit-box-shadow: none!important; -webkit-border-radius: 0 !important; border-radius: 0 !important; }
    #artistasIcon .ui-icon { background:  url(./imagen/icons/111-user.png) 50% 50% no-repeat;  }
    #localesIcon .ui-icon { background:  url(./imagen/icons/144-martini.png) 50% 50% no-repeat;   }
    #eventosIcon .ui-icon { background:  url(./imagen/icons/83-calendar.png) 50% 50% no-repeat;  }
    #tutorialesIcon .ui-icon { background:  url(./imagen/icons/174-imac.png) 50% 50% no-repeat;  }
    #image { width: 100px; height: 100px;}
  </style>