<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="expires" content="Sat, 27 May 1978 12:00:00 GMT" />
<meta name="country" content="es" />
<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>SHARK DJ</title>
<link type="text/css" href="css/custom-theme/jquery-ui-1.8.16.custom.css" rel="stylesheet" />

<link href="shark.css" rel="stylesheet" type="text/css" media="screen" />
<link href="css/admin.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="icon" type="image/png" href="images/logoSharkDJ.png" />
<script type="text/javascript" src="js/jquery-1.7.min.js"></script> 
<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript">
			$(function(){
				// Dialog			
				$('#dialog').dialog({
					autoOpen: true,
					height:100,
					hide:"fold",
					show:"fold",
					dialogClass: 'alert',
					closeOnEscape: true
				});
				
				// Dialog Link
				$('#dialog_link').click(function(){
					$('#dialog').dialog('open');
					return false;
				});

				// Datepicker
				$('#datepicker').datepicker({
					inline: true,
					dateFormat: 'yy-mm-dd'
				});
			});
		</script>
</head>